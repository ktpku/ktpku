<?php 
session_start();
require '../config.php';
require '../lib/session_login.php';
require '../lib/session_user.php';

        if (isset($_POST['ganti_profil'])) {
            $nama_depan = $conn->real_escape_string(trim(filter($_POST['nama_depan'])));
            $nama_belakang = $conn->real_escape_string(trim(filter($_POST['nama_belakang'])));
            $email = $conn->real_escape_string(trim(filter($_POST['email'])));
            $no_hp = $conn->real_escape_string(trim(filter($_POST['no_hp'])));
            $pin = $conn->real_escape_string(trim(filter($_POST['pin1'])));

            $cek_email = $conn->query("SELECT * FROM users WHERE email = '$email'");
            $cek_email_ulang = mysqli_num_rows($cek_email);
            $data_email = mysqli_fetch_assoc($cek_email);

            $cek_no_hp = $conn->query("SELECT * FROM users WHERE no_hp = '$no_hp'");
            $cek_no_hp_ulang = mysqli_num_rows($cek_no_hp);
            $data_no_hp = mysqli_fetch_assoc($cek_no_hp);

            $error = array();
            if (empty($nama_depan)) {
		        $error ['nama_depan'] = '*Tidak Boleh Kosong.';
            }
            if (empty($nama_belakang)) {
		        $error ['nama_belakang'] = '*Tidak Boleh Kosong.';
            }
            if (empty($email)) {
		        $error ['email'] = '*Tidak Boleh Kosong.';
            } else if ($cek_email_ulang == 0) {
		        $error ['email'] = '*Email Sudah Terdaftar.';
            }
            if (empty($no_hp)) {
		        $error ['no_hp'] = '*Tidak Boleh Kosong.';
            } else if ($cek_no_hp_ulang == 0) {
		        $error ['no_hp'] = '*Nomor HP Sudah Terdaftar.';
            }
            if (empty($pin)) {
		        $error ['pin1'] = '*Tidak Boleh Kosong.';
            } else if ($pin <> $data_user['pin']) {
		        $error ['pin1'] = '*PIN Yang Kamu Masukkan Salah.';
            } else {

   		    if ($conn->query("UPDATE users SET nama_depan = '$nama_depan', nama_belakang = '$nama_belakang', email = '$email', no_hp = '$no_hp' WHERE username = '$sess_username'") == true) {
   			    $_SESSION['hasil'] = array('alert' => 'success', 'judul' => 'Berhasil', 'pesan' => 'Yeah, Data Profil Kamu Berhasil Diubah.<script>swal("Berhasil!", "Data Profil Kamu Berhasil Diubah.", "success");</script>');
            } else {
   			    $_SESSION['hasil'] = array('alert' => 'danger', 'judul' => 'Gagal', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
   		    }

        }

        } else if (isset($_POST['ganti_password'])) {
            $password = $conn->real_escape_string(trim(filter($_POST['password_lama'])));
            $password_baru = $conn->real_escape_string(trim(filter($_POST['password_baru'])));
            $konf_pass_baru = $conn->real_escape_string(trim(filter($_POST['konf_pass_baru'])));

            $cek_passwordnya = password_verify($password, $data_user['password']);
            $hash_passwordnya = password_hash($password_baru, PASSWORD_DEFAULT);

            $error = array();
            if (empty($password)) {
		        $error ['password_lama'] = '*Tidak Boleh Kosong.';
            }
            if (empty($password_baru)) {
		        $error ['password_baru'] = '*Tidak Boleh Kosong.';
            } else if (strlen($password_baru) < 6 ){
		        $error ['password_baru'] = '*Kata Sandi Minimal 6 Karakter.';
            }
            if (empty($konf_pass_baru)) {
		        $error ['konf_pass_baru'] = '*Tidak Boleh Kosong.';
            } else if (strlen($konf_pass_baru) < 6 ){
		        $error ['konf_pass_baru'] = '*Kata Sandi Minimal 6 Karakter.';
            } else if ($password_baru <> $konf_pass_baru){
		        $error ['konf_pass_baru'] = '*Konfirmasi Kata Sandi Baru Tidak Sesuai.';
            } else {

            if ($cek_passwordnya <> $data_user['password']) {
                $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Kata Sandi Lama Yang Kamu Masukkan Tidak Sesuai.<script>swal("Gagal!", "Kata Sandi Lama Yang Kamu Masukkan Tidak Sesuai.", "error");</script>');
            } else {

   		    if ($conn->query("UPDATE users SET password = '$hash_passwordnya' WHERE username = '$sess_username'") == true) {
   			    $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Sip! Kata Sandi Kamu Berhasil Diubah.<script>swal("Berhasil!", "Kata Sandi Kamu Berhasil Diubah.", "success");</script>');
            } else {
   			    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
   		    }

            }
        
        }

        } else if (isset($_POST['ganti_pin'])) {
            $pin = $conn->real_escape_string(trim(filter($_POST['pin_lama'])));
            $pin_baru = $conn->real_escape_string(trim(filter($_POST['pin_baru'])));
            $konfirmasi_pin_baru = $conn->real_escape_string(trim(filter($_POST['konfirmasi_pin_baru'])));

            $error = array();
            if (empty($pin)) {
		        $error ['pin_lama'] = '*Tidak Boleh Kosong.';
            }
            if (empty($pin_baru)) {
		        $error ['pin_baru'] = '*Tidak Boleh Kosong.';
            } else if (strlen($pin_baru) <> 6) {
		        $error ['pin_baru'] = '*PIN Harus 6 Digit.';
            }
            if (empty($konfirmasi_pin_baru)) {
		        $error ['konfirmasi_pin_baru'] = '*Tidak Boleh Kosong.';
            } else if (strlen($konfirmasi_pin_baru) <> 6 ){
		        $error ['konfirmasi_pin_baru'] = '*PIN Harus 6 Digit.';
            } else if ($pin_baru <> $konfirmasi_pin_baru){
		        $error ['konfirmasi_pin_baru'] = '*Konfirmasi PIN Baru Tidak Sesuai.';
            } else {

            if ($pin <> $data_user['pin']) {
                $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, PIN Lama Yang Kamu Masukkan Tidak Sesuai.<script>swal("Gagal!", "PIN Lama Yang Kamu Masukkan Tidak Sesuai.", "error");</script>');
            } else {

   		    if ($conn->query("UPDATE users SET pin = '$pin_baru' WHERE username = '$sess_username'") == true) {
   			    $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Sip! PIN Kamu Berhasil Diubah.<script>swal("Berhasil!", "PIN Kamu Berhasil Diubah.", "success");</script>');
            } else {
   			    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
   		    }

            }
        
        }

        } else if (isset($_POST['ganti_api_key'])) {
		    $api_barunya = acak(20);
		    if ($conn->query("UPDATE users SET api_key = '$api_barunya' WHERE username = '$sess_username'") == true) {
   		        $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Yeah, Api Key Kamu Berhasil Diubah.<script>swal("Berhasil!", "API Key Kamu Berhasil Diubah.", "success");</script>');
		    } else {
   		        $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
		    }
        }

        require '../lib/header.php';

?>


        <!-- Start Content -->
	    <div class="kt-container  kt-grid__item kt-grid__item--fluid mt-4">

	   
        <div class="card p-3 mb-5">
          <div class="card-body">
                 
            <ul class="nav nav-tabs" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="one-tab" data-toggle="tab" href="#pillsone" role="tab" aria-controls="one" aria-selected="true">Profil</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="two-tab" data-toggle="tab" href="#pillstwo" role="tab" aria-controls="two" aria-selected="false">Ganti Kata Sandi</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="three-tab" data-toggle="tab" href="#pillsthree" role="tab" aria-controls="three" aria-selected="false">Ganti PIN</a>
              </li>
            </ul>
            <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="pillsone" role="tabpanel" aria-labelledby="one-tab">
                
                <form class="kt-form kt-form--label-right" method="POST">
                                    <input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
                                    <?php
                                    if (isset($_SESSION['hasil'])) {
                                    ?>
                                        <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <?php echo $_SESSION['hasil']['pesan'] ?>
                                        </div>
                                    <?php
                                    unset($_SESSION['hasil']);
                                    }
                                    ?>
                                    <div class="kt-section__body">
                                       
                                        <div class="form-group row">
                                            <label class="col-xl-12 col-lg-12 col-form-label">Terdaftar Sejak</label>
                                            <div class="col-lg-12 col-xl-12">
                                                <input class="form-control" type="text" value="<?php echo tanggal_indo($data_user['date']); ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-12 col-lg-12 col-form-label">Total Pemakaian Saldo</label>
                                            <div class="col-lg-12 col-xl-12">
                                                <input class="form-control" type="text" value="Rp <?php echo number_format($data_user['pemakaian_saldo'],0,',','.'); ?>" readonly>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-12 col-lg-12 col-form-label">NIK E-KTP</label>
                                            <div class="col-lg-12 col-xl-12">
                                                <input class="form-control" type="text" value="<?php echo $data_user['username']; ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-12 col-lg-12 col-form-label">Nama Lengkap</label>
                                            <div class="col-lg-12 col-xl-12">
                                                <input class="form-control" type="text" name="nama_depan" value="<?php echo $data_user['nama_depan']; ?>">
                                                <span class="form-text text-muted"><?php echo ($error['nama_depan']) ? $error['nama_depan'] : '';?></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-12 col-lg-12 col-form-label">Alamat</label>
                                            <div class="col-lg-12 col-xl-12">
                                                <input class="form-control" type="text" name="nama_belakang" value="<?php echo $data_user['nama_belakang']; ?>">
                                                <span class="form-text text-muted"><?php echo ($error['nama_belakang']) ? $error['nama_belakang'] : '';?></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-12 col-lg-12 col-form-label">Alamat Email</label>
                                            <div class="col-lg-12 col-xl-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-at text-primary"></i></span></div>
                                                    <input type="text" class="form-control" name="email" value="<?php echo $data_user['email']; ?>" placeholder="Email">
                                                </div>
                                                <span class="form-text text-muted"><?php echo ($error['email']) ? $error['email'] : '';?></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-12 col-lg-12 col-form-label">Nomer HP</label>
                                            <div class="col-lg-12 col-xl-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-phone text-primary"></i></span></div>
                                                    <input type="text" class="form-control" name="no_hp" value="<?php echo $data_user['no_hp']; ?>" placeholder="Contoh : 6281234567890">
                                                </div>
                                                <span class="form-text text-muted"><?php echo ($error['no_hp']) ? $error['no_hp'] : '';?></span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-12 col-lg-12 col-form-label">PIN</label>
                                            <div class="col-lg-12 col-xl-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-lock text-primary"></i></span></div>
                                                    <input type="password" name="pin1" class="form-control" placeholder="Masukkan PIN Kamu">
                                                </div>
                                                <span class="form-text text-muted"><?php echo ($error['pin1']) ? $error['pin1'] : '';?></span>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-0">
                                                
                                            <div class="col-lg-6 col-8">
                                                <button type="submit" name="ganti_profil" class="btn btn-primary btn-elevate btn-pill btn-elevate-air w-100">Update</button>
                                            </div>
                                            <div class="col-lg-6 col-4">
                                                <button type="reset" class="btn btn-danger btn-elevate btn-pill btn-elevate-air w-100">Reset</button>
                                            </div>
                                        
                                        </div>
                                    </div>
                                
                        </form>
                                
              </div>
              <div class="tab-pane fade" id="pillstwo" role="tabpanel" aria-labelledby="two-tab">
                
                <form class="kt-form kt-form--label-right" method="POST">
                    <input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
                        <div class="kt-portlet__body">
                            <div class="kt-section kt-section--first">
                                <div class="kt-section__body">
                                    <div class="form-group row">
                                        <div class="col-lg-12 col-xl-12">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-lock text-primary"></i></span></div>
                                                <input type="password" name="password_lama" class="form-control" value="<?php echo $password; ?>" placeholder="Kata Sandi Lama Kamu">
                                            </div>
                                            <span class="form-text text-muted"><?php echo ($error['password_lama']) ? $error['password_lama'] : '';?></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-12 col-xl-12">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-lock text-primary"></i></i></span></div>
                                                <input type="password" name="password_baru" class="form-control" value="<?php echo $password_baru; ?>" placeholder="Kata Sandi Baru Yang Mau Kamu Ganti">
                                            </div>
                                            <span class="form-text text-muted"><?php echo ($error['password_baru']) ? $error['password_baru'] : '';?></span>
                                        </div>
                                    </div>
                                    <div class="form-group form-group-last row">
                                        <div class="col-lg-12 col-xl-12">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text"><i class="la la-lock text-primary"></i></span></div>
                                                <input type="password" name="konf_pass_baru" class="form-control" value="<?php echo $konf_pass_baru; ?>" placeholder="Konfirmasi Kata Sandi Baru Sebelumnya">
                                            </div>
                                            <span class="form-text text-muted"><?php echo ($error['konf_pass_baru']) ? $error['konf_pass_baru'] : '';?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                                                
                            <div class="col-lg-6 col-8">
                                <button type="submit" name="ganti_password" class="btn btn-primary btn-elevate btn-pill btn-elevate-air w-100">Update</button>
                            </div>
                            <div class="col-lg-6 col-4">
                                <button type="reset" class="btn btn-danger btn-elevate btn-pill btn-elevate-air w-100">Reset</button>
                            </div>
                        
                        </div>
                    
                    </form>
                    
              </div>
              <div class="tab-pane fade" id="pillsthree" role="tabpanel" aria-labelledby="three-tab">
                
                <form class="kt-form kt-form--label-right" method="POST">
                    <input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
                    <div class="kt-portlet__body">
                        <div class="kt-section kt-section--first">
                            <div class="kt-section__body">
                                <div class="form-group row">
                                    <div class="col-lg-12 col-xl-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-key text-primary"></i></span></div>
                                            <input type="password" name="pin_lama" class="form-control" value="<?php echo $pin; ?>" placeholder="Masukkan PIN Lama Kamu">
                                        </div>
                                        <span class="form-text text-muted"><?php echo ($error['pin_lama']) ? $error['pin_lama'] : '';?></span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-12 col-xl-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-key text-primary"></i></span></div>
                                            <input type="password" name="pin_baru" class="form-control" value="<?php echo $pin_baru; ?>" placeholder="PIN Baru Yang Mau Kamu Ganti">
                                        </div>
                                        <span class="form-text text-muted"><?php echo ($error['pin_baru']) ? $error['pin_baru'] : '';?></span>
                                    </div>
                                </div>
                                <div class="form-group form-group-last row">
                                    <div class="col-lg-12 col-xl-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-key text-primary"></i></span></div>
                                            <input type="password" name="konfirmasi_pin_baru" class="form-control" value="<?php echo $konfirmasi_pin_baru; ?>" placeholder="Konfirmasi PIN Baru Sebelumnya">
                                        </div>
                                        <span class="form-text text-muted"><?php echo ($error['konfirmasi_pin_baru']) ? $error['konfirmasi_pin_baru'] : '';?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                                                
                        <div class="col-lg-6 col-8">
                            <button type="submit" name="ganti_pin" class="btn btn-primary btn-elevate btn-pill btn-elevate-air w-100">Update</button>
                        </div>
                        <div class="col-lg-6 col-4">
                            <button type="reset" class="btn btn-danger btn-elevate btn-pill btn-elevate-air w-100">Reset</button>
                        </div>
                    
                    </div>
                </form>
                
              </div>
              
            </div>
            
          </div>
        </div>
        
        
	    </div>

        
	<style>
	.kt-portlet.kt-portlet--height-fluid {background:#fff;}
	.kt-form.kt-form--label-right .form-group label:not(.kt-checkbox):not(.kt-radio):not(.kt-option) {
	    text-align:left;
	}
	</style>
<?php
require '../lib/footer.php';
?>