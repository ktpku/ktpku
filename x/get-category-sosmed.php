<?php
require_once("../config.php");

    $check_provider = $conn->query("SELECT * FROM provider WHERE code = 'MEDANPEDIA'");
    $data_provider = mysqli_fetch_assoc($check_provider);
    
    $cek_harga_website = $conn->query("SELECT * FROM setting_harga_untung WHERE kategori = 'WEBSITE' AND tipe = 'Sosial Media'");
    $data_harga_website = mysqli_fetch_assoc($cek_harga_website);

    $cek_harga_api = $conn->query("SELECT * FROM setting_harga_untung WHERE kategori = 'API' AND tipe = 'Sosial Media'");
    $data_harga_api = mysqli_fetch_assoc($cek_harga_api);

    $p_apiid = $data_provider['api_id'];
    $p_apikey = $data_provider['api_key'];
    
    $harga_website = $data_harga_website['harga'];
    $harga_api = $data_harga_api['harga'];

    $url = "https://medanpedia.co.id/api/services";
    $postdata = array( 
        'api_id' => $p_apiid,
	    'api_key' => $p_apikey
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $chresult = curl_exec($ch);
    $result = json_decode($chresult, true);
    // print_r($result);

// get data service
foreach($result['data'] as $data) {
       //INSERT KATEGORI
        $category = $data['category'];
		$cek_kategori = $conn->query("SELECT * FROM kategori_layanan WHERE nama = '$category'");
        $data_cat = mysqli_fetch_assoc($cek_kategori);
		if (mysqli_num_rows($cek_kategori) == 0) {
           $input_kategori = $conn->query("INSERT INTO kategori_layanan VALUES ('','$category','$category','Sosial Media','')");
           if ($input_kategori == TRUE){
               $check_data = $conn->query("SELECT * FROM kategori_layanan WHERE nama = '$category'");
               $get_data = mysqli_fetch_assoc($check_data);
               $data['category'] = $get_data['category'];
           } else {
               $data['category'] = $category;
           }
		} else {
            $data['category'] = $data_cat['kode'];
		}
        // end get data service 
	    $check_services = $conn->query("SELECT * FROM layanan_sosmed WHERE service_id = '".$data['id']."'");
        $data_services = mysqli_fetch_assoc($check_services);
        if (mysqli_num_rows($check_services) > 0) {
            echo "<br>Layanan Sudah Ada Di Database \n <br />";
        } else {
        $name = strtr($data['name'], array(
			'MP' => 'TB',
			'PNC' => 'TCB',
			'Medanpedia' => 'Technobooks',
			'MEdanpedia' => 'Technobooks',
			'MEDANPEDIA' => 'TECHNOBOOKS',
		));
		$catatan = strtr($data['description'], array(
			'MP' => 'TB',
			'PNC' => 'TCB',
			'Medanpedia' => 'Technobooks',
			'MEdanpedia' => 'Technobooks',
			'MEDANPEDIA' => 'TECHNOBOOKS',
		));
		
        		// setting price 
        $price_web = $data['price'] + $data['price'] * $harga_website / 100; //setting penambahan harga web
        
        $price_api = $data['price'] + $data['price'] * $harga_api / 100; //setting penambahan harga api
        // setting price
		
        $insert = $conn->query("INSERT INTO `layanan_sosmed`(`service_id`, `kategori`, `layanan`, `catatan`, `min`, `max`, `harga`, `harga_api`,`status`, `provider_id`, `provider`,`tipe`) VALUES ('".$data['id']."', '".$data['category']."', '".$name.' Rp.'.number_format($price_web,0,',','.')."', '".$catatan."', '".$data['min']."', '".$data['max']."','".$price_web."','".$price_api."','Aktif','".$data['id']."','MEDANPEDIA', 'Sosial Media')");
        if ($insert == TRUE) {
        $sid = $data['id'];
        $category = $data['category'];
        $price = $data['price'];
        $min = $data['min'];
        $max = $data['max'];
        $note = $data['description'];
        echo"===============<br>Layanan Sosmed Berhasil Di Tambahkan<br><br>ID Layanan : $sid<br>Kategori : $category<br>Nama Layanan : $name<br>Harga : $price<br>min : $min<br>max : $max<br>deskripsi : $note<br>===============<br>";
        } else {
            echo "Gagal Menampilkan Data Layanan sosmed.<br />";
        
        }
}
}
?>