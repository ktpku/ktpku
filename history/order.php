<?php
session_start();
require '../config.php';
require '../lib/session_user.php';
require '../lib/header.php';
?>

<style>
.table td, .table th {
    padding: 0 0 15px;
    border-top: none; 
}
.cardWrap {
    width: 100%;
    margin: auto;
    color: #fff;
    display: flex;
}
.cardWrap .cardz h2 {
    font-size: 16px;
}
.cardWrap .cardz {
  background: linear-gradient(to bottom, #0070C0 0%, #0070C0 45px, #f5f5f5 45px, #f5f5f5 100%);
  height: auto;
  position: relative;
  padding: 1em;
}
.cardWrap .cardLeft {
  border-top-left-radius: 8px;
  border-bottom-left-radius: 8px;
  width: 60%;
  white-space: normal;
}
.cardWrap .cardRight {
  width: 40%;
  border-left: .18em dashed #fff;
  border-top-right-radius: 8px;
  border-bottom-right-radius: 8px;
  white-space: normal;
}
.cardWrap .cardRight:before, .cardWrap .cardRight:after {
  content: "";
  position: absolute;
  display: block;
  width: .9em;
  height: .9em;
  background: #fff;
  border-radius: 50%;
  left: -.5em;
}
.cardWrap .cardRight:before {
  top: -.4em;
}
.cardWrap .cardRight:after {
  bottom: -.4em;
}
.cardWrap .title, .cardWrap .name, .cardWrap .seat, .cardWrap .time {
  text-transform: uppercase;
  font-weight: normal;
}
.cardWrap .title h2, .cardWrap .name h2, .cardWrap .seat h2, .cardWrap .time h2 {
  font-size: 14px;
  color: #525252;
  margin: 0;
}
.cardWrap .title span, .cardWrap .name span, .cardWrap .seat span, .cardWrap .time span {
  font-size: 10px;
  color: #a2aeae;
}
.cardWrap .title {
  margin: 2em 0 0 0;
}

.cardWrap .name, .cardWrap .seat {
  margin: .7em 0 0 0;
}
.cardWrap .number {
  text-align: center;
  text-transform: uppercase;
}
.cardWrap .number h3 {
  color: #e84c3d;
  margin: .9em 0 0 0;
  font-size: 2.5em;
}
.cardWrap .number span {
  display: block;
  color: #a2aeae;
}
li.page-item.disabled.riwayat {
    border: solid 1px;
    border-radius: .25rem;
    padding: 0 5px;
}
.kt-pagination.kt-pagination--brand .kt-pagination__links li:hover {
    background: #0070C0;
}
@media(max-width:767px){
    .cardWrap .cardz h2 {
        font-size: 12px;
    }
    .cardWrap .number h3 {
        font-size: 22px;
    }
}
</style>

        <!-- Start Content -->
        <div class="kt-container kt-grid__item kt-grid__item--fluid mt-4">

        <!-- Start Page History Order -->
        <div class="row">
	        <div class="col-lg-12">
		        <div class="kt-portlet">
			        <div class="kt-portlet__head">
				        <div class="kt-portlet__head-label">
					        <h3 class="kt-portlet__head-title">
					            <i class="flaticon2-shopping-cart-1 text-primary"></i>
					            Riwayat Pembelian
					        </h3>
				        </div>
			        </div>
			        <div class="kt-portlet__body">

                            <!--<form class="form-horizontal" method="GET">-->
                            <!--    <div class="row">-->
                            <!--        <div class="form-group col-lg-3 d-none">-->
                            <!--            <label>Tampilkan Beberapa</label>-->
                            <!--            <select class="form-control" name="tampil">-->
                            <!--                <option value="10">Default</option>-->
                            <!--                <option value="20">20</option>-->
                            <!--                <option value="50">50</option>-->
                            <!--                <option value="100">100</option>-->
                            <!--            </select>-->
                            <!--        </div>                                                -->
                            <!--        <div class="form-group col-lg-3">-->
                            <!--            <label>Filter Status</label>-->
                            <!--            <select class="form-control" name="status">-->
                            <!--                <option value="">Semua</option>-->
                            <!--                <option value="Pending">Pending</option>-->
                            <!--                <option value="Processing">Processing</option>-->
                            <!--                <option value="Success">Success</option>-->
                            <!--                <option value="Error">Error</option>-->
                            <!--                <option value="Partial">Partial</option>-->
                            <!--            </select>-->
                            <!--        </div>                                                -->
                            <!--        <div class="form-group col-lg-3">-->
                            <!--            <label>Cari Kode Pesanan</label>-->
                            <!--            <input type="number" class="form-control" name="cari" placeholder="Masukkan Kode Pesanan Kamu" value="">-->
                            <!--        </div>-->
                            <!--        <div class="form-group col-lg-3 offset-lg-3">-->
                            <!--            <label>Submit</label>-->
                            <!--            <button type="submit" class="btn btn-block btn-primary">Cari</button>-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</form>-->
                            <div class="table-responsive">
                                <table class="table table-checkable" id="kt_table_1">
                                    <tbody>
                                        <?php 
                                        // start paging config
                                        if (isset($_GET['cari'])) {
                                            $cari_oid = $conn->real_escape_string(filter($_GET['cari']));
                                            $cari_status = $conn->real_escape_string(filter($_GET['status']));
                                        
                                            $cek_pesanan = "SELECT * FROM semua_pembelian WHERE id_order LIKE '%$cari_oid%' AND status LIKE '%$cari_status%' AND user = '$sess_username' ORDER BY id DESC"; // edit
                                        } else {
                                            $cek_pesanan = "SELECT * FROM semua_pembelian WHERE user = '$sess_username' ORDER BY id DESC"; // edit
                                        }
                                        if (isset($_GET['cari'])) {
                                        $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                        $records_per_page = $cari_urut; // edit
                                        } else {
                                            $records_per_page = 10; // edit
                                        }
                                        
                                        $starting_position = 0;
                                        if(isset($_GET["halaman"])) {
                                            $starting_position = ($conn->real_escape_string(filter($_GET["halaman"]))-1) * $records_per_page;
                                        }
                                        $new_query = $cek_pesanan." LIMIT $starting_position, $records_per_page";
                                        $new_query = $conn->query($new_query);
                                        // end paging config
                                        while ($data_pesanan = $new_query->fetch_assoc()) {
                                            if ($data_pesanan['status'] == "Pending") {
                                                $label = "warning";
                                            } else if ($data_pesanan['status'] == "Partial") {
                                                $label = "danger";
                                            } else if ($data_pesanan['status'] == "Error") {
                                                $label = "danger";    
                                            } else if ($data_pesanan['status'] == "Processing") {
                                                $label = "primary";    
                                            } else if ($data_pesanan['status'] == "Success") {
                                                $label = "success";    
                                            }
                                            if ($data_pesanan['refund'] == "0") {
                                                $icon2 = "close";
                                                $label2 = "danger"; 
                                            } else if ($data_pesanan['refund'] == "1") {
                                                $icon2 = "check";
                                                $label2 = "success";
                                            }
                                        ?>
                                        <tr>
                                        <th scope="row">
        								        
        							        <div class="cardWrap">
        							            
                                              <div class="cardz cardLeft">
                                                <h2 class="mb-5"><?php echo $data_pesanan['kategori']; ?></h2>
                                                <div class="name">
                                                  <h2><?php echo $data_pesanan['layanan']; ?></h2>
                                                  <span>Layanan</span>
                                                </div>
                                                <div class="name">
                                                  <h2>Rp <?php echo number_format($data_pesanan['harga'],0,',','.'); ?></h2>
                                                  <span>Harga</span>
                                                </div>
                                                <div class="name">
                                                  <h2><span class="btn btn-<?php echo $label; ?> btn-elevate btn-pill btn-elevate-air btn-sm text-white"><?php echo $data_pesanan['status']; ?></span></h2>
                                                  <span>Status</span>
                                                </div>
                                                <div class="name">
                                                  <h2><?php echo tanggal_indo($data_pesanan['date']); ?></h2>
                                                  <span>Tanggal Transaksi</span>
                                                </div>
                                                <div class="name">
                                                  <h2><span class="btn btn-<?php echo $label2; ?> btn-elevate btn-circle btn-icon text-white"><i class="flaticon-<?php echo $icon2; ?>"></i></span></h2>
                                                  <span>Pengembalian Dana</span>
                                                </div>

                                              </div>
                                              <div class="cardz cardRight">
                                                <h2 class="mb-5 text-center">
                                                <?php if($data_pesanan['place_from'] == "API") { ?><i class="fa fa-random"></i><?php } else { ?><i class="flaticon-globe"></i><?php } ?>
                                                </h2>
                                                <div class="number">
                                                    <h3><?php echo $data_pesanan['id_order']; ?></h3>
                                                    <span class="text-muted">Kode Pesanan</span>
                                                    <hr>
                                                    <span class="text-muted"><i class="fas fa-clock"></i> <?php echo $data_pesanan['time']; ?></span>
                                                    <hr>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control form-control-sm" value="<?php echo $data_pesanan['target']; ?>" id="target-<?php echo $data_pesanan['oid']; ?>" readonly="">
                                                        <button data-toggle="tooltip" title="Copy Target" class="btn btn-primary btn-sm" type="button" onclick="copy_to_clipboard('target-<?php echo $data_pesanan['oid']; ?>')"><i class="fas fa-copy text-white"></i></button>
                                                    </div>
                                                </div>
                                              </div>
                                            
                                            </div>
                                            
        							    </th>
        							    </tr>
        							    
<?php } ?>

                                    </tbody>
                                </table>
                                <br>
                                <div class="kt-pagination kt-pagination--brand kt-pagination--circle">
                                    <ul class="kt-pagination__links">
                                    <?php
                                    // start paging link
                                    if (isset($_GET['cari'])) {
                                    $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                    } else {
                                    $cari_urut =  10;
                                    }  
                                    if (isset($_GET['cari'])) {
                                        $cari_oid = $conn->real_escape_string(filter($_GET['cari']));
                                        $cari_status = $conn->real_escape_string(filter($_GET['status']));
                                        $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                    } else {
                                        $self = $_SERVER['PHP_SELF'];
                                    }
                                    $cek_pesanan = $conn->query($cek_pesanan);
                                    $total_records = mysqli_num_rows($cek_pesanan);
                                    echo "<li class='disabled page-item riwayat'><a href='#'>Ada ".$total_records." Transaksi</a></li>";
                                    if($total_records > 0) {
                                        $total_pages = ceil($total_records/$records_per_page);
                                        $current_page = 1;
                                        if(isset($_GET["halaman"])) {
                                            $current_page = $conn->real_escape_string(filter($_GET["halaman"]));
                                            if ($current_page < 1) {
                                                $current_page = 1;
                                            }
                                        }
                                        if($current_page > 1) {
                                            $previous = $current_page-1;
                                        if (isset($_GET['cari'])) {
                                        $cari_oid = $conn->real_escape_string(filter($_GET['cari']));
                                        $cari_status = $conn->real_escape_string(filter($_GET['status']));
                                        $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=1&tampil=".$cari_urut."&status=".$cari_status."&cari=".$cari_oid."'><i class='fa fa-angle-double-left kt-font-brand'></i></a></li>";
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$previous."&tampil=".$cari_urut."&status=".$cari_status."&cari=".$cari_oid."'><i class='fa fa-angle-left kt-font-brand'></i></a></li>";
                                    } else {
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=1'><i class='fa fa-angle-double-left kt-font-brand'></i></a></li>";
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$previous."'><i class='fa fa-angle-left kt-font-brand'></i></a></li>";
                                    }
                                    }
                                        // limit page
                                        $limit_page = $current_page+3;
                                        $limit_show_link = $total_pages-$limit_page;
                                        if ($limit_show_link < 0) {
                                            $limit_show_link2 = $limit_show_link*2;
                                            $limit_link = $limit_show_link - $limit_show_link2;
                                            $limit_link = 3 - $limit_link;
                                        } else {
                                            $limit_link = 3;
                                        }
                                        $limit_page = $current_page+$limit_link;
                                        // end limit page
                                        // start page
                                        if ($current_page == 1) {
                                            $start_page = 1;
                                        } else if ($current_page > 1) {
                                            if ($current_page < 4) {
                                                $min_page  = $current_page-1;
                                            } else {
                                                $min_page  = 3;
                                            }
                                            $start_page = $current_page-$min_page;
                                        } else {
                                            $start_page = $current_page;
                                        }
                                        // end start page
                                        for($i=$start_page; $i<=$limit_page; $i++) {
                                        if (isset($_GET['cari'])) {
                                        $cari_oid = $conn->real_escape_string(filter($_GET['cari']));
                                        $cari_status = $conn->real_escape_string(filter($_GET['status']));
                                        $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                            if($i==$current_page) {
                                                echo "<li class='kt-pagination__link--active'><a href='#'>".$i."</a></li>";
                                            } else {
                                                echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$i."&tampil=".$cari_urut."&status=".$cari_status."&cari=".$cari_oid."'>".$i."</a></li>";
                                            }
                                        } else {
                                            if($i==$current_page) {
                                                echo "<li class='kt-pagination__link--active'><a href='#'>".$i."</a></li>";
                                            } else {
                                                echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$i."'>".$i."</a></li>";
                                            }        
                                        }
                                        }
                                        if($current_page!=$total_pages) {
                                            $next = $current_page+1;
                                        if (isset($_GET['cari'])) {
                                        $cari_oid = $conn->real_escape_string(filter($_GET['cari']));
                                        $cari_status = $conn->real_escape_string(filter($_GET['status']));
                                        $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$next."&tampil=".$cari_urut."&status=".$cari_status."&cari=".$cari_oid."'><i class='fa fa-angle-right kt-font-brand'></i></a></li>";
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$total_pages."&tampil=".$cari_urut."&status=".$cari_status."&cari=".$cari_oid."'><i class='fa fa-angle-double-right kt-font-brand'></i></a></li>";
                                    } else {
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$next."'><i class='fa fa-angle-right kt-font-brand'></i></a></li>";
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$total_pages."'><i class='fa fa-angle-double-right kt-font-brand'></i></a></li>";
                                        }
                                    }
                                    }
                                    // end paging link
                                    ?>

                                    </ul>
                                </div>
                            </div>
		      
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page History Order -->

        </div>
        <!-- End Content -->


        <!-- Start Modal History All Order -->
        <div class="modal fade" id="myDetailAllOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		    <div class="modal-dialog" role="document">
			    <div class="modal-content">
				    <div class="modal-header">
                    <h4 class="modal-title mt-0" id="myModalLabel"><i class="flaticon-eye text-primary"></i> Detail Pesanan</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
				    </div>
				    <div class="modal-body" id="data_all_order">
				    </div>
				    <div class="modal-footer">
				    	<button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
				    </div>
			    </div>
		    </div>
	    </div>
	    <!-- End Modal History All Order -->

        <!-- Start Modal History Order Social Media -->
        <div class="modal fade" id="myDetailSosmed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		    <div class="modal-dialog" role="document">
			    <div class="modal-content">
				    <div class="modal-header">
                    <h4 class="modal-title mt-0" id="myModalLabel"><i class="flaticon-eye text-primary"></i> Detail Pesanan Sosial Media</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
				    </div>
				    <div class="modal-body" id="data_sosmed_order">
				    </div>
				    <div class="modal-footer">
				    	<button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
				    </div>
			    </div>
		    </div>
	    </div>
	    <!-- End Modal History Order Social Media -->

        <!-- Start Modal History Order Top Up -->
        <div class="modal fade" id="myDetailTopUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		    <div class="modal-dialog" role="document">
			    <div class="modal-content">
				    <div class="modal-header">
                    <h4 class="modal-title mt-0" id="myModalLabel"><i class="flaticon-eye text-primary"></i> Detail Pesanan Top Up</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
				    </div>
				    <div class="modal-body" id="data_top_up_order">
				    </div>
				    <div class="modal-footer">
				    	<button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
				    </div>
			    </div>
		    </div>
	    </div>
	    <!-- End Modal History Order Top Up -->

<?php
require '../lib/footer.php';
?>

	    <script type="text/javascript">
	    function copy_to_clipboard(element) {
	        var copyText = document.getElementById(element);
	        copyText.select();
	        document.execCommand("copy");
	    }
	    </script>

	    <script type="text/javascript">
	        $(document).ready(function(){
		        $('.view_all_order').click(function(){
		        	var id = $(this).attr("id");
			        $.ajax({
				        url: '<?php echo $config['web']['url']; ?>history/ajax/detail-order.php',
				        method: 'post',		
				        data: {id:id},	
				        success:function(data){	
					        $('#data_all_order').html(data);
					        $('#myDetailAllOrder').modal("show");
				        }
			        });
		        });
	        });
	    </script>

	    <script type="text/javascript">
	        $(document).ready(function(){
		        $('.view_sosmed_order').click(function(){
		        	var id = $(this).attr("id");
			        $.ajax({
				        url: '<?php echo $config['web']['url']; ?>history/ajax/detail-sosmed.php',
				        method: 'post',		
				        data: {id:id},	
				        success:function(data){	
					        $('#data_sosmed_order').html(data);
					        $('#myDetailSosmed').modal("show");
				        }
			        });
		        });
	        });
	    </script>

	    <script type="text/javascript">
	        $(document).ready(function(){
		        $('.view_top_up_order').click(function(){
		        	var id = $(this).attr("id");
			        $.ajax({
				        url: '<?php echo $config['web']['url']; ?>history/ajax/detail-top-up.php',
				        method: 'post',		
				        data: {id:id},	
				        success:function(data){	
					        $('#data_top_up_order').html(data);
					        $('#myDetailTopUp').modal("show");
				        }
			        });
		        });
	        });
	    </script>