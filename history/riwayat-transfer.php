<?php 
session_start();
require '../config.php';
require '../lib/session_login.php';
require '../lib/session_user.php';

	    if (isset($_POST['transfer'])) {
	        $post_metode = $conn->real_escape_string($_POST['radio7']);
            $tujuan = $conn->real_escape_string(trim(filter($_POST['tujuan'])));
            $jumlah = $conn->real_escape_string(trim(filter($_POST['jumlah'])));
            $pin = $conn->real_escape_string(trim(filter($_POST['pin'])));

            $cek_tujuan = $conn->query("SELECT * FROM users WHERE username = '$tujuan'");
            $cek_tujuan_rows = mysqli_num_rows($cek_tujuan);

            if ($post_metode == "saldo_sosmed") {
		        $post_metodee = "Saldo Sosial Media";
            } else if ($post_metode == "saldo_top_up") {
		        $post_metodee = "Saldo Top Up";
            }

            $error = array();
            if (empty($post_metode)) {
		        $error ['radio7'] = '*Wajib Pilih Salah Satu.';
            }
            if (empty($tujuan)) {
		        $error ['tujuan'] = '*Tidak Boleh Kosong.';
            }
            if (empty($jumlah)) {
		        $error ['jumlah'] = '*Tidak Boleh Kosong.';
            }
            if (empty($pin)) {
		        $error ['pin'] = '*Tidak Boleh Kosong.';
            } else if ($pin <> $data_user['pin']) {
		        $error ['pin'] = '*PIN Yang Kamu Masukkan Salah.';
            } else {

		    if ($cek_tujuan_rows == 0 ) {
			    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Nama Pengguna Tujuan Tidak Ditemukan.<script>swal("Ups Gagal!", "Nama Pengguna Tujuan Tidak Ditemukan.", "error");</script>');
		    } else if ($jumlah < 5000 ) {
			    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Minimal Transfer Saldo Adalah Rp 5.000.<script>swal("Ups Gagal!", "Minimal Transfer Saldo Adalah Rp 5.000.", "error");</script>');
		    } else if ($data_user['saldo_top_up'] < $jumlah ) {
			    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Saldo Top Up Kamu Tidak Mencukupi Untuk Melakukan Transfer Saldo.<script>swal("Ups Gagal!", "Saldo Top Up Kamu Tidak Mencukupi Untuk Melakukan Transfer Saldo.", "error");</script>');
		    } else {

			            $check_top = $conn->query("SELECT * FROM top_depo WHERE username = '$tujuan'");
			            $data_top = mysqli_fetch_assoc($check_top);
			        if ($conn->query("UPDATE users set $post_metode = $post_metode + $jumlah WHERE username = '$tujuan'") == true) {
				        $conn->query("UPDATE users set saldo_top_up = saldo_top_up - $jumlah, pemakaian_saldo = pemakaian_saldo + $jumlah  WHERE username = '$sess_username'");
                        $conn->query("INSERT INTO riwayat_saldo_koin VALUES ('', '$sess_username', 'Saldo', 'Pengurangan Saldo', '$jumlah', 'Mengurangi Saldo Top Up Melalui Transfer Saldo Ke $tujuan Sejumlah Rp $jumlah', '$date', '$time')");	
                        $conn->query("INSERT INTO riwayat_saldo_koin VALUES ('', '$tujuan', 'Saldo', 'Penambahan Saldo', '$jumlah', 'Mendapatkan $post_metodee Melalui Transfer Saldo Dari $sess_username Sejumlah Rp $jumlah ', '$date', '$time')");
                        $conn->query("INSERT INTO riwayat_transfer VALUES ('', '$post_metodee', '$sess_username', '$tujuan', '$jumlah','$date', '$time')");
					    if (mysqli_num_rows($check_top) == 0) {
				            $insert_topup = $conn->query("INSERT INTO top_depo VALUES ('', 'Deposit', '$tujuan', '$jumlah', '1')");
				        } else {
				            $insert_topup = $conn->query("UPDATE top_depo SET jumlah = ".$data_top['jumlah']."+$jumlah, total = ".$data_top['total']."+1 WHERE username = '$tujuan' AND method = 'Deposit'");
				        }
                        $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Sip, Berhasil Transfer Saldo.');
				    } else {
				        $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
			        }
			    }					
		    }
	    }

        require '../lib/header.php';

?>

<style>
.table td, .table th {
    padding: 0 0 15px;
    border-top: none; 
}
.cardWrap {
    width: 100%;
    margin: auto;
    color: #fff;
    display: flex;
}
.cardWrap .cardz h2 {
    font-size: 16px;
}
.cardWrap .cardz {
  background: linear-gradient(to bottom, #0070C0 0%, #0070C0 45px, #f5f5f5 45px, #f5f5f5 100%);
  height: auto;
  position: relative;
  padding: 1em;
}
.cardWrap .cardLeft {
  border-top-left-radius: 8px;
  border-bottom-left-radius: 8px;
  width: 60%;
  white-space: normal;
}
.cardWrap .cardRight {
  width: 40%;
  border-left: .18em dashed #fff;
  border-top-right-radius: 8px;
  border-bottom-right-radius: 8px;
  white-space: normal;
}
.cardWrap .cardRight:before, .cardWrap .cardRight:after {
  content: "";
  position: absolute;
  display: block;
  width: .9em;
  height: .9em;
  background: #fff;
  border-radius: 50%;
  left: -.5em;
}
.cardWrap .cardRight:before {
  top: -.4em;
}
.cardWrap .cardRight:after {
  bottom: -.4em;
}
.cardWrap .title, .cardWrap .name, .cardWrap .seat, .cardWrap .time {
  text-transform: uppercase;
  font-weight: normal;
}
.cardWrap .title h2, .cardWrap .name h2, .cardWrap .seat h2, .cardWrap .time h2 {
  font-size: 14px;
  color: #525252;
  margin: 0;
}
.cardWrap .title span, .cardWrap .name span, .cardWrap .seat span, .cardWrap .time span {
  font-size: 10px;
  color: #a2aeae;
}
.cardWrap .title {
  margin: 2em 0 0 0;
}

.cardWrap .name, .cardWrap .seat {
  margin: .7em 0 0 0;
}
.cardWrap .number {
  text-align: center;
  text-transform: uppercase;
}
.cardWrap .number h3 {
  color: #e84c3d;
  margin: .9em 0 0 0;
  font-size: 2.5em;
}
.cardWrap .number span {
  display: block;
  color: #a2aeae;
}
li.page-item.disabled.riwayat {
    border: solid 1px;
    border-radius: .25rem;
    padding: 0 5px;
}
.kt-pagination.kt-pagination--brand .kt-pagination__links li:hover {
    background: #0070C0;
}
@media(max-width:767px){
    .cardWrap .cardz h2 {
        font-size: 12px;
    }
    .cardWrap .number h3 {
        font-size: 22px;
    }
}
</style>

        <!-- Start Content -->
        <div class="kt-container kt-grid__item kt-grid__item--fluid mt-4">

        <!-- Start Page Transfer Balance -->
        <div class="row">
	        <div class="col-md-12">
		        <div class="kt-portlet">
			        <div class="kt-portlet__head">
				        <div class="kt-portlet__head-label">
					        <h3 class="kt-portlet__head-title">
					            <i class="flaticon2-time text-primary"></i>
					            Riwayat Transfer/Bayar
					        </h3>
				        </div>
			        </div>
			        <div class="kt-portlet__body">
                    <div class="table-responsive">
						<table class="table table-checkable" id="kt_table_1">
							<tbody>
                                <?php 
                                // start paging config
                                $no=1;
                                if (isset($_GET['tampil'])) {
                                    $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                    $cari_aksi = $conn->real_escape_string(filter($_GET['aksi']));
                                
                                    $cek_riwayat = "SELECT * FROM riwayat_transfer WHERE penerima LIKE '%$cari_aksi%' AND pengirim = '$sess_username' ORDER BY id DESC"; // edit
                                } else {
                                    $cek_riwayat = "SELECT * FROM riwayat_transfer WHERE pengirim = '$sess_username' ORDER BY id DESC"; // edit
                                }
                                if (isset($_GET['tampil'])) {
                                $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                $records_per_page = $cari_urut; // edit
                                } else {
                                    $records_per_page = 10; // edit
                                }
                                
                                $starting_position = 0;
                                if(isset($_GET["halaman"])) {
                                    $starting_position = ($conn->real_escape_string(filter($_GET["halaman"]))-1) * $records_per_page;
                                }
                                $new_query = $cek_riwayat." LIMIT $starting_position, $records_per_page";
                                $new_query = $conn->query($new_query);
                                $no = $starting_position+1;
                                // end paging config
                                while ($data_riwayat = $new_query->fetch_assoc()) {
                                ?>
							    <tr>
								    <th scope="row">
								        
								        <div class="cardWrap">
								            
                                          <div class="cardz cardLeft">
                                            <h2 class="mb-5"><span>Rp <?php echo number_format($data_riwayat['jumlah'],0,',','.'); ?></span></h2>
                                            <div class="name">
                                              <h2><?php echo $data_riwayat['penerima']; ?></h2>
                                              <span>Penerima</span>
                                            </div>
                                            <div class="name">
                                              <h2><?php echo $data_riwayat['tipe']; ?></h2>
                                              <span>Jenis Pebayaran</span>
                                            </div>
                                            <div class="name">
                                              <h2><?php echo tanggal_indo($data_riwayat['date']); ?></h2>
                                              <span>Tanggal</span>
                                            </div>
                                            
                                          </div>
                                          <div class="cardz cardRight">
                                            <h2 class="mb-5 text-center"><i class="fas fa-hashtag"></i></h2>
                                            <div class="number">
                                              <h3><?php echo $no++ ?></h3>
                                              <span class="text-muted">No.</span>
                                              <hr>
                                              <span class="text-muted"><i class="fas fa-clock"></i> <?php echo $data_riwayat['time']; ?></span>
                                            </div>
                                          </div>
                                        
                                        </div>
                                        
								    </th>
							    </tr>
                                <?php } ?>
							</tbody>
                        </table>
                        
                            <div class="kt-pagination kt-pagination--brand kt-pagination--circle">
                                <ul class="kt-pagination__links">
                                    <?php
                                    // start paging link
                                    if (isset($_GET['tampil'])) {
                                    $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                    } else {
                                    $cari_urut =  10;
                                    }  
                                    if (isset($_GET['tampil'])) {
                                        $cari_aksi = $conn->real_escape_string(filter($_GET['aksi']));
                                        $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                    } else {
                                        $self = $_SERVER['PHP_SELF'];
                                    }
                                    $cek_riwayat = $conn->query($cek_riwayat);
                                    $total_records = mysqli_num_rows($cek_riwayat);
                                    echo "<li class='page-item disabled riwayat'><a href='#'>Ada ".$total_records." Transaksi</a></li>";
                                    if($total_records > 0) {
                                        $total_pages = ceil($total_records/$records_per_page);
                                        $current_page = 1;
                                        if(isset($_GET["halaman"])) {
                                            $current_page = $conn->real_escape_string(filter($_GET["halaman"]));
                                            if ($current_page < 1) {
                                                $current_page = 1;
                                            }
                                        }
                                        if($current_page > 1) {
                                            $previous = $current_page-1;
                                        if (isset($_GET['tampil'])) {
                                        $cari_aksi = $conn->real_escape_string(filter($_GET['aksi']));
                                        $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=1&tampil=".$cari_urut."&aksi=".$cari_aksi."'><i class='fa fa-angle-double-left kt-font-brand'></i></a></li>";
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$previous."&tampil=".$cari_urut."&aksi=".$cari_aksi."'><i class='fa fa-angle-left kt-font-brand'></i></a></li>";
                                    } else {
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=1'><i class='fa fa-angle-double-left kt-font-brand'></i></a></li>";
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$previous."'><i class='fa fa-angle-left kt-font-brand'></i></a></li>";
                                    }
                                    }
                                        // limit page
                                        $limit_page = $current_page+3;
                                        $limit_show_link = $total_pages-$limit_page;
                                        if ($limit_show_link < 0) {
                                            $limit_show_link2 = $limit_show_link*2;
                                            $limit_link = $limit_show_link - $limit_show_link2;
                                            $limit_link = 3 - $limit_link;
                                        } else {
                                            $limit_link = 3;
                                        }
                                        $limit_page = $current_page+$limit_link;
                                        // end limit page
                                        // start page
                                        if ($current_page == 1) {
                                            $start_page = 1;
                                        } else if ($current_page > 1) {
                                            if ($current_page < 4) {
                                                $min_page  = $current_page-1;
                                            } else {
                                                $min_page  = 3;
                                            }
                                            $start_page = $current_page-$min_page;
                                        } else {
                                            $start_page = $current_page;
                                        }
                                        // end start page
                                        for($i=$start_page; $i<=$limit_page; $i++) {
                                        if (isset($_GET['tampil'])) {
                                        $cari_aksi = $conn->real_escape_string(filter($_GET['aksi']));
                                        $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                            if($i==$current_page) {
                                                echo "<li class='kt-pagination__link--active'><a href='#'>".$i."</a></li>";
                                            } else {
                                                echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$i."&tampil=".$cari_urut."&aksi=".$cari_aksi."'>".$i."</a></li>";
                                            }
                                        } else {
                                            if($i==$current_page) {
                                                echo "<li class='kt-pagination__link--active'><a href='#'>".$i."</a></li>";
                                            } else {
                                                echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$i."'>".$i."</a></li>";
                                            }        
                                        }
                                        }
                                        if($current_page!=$total_pages) {
                                            $next = $current_page+1;
                                        if (isset($_GET['tampil'])) {
                                        $cari_aksi = $conn->real_escape_string(filter($_GET['aksi']));
                                        $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$next."&tampil=".$cari_urut."&aksi=".$cari_aksi."'><i class='fa fa-angle-right kt-font-brand'></i></i></a></li>";
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$total_pages."&tampil=".$cari_urut."&aksi=".$cari_aksi."'><i class='fa fa-angle-double-right kt-font-brand'></i></a></li>";
                                    } else {
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$next."'><i class='fa fa-angle-right kt-font-brand'></i></a></li>";
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$total_pages."'><i class='fa fa-angle-double-right kt-font-brand'></i></a></li>";
                                        }
                                    }
                                    }
                                    // end paging link
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Transfer Balance -->

        </div>
        <!-- End Content -->


<?php
require '../lib/footer.php';