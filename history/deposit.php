<?php
session_start();
require '../config.php';
require '../lib/session_user.php';
require '../lib/header.php';

    	if (isset($_POST['kode_deposit'])) {
    	    $post_kode = $conn->real_escape_string(trim(filter($_POST['kode_deposit'])));

    	    $cek_deposit = $conn->query("SELECT * FROM deposit WHERE kode_deposit = '$post_kode'");
    	    $data_deposit = mysqli_fetch_assoc($cek_deposit);

    	    if (mysqli_num_rows($cek_deposit) == 0) {
    	        $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Isi Saldomu Tidak Di Temukan.<script>swal("Ups Gagal!", "Isi Saldomu Tidak Di Temukan.", "error");</script>');
    	    } else if($data_deposit['status'] !== "Pending" AND $data_deposit['status'] !== "Processing") {
    	        $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Isi Saldomu Gak Bisa Dibatalkan.<script>swal("Ups Gagal!", "Isi Saldomu Gak Bisa Dibatalkan.", "error");</script>');
    	    } else {

    	    $update_deposit = $conn->query("UPDATE deposit set status = 'Error' WHERE kode_deposit = '$post_kode'");
    	    if($update_deposit == TRUE) {
    	        $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Sip! Isi Saldomu Berhasil Di Batalkan.<script>swal("Berhasil!", "Isi Saldomu Berhasil Di Batalkan.", "success");</script>');
    	    } else {
    			$_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
	        }
	    }

        } else if (isset($_POST['confirm'])) {
    	    $post_kode = $conn->real_escape_string(trim(filter($_POST['confirm'])));

    	    $cek_deposit = $conn->query("SELECT * FROM deposit WHERE kode_deposit = '$post_kode'");
    	    $data_deposit = mysqli_fetch_assoc($cek_deposit);

    	    $post_jumlah = $data_deposit['jumlah_transfer'];
    	    $post_saldo = $data_deposit['get_saldo'];
    	    $post_tipe = $data_deposit['metode_isi_saldo'];

    	    $cek_mutasi = $conn->query("SELECT * FROM mutasi WHERE jumlah = '$post_jumlah'");
    	    $data_mutasi = mysqli_fetch_assoc($cek_mutasi);

    	    $post_status = $data_mutasi['status'];

    	    if (mysqli_num_rows($cek_deposit) == 0) {
    	        $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Isi Saldomu Tidak Di Temukan.<script>swal("Ups Gagal!", "Isi Saldomu Tidak Di Temukan.", "error");</script>');
    	    } else if (mysqli_num_rows($cek_mutasi) == 0) {
    	        $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Dana Kamu Tidak Ditemukan.<script>swal("Ups Gagal!", "Dana Kamu Tidak Ditemukan.", "error");</script>');
    	    } else if ($data_mutasi['status'] == "UNREAD") {
    	        $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Dana Belum Kami Terima.<script>swal("Ups Gagal!", "Dana Belum Kami Terima.", "error");</script>');
    	    } else {

        	    $check_top = $conn->query("SELECT * FROM top_depo WHERE username = '$sess_username'");
        	    $data_top = mysqli_fetch_assoc($check_top);
        	    $update = $conn->query("UPDATE deposit set status = 'Success' WHERE kode_deposit = '$post_kode'");
        	    $update = $conn->query("UPDATE users SET $post_tipe = $post_tipe + $post_saldo, pemakaian_saldo = pemakaian_saldo + $post_saldo WHERE username = '$sess_username'");
        	    $update = $conn->query("UPDATE mutasi SET status = 'UNREAD' WHERE code = '$post_kode'");
        	    if ($update == TRUE) {
            	    $insert = $conn->query("INSERT INTO riwayat_saldo_koin VALUES ('', '$sess_username', 'Saldo', 'Penambahan Saldo', '$post_saldo', 'Mendapatkan Saldo Melalui Isi Saldo Via Transfer Bank ".$data_mutasi['provider']." Dengan Kode Isi Saldo : $post_kode', '$date', '$time')");
            	    if($insert == TRUE) {
						if (mysqli_num_rows($check_top) == 0) {
							$insert_topup = $conn->query("INSERT INTO top_depo VALUES ('', 'Deposit', '$sess_username', '$post_saldo', '1')");
						} else {
							$insert_topup = $conn->query("INSERT top_depo SET jumlah = ".$data_top['jumlah']."+$post_saldo, total = ".$data_top['total']."+1 WHERE username = '$sess_username' AND method = 'Deposit'");
						}
            	        $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Sip! Saldo Kamu Berhasil Dikonfirmasi.<script>swal("Berhasil!", "Saldo Kamu Udah Masuk.", "success");</script>');
            	    } else {
            			$_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
        	        }
        	    }
            }
        }

?>

<style>
.table td, .table th {
    padding: 0 0 15px;
    border-top: none; 
}
.cardWrap {
    width: 100%;
    margin: auto;
    color: #fff;
    display: flex;
}
.cardWrap .cardz h2 {
    font-size: 16px;
}
.cardWrap .cardz {
  background: linear-gradient(to bottom, #0070C0 0%, #0070C0 45px, #f5f5f5 45px, #f5f5f5 100%);
  height: auto;
  position: relative;
  padding: 1em;
}
.cardWrap .cardLeft {
  border-top-left-radius: 8px;
  border-bottom-left-radius: 8px;
  width: 60%;
  white-space: normal;
}
.cardWrap .cardRight {
  width: 40%;
  border-left: .18em dashed #fff;
  border-top-right-radius: 8px;
  border-bottom-right-radius: 8px;
  white-space: normal;
}
.cardWrap .cardRight:before, .cardWrap .cardRight:after {
  content: "";
  position: absolute;
  display: block;
  width: .9em;
  height: .9em;
  background: #fff;
  border-radius: 50%;
  left: -.5em;
}
.cardWrap .cardRight:before {
  top: -.4em;
}
.cardWrap .cardRight:after {
  bottom: -.4em;
}
.cardWrap .title, .cardWrap .name, .cardWrap .seat, .cardWrap .time {
  text-transform: uppercase;
  font-weight: normal;
}
.cardWrap .title h2, .cardWrap .name h2, .cardWrap .seat h2, .cardWrap .time h2 {
  font-size: 14px;
  color: #525252;
  margin: 0;
}
.cardWrap .title span, .cardWrap .name span, .cardWrap .seat span, .cardWrap .time span {
  font-size: 10px;
  color: #a2aeae;
}
.cardWrap .title {
  margin: 2em 0 0 0;
}

.cardWrap .name, .cardWrap .seat {
  margin: .7em 0 0 0;
}
.cardWrap .number {
  text-align: center;
  text-transform: uppercase;
}
.cardWrap .number h3 {
  color: #e84c3d;
  margin: .9em 0 0 0;
  font-size: 2.5em;
}
.cardWrap .number span {
  display: block;
  color: #a2aeae;
}
li.page-item.disabled.riwayat {
    border: solid 1px;
    border-radius: .25rem;
    padding: 0 5px;
}
.kt-pagination.kt-pagination--brand .kt-pagination__links li:hover {
    background: #0070C0;
}
@media(max-width:767px){
    .cardWrap .cardz h2 {
        font-size: 12px;
    }
    .cardWrap .number h3 {
        font-size: 22px;
    }
}
</style>

        <!-- Start Content -->
        <div class="kt-container kt-grid__item kt-grid__item--fluid">

        <!-- Start Page History Top Up Balance -->
        <div class="row mt-4">
	        <div class="col-lg-12">
		        <div class="kt-portlet">
			        <div class="kt-portlet__head">
				        <div class="kt-portlet__head-label">
					        <h3 class="kt-portlet__head-title">
					            <i class="flaticon2-time text-primary"></i>
					            Riwayat Isi Saldo
					        </h3>
				        </div>
			        </div>
			        <div class="kt-portlet__body">
                    <?php
                    if (isset($_SESSION['hasil'])) {
                    ?>
                    <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $_SESSION['hasil']['pesan'] ?>
                    </div>
                    <?php
                    unset($_SESSION['hasil']);
                    }
                    ?>
                    <!--<form class="form-horizontal" method="GET">-->
                    <!--    <div class="row">-->
                    <!--        <div class="form-group col-lg-3 d-none">-->
                    <!--            <label>Tampilkan Beberapa</label>-->
                    <!--            <select class="form-control" name="tampil">-->
                    <!--                <option value="10">Default</option>-->
                    <!--                <option value="20">20</option>-->
                    <!--                <option value="50">50</option>-->
                    <!--                <option value="100">100</option>-->
                    <!--            </select>-->
                    <!--        </div>-->
                    <!--        <div class="form-group col-lg-3">-->
                    <!--            <label>Filter Status</label>-->
                    <!--            <select class="form-control" name="status">-->
                    <!--                <option value="">Semua</option>-->
                    <!--                <option value="Pending">Pending</option>-->
                    <!--                <option value="Success">Success</option>-->
                    <!--                <option value="Error">Error</option>-->
                    <!--            </select>-->
                    <!--        </div>-->
                    <!--        <div class="form-group col-lg-3">-->
                    <!--            <label>Cari Kode Isi Saldo</label>-->
                    <!--            <input type="number" class="form-control" name="cari" placeholder="Masukkan Kode Isi Saldo Kamu" value="">-->
                    <!--        </div>-->
                    <!--        <div class="form-group col-lg-3 offset-lg-3">-->
                    <!--            <label>Submit</label>-->
                    <!--            <button type="submit" class="btn btn-block btn-primary">Cari</button>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</form>-->
                    <div class="table-responsive">
                        <table class="table table-checkable" id="kt_table_1">
                            <tbody>
                                <?php
                                // start paging config
                                $no = 1;
                                if (isset($_GET['cari'])) {
                                    $cari_id = $conn->real_escape_string(filter($_GET['cari']));
                                    $cari_status = $conn->real_escape_string(filter($_GET['status']));
                                
                                    $cek_depo = "SELECT * FROM deposit WHERE kode_deposit LIKE '%$cari_id%' AND status LIKE '%$cari_status%' AND username = '$sess_username' ORDER BY id DESC"; // edit
                                } else {
                                    $cek_depo = "SELECT * FROM deposit WHERE username = '$sess_username' ORDER BY id DESC"; // edit
                                }
                                if (isset($_GET['cari'])) {
                                $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                $records_per_page = $cari_urut; // edit
                                } else {
                                    $records_per_page = 10; // edit
                                }
                                
                                $starting_position = 0;
                                if(isset($_GET["halaman"])) {
                                    $starting_position = ($conn->real_escape_string(filter($_GET["halaman"]))-1) * $records_per_page;
                                }
                                $new_query = $cek_depo." LIMIT $starting_position, $records_per_page";
                                $new_query = $conn->query($new_query);
                                // end paging config
                                while ($data_depo = $new_query->fetch_assoc()) {
                                    if ($data_depo['status'] == "Pending") {
                                        $label = "warning";
                                    } else if ($data_depo['status'] == "Error") {
                                        $label = "danger";     
                                    } else if ($data_depo['status'] == "Success") {
                                        $label = "success";    
                                    }
                                ?>
                                <tr>
                                <th scope="row">
								        
							        <div class="cardWrap">
							            
                                      <div class="cardz cardLeft">
                                        <h2 class="mb-5">Jenis : <?php echo $data_depo['provider']; ?></h2>
                                        <div class="name">
                                          <h2><?php echo $data_depo['pengirim']; ?></h2>
                                          <span>Pengirim</span>
                                        </div>
                                        <div class="name">
                                          <h2><?php echo $data_depo['penerima']; ?></h2>
                                          <span>Penerima</span>
                                        </div>
                                        <div class="name">
                                          <h2><?php echo $data_depo['catatan']; ?></h2>
                                          <span>Keterangan</span>
                                        </div>
                                        <div class="name">
                                          <h2><?php echo tanggal_indo($data_depo['date']); ?></h2>
                                          <span>Tanggal</span>
                                        </div>
                                        <div class="name">
                                          <h2>Rp <?php echo number_format($data_depo['jumlah_transfer'],0,',','.'); ?></h2>
                                          <span>Total Pembayaran</span>
                                        </div>
                                        <div class="name">
                                          <h2>Rp <?php echo number_format($data_depo['get_saldo'],0,',','.'); ?></h2>
                                          <span>Total Yang Di Dapat</span>
                                        </div>
                                        
                                      </div>
                                      <div class="cardz cardRight">
                                        <h2 class="mb-5 text-center"><?php echo $data_depo['status']; ?></h2>
                                        <div class="number">
                                          <h3><?php echo $data_depo['kode_deposit']; ?></h3>
                                          <span class="text-muted">No.</span>
                                          <hr>
                                          <span class="text-muted"><i class="fas fa-clock"></i> <?php echo $data_depo['time']; ?></span>
                                          <hr>
                                          <a href="<?php echo $config['web']['url'] ?>deposit-balance/invoice?kode_deposit=<?php echo $data_depo['kode_deposit']; ?>" class="btn btn-<?php echo $label; ?> btn-elevate btn-pill btn-elevate-air btn-sm w-100 text-white">Lihat</a>
                                        </div>
                                      </div>
                                    
                                    </div>
                                    
							    </th>
							    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <br>
                            <div class="kt-pagination kt-pagination--brand kt-pagination--circle">
                                <ul class="kt-pagination__links">
                                <?php
                                // start paging link
                                if (isset($_GET['cari'])) {
                                $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                } else {
                                $cari_urut =  10;
                                }  
                                if (isset($_GET['cari'])) {
                                    $cari_oid = $conn->real_escape_string(filter($_GET['cari']));
                                    $cari_status = $conn->real_escape_string(filter($_GET['status']));
                                    $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                } else {
                                    $self = $_SERVER['PHP_SELF'];
                                }
                                $cek_depo = $conn->query($cek_depo);
                                $total_records = mysqli_num_rows($cek_depo);
                                echo "<li class='page-item disabled riwayat'><a href='#'>Ada ".$total_records." Transaksi</a></li>";
                                if($total_records > 0) {
                                    $total_pages = ceil($total_records/$records_per_page);
                                    $current_page = 1;
                                    if(isset($_GET["halaman"])) {
                                        $current_page = $conn->real_escape_string(filter($_GET["halaman"]));
                                        if ($current_page < 1) {
                                            $current_page = 1;
                                        }
                                    }
                                    if($current_page > 1) {
                                        $previous = $current_page-1;
                                    if (isset($_GET['cari'])) {
                                    $cari_id = $conn->real_escape_string(filter($_GET['cari']));
                                    $cari_status = $conn->real_escape_string(filter($_GET['status']));
                                    $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                        echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=1&tampil=".$cari_urut."&status=".$cari_status."&cari=".$cari_id."'><i class='fa fa-angle-double-left kt-font-brand'></i></a></li>";
                                        echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$previous."&tampil=".$cari_urut."&status=".$cari_status."&cari=".$cari_id."'><i class='fa fa-angle-left kt-font-brand'></i></a></li>";
                                } else {
                                        echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=1'><i class='fa fa-angle-double-left kt-font-brand'></i></a></li>";
                                        echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$previous."'><i class='fa fa-angle-left kt-font-brand'></i></a></li>";
                                }
                                }
                                    // limit page
                                    $limit_page = $current_page+3;
                                    $limit_show_link = $total_pages-$limit_page;
                                    if ($limit_show_link < 0) {
                                        $limit_show_link2 = $limit_show_link*2;
                                        $limit_link = $limit_show_link - $limit_show_link2;
                                        $limit_link = 3 - $limit_link;
                                    } else {
                                        $limit_link = 3;
                                    }
                                    $limit_page = $current_page+$limit_link;
                                    // end limit page
                                    // start page
                                    if ($current_page == 1) {
                                        $start_page = 1;
                                    } else if ($current_page > 1) {
                                        if ($current_page < 4) {
                                            $min_page  = $current_page-1;
                                        } else {
                                            $min_page  = 3;
                                        }
                                        $start_page = $current_page-$min_page;
                                    } else {
                                        $start_page = $current_page;
                                    }
                                    // end start page
                                    for($i=$start_page; $i<=$limit_page; $i++) {
                                    if (isset($_GET['cari'])) {
                                    $cari_id = $conn->real_escape_string(filter($_GET['cari']));
                                    $cari_status = $conn->real_escape_string(filter($_GET['status']));
                                    $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                        if($i==$current_page) {
                                            echo "<li class='kt-pagination__link--active'><a href='#'>".$i."</a></li>";
                                        } else {
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$i."&tampil=".$cari_urut."&status=".$cari_status."&cari=".$cari_id."'>".$i."</a></li>";
                                        }
                                    } else {
                                        if($i==$current_page) {
                                            echo "<li class='kt-pagination__link--active'><a href='#'>".$i."</a></li>";
                                        } else {
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$i."'>".$i."</a></li>";
                                        }        
                                    }
                                    }
                                    if($current_page!=$total_pages) {
                                        $next = $current_page+1;
                                    if (isset($_GET['cari'])) {
                                    $cari_id = $conn->real_escape_string(filter($_GET['cari']));
                                    $cari_status = $conn->real_escape_string(filter($_GET['status']));
                                    $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                        echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$next."&tampil=".$cari_urut."&status=".$cari_status."&cari=".$cari_id."'><i class='fa fa-angle-right kt-font-brand'></i></a></li>";
                                        echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$total_pages."&tampil=".$cari_urut."&status=".$cari_status."&cari=".$cari_id."'><i class='fa fa-angle-double-right kt-font-brand'></i></a></li>";
                                } else {
                                        echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$next."'><i class='fa fa-angle-right kt-font-brand'></i></a></li>";
                                        echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$total_pages."'><i class='fa fa-angle-double-right kt-font-brand'></i></a></li>";
                                    }
                                }
                                }
                                // end paging link
                                ?>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page History Top Up Balance -->

        </div>
        <!-- End Content -->

        <!-- Start Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
		    <i class="fa fa-arrow-up"></i>
		</div>
		<!-- End Scrolltop -->

        <!-- Start Modal History Top Up Balance -->
        <div class="modal fade" id="myDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		    <div class="modal-dialog" role="document">
			    <div class="modal-content">
				    <div class="modal-header">
                    <h4 class="modal-title mt-0" id="myModalLabel"><i class="flaticon-eye text-primary"></i> Detail Isi Saldo</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
				    </div>
				    <div class="modal-body" id="data_deposit">
				    </div>
				    <div class="modal-footer">
					    <button type="button" class="btn btn-primary" data-dismiss="modal">Tutup</button>
				    </div>
			    </div>
		    </div>
	    </div>
	    <!-- End Modal History Top Up Balance -->

<?php
require '../lib/footer.php';
?>

	    <script type="text/javascript">
	        $(document).ready(function(){
		        $('.view_deposit').click(function(){
		        	var id = $(this).attr("id");
			        $.ajax({
				        url: '<?php echo $config['web']['url']; ?>history/ajax/detail-deposit.php',
				        method: 'post',		
				        data: {id:id},	
				        success:function(data){	
					        $('#data_deposit').html(data);
					        $('#myDetail').modal("show");
				        }
			        });
		        });
	        });
	    </script>