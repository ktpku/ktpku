<?php
session_start();
require '../config.php';
require '../lib/session_user.php';
require '../lib/header.php';
?>

<style>
.table td, .table th {
    padding: 0 0 15px;
    border-top: none; 
}
.cardWrap {
    width: 100%;
    margin: auto;
    color: #fff;
    display: flex;
}
.cardWrap .cardz h2 {
    font-size: 16px;
}
.cardWrap .cardz {
  background: linear-gradient(to bottom, #0070C0 0%, #0070C0 45px, #f5f5f5 45px, #f5f5f5 100%);
  height: auto;
  position: relative;
  padding: 1em;
}
.cardWrap .cardLeft {
  border-top-left-radius: 8px;
  border-bottom-left-radius: 8px;
  width: 60%;
  white-space: normal;
}
.cardWrap .cardRight {
  width: 40%;
  border-left: .18em dashed #fff;
  border-top-right-radius: 8px;
  border-bottom-right-radius: 8px;
  white-space: normal;
}
.cardWrap .cardRight:before, .cardWrap .cardRight:after {
  content: "";
  position: absolute;
  display: block;
  width: .9em;
  height: .9em;
  background: #fff;
  border-radius: 50%;
  left: -.5em;
}
.cardWrap .cardRight:before {
  top: -.4em;
}
.cardWrap .cardRight:after {
  bottom: -.4em;
}
.cardWrap .title, .cardWrap .name, .cardWrap .seat, .cardWrap .time {
  text-transform: uppercase;
  font-weight: normal;
}
.cardWrap .title h2, .cardWrap .name h2, .cardWrap .seat h2, .cardWrap .time h2 {
  font-size: 14px;
  color: #525252;
  margin: 0;
}
.cardWrap .title span, .cardWrap .name span, .cardWrap .seat span, .cardWrap .time span {
  font-size: 10px;
  color: #a2aeae;
}
.cardWrap .title {
  margin: 2em 0 0 0;
}

.cardWrap .name, .cardWrap .seat {
  margin: .7em 0 0 0;
}
.cardWrap .number {
  text-align: center;
  text-transform: uppercase;
}
.cardWrap .number h3 {
  color: #e84c3d;
  margin: .9em 0 0 0;
  font-size: 2.5em;
}
.cardWrap .number span {
  display: block;
  color: #a2aeae;
}
li.page-item.disabled.riwayat {
    border: solid 1px;
    border-radius: .25rem;
    padding: 0 5px;
}
.kt-pagination.kt-pagination--brand .kt-pagination__links li:hover {
    background: #0070C0;
}
@media(max-width:767px){
    .cardWrap .cardz h2 {
        font-size: 12px;
    }
    .cardWrap .number h3 {
        font-size: 22px;
    }
    .cardRight .number h3 {
        font-size: 18px;
    }
}
</style>

        <!-- Start Content -->
        <div class="kt-container kt-grid__item kt-grid__item--fluid">

        <!-- Start Page History Balance Usage -->
        <div class="row mt-4">
	        <div class="col-lg-12">
		        <div class="kt-portlet">
			        <div class="kt-portlet__head">
				        <div class="kt-portlet__head-label">
					        <h3 class="kt-portlet__head-title">
					            <i class="flaticon2-time text-primary"></i>
					            Riwayat Pemakaian Saldo
					        </h3>
				        </div>
			        </div>
			        <div class="kt-portlet__body">                           
                        <!--<form class="form-horizontal" method="GET">-->
                        <!--    <div class="row">-->
                        <!--        <div class="form-group col-lg-4 d-none">-->
                        <!--            <label>Tampilkan Beberapa</label>-->
                        <!--            <select class="form-control" name="tampil">-->
                        <!--                <option value="10">Default</option>-->
                        <!--                <option value="20">10</option>-->
                        <!--                <option value="50">50</option>-->
                        <!--                <option value="100">100</option>-->
                        <!--            </select>-->
                        <!--        </div>                                                -->
                        <!--        <div class="form-group col-lg-4">-->
                        <!--            <label>Filter Tipe</label>-->
                        <!--            <select class="form-control" name="tipe">-->
                        <!--                <option value="">Semua</option>-->
                        <!--                <option value="Penambahan Saldo">Penambahan Saldo</option>-->
                        <!--                <option value="Pengurangan Saldo">Pengurangan Saldo</option>-->
                        <!--                <option value="Penambahan Koin">Penambahan Koin</option>-->
                        <!--                <option value="Pengurangan Koin">Pengurangan Koin</option>-->
                        <!--            </select>-->
                        <!--        </div>                                                -->
                        <!--        <div class="form-group col-lg-4 offset-lg-4">-->
                        <!--            <label>Submit</label>-->
                        <!--            <button type="submit" class="btn btn-block btn-primary">Cari</button>-->
                        <!--        </div>-->
                        <!--    </div>-->
                        <!--</form>-->
                        <div class="table-responsive">
                            <table class="table table-checkable" id="kt_table_1">
                                <tbody>
                                <?php 
                                // start paging config
                                if (isset($_GET['tipe'])) {
                                    $cari_tipe = $conn->real_escape_string(filter($_GET['tipe']));
                                
                                    $cek_data = "SELECT * FROM riwayat_saldo_koin WHERE aksi LIKE '%$cari_tipe%' AND username = '$sess_username' ORDER BY id DESC"; // edit
                                } else {
                                    $cek_data = "SELECT * FROM riwayat_saldo_koin WHERE username = '$sess_username' ORDER BY id DESC"; // edit
                                }
                                if (isset($_GET['tipe'])) {
                                $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                $records_per_page = $cari_urut; // edit
                                } else {
                                    $records_per_page = 10; // edit
                                }
                                
                                $starting_position = 0;
                                if(isset($_GET["halaman"])) {
                                    $starting_position = ($conn->real_escape_string(filter($_GET["halaman"]))-1) * $records_per_page;
                                }
                                $new_query = $cek_data." LIMIT $starting_position, $records_per_page";
                                $new_query = $conn->query($new_query);
                                $no = $starting_position+1;
                                // end paging config
                                while ($view_data = $new_query->fetch_assoc()) {
                                    if ($view_data['tipe'] == "Saldo") {
                                        $label = "success";
                                        $icon = "la la-credit-card";
                                    } else if ($view_data['tipe'] == "Koin") {
                                        $label = "primary";
                                        $icon = "flaticon-coins";
                                    }
                                    if ($view_data['aksi'] == "Penambahan Saldo") {
                                        $label = "primary";
                                    } else if ($view_data['aksi'] == "Pengurangan Saldo") {
                                        $label = "danger";
                                    }
                                    if ($view_data['aksi'] == "Penambahan Koin") {
                                        $label = "primary";
                                    } else if ($view_data['aksi'] == "Pengurangan Koin") {
                                        $label = "danger";
                                    }
                                ?>
                                <tr>
                                <th scope="row">
								        
							        <div class="cardWrap">
							            
                                      <div class="cardz cardLeft">
                                        <h2 class="mb-5"><?php echo $view_data['aksi']; ?></h2>
                                        <div class="name">
                                          <h2><?php echo tanggal_indo($view_data['date']); ?></h2>
                                          <span>Tanggal</span>
                                        </div>
                                        <div class="name">
                                          <h2><?php echo $view_data['pesan']; ?></h2>
                                          <span>Keterangan</span>
                                        </div>
                                        <div class="name">
                                          <h2>Rp <?php echo number_format($view_data['nominal'],0,',','.'); ?></h2>
                                          <span>Jumlah</span>
                                        </div>
                                        
                                      </div>
                                      <div class="cardz cardRight">
                                        <h2 class="mb-5 text-center"><i class="<?php echo $icon; ?>"></i></h2>
                                        <div class="number">
                                          <h3>#<?php echo $view_data['id_transaksi']; ?></h3>
                                          <span class="text-muted">No. Transaksi</span>
                                          <hr>
                                          <span class="text-muted"><i class="fas fa-clock"></i> <?php echo $view_data['time']; ?></span>
                                          <hr>
                                          <span class="btn btn-<?php echo $label; ?> btn-elevate btn-pill btn-elevate-air btn-sm text-white"><?php echo $view_data['aksi']; ?></span>
                                        </div>
                                      </div>
                                    
                                    </div>
                                    
							    </th>
							    </tr>
	 
<?php } ?>
                                </tbody>
                            </table>
                            <br>
                            <div class="kt-pagination kt-pagination--brand kt-pagination--circle">
                                <ul class="kt-pagination__links">
                                    <?php
                                    // start paging link
                                    if (isset($_GET['tipe'])) {
                                    $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                    } else {
                                    $cari_urut =  10;
                                    }  
                                    if (isset($_GET['tipe'])) {
                                        $cari_tipe = $conn->real_escape_string(filter($_GET['tipe']));
                                        $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                        $self = $_SERVER['PHP_SELF'];
                                    } else {
                                        $self = $_SERVER['PHP_SELF'];
                                    }
                                    $cek_data = $conn->query($cek_data);
                                    $total_records = mysqli_num_rows($cek_data);
                                    echo "<li class='disabled page-item riwayat'><a href='#'>Ada ".$total_records." Transaksi</a></li>";
                                    if($total_records > 0) {
                                        $total_pages = ceil($total_records/$records_per_page);
                                        $current_page = 1;
                                        if(isset($_GET["halaman"])) {
                                            $current_page = $conn->real_escape_string(filter($_GET["halaman"]));
                                            if ($current_page < 1) {
                                                $current_page = 1;
                                            }
                                        }
                                        if($current_page > 1) {
                                            $previous = $current_page-1;
                                        if (isset($_GET['tipe'])) {
                                        $cari_tipe = $conn->real_escape_string(filter($_GET['tipe']));
                                        $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=1&tampil=".$cari_urut."&tipe=".$cari_tipe."'><i class='fa fa-angle-double-left kt-font-brand'></i></a></li>";
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$previous."&tampil=".$cari_urut."&tipe=".$cari_tipe."'><i class='fa fa-angle-left kt-font-brand'></i></a></li>";
                                    } else {
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=1'><i class='fa fa-angle-double-left kt-font-brand'></i></a></li>";
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$previous."'><i class='fa fa-angle-left kt-font-brand'></i></a></li>";
                                    }
                                    }
                                        // limit page
                                        $limit_page = $current_page+3;
                                        $limit_show_link = $total_pages-$limit_page;
                                        if ($limit_show_link < 0) {
                                            $limit_show_link2 = $limit_show_link*2;
                                            $limit_link = $limit_show_link - $limit_show_link2;
                                            $limit_link = 3 - $limit_link;
                                        } else {
                                            $limit_link = 3;
                                        }
                                        $limit_page = $current_page+$limit_link;
                                        // end limit page
                                        // start page
                                        if ($current_page == 1) {
                                            $start_page = 1;
                                        } else if ($current_page > 1) {
                                            if ($current_page < 4) {
                                                $min_page  = $current_page-1;
                                            } else {
                                                $min_page  = 3;
                                            }
                                            $start_page = $current_page-$min_page;
                                        } else {
                                            $start_page = $current_page;
                                        }
                                        // end start page
                                        for($i=$start_page; $i<=$limit_page; $i++) {
                                        if (isset($_GET['tipe'])) {
                                        $cari_tipe = $conn->real_escape_string(filter($_GET['tipe']));
                                        $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                            if($i==$current_page) {
                                                echo "<li class='kt-pagination__link--active'><a href='#'>".$i."</a></li>";
                                            } else {
                                                echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$i."&tampil=".$cari_urut."&tipe=".$cari_tipe."'>".$i."</a></li>";
                                            }
                                        } else {
                                            if($i==$current_page) {
                                                echo "<li class='kt-pagination__link--active'><a href='#'>".$i."</a></li>";
                                            } else {
                                                echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$i."'>".$i."</a></li>";
                                            }        
                                        }
                                        }
                                        if($current_page!=$total_pages) {
                                            $next = $current_page+1;
                                        if (isset($_GET['tipe'])) {
                                        $cari_tipe = $conn->real_escape_string(filter($_GET['tipe']));
                                        $cari_urut = $conn->real_escape_string(filter($_GET['tampil']));
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$next."&tampil=".$cari_urut."&tipe=".$cari_tipe."'><i class='fa fa-angle-right kt-font-brand'></i></a></li>";
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$total_pages."&tampil=".$cari_urut."&tipe=".$cari_tipe."'><i class='fa fa-angle-double-right kt-font-brand'></i></a></li>";
                                    } else {
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$next."'><i class='fa fa-angle-right kt-font-brand'></i></a></li>";
                                            echo "<li class='kt-pagination__link--first'><a href='".$self."?halaman=".$total_pages."'><i class='fa fa-angle-double-right kt-font-brand'></i></a></li>";
                                        }
                                    }
                                    }
                                    // end paging link
                                    ?>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page History Balance Usage -->

        </div>
        <!-- End Content -->
<?php
require '../lib/footer.php';
?>