<?php 
session_start();
require("config.php");

    if (isset($_SESSION['user'])) {
        $sess_username = $_SESSION['user']['username'];
        $check_user = $conn->query("SELECT * FROM users WHERE username = '$sess_username'");
        $data_user = $check_user->fetch_assoc();
        $check_username = $check_user->num_rows;
        if ($check_username == 0) {
	        header("Location: ".$config['web']['url']."logout.php");
        } else if ($data_user['status'] == "Tidak Aktif") {
	        header("Location: ".$config['web']['url']."logout.php");
        }

        } else {
	        $_SESSION['user'] = $data_user;
	        header("Location: ".$config['web']['url']."dashboard");
        }

include("lib/header.php");
if (isset($_SESSION['user'])) {
?>


		<!-- Start Card Saldo -->
		<div class="kt-container mt-4">
		    <div class="row">
		        <div class="col-lg-12">
		            <div class="kt-portlet kt-portlet--height-fluid" style="background: #fff;box-shadow: 0 0 9px rgba(82,63,105,.15);">
                        <div class="kt-portlet__body">
	                        <div class="row align-items-center">
		                        <div class="col-12 mb-3">
			                        <a href="<?php echo $config['web']['url'] ?>history/deposit" class="btn btn-primary btn-md w-100"><i class="fa fa-plus-square fa-2x"></i><br> <small>Riwayat Isi Saldo</small></a>
		                        </div>
		                        <div class="col-12 mb-3">
			                        <a href="<?php echo $config['web']['url'] ?>history/order" class="btn btn-primary btn-md w-100"><i class="fas fa-cart-arrow-down fa-2x"></i><br> <small>Riwayat Pembelian</small></a>
		                        </div>
		                        <div class="col-12">
			                        <a href="<?php echo $config['web']['url'] ?>history/riwayat-pemakaian" class="btn btn-primary btn-md w-100"><i class="fas fa-history fa-2x"></i><br> <small>Riwayat Pemakaian Saldo</small></a>
		                        </div>
	                        </div>
                        </div>
		            </div>
		        </div>
		    </div>
		</div>
		<!-- End Card Poin -->


        <br />


<?php 
}
require 'lib/footer.php';
?>