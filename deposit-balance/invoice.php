 <?php
session_start();
require '../config.php';
require '../lib/session_user.php';
require '../lib/header.php';

		if (isset($_GET['kode_deposit'])) {
			$post_kode = abs($_GET['kode_deposit']);
			$cek_deposit = $conn->query("SELECT * FROM deposit WHERE kode_deposit = '$post_kode' AND username = '$sess_username'");
			$data_deposit = mysqli_fetch_assoc($cek_deposit);

			if ($data_deposit['status'] == "Pending") {
	            $label = "warning";
			} else if($data_deposit['status'] == "Error") {
	            $label = "danger";
			} else if($data_deposit['status'] == "Success") {
	            $label = "success";
			}

			if ($cek_deposit->num_rows == 0) {
	            header("Location: ".$config['web']['url']."history/deposit");
			} else {

?>


        <!-- Start Content -->
        <div class="kt-container kt-grid__item kt-grid__item--fluid">

        <!-- Start Page Invoice Top Up Balance -->
		<div class="kt-portlet mt-4">
            <div class="kt-portlet__body kt-portlet__body--fit">
                <div class="kt-invoice-1">
                    <div class="kt-invoice__head" style="background-image: linear-gradient(45deg, #fbfbfb, #fff);">
                        <div class="kt-invoice__container">
                            <div class="kt-invoice__brand">
						        <h1 class="kt-invoice__title">INVOICE</h1>
                                <div href="#" class="kt-invoice__logo">
							        <a href="#"><img src="<?php echo $config['web']['url'] ?>assets/media/logos/logo_color.png" class="img-logo-invoice"></a>
							        <span class="kt-invoice__desc">
								        <span><?php echo $data['lokasi']; ?> - <?php echo $data['kode_pos']; ?></span>
							        </span>
						        </div>
					        </div>
                            <div class="kt-invoice__items">
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">TANGGAL & WAKTU</span>
                                    <span class="kt-invoice__text"><?php echo tanggal_indo($data_deposit['date']); ?>, <?php echo $data_deposit['time']; ?></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">KODE INVOICE</span>
                                    <span class="kt-invoice__text"><?php echo $data_deposit['kode_deposit']; ?></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">STATUS</span>
                                    <span class="kt-invoice__text"><span class="btn btn-<?php echo $label; ?> btn-elevate btn-pill btn-elevate-air btn-sm"><?php echo $data_deposit['status']; ?></span></span>
                                </div>
                            </div>
                            
                            <div class="kt-invoice__items">
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">TIPE BANK</span>
                                    <span class="kt-invoice__text"><?php echo $data_deposit['provider']; ?></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">PENERIMA</span>
                                    <span class="kt-invoice__text"><?php echo $data_deposit['penerima']; ?></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">KETERANGAN</span>
                                    <span class="kt-invoice__text"><?php echo $data_deposit['catatan']; ?></span>
                                </div>
                            </div>
                            <div class="kt-invoice__items">
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">SALDO YANG DIDAPATKAN</span>
                                    <span class="kt-invoice__text">Rp <?php echo number_format($data_deposit['get_saldo'],0,',','.'); ?></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle">JUMLAH PEMBAYARAN</span>
                                    <span class="kt-invoice__text">Rp <?php echo number_format($data_deposit['jumlah_transfer'],0,',','.'); ?></span>
                                </div>
                                <div class="kt-invoice__item">
                                    <span class="kt-invoice__subtitle"></span>
                                    <span class="kt-invoice__text"></span>
                                </div>
                            </div>
                            
                        </div>
                    </div>
			        <div class="kt-invoice__actions">
                        <div class="kt-invoice__container">
                            <button type="button" class="btn btn-label-brand btn-bold" onclick="window.print();"><i class="fa fa-print"></i> Print Invoice</button>
                        <form class="form-horizontal" role="form" method="POST" action="<?php echo $config['web']['url'] ?>history/deposit">
                    <?php if($data_deposit['status'] !== "Success" AND $data_deposit['status'] !== "Error") { ?>
                            <input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
                            <button type="submit" class="btn btn-danger btn-bold" name="kode_deposit" value="<?php echo $data_deposit['kode_deposit']; ?>">Batalkan <i class="fa fa-times"></i></button>
                            <button type="submit" class="btn btn-brand btn-bold" name="confirm" value="<?php echo $data_deposit['kode_deposit']; ?>">Konfirmasi <i class="fa fa-check"></i></button>
                        </form>
                        </div>
                    </div>
			        <?php } ?>
                </div>
            </div>
        </div>

		</div>
        <!-- End Content -->

        <!-- Start Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
		    <i class="fa fa-arrow-up"></i>
		</div>
		<!-- End Scrolltop -->

<?php 
			    
require '../lib/footer.php';
			    
}
?>

<?php 
} else {
header("Location: ".$config['web']['url']."history/deposit");
}
?>