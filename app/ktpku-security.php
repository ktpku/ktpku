<?php
session_start();
require '../config.php';
require '../lib/session_user.php';

	    if (isset($_POST['kirim'])) {
			require '../lib/session_login.php';
			$post_subjek = $conn->real_escape_string(trim(filter($_POST['subjek'])));
			$post_pesan = $conn->real_escape_string(trim(filter($_POST['pesan'])));

	        $error = array();
	        if (empty($post_subjek)) {
			    $error ['subjek'] = '*Tidak Boleh Kosong.';
	        } else if (strlen($post_subjek) > 200) {
			    $error ['subjek'] = '*Maksimal Pengisian Subjek Adalah 200 Karakter.';
	        }
	        if (empty($post_pesan)) {
			    $error ['pesan'] = '*Tidak Boleh Kosong.';
	        } else if (strlen($post_pesan) > 500) {
			    $error ['pesan'] = '*Maksimal Pengisian Pesan Adalah 500 Karakter.';
	        } else {

				$insert_tiket = $conn->query("INSERT INTO tiket VALUES ('', '$sess_username', '$post_subjek', '$post_pesan', '$date', '$time', '$date $time', 'Pending','1','0')");
				if ($insert_tiket == TRUE) {
					$_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Sip! Tiketmu Berhasil Dibuat, Harap Menunggu Respon Dari Admin Ya.<script>swal("Berhasil!", "Tiketmu Berhasil Dibuat Nih.", "success");</script>');
				} else {
					$_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
				}

			}
		}

        // Get Data Haveibeenpwned
        // =======
        $url = "https://haveibeenpwned.com/api/v3/breachedaccount/".$_GET['email']."?truncateResponse=false";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $headers = array(
           "hibp-api-key: 1583ad72ac504f67b120431164a35017",
           "Access-Control-Allow-Origin: *",
           "user-agent: KTPKU"
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        //for debug only!
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $getdatabranch = curl_exec($curl);
        curl_close($curl);
        // =======
        // Get Data Haveibeenpwned

		require("../lib/header.php");

?>

        <style>
        .box-show-desc {
            color: #000;
            border: solid 1px #ebedf2;
            background: #fff;
            padding: 15px;
            margin-bottom: 20px;
            box-shadow: 0 0 1px 1px #607d8b08, 1px 1px 10px #4341571a;
            transition: all .15s ease-in-out;
            border-radius: 8px;
        }
        .cardWrap {
            width: 100%;
            margin: auto;
            margin-bottom: 15px;
            color: #fff;
            display: flex;
        }
        .cardWrap .cardz h2 {
            font-size: 16px;
        }
        .cardWrap .cardz {
          background: linear-gradient(to bottom, #dc3545 0%, #dc3545 45px, #333 45px, #333 100%);
          height: auto;
          position: relative;
          padding: 1em;
        }
        .cardWrap .cardLeft {
          border-top-left-radius: 8px;
          border-bottom-left-radius: 8px;
          width: 60%;
          white-space: normal;
        }
        .cardWrap .cardRight {
          width: 40%;
          border-left: .18em dashed #fff;
          border-top-right-radius: 8px;
          border-bottom-right-radius: 8px;
          white-space: normal;
        }
        .cardWrap .cardRight:before, .cardWrap .cardRight:after {
          content: "";
          position: absolute;
          display: block;
          width: .9em;
          height: .9em;
          background: #fff;
          border-radius: 50%;
          left: -.5em;
        }
        .cardWrap .cardRight:before {
          top: -.4em;
        }
        .cardWrap .cardRight:after {
          bottom: -.4em;
        }
        .cardWrap .title, .cardWrap .name, .cardWrap .seat, .cardWrap .time {
          text-transform: uppercase;
          font-weight: normal;
        }
        .cardWrap .title h2, .cardWrap .name h2, .cardWrap .seat h2, .cardWrap .time h2 {
          font-size: 14px;
          color: #fff;
          margin: 0;
        }
        .cardWrap .title span, .cardWrap .name span, .cardWrap .seat span, .cardWrap .time span {
          font-size: 12px;
          color: #fff;
        }
        .cardWrap .title {
          margin: 2em 0 0 0;
        }
        
        .cardWrap .name, .cardWrap .seat {
          margin: 0 0 8px 0;
        }
        .cardWrap .number {
          text-align: center;
          text-transform: uppercase;
        }
        .cardWrap .number h3 {
          color: #e84c3d;
          margin: .9em 0 0 0;
          font-size: 2.5em;
        }
        .cardWrap .number span {
          display: block;
          color: #a2aeae;
        }
        li.page-item.disabled.riwayat {
            border: solid 1px;
            border-radius: .25rem;
            padding: 0 5px;
        }
        .kt-pagination.kt-pagination--brand .kt-pagination__links li:hover {
            background: #0070C0;
        }
        .cardWrap hr {
            border-color: #fff;
        }
        @media(max-width:767px){
            .cardWrap .cardz h2 {
                font-size: 12px;
            }
            .cardWrap .number h3 {
                font-size: 22px;
            }
        }
        </style>

        <!-- Start Content -->
        <div class="kt-container kt-grid__item kt-grid__item--fluid">

        <!-- Start Page -->
        <div class="row mt-4">
	        <div class="col-lg-12">
		        <div class="kt-portlet">
			        <div class="kt-portlet__head">
				        <div class="kt-portlet__head-label">
					        <h3 class="kt-portlet__head-title">
					            <i class="flaticon2-protection text-primary"></i>
					            KTPKU Security
					        </h3>
				        </div>
			        </div>
			        <div class="kt-portlet__body">
                        <div class="row" id="data-wrapper-security"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page -->

        </div>
        <!-- End Content -->

        <!-- Start Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
		    <i class="fa fa-arrow-up"></i>
		</div>
		<!-- End Scrolltop -->

<?php 
require '../lib/footer.php';
?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/json2html/1.4.0/json2html.min.js"></script>
<script>
let template = {
    '<>':'div class="col-12 col-md-6"',
    'html':'<div class="cardWrap"><div class="cardz cardLeft"> <h2 class="mb-5">${Name}</h2> <div class="name"> <span>Data yang bocor :</span><h2>${DataClasses}</h2> </div> <div class="name"> <span>Perkiraan Data Yang Bocor</span><h2>${PwnCount}</h2> </div> <div class="name"> <span>Tanggal Kejadian :</span><h2>${AddedDate}</h2> </div> <div class="name"> <span>Tanggal Kejadian Terbaru :</span><h2>${AddedDate}</h2> </div> </div> <div class="cardz cardRight"> <h2 class="mb-5 text-center">Akun Bocor!</h2> <div class="number"> <h3><img src="${LogoPath}" style=" background: #333; padding: 10px;height: 64px;width: 100%; object-fit: contain; "/></h3><hr> <a href="#" class="btn btn-primary btn-elevate btn-pill btn-elevate-air btn-sm w-100 text-white">Detail</a> </div> </div> </div>'
};
// <div class="box-show-desc">  <br>  <br> ${Domain} <br> ${Description} </div>
let data = <?php echo $getdatabranch ?>;
//native javascript
// document.write( json2html.transform(data,template) );
//or with jQuery
$("#data-wrapper-security").json2html(data,template);
console.log(<?php echo $getdatabranch ?>);
</script>
