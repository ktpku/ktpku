<?php
session_start();
require("../config.php");
require("../lib/class.phpmailer.php");
$tipe = "Daftar";

        function dapetin($url) {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_VERBOSE, 1);
                $data = curl_exec($ch);
                curl_close($ch);
                return json_decode($data, true);
        }
        
        if (isset($_SESSION['user'])) {

            header("Location: ".$config['web']['url']);
            
        } else {
            
        if (isset($_POST['daftar'])) {
            // $nama_depan = $conn->real_escape_string(trim(filter($_POST['nama_depan'])));
            // $nama_belakang = $conn->real_escape_string(trim(filter($_POST['nama_belakang'])));
            $email = $conn->real_escape_string(trim(filter($_POST['email'])));
            $username = $conn->real_escape_string(trim(filter($_POST['username'])));
            $no_hp = $conn->real_escape_string(trim(filter( '628'.$_POST['no_hp'])));
            $password = $conn->real_escape_string(trim(filter($_POST['password'])));
            $password2 = $conn->real_escape_string(trim(filter($_POST['password2'])));
            $pin = $conn->real_escape_string(trim(filter($_POST['pin'])));
            $kode_referral = $conn->real_escape_string(trim(filter($_POST['kode_referral'])));

            $secret_key = '6LdzEt8ZAAAAAHttjnBd2jAYJzzPcQw1D3tWzAzG'; //masukkan secret key-nya berdasarkan secret key masing-masing saat create api key nya
            $captcha = $_POST['g-recaptcha-response'];
            $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secret_key) . '&response=' . $captcha;
            $recaptcha = dapetin($url);

            $cek_email = $conn->query("SELECT * FROM users WHERE email = '$email'");
            $cek_email_ulang = mysqli_num_rows($cek_email);
            $data_email = mysqli_fetch_assoc($cek_email);

            $cek_pengguna = $conn->query("SELECT * FROM users WHERE username = '$username'");
            $cek_pengguna_ulang = mysqli_num_rows($cek_pengguna);
            $data_pengguna = mysqli_fetch_assoc($cek_pengguna);

            $cek_no_hp = $conn->query("SELECT * FROM users WHERE no_hp = '$no_hp'");
            $cek_no_hp_ulang = mysqli_num_rows($cek_no_hp);
            $data_no_hp = mysqli_fetch_assoc($cek_no_hp);

            $cek_kode = $conn->query("SELECT * FROM users WHERE kode_referral = '$kode_referral'");
            $cek_kode_ulang = mysqli_num_rows($cek_kode);
            $data_kode = mysqli_fetch_assoc($cek_kode);

            $pengguna = $data_kode['username'];
            $kode_ref = acak(3).acak_nomor(4);
            
            $kode_verifikasi = acak_nomor(6);

            $error = array();
            //if (empty($nama_depan)) {
    		    //$error ['nama_depan'] = '*Tidak Boleh Kosong';
            //} 
            //else if (empty($nama_belakang)) {
    		    //$error ['nama_belakang'] = '*Tidak Boleh Kosong';
            //}
            if (empty($email)) {
    		    $error ['email'] = '*Tidak Boleh Kosong';
            } else if ($cek_email->num_rows > 0) {
    		    $error ['email'] = '*Email Sudah Terdaftar';
            }
            else if (empty($username)) {
    		    $error ['username'] = '*Tidak Boleh Kosong';
            } else if (strlen($username) < 16) {
    		    $error ['username'] = '*NIK eKTP Harus 16 Digit';
            } else if ($cek_pengguna->num_rows > 0) {
    		    $error ['username'] = '*NIK eKTP Sudah Terdaftar';
            }
            else if (empty($no_hp)) {
    		    $error ['no_hp'] = '*Tidak Boleh Kosong';
            } else if (!preg_match("/628/",$no_hp)) {
    		    $error ['no_hp'] = '*Format Nomor HP Harus 628';
            } else if ($cek_no_hp->num_rows > 0) {
    		    $error ['no_hp'] = '*Nomor HP Sudah Terdaftar';
            }
            else if (empty($password)) {
    		    $error ['password'] = '*Tidak Boleh Kosong';
            } else if (strlen($password) < 6) {
    		    $error ['password'] = '*Minimal 6 Karakter';
            }
            else if (empty($password2)) {
    		    $error ['password2'] = '*Tidak Boleh Kosong';
            } else if ($password <> $password2) {
    		    $error ['password2'] = '*Ulangi Kata Sandi Tidak Sesuai';
            }
            else if (empty($pin)) {
    		    $error ['pin'] = '*Tidak Boleh Kosong.';
            } else if (strlen($pin) <> 6 ){
    		    $error ['pin'] = '*PIN Harus 6 Digit.';
            }
            else if (empty($pin)) {
    		    $error ['pin'] = '*Tidak Boleh Kosong.';
            } else if (strlen($pin) <> 6 ){
    		    $error ['pin'] = '*PIN Harus 6 Digit.';
            }
            else if ($recaptcha['success'] == false) {
    		    $error ['kode'] = '*Mohon Mengisi Captcha.';
            } else {
                
	        if (mysqli_num_rows($cek_kode) == 0) {
	            
	                $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Kode Referral Tidak Ditemukan, kode default.<script>swal("Gagal!", "Kode Referral Tidak Ditemukan, kode default (ktpku).", "error");</script>');
	            
	        } else {

                    $hash_password = password_hash($password, PASSWORD_DEFAULT);
                    $api_key =  acak(20);
                    
                    //Kirim Email
                    $mail = new PHPMailer;
        	        $mail->IsSMTP();
        	        $mail->SMTPSecure = 'tls'; 
                    $mail->Host = "ktpku.com"; //host masing2 provider email
                    $mail->SMTPDebug = 2;
                    $mail->Port = 587;
                    $mail->SMTPAuth = true;
                    $mail->Username = "admin@ktpku.com"; //user email
                    $mail->Password = "qazwsx123098"; //password email 
                    $mail->SetFrom("admin@ktpku.com","KTPKu"); //set email pengirim
        	        $mail->Subject = "Verifikasi Akun Anda"; //subyek email
        	        $mail->AddAddress("$email");  //tujuan email
                    $mail->MsgHTML("Berikut Kode Verifikasi Anda : <br><h2>$kode_verifikasi</h2><hr><br>Berikut data login untuk masuk ke akun anda :<br><br><b>NIK-EKTP : $username<br>Kata Sandi : $password<br>PIN : $pin</b><br><br><small>PENTING! Pastikan data diatas hanya anda sendiri yang tahu. Tidak ada orang lain yang boleh tahu termasuk dari pihak KTPKu.</small><br><br>Terima Kasih.<br>Admin KTPKu<br>admin@ktpku.com");
                    if ($mail->Send());
                    
                    if ($conn->query("INSERT INTO users VALUES ('', '', '', '', '$email', '$username' , '$hash_password', '0', '0', '0', 'Member', 'Aktif', 'Belum Verifikasi', '$pin', '$api_key', 'Pendaftaran Gratis', '$pengguna', '$date', '$time', '0', '$no_hp', '$kode_verifikasi', '$kode_ref', '', '0', '')") == true) {
                        $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Pendaftaran berhasil, cek email anda untuk melihat kode verifikasi yang kami kirim.<script>swal("Berhasil!", "Akun Anda Berhasil Di Daftarkan. Verifikasi akun anda sekarang juga.", "success");</script>');
                        exit(header("Location: ".$config['web']['url']."auth/verification-account"));
                    } else {
                        $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
                    }
                        
	            }
            }
        }
        
        }
        
        require '../lib/header_home.php';

?>

	<style>
	.keyboard { position: fixed; left: 0; z-index: 9999;bottom: 0; width: 100%; padding: 5px 0; background: #0070C0; box-shadow: 0 0 50px rgba(0, 0, 0, 0.5); user-select: none; transition: bottom 0.4s;} .keyboard--hidden { bottom: -100%; } .keyboard__keys { text-align: center; } .keyboard__key { height: 45px; width: 6%; max-width: 90px; margin: 3px; border-radius: 4px; border: none; background: rgba(255, 255, 255, 0.2); color: #ffffff; font-size: 1.05rem; outline: none; cursor: pointer; display: inline-flex; align-items: center; justify-content: center; vertical-align: top; padding: 0; -webkit-tap-highlight-color: transparent; position: relative; } .keyboard__key:active { background: rgba(255, 255, 255, 0.12); } .keyboard__key--wide { width: 12%; } .keyboard__key--extra-wide { width: 36%; max-width: 500px; } .keyboard__key--activatable::after { content: ''; top: 10px; right: 10px; position: absolute; width: 8px; height: 8px; background: rgba(0, 0, 0, 0.4); border-radius: 50%; } .keyboard__key--active::after { background: #08ff00; } .keyboard__key--dark { background: rgba(0, 0, 0, 0.25); }
	input.invalid { border: solid 1px #ff0000!important; } .tab { display: none; } #prevBtn { cursor: pointer; } /* Make circles that indicate the steps of the form: */ .step { height: 15px; width: 15px; margin: 0 2px; background-color: #bbbbbb; border: none; border-radius: 50%; display: inline-block; opacity: 0.5; } .step.active { opacity: 1; } /* Mark the steps that are finished and valid: */ .step.finish { background-color: #4CAF50; }
	</style>
	
        <!-- Start Page Register -->
        <div class="login-2">
            <div class="container">
                <div class="row">
                
                    <div class="col-lg-12">
                    	<br>
                    	<a href="<?php echo $config['web']['url'] ?>" class="forgot-password-1">
                        	<img alt="Logo" src="<?php echo $config['web']['url'] ?>assets/media/logos/logo_color.png" />			
                        </a>
                        <div class="form-section">
                            <h3>Daftar Akun</h3>
                            <?php
                            if (isset($_SESSION['hasil'])) {
                            ?>
                            <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible" role="alert">
                                <?php echo $_SESSION['hasil']['pesan'] ?>
                            </div>
                            <?php
                            unset($_SESSION['hasil']);
                            }
                            ?>
                            <div class="login-inner-form">
                                <form class="form-horizontal" role="form" method="POST" id="regForm">
                                	 
				                <div class="tab">
                                    <input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
                                    <div class="row">
                                        <div class="form-group form-box col-md-12 col-12">
	                                        <input type="text" class="input-text numeric-16" oninput="this.className = 'input-text numeric-16'" placeholder="NIK E-KTP (16 Digit)" name="username" value="<?php echo $username; ?>">
	                                        <i class="fa fa-id-card-o mt-1 mr-3"></i>
                                        	<small class="text-danger font-13 pull-right"><?php echo ($error['username']) ? $error['username'] : '';?></small>
                                    	</div>
                                    	<!--<div class="form-group form-box col-md-2 col-3 pl-0">
	                                       <a href="#" class="scan-ktp-button"><span class="fa fa-barcode fa-lg"></span></a>
                                    	</div>-->
                                    </div>
                              
                                    <!--<div class="row">-->
                                    <!--    <div class="form-group form-box col-md-12 col-12">-->
                                    <!--        <input type="text" class="input-text" oninput="this.className = 'input-text'" placeholder="Nama Lengkap" name="nama_depan" value="<?php echo $nama_depan; ?>">-->
                                    <!--        <small class="text-danger font-13 pull-right"><?php echo ($error['nama_depan']) ? $error['nama_depan'] : '';?></small>-->
                                    <!--    </div>-->
                                    <!--</div>-->
                                </div>
                                <div class="tab">
                                    <div class="row">
                                        <div class="form-group form-box col-md-12 col-12">
                                            <input type="email" class="input-text" oninput="this.className = 'input-text'" placeholder="Email Aktif" name="email" value="<?php echo $email; ?>">
                                            <i class="fa fa-envelope-o mt-1 mr-3"></i>
                                            <small class="text-danger font-13 pull-right"><?php echo ($error['email']) ? $error['email'] : '';?></small>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group form-box col-md-12 col-12">
                                            <span class="icon-sub-login">08</span>
                                            <input type="number" class="icon-sub-login-2 input-text" oninput="this.className = 'icon-sub-login-2 input-text'" placeholder="123456789 (No. Hp / Wa)" name="no_hp">
                                            <i class="fa fa-whatsapp mt-1 mr-3"></i>
                                            <small class="text-danger font-13 pull-right"><?php echo ($error['no_hp']) ? $error['no_hp'] : '';?></small>
                                        </div>
                                    </div>
                                    <div class="form-group form-box">
                                        <input id="show-kata-sandi-3" type="number" class="input-text numeric-6" oninput="this.className = 'input-text use-keyboard-input numeric-6'" placeholder="PIN Transaksi (6 Digit Angka)" name="pin" value="<?php echo $pin; ?>">
                                        <i class="fa fa-key mt-1"></i>
                                        <small class="text-danger font-13 pull-right"><?php echo ($error['pin']) ? $error['pin'] : '';?></small>
                                    </div>
                                    <div class="form-group form-box">
                                        <input id="show-kata-sandi-1" type="password" class="input-text" oninput="this.className = 'input-text'" placeholder="Kata Sandi" name="password">
                                        <i class="fa fa-eye mt-1" id="showkatasandisatu" onclick="showkatasandisatu()" style=" cursor: pointer; "></i>
                                        <small class="text-danger font-13 pull-right"><?php echo ($error['password']) ? $error['password'] : '';?></small>
                                    </div>
                                    <div class="form-group form-box">
                                        <input id="show-kata-sandi-2" type="password" class="input-text" oninput="this.className = 'input-text'" placeholder="Ulangi Kata Sandi" name="password2">
                                        <i class="fa fa-eye mt-1" id="showkatasandidua" onclick="showkatasandidua()" style=" cursor: pointer; "></i>
                                        <small class="text-danger font-13 pull-right"><?php echo ($error['password2']) ? $error['password2'] : '';?></small>
                                    </div>
                                    
                                    
                                    <input type="hidden" name="kode_referral" value="ktpku">
                                    <!-- <div class="row">
                                        <div class="form-group form-box col-md-12 col-12">
                                             <input type="text" class="input-text" placeholder="Kode Referral (Boleh Tidak Diisi)" name="kode_referral" value="<?php echo $kode_referral; ?>">
                                            <small class="text-danger font-13 pull-right"><?php echo ($error['kode_referral']) ? $error['kode_referral'] : '';?></small>
                                        </div> 
                                    </div>-->
                                    
                				    <div class="form-group form-box">
                                        <div class="g-recaptcha" data-sitekey="6LdzEt8ZAAAAAOrnT9_8xJ0hnGOSBKAbzVwlcAkR"></div>
                				    </div>
                                    <div class="checkbox clearfix">
                                        <div class="form-check checkbox-theme">
                                            <input class="form-check-input" type="checkbox" value="" id="rememberMe" checked>
                                            <label class="form-check-label" for="rememberMe">
                                                Saya Setuju Dengan <a href="<?php echo $config['web']['url'] ?>dashboard/ketentuan-layanan" target="_blank">Syarat & Ketentuan</a>
                                            </label>
                                        </div>
                                    </div>
                              </div>
                                    <div class="form-group mb-0">
	                                  <div style="overflow:auto;">
                					    <div class="d-flex align-content-stretch">
                					      <button type="button" class="btn btn-default w-50" id="prevBtn" onclick="nextPrev(-1)" style="border-radius:20px 0 0 20px;"><i class="fa fa-arrow-left"></i> Kembali</button>
                					      <button type="button" class="btn btn-primary w-100" id="nextBtn" onclick="nextPrev(1)" name="daftar">Lanjut <i class="fa fa-arrow-right"></i></button>
                					    </div>
                					  </div>
                					  <div style="text-align:center;margin-top:40px;display: none;">
                					    <span class="step"></span>
                					    <span class="step"></span>
                					    <span class="step"></span>
                					    <span class="step"></span>
                					  </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                        
                        <br>
                        <div class="form-section">
                            <div class="login-inner-form">
	                            <div class="row">
	                            	<div class="col-12 col-md-12">
	                            		<p>Sudah Punya Akun ?</p>
	                            		<a href="<?php echo $config['web']['url'] ?>auth/login.php"><i class="fa fa-sign-in"></i> Masuk</a>
	                            	</div>
	                            </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Register -->

        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script src="<?php echo $config['web']['url'] ?>assets/js/pages/custom/login/vkeyboard.js"></script>
	<script>
	function showkatasandisatu() {
      var satu = document.getElementById("show-kata-sandi-1");
      if (satu.type === "password") {
        satu.type = "text";
        document.getElementById("showkatasandisatu").setAttribute("class", "fa fa-eye-slash mt-1");
      } else {
        satu.type = "password";
        document.getElementById("showkatasandisatu").setAttribute("class", "fa fa-eye mt-1");
      }
    }
    function showkatasandidua() {
      var dua = document.getElementById("show-kata-sandi-2");
      if (dua.type === "password") {
        dua.type = "text";
        document.getElementById("showkatasandidua").setAttribute("class", "fa fa-eye-slash mt-1");
      } else {
        dua.type = "password";
        document.getElementById("showkatasandidua").setAttribute("class", "fa fa-eye mt-1");
      }
    }
    // function showkatasanditiga() {
    //   var tiga = document.getElementById("show-kata-sandi-3");
    //   if (tiga.type === "password") {
    //     tiga.type = "text";
    //   } else {
    //     tiga.type = "password";
    //   }
    // }
	
	var currentTab = 0; // Current tab is set to be the first tab (0)
	showTab(currentTab); // Display the current tab
	
	function showTab(n) {
	  // This function will display the specified tab of the form...
	  var x = document.getElementsByClassName("tab");
	  x[n].style.display = "block";
	  //... and fix the Previous/Next buttons:
	  if (n == 0) {
	    document.getElementById("prevBtn").style.display = "none";
	  } else {
	    document.getElementById("prevBtn").style.display = "inline";
	  }
	  if (n == (x.length - 1)) {
	    document.getElementById("nextBtn").outerHTML = '<button type="submit" id="nextBtn" class="btn btn-primary w-50" name="daftar" style="border-radius:0 20px 20px 0;">Daftar <i class="fa fa-check"></i></button>';
	  } else {
	    document.getElementById("nextBtn").outerHTML = '<button type="button" class="btn btn-primary w-100" id="nextBtn" onclick="nextPrev(1)" name="daftar">Lanjut <i class="fa fa-arrow-right"></i></button>';
	  }
	  //... and run a function that will display the correct step indicator:
	  fixStepIndicator(n)
	}
	
	function nextPrev(n) {
	  // This function will figure out which tab to display
	  var x = document.getElementsByClassName("tab");
	  // Exit the function if any field in the current tab is invalid:
	  if (n == 1 && !validateForm()) return false;
	  // Hide the current tab:
	  x[currentTab].style.display = "none";
	  // Increase or decrease the current tab by 1:
	  currentTab = currentTab + n;
	  // if you have reached the end of the form...
	  if (currentTab >= x.length) {
	    // ... the form gets submitted:
	    document.getElementById("regForm").submit();
	    return false;
	  }
	  // Otherwise, display the correct tab:
	  showTab(currentTab);
	}
	
	function validateForm() {
	  // This function deals with validation of the form fields
	  var x, y, i, valid = true;
	  x = document.getElementsByClassName("tab");
	  y = x[currentTab].getElementsByTagName("input");
	  // A loop that checks every input field in the current tab:
	  for (i = 0; i < y.length; i++) {
	    // If a field is empty...
	    if (y[i].value == "") {
	      // add an "invalid" class to the field:
	      y[i].className += " invalid";
	      // and set the current valid status to false
	      valid = false;
	    }
	  }
	  // If the valid status is true, mark the step as finished and valid:
	  if (valid) {
	    document.getElementsByClassName("step")[currentTab].className += " Selesai";
	  }
	  return valid; // return the valid status
	}
	
	function fixStepIndicator(n) {
	  // This function removes the "active" class of all steps...
	  var i, x = document.getElementsByClassName("step");
	  for (i = 0; i < x.length; i++) {
	    x[i].className = x[i].className.replace(" active", "");
	  }
	  //... and adds the "active" class on the current step:
	  x[n].className += " active";
	}
	</script>
<?php
require '../lib/footer_home.php';
?>