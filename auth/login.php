<?php
session_start();
require '../config.php';
$tipe = "Masuk";

        if (isset($_SESSION['user'])) {
            
            header("Location: ".$config['web']['url']);
            
        } else {

            if (isset($_POST['masuk'])) {
                $username = $conn->real_escape_string(trim(filter($_POST['username'])));
                $password = $conn->real_escape_string(trim(filter($_POST['password'])));
                
                // Simpen Cookie NIK KTP
                if ( isset($_POST['remember']) ) {
                    setcookie("nik_ktp", $username, time() + 2*60*60*24);
                }
                
                $cek_pengguna = $conn->query("SELECT * FROM users WHERE username = '$username'");
                $cek_pengguna_ulang = mysqli_num_rows($cek_pengguna);
                $data_pengguna = mysqli_fetch_assoc($cek_pengguna);

                $verif_password = password_verify($password, $data_pengguna['password']);

                $error = array();
                if (empty($username)) {
        		    $error ['username'] = '*Tidak Boleh Kosong';
                } else if ($cek_pengguna_ulang == 0) {
        		    $error ['username'] = '*Pengguna Tidak Terdaftar';
                }
                if (empty($password)) {
        		    $error ['password'] = '*Tidak Boleh Kosong';
                } else if ($verif_password <> $data_pengguna['password']) {
        		    $error ['password'] = '*Kata Sandi Anda Salah';
                } else {

                if ($data_pengguna['status'] == "Tidak Aktif") {
                    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Akun Sudah Tidak Aktif.<script>swal("Gagal!", "Akun Sudah Tidak Aktif.", "error");</script>');

                } else if ($data_pengguna['status_akun'] == "Belum Verifikasi") {
                    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Akun Anda Belum Di Verifikasi.<script>swal("Gagal!", "Akun Anda Belum Di Verifikasi.", "error");</script>');

                } else {

                        if ($cek_pengguna_ulang == 1) {
                            if ($verif_password == true) {
                                $conn->query("INSERT INTO aktifitas VALUES ('','$username', 'Masuk', '".get_client_ip()."','$date','$time')");
                                $_SESSION['user'] = $data_pengguna;
                                exit(header("Location: ".$config['web']['url']));
                            } else {
                                $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
                            }
                        }
                    }
                }
            }
            
        }

        require '../lib/header_home.php';

?>

        <!-- Start Page Login -->
        <div class="login-2">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
	            	<a href="<?php echo $config['web']['url'] ?>" class="forgot-password-1">
                        	<img alt="Logo" src="<?php echo $config['web']['url'] ?>assets/media/logos/logo_color.png" />			
                        </a>
                        <div class="form-section">
	                        
	                    <h3>Masuk</h3>
                            <?php
                            if (isset($_SESSION['hasil'])) {
                            ?>
                            <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible" role="alert">
                                <?php echo $_SESSION['hasil']['pesan'] ?>
                            </div>
                            <?php
                            unset($_SESSION['hasil']);
                            }
                            ?>
                            <div class="login-inner-form">
                                <form class="form-horizontal" role="form" method="POST">
                                    <input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
                                    <div class="form-group form-box">
                                        <input type="text" name="username" class="input-text" placeholder="NIK E-KTP" value="<?php echo ( $username ) ? $username : $_COOKIE["nik_ktp"]; ?>">
                                        <i class="fa fa-id-card-o mt-1"></i>
                                        <small class="text-danger font-13 pull-right"><?php echo ($error['username']) ? $error['username'] : '';?></small>
                                    </div>
                                    <div class="form-group form-box">
                                        <input id="show-kata-sandi-1" type="password" name="password" class="input-text" placeholder="Kata Sandi">
                                        <i class="fa fa-eye mt-1" id="showkatasandisatu" onclick="showkatasandisatu()" style=" cursor: pointer; "></i>
                                        <small class="text-danger font-13 pull-right"><?php echo ($error['password']) ? $error['password'] : '';?></small>
                                    </div>
                                    <div class="checkbox clearfix">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" value="true" id="rememberMe" checked>
                                            <label class="form-check-label" for="rememberMe">
                                                Simpan NIK E-KTP
                                            </label>
                                        </div>
                                        <span class="pull-right"><a href="<?php echo $config['web']['url'] ?>auth/forgot-password.php">Lupa Kata Sandi ?</a></span>
                                    </div>
                                    <div class="form-group mb-0">
                                        <button type="submit" class="btn btn-primary btn-block" name="masuk">Masuk</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <br>
                        <div class="form-section">
                            <div class="login-inner-form">
	                            <div class="row">
	                            	<div class="col-12 col-md-6">
	                            		<p>Belum Punya Akun ?</p>
	                            		<a href="<?php echo $config['web']['url'] ?>auth/register.php"><i class="flaticon-user"></i> Daftar</a>
	                            		<hr class="d-block d-sm-none">
	                            	</div>
	                            	<div class="col-12 col-md-6">
	                            		<p>Verifikasi Akun ?</p>
	                            		<a href="<?php echo $config['web']['url'] ?>auth/verification-account.php"><i class="flaticon-password-1"></i> Verifikasi Disini</a>
	                            	</div>
	                            </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Login -->
        
        <script>
        function showkatasandisatu() {
          var satu = document.getElementById("show-kata-sandi-1");
          if (satu.type === "password") {
            satu.type = "text";
            document.getElementById("showkatasandisatu").setAttribute("class", "fa fa-eye-slash mt-1");
          } else {
            satu.type = "password";
            document.getElementById("showkatasandisatu").setAttribute("class", "fa fa-eye mt-1");
          }
        }
        </script>

<?php
require '../lib/footer_home.php';
?>