<?php
session_start();
require("../config.php");
require("../lib/class.phpmailer.php");
$tipe = "Verifikasi Akun";
        
        if (isset($_SESSION['user'])) {
            
            header("Location: ".$config['web']['url']);
            
        } else {
            
	    if (isset($_POST['kirim_kode'])) {
	        $email = $conn->real_escape_string(filter(trim($_POST['email'])));

	        $cek_pengguna = $conn->query("SELECT * FROM users WHERE email = '$email'");
	        $cek_pengguna_ulang = mysqli_num_rows($cek_pengguna);
	        $data_pengguna = mysqli_fetch_assoc($cek_pengguna);

	        $error = array();
	        if (empty($email)) {
			    $error ['email'] = '*Tidak Boleh Kosong';
	        } else if ($cek_pengguna_ulang == 0 ) {
			    $error ['email'] = '*Email Tidak Ditemukan';
	        } else {

	        if ($data_email['status_akun'] == "Sudah Verifikasi") {
	            $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Akun Kamu Sudah Di Verifikasi.<script>swal("Gagal!", "Akun Kamu Sudah Di Verifikasi.", "error");</script>');

	        } else {

	        $kode_verifikasi = acak_nomor(3).acak_nomor(3);
	        $pengguna = $data_pengguna['username'];

	        $mail = new PHPMailer;
	        $mail->IsSMTP();
	        $mail->SMTPSecure = 'tls'; 
            $mail->Host = "ktpku.com"; //host masing2 provider email
            $mail->SMTPDebug = 2;
            $mail->Port = 587;
            $mail->SMTPAuth = true;
            $mail->Username = "admin@ktpku.com"; //user email
            $mail->Password = "qazwsx123098"; //password email 
            $mail->SetFrom("admin@ktpku.com","KTPKu"); //set email pengirim
	        $mail->Subject = "Verifikasi Akun KTPKu"; //subyek email
	        $mail->AddAddress("$email");  //tujuan email
	        $mail->MsgHTML("Verifikasi Akun<br><br><b>Email : $email<br>Kode Verifikasi : $kode_verifikasi</b><br><br>Silahkan verifikasi Akun Anda dengan menggunakan kode verifikasi tersebut, Terima Kasih.<br><br>Admin KTPKu<br>admin@ktpku.com");
	        if ($mail->Send());
	            if ($conn->query("UPDATE users SET kode_verifikasi = '$kode_verifikasi' WHERE username = '".$data_pengguna['username']."'") == true) {
                    $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Kode Verifikasi Berhasil Dikirim Ke Email Kamu.<script>swal("Berhasil!", "Kode Verifikasi Berhasil Dikirim.", "success");</script>');
                } else {
                    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
                }
            }
        }

	    } else if (isset($_POST['verifikasi'])) { }
        
        }
        
        require '../lib/header_home.php';

?>

        <!-- Start Page Verification Account -->
        <div class="login-2">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                    	<a href="<?php echo $config['web']['url'] ?>" class="forgot-password-1">
	                	<img alt="Logo" src="<?php echo $config['web']['url'] ?>assets/media/logos/logo_color.png" />			
	                </a>
                        <div class="form-section">
                            <?php
                            if (isset($_SESSION['hasil'])) {
                            ?>
                            <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible" role="alert">
                                <?php echo $_SESSION['hasil']['pesan'] ?>
                            </div>
                            <?php
                            unset($_SESSION['hasil']);
                            }
                            ?>
                            <div class="login-inner-form">
                                <form class="form-horizontal" role="form" method="POST">
                                    <h3>Kirim Ulang Verifikasi</h3>
                                    <input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
                                    <div class="form-group form-box">
                                        <input type="email" name="email" class="input-text" placeholder="Masukkan Email Terdaftar" value="<?php echo $email; ?>">
                                        <i class="flaticon-mail-2"></i>
                                        <small class="text-danger font-13 pull-right"><?php echo ($error['email']) ? $error['email'] : '';?></small>
                                    </div>
                                    <div class="form-group mb-0">
                                        <button type="submit" class="btn btn-primary btn-block" name="kirim_kode">Kirim Ulang Kode <i class="fa fa-retweet"></i></button>
                                    </div>
                                </form>
                            </div>                            
                        </div>
                        <br>
                    	<div class="form-section">
                            <div class="login-inner-form">
	                            <div class="row">
	                            	<div class="col-12 col-md-6">
	                            		<p>Sudah Verifikasi Akun ?</p>
	                            		<a href="<?php echo $config['web']['url'] ?>auth/login.php"><i class="fa fa-sign-in"></i> Masuk</a>
	                            		<hr class="d-block d-sm-none">
	                            	</div>
	                            	<div class="col-12 col-md-6">
	                            		<p>Verifikasi Akun</p>
	                            		<a href="<?php echo $config['web']['url'] ?>auth/verification-account.php"><i class="flaticon-password-1"></i> Verifikasi Disini</a>
	                            	</div>
	                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Verification Account -->

<?php
require '../lib/footer_home.php';
?>