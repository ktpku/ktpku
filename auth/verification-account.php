<?php
session_start();
require("../config.php");
require("../lib/class.phpmailer.php");
$tipe = "Verifikasi Akun";
        
        if (isset($_SESSION['user'])) {
            
            header("Location: ".$config['web']['url']);
            
        } else {
            
	    if (isset($_POST['kirim_kode'])) {
	    } else if (isset($_POST['verifikasi'])) {
	        $email = $conn->real_escape_string(filter(trim($_POST['email'])));
	        $kode = $conn->real_escape_string(filter(trim($_POST['kode'])));

            $cek_pengguna = $conn->query("SELECT * FROM users WHERE email = '$email'");
            $cek_pengguna_ulang = mysqli_num_rows($cek_pengguna);
            $data_pengguna = mysqli_fetch_assoc($cek_pengguna);

            $cek_kode = $conn->query("SELECT * FROM users WHERE kode_verifikasi = '$kode'");
            $cek_kode_ulang = mysqli_num_rows($cek_kode);
            $data_kode = mysqli_fetch_assoc($cek_kode);

            $cek_referral = $conn->query("SELECT * FROM setting_referral WHERE status = 'Aktif'");
            $cek_referral_ulang = mysqli_num_rows($cek_referral);
            $data_referral = mysqli_fetch_assoc($cek_referral);

            $error = array();
            if (empty($kode)) {
		        $error ['kode'] = '*Tidak Boleh Kosong';
            } else if ($data_kode['kode_verifikasi'] <> $kode) {
		        $error ['kode'] = '*Kode Verifikasi Tidak Sesuai';
            } else {

	        if ($data_pengguna['status_akun'] == "Sudah Verifikasi") {
	            $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Akun Kamu Sudah Di Verifikasi.<script>swal("Gagal!", "Akun Kamu Sudah Di Verifikasi. Silakan masuk ke akun anda.", "error");</script>');

	        } else {

                if ($conn->query("UPDATE users SET status_akun = 'Sudah Verifikasi' WHERE username = '".$data_kode['username']."'") == true) {
                    $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Verifikasi Akun Berhasil, Silahkan Masuk Untuk Menggunakan KTPKu.<script>swal("Berhasil!", "Akun Kamu Berhasil Di Verifikasi.", "success");</script>');
                    exit(header("Location: ".$config['web']['url']."auth/login"));
                } else {
                    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
                }
            }
        }

	    }
        
        }
        
        require '../lib/header_home.php';

?>

        <!-- Start Page Verification Account -->
        <div class="login-2">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                    	<a href="<?php echo $config['web']['url'] ?>" class="forgot-password-1">
	                	<img alt="Logo" src="<?php echo $config['web']['url'] ?>assets/media/logos/logo_color.png" />			
	                </a>
                        <div class="form-section">
                            <?php
                            if (isset($_SESSION['hasil'])) {
                            ?>
                            <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible" role="alert">
                                <?php echo $_SESSION['hasil']['pesan'] ?>
                            </div>
                            <?php
                            unset($_SESSION['hasil']);
                            }
                            ?>
                            <div class="login-inner-form">
                                <form class="form-horizontal" role="form" method="POST">
                                    <h3>Verifikasi Akun</h3>
                                    <input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
                                    <!-- <div class="form-group form-box">
                                        <input type="email" name="email" class="input-text" placeholder="Masukkan Email Terdaftar" value="<?php echo $email; ?>">
                                        <i class="flaticon-mail-2"></i>
                                        <small class="text-danger font-13 pull-right"><?php echo ($error['email']) ? $error['email'] : '';?></small>
                                    </div>-->
                                    <div class="form-group form-box">
                                        <input type="number" name="kode" class="input-text" placeholder="Masukkan Kode Verifikasi">
                                        <i class="flaticon-password"></i>
                                        <small class="text-danger font-13 pull-right"><?php echo ($error['kode']) ? $error['kode'] : '';?></small>
                                    </div>
                                    <div class="form-group mb-0">
                                        <button type="submit" class="btn btn-primary btn-block" name="verifikasi">Verifikasi Akun <i class="flaticon-password-1"></i></button>
                                    </div>
                                </form>
                            </div>                            
                        </div>
                        <br>
                        <div class="form-section">
                            <div class="login-inner-form">
	                            <div class="row">
	                            	<div class="col-12 col-md-6">
	                            		<p>Sudah Verifikasi Akun ?</p>
	                            		<a href="<?php echo $config['web']['url'] ?>auth/login.php"><i class="fa fa-sign-in"></i> Masuk</a>
	                            		<hr class="d-block d-sm-none">
	                            	</div>
	                            	<div class="col-12 col-md-6">
	                            		<p>Kirim Ulang Verifikasi</p>
	                            		<a href="<?php echo $config['web']['url'] ?>auth/kirim-ulang-verifikasi.php"><i class="fa fa-retweet"></i> Kirim Disini</a>
	                            	</div>
	                            </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page Verification Account -->

<?php
require '../lib/footer_home.php';
?>