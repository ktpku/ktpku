<?php 
session_start();
require("config.php");

    if (isset($_SESSION['user'])) {
        
            $sess_username = $_SESSION['user']['username'];
            $check_user = $conn->query("SELECT * FROM users WHERE username = '$sess_username'");
            $data_user = $check_user->fetch_assoc();
            $check_username = $check_user->num_rows;
            if ($check_username == 0) {
    	        header("Location: ".$config['web']['url']."logout.php");
            } else if ($data_user['status'] == "Tidak Aktif") {
    	        header("Location: ".$config['web']['url']."logout.php");
            }

        } else {
            
	        $_SESSION['user'] = $data_user;
	        header("Location: ".$config['web']['url']."dashboard");
	        
        }

include("lib/header.php");
if (isset($_SESSION['user'])) {
?>

<style>
    #data_barcode {
        margin: auto;
        width: 100%;
    }
    #data_qr_code img {
        margin: auto;
        width: 100%;
        max-width: 400px;
        padding: 20px;
    }
    @media(max-width: 767px){
        body {
            background-image: linear-gradient(45deg, rgb(255 255 255), rgb(255 255 255))!important;
        }
    }
</style>

		<!-- Start Card Box Order -->
        <div class="kt-container mt-4 mb-5">
            <div class="product-catagory-wrap" style=" background: #fff; padding: 15px; border-radius: 10px; box-shadow: 0 0 9px rgba(82,63,105,.15); ">
                
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                  <li class="nav-item">
                    <a class="nav-link active" id="one-tab" data-toggle="tab" href="#pillsone" role="tab" aria-controls="one" aria-selected="true">QR Code</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" id="two-tab" data-toggle="tab" href="#pillstwo" role="tab" aria-controls="two" aria-selected="false">Barcode</a>
                  </li>
                </ul>
                
                 <div class="tab-content" id="myTabContent">
                     
                      <div class="tab-pane fade show active" id="pillsone" role="tabpanel" aria-labelledby="one-tab">
                            
                            <div class="row align-items-center" style="min-height:300px">
                        
                                <div class="col-12" >
                                    <div class="card mb-3 text-center" >
                                        <p>QR Code NIK EKTP Anda :</p>
                                        <div id="data_qr_code"></div>
                                    </div>
                                </div>
                                
                            </div>
                            
                      </div>
                      <div class="tab-pane fade" id="pillstwo" role="tabpanel" aria-labelledby="two-tab">
                            
                            <div class="row align-items-center" style="min-height:300px">
                        
                                <div class="col-12" >
                                    <div class="card mb-3 text-center" >
                                        <p>Kode Barcode NIK EKTP Anda :</p>
                                        <svg id="data_barcode"></svg>
                                    </div>
                                </div>
                                
                            </div>
                            
                      </div>
                    
                </div>
    
                
            </div>
        </div>
        <!--<div class="kt-container">-->
        <!--    <div class="row">-->
                
        <!--        <div class="col-12 mb-5 text-center">-->
        <!--            <a href="<?php echo $config['web']['url'] ?>staff/transfer-balance" class="h5">Transfer / Bayar <i class="fa fa-exchange-alt"></i></a>-->
        <!--        </div>-->
                
        <!--    </div>-->
        <!--</div>-->
        <!-- End Card Box Order -->


<script src="<?php echo $config['web']['url'] ?>assets/js/pages/scan/qrcode.min.js" type="text/javascript"></script>
<script src="<?php echo $config['web']['url'] ?>assets/js/pages/scan/JsBarcode.all.min.js" type="text/javascript"></script>
<script>
$(document).ready(function() {
    JsBarcode("#data_barcode", "<?php echo $data_user['username']; ?>", {
      width:2.7,
      height:100,
      textMargin: 15,
      margin: 0,
    });
    var qrcode = new QRCode(document.getElementById("data_qr_code"), {
    	text: "<?php echo $data_user['username']; ?>",
    	width: 400,
    	height: 400,
    	colorDark : "#000000",
    	colorLight : "#ffffff",
    	correctLevel : QRCode.CorrectLevel.H
    });
});
</script>
   
<?php 
}
require 'lib/footer.php';
?>