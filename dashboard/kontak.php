<?php
require '../config.php';
require '../lib/database.php';
include("header_frontsite.php");
?>

        <?php
        $cek_kontak = $conn->query("SELECT * FROM kontak_website ORDER BY id DESC");
        while ($data_kontak = $cek_kontak->fetch_assoc()) {
        ?>
        
		<section class="section overlay section_two">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-12">
						<div class="box-wrapper">
							<span id="seciontwo"></span> 
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="section overlay section_three padding-on-body-mobile">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-12 col-lg-12">
						<h3><?php echo $data['short_title']; ?></h3>
						<p><?php echo $data['title']; ?></p>
						<p>Kritik, Saran Dan Pertanyaan : <b>admin@ktpku.com</b></p>
						
						<br>
						
						<a href="<?php echo $data_kontak['link_ig']; ?>" target="_blank">Instagram <i class="flaticon-instagram-logo"></i></a> 
						<span class="mx-2"> - </span>
                        <a href="https://api.whatsapp.com/send?phone=<?php echo $data_kontak['no_wa']; ?>" target="_blank"><i class="flaticon-whatsapp"></i> Whatsapp</a>
					</div>
				</div>
			</div>
		</section>
		
	<div class="clearfix"></div>
	
	    <?php
        }
        ?>
        
		<?php
		include("footer_frontsite.php");
		?>
		
		<script>
		 var typed3 = new Typed('#seciontwo', {
		    strings: ['Kontak', 'EKTPKu'],
		    typeSpeed: 50,
		    backSpeed: 50,
		    loop: true
		  });
		</script>

		</body>

</html>