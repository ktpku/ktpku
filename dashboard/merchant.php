<?php
require '../config.php';
require '../lib/database.php';
include("header_frontsite.php");
?>

		<section class="section overlay section_two">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-12">
						<div class="box-wrapper">
							<span id="seciontwo"></span> 
						</div>
					</div>
				</div>
			</div>
		</section>
	
		
		<section class="section overlay section_three padding-on-body-mobile">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-12 col-lg-12">
						<div class="box-wrapper">
							<h2>Keuntungan Merchant<div></div></h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-4 col-lg-4">
						<div class="box-wrapper">
							<img src="https://www.flaticon.com/svg/static/icons/svg/844/844256.svg" class="w-100" />
							<h3>Transaksi Mudah</h3>
							<p>Nikmati nyamannya bertransaksi di sistem pembayaran EKTPKu dengan sistem transaksi yang mudah dan fleksibel.</p>
						</div>
					</div>
					<div class="col-4 col-lg-4">
						<div class="box-wrapper">
							<img src="https://www.flaticon.com/svg/static/icons/svg/849/849307.svg" class="w-100" />
							<h3>Aman dan Terjamin</h3>
							<p>Setiap transaksi pasti aman dan terjamin transaksinya dengan teknologi dan sistem keamanan yang tinggi.</p>
						</div>
					</div>
					<div class="col-4 col-lg-4">
						<div class="box-wrapper">
							<img src="https://www.flaticon.com/svg/static/icons/svg/852/852221.svg" class="w-100" />
							<h3>1 EKTP Untuk Semua</h3>
							<p>EKTPKu merupakan sistem pembayaran yang paling cocok untuk semua jenis usaha baik digital dan non digital.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="section overlay section_one" style=" min-height: auto; ">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-9">
						<h2 class="mb-0 text-center" style=" text-shadow: 0 0 5px #e1e1ef; ">Gabung Menjadi Merchant EKTPKu Sekaran Juga...</h2>
					</div>
					<div class="col-lg-3">
						<a href="<?php echo $config['web']['url'] ?>auth/register-merchant.php" class="btn-web-login" style=" border: solid; box-shadow: 0 0 10px #e1e1ef; "><i class="fas fa fa-store-alt mr-2"></i> Daftar Menjadi Merchant</a>
					</div>
				</div>
			</div>
		</section>
		
		
	<div class="clearfix"></div>
	
		<?php
		include("footer_frontsite.php");
		?>
		
		<script>
		 var typed3 = new Typed('#seciontwo', {
		    strings: ['Merchant', 'EKTPKu'],
		    typeSpeed: 50,
		    backSpeed: 50,
		    loop: true
		  });
		</script>

		</body>

</html>