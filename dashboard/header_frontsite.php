<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title><?php echo $data['title']; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta content="<?php echo $data['deskripsi_web']; ?>" name="description" />
        <meta content="EKTPKu" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- Start Favicon Icon -->
        <link rel="shortcut icon" href="<?php echo $config['web']['url'] ?>assets/media/logos/rsz_logos.png" />
        <!-- End Favicon Icon -->

        <!-- Start CSS -->
        <link href="<?php echo $config['web']['url'] ?>assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $config['web']['url'] ?>assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $config['web']['url'] ?>assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $config['web']['url'] ?>assets/css/style-home.css" rel="stylesheet" type="text/css" />
        <!-- End CSS -->
    
        <!-- Start Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
        <!-- End Fonts -->

        <!-- Start Hotjar Tracking -->
        <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1070954,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>
        <!-- End Hotjar Tracking -->
        
        <!-- Start Global Site Tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=#1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '#');
        </script>
        <!-- End Global Site Tag (gtag.js) - Google Analytics -->

</head>
	<body>
		<nav class="navbar navbar-expand-lg navbar-default navbar-light navbar-custom">
			<div class="container">
				<a class="navbar-brand logo text-white" href="index.php">
					<img alt="Logo" src="<?php echo $config['web']['url'] ?>assets/media/logos/logo_color.png" />			</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarCollapse">
					<ul class="nav navbar-nav ml-auto navbar-center" id="mySidenav">
						<li class="nav-item">
							<a href="<?php echo $config['web']['url'] ?>" class="nav-link scroll" >Depan</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo $config['web']['url'] ?>dashboard/tentang" class="nav-link scroll" >Tentang</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo $config['web']['url'] ?>dashboard/faq" class="nav-link scroll" >FAQ</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo $config['web']['url'] ?>auth/register" class="nav-link scroll" >Daftar User</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo $config['web']['url'] ?>dashboard/merchant" class="nav-link scroll" >Daftar Merchant</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo $config['web']['url'] ?>auth/login" class="nav-link scroll" ><i class="fa fa-sign-in-alt"></i></a>
						</li>
						<li class="nav-item dropdown">
					        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					          ID
					        </a>
					        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					          <a class="dropdown-item" href="<?php echo $config['web']['url'] ?>">Bahasa (ID)</a>
					        </div>
				        </li>
					</ul>
				</div>
			</div>
		</nav>