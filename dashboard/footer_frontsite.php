                    <!-- Start Footer -->
        		    <div class="kt-footer  kt-footer--extended  kt-grid__item" id="kt_footer">
            			<div class="kt-footer__top">
                			<div class="kt-container ">
                				<div class="row">
                					<div class="col-lg-3">
                						<div  class="kt-footer__section">
                							<h3 class="kt-footer__title">Tentang <?php echo $data['short_title']; ?></h3>
                							<div class="kt-footer__content">
                								<img class="mb-3 d-block" alt="Logo" src="<?php echo $config['web']['url'] ?>assets/media/logos/logo_color.png" style=" height: 40px; "/>
                								<b><?php echo $data['short_title']; ?></b> Dompet Digital Berbasis E-KTP Pertama Di Indonesia
                							</div>
                						</div>
                					</div>
                					<div class="col-lg-2">
                						<div  class="kt-footer__section">
                							<h3 class="kt-footer__title">Informasi</h3>
                							<div class="kt-footer__content">
                								<div class="kt-footer__nav">
                									<div class="kt-footer__nav-section">
                										<a href="<?php echo $config['web']['url'] ?>page/how-to-top-up-balance">Cara Isi Saldo</a>
                										<a href="<?php echo $config['web']['url'] ?>page/how-to-transaction">Cara Transaksi</a>
                										<a href="<?php echo $config['web']['url'] ?>page/balance-top-up-method">Pembayaran Isi Saldo</a>
                										<a href="<?php echo $config['web']['url'] ?>page/service-promo">Layanan Promo</a>
                									</div>
                								</div>	
                							</div>
                						</div>
                					</div>
                					<div class="col-lg-2">
                						<div  class="kt-footer__section">
                							<h3 class="kt-footer__title">Lainnya</h3>
                							<div class="kt-footer__content">
                								<div class="kt-footer__nav">
                									<div class="kt-footer__nav-section">
                										<a href="<?php echo $config['web']['url'] ?>dashboard/kontak" class="kt-link">Kontak Kami</a>
                										<a href="<?php echo $config['web']['url'] ?>dashboard/ketentuan-layanan" class="kt-link">Ketentuan Layanan</a>
                										<a href="<?php echo $config['web']['url'] ?>dashboard/faq" class="kt-link">FAQ</a>
                										<a href="<?php echo $config['web']['url'] ?>dashboard/karir" class="kt-link">Karir</a>
                									</div>
                								</div>	
                							</div>
                						</div>
                					</div>
                					<?php
                					$cek_kontak = $conn->query("SELECT * FROM kontak_website ORDER BY id DESC");
                					while ($data_kontak = $cek_kontak->fetch_assoc()) {
                					?>
                					<div class="col-lg-2">
                						<div  class="kt-footer__section">
                							<h3 class="kt-footer__title">Sosial Media</h3>
                							<div class="kt-footer__content">
                								<div class="kt-footer__nav">
                									<div class="kt-footer__nav-section">
                										<a href="<?php echo $data_kontak['link_ig']; ?>" target="_blank"><i class="flaticon-instagram-logo fa-2x"></i> &nbsp;Instagam</a>
                										<a href="https://api.whatsapp.com/send?phone=<?php echo $data_kontak['no_wa']; ?>" target="_blank"><i class="flaticon-whatsapp fa-2x"></i> &nbsp;WhatsApp</a>
                									</div>
                								</div>	
                							</div>
                						</div>
                					</div>
                					<?php
                					}
                					?>
                					<div class="col-lg-3">
                						<div class="kt-footer__section">
                							<h3 class="kt-footer__title">Download App</h3>
                							<div class="kt-footer__content">
                							    <div class="row">
                							        <div class="col-xs-6 col-md-6 pr-2">
                							        	<a target="_BLANK" href="https://play.google.com/store/apps/details?id=ktpku.com" class="footer-btn-app"><img data-src="<?php echo $config['web']['url'] ?>assets/media/icon/download_play.png" alt="playstore" class="w-100" src="<?php echo $config['web']['url'] ?>assets/media/icon/download_play.png"></a>
                							        </div>
                							        <div class="col-xs-6 col-md-6 pl-2">
                							        	<a href="#" class="footer-btn-app"><img data-src="<?php echo $config['web']['url'] ?>assets/media/icon/download_app.png" alt="playstore" class="w-100" src="<?php echo $config['web']['url'] ?>assets/media/icon/download_app.png"></a>
                							        </div>
                					    		    </div>								
                							</div>
                						</div>
                					</div>				
                				</div>				
                			</div>	
                		</div> 	 
            		    <div class="kt-footer__bottom">
            		        <div class="kt-container ">
            			        <div class="kt-footer__wrapper">
            						<div class="kt-footer__logo w-100">	 			 
            							<div class="kt-footer__copyright text-center w-100 pb-5">
            								© 2020 <?php echo $data['short_title']; ?>. All Rights Reserved.
            							</div>
            						</div>
            					</div>
            				</div>
            			</div>
            		</div>
            		<!-- End Footer -->
                    
                    <!-- End Footer Mobile -->
                    <div class="kt-footer  kt-footer--extended  kt-grid__item d-block d-sm-none">
            		    <div class="kt-footer__bottom">
            		        <div class="kt-container ">
            			        <div class="kt-footer__wrapper">
            						<div class="kt-footer__logo w-100">	 			 
            							<div class="kt-footer__copyright text-center w-100 text-white">
            								© 2020 <?php echo $data['short_title']; ?>. All Rights Reserved.
            							</div>
            						</div>
            					</div>
            				</div>
            			</div>
            		</div>
            		<!-- End Footer Mobile -->
            		
		       </div>
		    </div>
    	</div>
		<!-- End Page -->
		

		<!-- Global Theme Bundle (used by all pages) -->
		<script src="<?php echo $config['web']['url'] ?>assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
		<script src="<?php echo $config['web']['url'] ?>assets/js/scripts.bundle.js" type="text/javascript"></script>
		<script src="https://mattboldt.github.io/typed.js/lib/typed.js" type="text/javascript"></script>
		<!-- End Global Theme Bundle -->
		
		<!-- coomingsoon -->
        <div class="modal fade" id="coomingsoon" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="background: #000;">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content bg-transparent border-0 text-white">
              <div class="modal-body">
                <h1 class="text-center">Cooming Soon <span data-dismiss="modal">!!!</span></h1>
              </div>
            </div>
          </div>
        </div>
		<!-- <script>
		    $('#coomingsoon').modal({
                backdrop: 'static',
                keyboard: false
            })
		</script>-->
		<!-- coomingsoon -->
		<script src="<?php echo $config['web']['url'] ?>assets/js/pages/components/topbar/topbar.min.js" type="text/javascript"></script>
        <script>
        function resetToDefaults() {
            topbar.config({
              autoRun      : true,
              barThickness : 4,
              barColors    : {
                '0'      : 'rgba(26,  188, 156, .9)',
                '.25'    : 'rgba(52,  152, 219, .9)',
                '.50'    : 'rgba(241, 196, 15,  .9)',
                '.75'    : 'rgba(230, 126, 34,  .9)',
                '1.0'    : 'rgba(211, 84,  0,   .9)'
              },
              shadowBlur   : 10,
              shadowColor  : 'rgba(0,   0,   0,   .6)'
            })
          }

          // Page load
          resetToDefaults();
          topbar.show();
          $(document).ready(function() {  
            topbar.hide()
          });
        </script>
		