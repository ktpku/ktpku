<?php
require '../config.php';
require '../lib/database.php';
include("header_frontsite.php");
?>

		<section class="section overlay section_one">
			<div class="container">
				<div class="row align-items-center">
					<div class="offset-lg-6 col-lg-6">
						<div class="home-wrapper">
							<h1 class="text-center d-none d-sm-block">Selamat Datang <span class="d-none d-sm-block">di KTPKu</span></h1>
							<p class="mb-0">KTPKu adalah layanan uang elektronik berbasis eKTP pertama di indonesia. Lewat KTPKu, kamu bisa melakukan transaksi apapun dengan lebih cepat, praktis dan aman.</p>
						</div>
						<div class="footer__section_top">
							<div class="kt-footer__content">
							    <div class="row justify-content-center">
							    	<div class="col-4 col-md-4"> 
							        	<a href="<?php echo $config['web']['url'] ?>auth/login.php" class="btn-web-login"><i class="fas fa fa-globe mr-2"></i> Login WEB</a> 
							       	</div>
							        <div class="col-4 col-md-4">
							        	<a target="_BLANK" href="https://play.google.com/store/apps/details?id=ktpku.com" class="footer-btn-app"><img data-src="<?php echo $config['web']['url'] ?>assets/media/icon/download_play.png" alt="playstore" class="w-100" src="<?php echo $config['web']['url'] ?>assets/media/icon/download_play.png"></a>
							        </div>
							        <div class="col-4 col-md-4">
							        	<div class="footer-btn-app"><img data-src="<?php echo $config['web']['url'] ?>assets/media/icon/download_app.png" alt="playstore" class="w-100" src="<?php echo $config['web']['url'] ?>assets/media/icon/download_app.png"><span class="span-soon">Soon</span></div>
							        </div>
					    		    </div>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="section overlay section_two">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-12">
						<div class="box-wrapper">
							<span id="seciontwo"></span> 
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="section overlay section_three padding-on-body-mobile">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-12 col-lg-12">
						<div class="box-wrapper">
							<h2>Sekarang eKTP Kamu Bisa <div></div></h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-4 col-lg-4">
						<div class="box-wrapper">
							<img src="https://www.flaticon.com/svg/static/icons/svg/850/850053.svg" class="w-100" />
							<p>Isi Saldo</p>
						</div>
					</div>
					<div class="col-4 col-lg-4">
						<div class="box-wrapper">
							<img src="https://www.flaticon.com/svg/static/icons/svg/852/852237.svg" class="w-100" />
							<p>Transfer Antar eKTP</p>
						</div>
					</div>
					<div class="col-4 col-lg-4">
						<div class="box-wrapper">
							<img src="https://www.flaticon.com/svg/static/icons/svg/844/844255.svg" class="w-100" />
							<p>Transaksi Digital & Non-Digital</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- <section class="section_four">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-12 col-lg-12">
						<div class="box-wrapper">
							<h2>Partner <div></div></h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-12 col-lg-2">
						<img src="https://www.linkaja.id//uploads/images/merchant//YW50aWtvZGVfXzE1MDQ1OTcyMzRfbG9nby1tY2QtcG5n.png" class="w-100" />
					</div>
					<div class="col-12 col-lg-2">
						<img src="https://www.linkaja.id//uploads/images/merchant//YW50aWtvZGVfXzE1MDUyNzM5ODBfNmxvZ28ta2ZjLW1pbi1wbmc.png" class="w-100" />
					</div>
					<div class="col-12 col-lg-2">
						<img src="https://www.linkaja.id//uploads/images/merchant//YW50aWtvZGVfXzE1MjcyMzIzOTVfbG9nby1qY28tbmV3LXBuZw.png" class="w-100" />
					</div>
					<div class="col-12 col-lg-2">
						<img src="https://www.linkaja.id//uploads/images/merchant/logo_alfamart.png" class="w-100" />
					</div>
					<div class="col-12 col-lg-2">
						<img src="https://www.linkaja.id//uploads/images/merchant//YW50aWtvZGVfXzE1MDUyNzQyNjdfOGxvZ29jaHRtLW1pbi1wbmc.png" class="w-100" />
					</div>
					<div class="col-12 col-lg-2">
						<img src="https://www.linkaja.id//uploads/images/merchant/logo_bernardi-the-factory-shop.png" class="w-100" />
					</div>
				</div>
			</div>
		</section> -->
<div class="modal" id="coming">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">

      <!-- Modal body -->
      <div class="modal-body text-center">
        Coming Soon 2021 !
      </div>

    </div>
  </div>
</div>
		
	<div class="clearfix"></div>
	
	<?php
	include("footer_frontsite.php");
	?>
		
		<script>
		 var typed3 = new Typed('#seciontwo', {
		    strings: ['Buat EKTP Kamu Lebih Bernilai...', 'Buat EKTP Kamu Lebih Fleksibel...', 'Buat EKTP Kamu Lebih Keren...'],
		    typeSpeed: 50,
		    backSpeed: 50,
		    loop: true
		  });
		  
		  $('#coming').modal({backdrop: 'static', keyboard: false});
		</script>

		</body>

</html>