<?php
require '../config.php';
require '../lib/database.php';
include("header_frontsite.php");
?>
		
	<section class="section overlay section_two">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-12">
					<div class="box-wrapper">
						<span id="seciontwo"></span> 
					</div>
				</div>
			</div>
		</div>
	</section>
	
        <!-- Start Content -->
        <div class="kt-container kt-grid__item kt-grid__item--fluid mt-5">

	        <!-- Start Page Faq Tab -->
	        <div class="row">
		
		<style>
		ul.nav.nav-tabs.nav-tabs-line {
		    display: block;
		}
		.nav-tabs.nav-tabs-line {border:none;}
		.nav-tabs.nav-tabs-line .nav-item:last-child {margin-right: 20px;}
		.nav-tabs.nav-tabs-line .nav-item {margin-left: 20px;}
		.kt-portlet.kt-portlet--height-fluid {
		    background-color: #fff;
		}
		</style>
                <div class="col-md-2">				
                    <div class="kt-portlet kt-portlet--height-fluid kt-sc-2">
                        <ul class="nav nav-tabs nav-tabs-line" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#account" role="tab">Akun</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#orders" role="tab">Pesanan</a>
                            </li>
                            <!--<li class="nav-item">-->
                            <!--    <a class="nav-link" data-toggle="tab" href="#top-up" role="tab">Top Up</a>-->
                            <!--</li>-->
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#top-up-balance" role="tab">Isi Saldo</a>
                            </li>
                            <!--<li class="nav-item">-->
                            <!--    <a class="nav-link" data-toggle="tab" href="#reffund" role="tab">Pengembalian Dana</a>-->
                            <!--</li>-->
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#status" role="tab">Status</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-md-10">	

                <!-- Start Tab Content -->
                <div class="tab-content">

                    <!-- Start Page Faq Tab Account -->
                    <div role="tabpanel" class="tab-pane fade active show" id="account">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="kt-portlet kt-portlet--height-fluid kt-sc-2">
                                    <div class="kt-portlet__body">
                                        <div class="kt-infobox">
                                            <div class="kt-infobox__header">
                                                <h2 class="kt-infobox__title">Akun</h2>
                                            </div>
                                            <div class="kt-infobox__body">
                                                <div class="accordion accordion-light accordion--no-bg accordion-svg-icon" id="accordionExample1">
                                                <?php         
                                                $cek_konten = $conn->query("SELECT * FROM pertanyaan_umum WHERE tipe = 'Akun' ORDER BY id DESC");
                                                while ($data_konten = $cek_konten->fetch_assoc()) {
                                                ?>
                                                    <div class="card">
                                                        <div class="card-header" id="heading<?php echo $data_konten['number']; ?>1">
                                                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapse<?php echo $data_konten['number']; ?>1" aria-expanded="false" aria-controls="collapse<?php echo $data_konten['number']; ?>1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                        <polygon points="0 0 24 0 24 24 0 24"/>
                                                                        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
                                                                        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
                                                                    </g>
                                                                </svg>
                                                                <?php echo $data_konten['title']; ?>
                                                            </div>
                                                        </div>
                                                        <div id="collapse<?php echo $data_konten['number']; ?>1" class="collapse" aria-labelledby="heading<?php echo $data_konten['number']; ?>1" data-parent="#accordionExample1">
                                                            <div class="card-body">
                                                                <?php echo $data_konten['konten']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Page Faq Tab Account -->
            
                    <!-- Start Page Faq Tab Orders -->
                    <div role="tabpanel" class="tab-pane fade" id="orders">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="kt-portlet kt-portlet--height-fluid kt-sc-2">
                                    <div class="kt-portlet__body">
                                        <div class="kt-infobox">
                                            <div class="kt-infobox__header">
                                                <h2 class="kt-infobox__title">Pesanan</h2>
                                            </div>
                                            <div class="kt-infobox__body">
                                                <div class="accordion accordion-light accordion--no-bg accordion-svg-icon" id="accordionExample1">
                                                <?php         
                                                $cek_konten = $conn->query("SELECT * FROM pertanyaan_umum WHERE tipe = 'Pesanan'");
                                                while ($data_konten = $cek_konten->fetch_assoc()) {
                                                ?>
                                                    <div class="card">
                                                        <div class="card-header" id="heading<?php echo $data_konten['number']; ?>1">
                                                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapse<?php echo $data_konten['number']; ?>1" aria-expanded="false" aria-controls="collapse<?php echo $data_konten['number']; ?>1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                        <polygon points="0 0 24 0 24 24 0 24"/>
                                                                        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
                                                                        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
                                                                    </g>
                                                                </svg>
                                                                <?php echo $data_konten['title']; ?>
                                                            </div>
                                                        </div>
                                                        <div id="collapse<?php echo $data_konten['number']; ?>1" class="collapse" aria-labelledby="heading<?php echo $data_konten['number']; ?>1" data-parent="#accordionExample1">
                                                            <div class="card-body">
                                                                <?php echo $data_konten['konten']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Page Faq Tab Orders -->
            
                    <!-- Start Page Faq Tab Top Up -->
                    <div role="tabpanel" class="tab-pane fade" id="top-up">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="kt-portlet kt-portlet--height-fluid kt-sc-2">
                                    <div class="kt-portlet__body">
                                        <div class="kt-infobox">
                                            <div class="kt-infobox__header">
                                                <h2 class="kt-infobox__title">Top Up</h2>
                                            </div>
                                            <div class="kt-infobox__body">
                                                <div class="accordion accordion-light accordion--no-bg accordion-svg-icon" id="accordionExample1">
                                                <?php         
                                                $cek_konten = $conn->query("SELECT * FROM pertanyaan_umum WHERE tipe = 'Top Up'");
                                                while ($data_konten = $cek_konten->fetch_assoc()) {
                                                ?>
                                                    <div class="card">
                                                        <div class="card-header" id="heading<?php echo $data_konten['number']; ?>1">
                                                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapse<?php echo $data_konten['number']; ?>1" aria-expanded="false" aria-controls="collapse<?php echo $data_konten['number']; ?>1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                        <polygon points="0 0 24 0 24 24 0 24"/>
                                                                        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
                                                                        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
                                                                    </g>
                                                                </svg>
                                                                <?php echo $data_konten['title']; ?>
                                                            </div>
                                                        </div>
                                                        <div id="collapse<?php echo $data_konten['number']; ?>1" class="collapse" aria-labelledby="heading<?php echo $data_konten['number']; ?>1" data-parent="#accordionExample1">
                                                            <div class="card-body">
                                                                <?php echo $data_konten['konten']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Page Faq Tab Top Up -->
            
                    <!-- Start Page Faq Tab Top Up Balance -->
                    <div role="tabpanel" class="tab-pane fade" id="top-up-balance">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="kt-portlet kt-portlet--height-fluid kt-sc-2">
                                    <div class="kt-portlet__body">
                                        <div class="kt-infobox">
                                            <div class="kt-infobox__header">
                                                <h2 class="kt-infobox__title">Isi Saldo</h2>
                                            </div>
                                            <div class="kt-infobox__body">
                                                <div class="accordion accordion-light accordion--no-bg accordion-svg-icon" id="accordionExample1">
                                                <?php         
                                                $cek_konten = $conn->query("SELECT * FROM pertanyaan_umum WHERE tipe = 'Isi Saldo'");
                                                while ($data_konten = $cek_konten->fetch_assoc()) {
                                                ?>
                                                    <div class="card">
                                                        <div class="card-header" id="heading<?php echo $data_konten['number']; ?>1">
                                                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapse<?php echo $data_konten['number']; ?>1" aria-expanded="false" aria-controls="collapse<?php echo $data_konten['number']; ?>1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                        <polygon points="0 0 24 0 24 24 0 24"/>
                                                                        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
                                                                        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
                                                                    </g>
                                                                </svg>
                                                                <?php echo $data_konten['title']; ?>
                                                            </div>
                                                        </div>
                                                        <div id="collapse<?php echo $data_konten['number']; ?>1" class="collapse" aria-labelledby="heading<?php echo $data_konten['number']; ?>1" data-parent="#accordionExample1">
                                                            <div class="card-body">
                                                                <?php echo $data_konten['konten']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Page Faq Tab Top Up Balance -->
            
                    <!-- Start Page Faq Tab Reffund -->
                    <div role="tabpanel" class="tab-pane fade" id="reffund">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="kt-portlet kt-portlet--height-fluid kt-sc-2">
                                    <div class="kt-portlet__body">
                                        <div class="kt-infobox">
                                            <div class="kt-infobox__header">
                                                <h2 class="kt-infobox__title">Pengembalian Dana</h2>
                                            </div>
                                            <div class="kt-infobox__body">
                                                <div class="accordion accordion-light accordion--no-bg accordion-svg-icon" id="accordionExample1">
                                                <?php         
                                                $cek_konten = $conn->query("SELECT * FROM pertanyaan_umum WHERE tipe = 'Pengembalian Dana'");
                                                while ($data_konten = $cek_konten->fetch_assoc()) {
                                                ?>
                                                    <div class="card">
                                                        <div class="card-header" id="heading<?php echo $data_konten['number']; ?>1">
                                                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapse<?php echo $data_konten['number']; ?>1" aria-expanded="false" aria-controls="collapse<?php echo $data_konten['number']; ?>1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                        <polygon points="0 0 24 0 24 24 0 24"/>
                                                                        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
                                                                        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
                                                                    </g>
                                                                </svg>
                                                                <?php echo $data_konten['title']; ?>
                                                            </div>
                                                        </div>
                                                        <div id="collapse<?php echo $data_konten['number']; ?>1" class="collapse" aria-labelledby="heading<?php echo $data_konten['number']; ?>1" data-parent="#accordionExample1">
                                                            <div class="card-body">
                                                                <?php echo $data_konten['konten']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Page Faq Tab Reffund -->
            
                    <!-- Start Page Faq Tab Status -->
                    <div role="tabpanel" class="tab-pane fade" id="status">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="kt-portlet kt-portlet--height-fluid kt-sc-2">
                                    <div class="kt-portlet__body">
                                        <div class="kt-infobox">
                                            <div class="kt-infobox__header">
                                                <h2 class="kt-infobox__title">Status</h2>
                                            </div>
                                            <div class="kt-infobox__body">
                                                <div class="accordion accordion-light accordion--no-bg accordion-svg-icon" id="accordionExample1">
                                                <?php         
                                                $cek_konten = $conn->query("SELECT * FROM pertanyaan_umum WHERE tipe = 'Status'");
                                                while ($data_konten = $cek_konten->fetch_assoc()) {
                                                ?>
                                                    <div class="card">
                                                        <div class="card-header" id="heading<?php echo $data_konten['number']; ?>1">
                                                            <div class="card-title collapsed" data-toggle="collapse" data-target="#collapse<?php echo $data_konten['number']; ?>1" aria-expanded="false" aria-controls="collapse<?php echo $data_konten['number']; ?>1">
                                                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                        <polygon points="0 0 24 0 24 24 0 24"/>
                                                                        <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" fill="#000000" fill-rule="nonzero"/>
                                                                        <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) "/>
                                                                    </g>
                                                                </svg>
                                                                <?php echo $data_konten['title']; ?>
                                                            </div>
                                                        </div>
                                                        <div id="collapse<?php echo $data_konten['number']; ?>1" class="collapse" aria-labelledby="heading<?php echo $data_konten['number']; ?>1" data-parent="#accordionExample1">
                                                            <div class="card-body">
                                                                <?php echo $data_konten['konten']; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Page Faq Tab Status -->

                </div>
                <!-- End Tab Content -->
                
                </div>

            </div>
	        <!-- End Page Faq Tab -->

	    </div>
	    
		<br>
		
	    <div class="clearfix"></div>
	
		<?php
		include("footer_frontsite.php");
		?>
		
		<script>
		 var typed3 = new Typed('#seciontwo', {
		    strings: ['FAQ', 'EKTPKu'],
		    typeSpeed: 50,
		    backSpeed: 50,
		    loop: true
		  });
		</script>

		</body>

</html>