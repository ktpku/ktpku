<?php
require '../config.php';
require '../lib/database.php';
include("header_frontsite.php");
?>

		<section class="section overlay section_two">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-12">
						<div class="box-wrapper">
							<span id="seciontwo"></span> 
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="section overlay section_three padding-on-body-mobile">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-12 col-lg-12">
						<div class="box-wrapper">
							<h2>Sekarang eKTP Kamu Bisa <div></div></h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-4 col-lg-4">
						<div class="box-wrapper">
							<img src="https://www.flaticon.com/svg/static/icons/svg/850/850053.svg" class="w-100" />
							<p>Isi Saldo</p>
						</div>
					</div>
					<div class="col-4 col-lg-4">
						<div class="box-wrapper">
							<img src="https://www.flaticon.com/svg/static/icons/svg/852/852237.svg" class="w-100" />
							<p>Transfer Antar eKTP</p>
						</div>
					</div>
					<div class="col-4 col-lg-4">
						<div class="box-wrapper">
							<img src="https://www.flaticon.com/svg/static/icons/svg/844/844255.svg" class="w-100" />
							<p>Transaksi Digital & Non-Digital</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="section overlay section_two">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-10 offset-lg-1">
						<h4 class="mb-0">KTPKu adalah layanan uang elektronik berbasis eKTP pertama di indonesia. Lewat KTPKu, kamu bisa melakukan transaksi apapun dengan lebih cepat, praktis dan aman.</h4>
					</div>
				</div>
			</div>
		</section>
		
	<div class="clearfix"></div>
	
		<?php
		include("footer_frontsite.php");
		?>
		
		<script>
		 var typed3 = new Typed('#seciontwo', {
		    strings: ['Tentang', 'EKTPKu'],
		    typeSpeed: 50,
		    backSpeed: 50,
		    loop: true
		  });
		</script>

		</body>

</html>