<?php
require '../config.php';
require '../lib/database.php';
include("header_frontsite.php");
?>
		
	<section class="section overlay section_two">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-12">
					<div class="box-wrapper">
						<span id="seciontwo"></span> 
					</div>
				</div>
			</div>
		</div>
	</section>
	
        <!-- Start Content -->
        <div class="kt-container kt-grid__item kt-grid__item--fluid mt-5">

    		<style>
    		ul.nav.nav-tabs.nav-tabs-line {
    		    display: block;
    		}
    		.nav-tabs.nav-tabs-line {border:none;}
    		.nav-tabs.nav-tabs-line .nav-item:last-child {margin-right: 20px;}
    		.nav-tabs.nav-tabs-line .nav-item {margin-left: 20px;}
    		</style>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="kt-portlet">
			        <div class="kt-portlet__body">


<p>Terima kasih Anda telah mengakses situs atau mengunduh, menggunakan layanan, produk dan/atau aplikasi (“Aplikasi”) KTPKu. Aplikasi KTPKu adalah layanan aplikasi uang elektronik yang diterbitkan oleh Kami. Pada saat Anda mengakses, mengunduh atau menggunakan Aplikasi KTPKu maka Syarat dan Ketentuan ini berlaku terhadap Anda. Dengan melanjutkan mengakses, mengunduh atau menggunakan Aplikasi KTPKu, Anda mengakui dan menyetujui bahwa Anda telah membaca dengan teliti, memahami, menerima dan menyetujui seluruh Syarat dan Ketentuan ini yang akan berlaku sebagai perjanjian antara Anda dengan Kami. Syarat dan Ketentuan ini juga berlaku setiap kali Anda melakukan transaksi menggunakan Aplikasi KTPKu melalui mitra penyedia platform lain yang menandatangani perjanjian kerjasama dengan Kami dan menggunakan Aplikasi KTPKu sebagai salah satu metode pembayaran (“Partner KTPKu”).</p>
<h4>A. Registrasi, Kode Verifikasi (OTP) dan Security Code</h4>
<p>- Registrasi</p>
<p>Untuk dapat menjadi pengguna Aplikasi KTPKu (“Pengguna KTPKu”), Anda harus melakukan registrasi (i) melalui Aplikasi KTPKu, atau (ii) Partner KTPKu.
Anda wajib memastikan kebenaran dan keakuratan setiap data, informasi dan/atau keterangan dalam bentuk apapun yang dari waktu ke waktu Anda sampaikan kepada Kami atau yang Anda cantumkan atau sampaikan dalam, pada atau melalui aplikasi Partner KTPKu (“Data”).
Registrasi Pengguna KTPKu baik melalui Aplikasi KTPKu atau melalui Partner KTPKu diwajibkan untuk memiliki nomor ponsel aktif yang dibuktikan dengan memasukkan kode verifikasi berupa kode One Time Password atau “OTP” yang dikirim kepada nomor ponsel tersebut.</p>
<p>- Kode Verifikasi (kode OTP)</p>
<p>Kode OTP akan dikirimkan melalui Layanan Pesan Singkat (Short Messaging Service / SMS) ke nomor telepon terdaftar ketika Pengguna KTPKu membuat akun KTPKu baru, masuk ke Aplikasi KTPKu, atau menghubungkan akun KTPKu dengan aplikasi Partner KTPKu.
Pengguna KTPKu wajib menjaga kerahasiaan kode OTP dan tidak membagikan kode OTP kepada siapa pun.</p>
<p>- Security Code</p>
<p>Saat proses registrasi akun baru, Pengguna KTPKu diwajibkan membuat Security Code.
Security Code dibuat oleh Pengguna KTPKu saat melakukan registrasi akun, yang terdiri dari 6 (enam) digit angka yang tidak boleh berulang atau berurutan.
Kami berhak untuk melakukan blokir sementara atas akun KTPKu apabila Pengguna KTPKu memasukkan Security Code yang salah sebanyak 5 (lima) kali berturut - turut.
Pengguna KTPKu wajib menjaga kerahasiaan Security Code dan tidak membagikannya kepada siapa pun.
Untuk beberapa smartphone tertentu, terdapat kapabilitas mengaktifkan biometric recognition sebagai pengganti security code. Segala hal yang terjadi dengan diaktifkannya fitur tersebut merupakan tanggung jawab Anda.</p>

<h4>B. Biaya Layanan</h4>
<p>Kami dapat mengenakan biaya dalam rangka penggunaan layanan pada Aplikasi KTPKu, termasuk namun tidak terbatas pada layanan pengisian saldo (top-up), pembayaran, transfer, redeem, dan/atau pencairan uang. Keterangan mengenai jumlah biaya yang dikenakan akan kami sediakan melalui media komunikasi, website, dan/atau media lain yang Kami tentukan dari waktu ke waktu.</p>

<h4>C. Klasifikasi Akun KTPKu</h4>
<p>Aplikasi KTPKu menawarkan 2 (dua) jenis klasifikasi akun Pengguna KTPKu dengan jenis layanan atau fitur-fitur yang berbeda. Klasifikasi akun tersebut adalah:</p>
<p>- KTPKu Premium</p>
<p>KTPKu Club adalah klasifikasi akun KTPKu unregistered sebagaimana diatur dalam Peraturan Bank Indonesia yang memungkinkan Pengguna KTPKu untuk dapat menikmati beberapa fitur layanan uang elektronik secara terbatas.
Untuk KTPKu Club, maksimum saldo KTPKu Cash adalah Rp 2.000.000 (dua juta Rupiah) dengan batas nilai transaksi dalam 1 (satu) bulan paling banyak Rp20.000.000 (dua puluh juta Rupiah), yang diperhitungkan dari transaksi yang bersifat incoming sebagaimana diatur dalam Peraturan Bank Indonesia.
Untuk dapat memanfaatkan fitur layanan lainnya pada Aplikasi KTPKu, maka Anda dapat meng-upgrade akun KTPKu Club Anda menjadi akun KTPKu Premier.</p>
<p>- KTPKu Gold</p>
<p>KTPKu Premier adalah klasifikasi akun KTPKu yang registered sebagaimana diatur dalam Peraturan Bank Indonesia memungkinkan Pengguna KTPKu untuk dapat menikmati fasilitas uang elektronik secara lebih luas dibandingkan KTPKu Club.
Untuk KTPKu Premier, maksimum saldo KTPKu Cash adalah Rp 10.000.000 (sepuluh juta Rupiah) dengan batas nilai transaksi dalam 1 (satu) bulan paling banyak Rp 20.000.000 (dua puluh juta Rupiah), yang diperhitungkan dari transaksi yang bersifat incoming.
Pengguna KTPKu Premier dapat memanfaatkan seluruh layanan yang disediakan oleh Kami saat ini dan/atau yang akan ada di kemudian hari, termasuk namun tidak terbatas pada fitur transfer, layanan budgeting, redeem dan/atau pencairan uang, dan layanan lainnya.</p>
<p>Prosedur untuk meng-upgrade akun KTPKu Club menjadi KTPKu Premier:</p>
<p>Pengguna KTPKu dapat melakukan upgrade melalui prosedur Know Your Customer (KYC) melalui metode/tempat yang ditentukan oleh KTPKu.</p>
<p>Untuk melakukan upgrade, Pengguna KTPKu wajib menyampaikan kartu identitas yang masih berlaku dan mengisi spesimen yang disediakan oleh Kami, dan apabila diperlukan melakukan swafoto (selfie) dengan kartu identitas yang masih berlaku melalui media yang diatur oleh Kami.</p>
<p>Kartu identitas yang digunakan untuk melakukan upgrade akun adalah Kartu Tanda Penduduk (KTP) yang masih berlaku (untuk WNI) dan/atau paspor yang masih berlaku (untuk WNA).
Semua data yang terdapat di kartu identitas tersebut harus dapat terbaca untuk mempermudah verifikasi Kami.</p>
<p>1 (satu) kartu identitas hanya dapat digunakan untuk 1 (satu) akun KTPKu.<p>
<p>Kami berhak sewaktu-waktu meminta Pengguna KTPKu untuk memberikan Data pendukung ketika melakukan upgrade dan Pengguna KTPKu setuju akan hal tersebut.
Kami berhak melakukan pengkinian data Pengguna KTPKu dalam hal termasuk namun tidak terbatas pada apabila Kami menemukan Data yang tidak berlaku lagi, dan Pengguna KTPKu setuju akan hal tersebut.
Apabila Pengguna KTPKu tidak berhasil melakukan pengkinian data di jangka waktu yang diberikan KTPKu dan apabila KTPKu menemukan Data yang tidak berlaku, atau tidak sesuai, KTPKu berhak untuk meng-downgrade akun pengguna tersebut dari status Premier ke Club.
Kami berhak menolak permintaan upgrade Pengguna KTPKu, jika Data yang diberikan tidak sesuai dengan Syarat dan Ketentuan Kami.</p>

<h4>D. KTPKu CASH</h4>
<p>KTPKu Cash adalah saldo uang elektronik yang terdapat di dalam akun Anda yang dapat Anda gunakan untuk berbagai macam transaksi melalui layanan yang tersedia di Aplikasi KTPKu.
Pengguna dapat melakukan top-up (atau menambah) saldo KTPKu Cash melalui media-media top-up resmi yang Kami sediakan dan diinformasikan melalui situs resmi KTPKu dan/atau media komunikasi lainnya.
Kami tidak bertanggung jawab atas keberhasilan dan/atau kegagalan top-up saldo yang dilakukan melalui media-media top-up, seperti bank, Partner KTPKu, dan/atau media-media top-up lainnya.</p>

<h4>E. KTPKu POINTS</h4>
<p>Layanan belum tersedia.</p>

<h4>F. AKUN TIDAK AKTIF (DORMANT ACCOUNT)</h4>
<p>“Akun Tidak Aktif” atau Dormant Account adalah akun yang tidak terdapat transaksi keuangan selama 6 (enam) bulan.</p>
<p>Pengguna KTPKu dapat mengaktifkan kembali Akun Tidak Aktif, dengan cara melakukan 1 kali transaksi selambat-lambatnya dalam waktu 1 (satu) bulan sejak tanggal dinyatakan Akun Tidak Aktif. Akun Tidak Aktif tidak dapat melakukan transaksi finansial dan pembayaran di Partner KTPKu.</p>
<p>Segera setelah jangka waktu sesuai nomor (2) diatas berakhir, maka:</p>
<p>Jika Akun Saldo KTPKu Cash anda Rp 0 (nol Rupiah), kami akan melakukan penutupan atas Akun Tidak Aktif Anda tersebut secara permanen.
Jika anda masih mempunyai saldo KTPKu Cash, kami akan melakukan penonakifkan akun:
Setelah akun KTPKu sudah di non-aktifkan, KTPKu akan memberlakukan biaya pemeliharaan akun yang sudah di blokir karena Dormant Account yang akan diambil dari saldo KTPKu Cash yang tersisa di dalam akun KTPKu. Ketentuan mengenai biaya pemeliharaan akun akan ditentukan kemudian oleh kami sebagai bagian yang tidak terpisahkan dari Syarat dan Ketentuan ini.
Anda dapat mengklaim saldo KTPKu anda dengan cara menghubungi Customer Service KTPKu setelah akun anda dinonaktifkan.
Dalam jangka waktu 1 (satu) bulan tersebut, Anda juga dapat melakukan penarikan saldo KTPKu Cash dalam Akun Tidak Aktif Anda tersebut, dan Anda dapat juga menghubungi kami untuk meminta informasi lebih lanjut.</p>

<h4>G. PENONAKTIFAN AKUN (BLOKIR)</h4>
<p>Penonaktifan akun (blokir), baik secara sementara atau permanen atas akun KTPKu Premier atau Club Anda dapat terjadi, termasuk namun tidak terbatas pada kejadian-kejadian berikut ini:</p>
<p>Permintaan blokir dari otoritas, instansi pemerintahan atau kepolisian;</p>
<p>Perintah pengadilan;</p>
<p>Laporan Anda bahwa telepon genggam Anda telah hilang, diretas, atau dicuri yang menyebabkan akun KTPKu Anda diretas;</p>
<p>Sebagian atau seluruh data yang Anda sampaikan tidak benar, tidak lengkap, palsu, fiktif dan/atau menyesatkan;</p>
<p>Terjadi penggunaan abusif atau indikasi transaksi yang mencurigakan;</p>
<p>Tindakan yang tidak sesuai dengan Syarat dan Ketentuan ini, Kebijakan Privasi, dan/atau syarat dan ketentuan lainnya yang terkait; atau
Tindakan atau transaksi yang merugikan Kami atau Anda.</p>
<p>Pihak lain yang melaporkan adanya tindakan mencurigakan dari akun tersebut disertai dengan bukti-bukti yang dianggap cukup untuk keperluan investigasi.</p>
<p>Berdasarkan penilaian dan kewenangan kami sendiri atas validitas akun Anda.</p>
<p>Dalam hal akun Anda telah diblokir sementara dan berdasarkan bukti pendukung yang kuat yang Anda berikan tidak terdapat hal yang mencurigakan, Anda dapat menghubungi Kami sesuai ketentuan Syarat dan Ketentuan ini. Kami akan melakukan pemeriksaan lebih lanjut untuk menentukan apakah akan mengakhiri atau meneruskan pemblokiran akun Anda tersebut.</p>
<p>Kami berhak untuk menonaktifkan secara permanen atau sementara akses Anda ke akun KTPKu Anda atau platform Partner KTPKu apabila ditemukan terjadinya indikasi penipuan atau kejahatan, baik yang Kami temukan atau atas bukti pendukung yang kuat yang diajukan oleh pihak ketiga atas akun Anda.</p>
<p>Di samping kami berwenang untuk melakukan penonaktifan (blokir) akun Anda berdasarkan salah satu atau lebih hal yang disebutkan dalam poin (1) di atas, kami juga berhak untuk melakukan penurunan status (downgrade) akun KTPKu Premier anda menjadi KTPKu Club, sesuai dengan ketentuan yang berlaku.</p>

<h4>H. PENUTUPAN AKUN</h4>
<p>Kami berhak untuk menutup akun Anda, di mana untuk jangka waktu tertentu atau untuk seterusnya berdasarkan pertimbangan atau keputusan Kami yang Kami anggap baik, apabila terjadinya satu atau lebih kondisi di bawah ini:
Permintaan Anda sendiri selaku pemilik akun, dengan ketentuan Anda telah menyelesaikan segala kewajiban Anda kepada Kami;</p>
<p>1 (satu) bulan setelah akun Anda dinyatakan Akun Tidak Aktif (Dormant Account);</p>
<p>Anda meninggal dunia atau perusahaan, badan usaha, badan hukum atau persekutuan perdata yang Anda wakili dinyatakan atau dimohonkan pailit atau likuidasi atau berada dalam kondisi pailit atau likuidasi;</p>
<p>Akun atau Transaksi dalam akun Anda dinyatakan fraud (berdasarkan hasil investigasi Kami sendiri dan/atau dengan pihak kepolisian, dan/atau pihak ketiga);</p>
<p>Keputusan Kami berdasarkan hukum dan peraturan perundang-undangan yang berlaku;</p>
<p>Tindak lanjut sehubungan dengan blokir atau penonaktifan akun berdasarkan poin (G) di atas.</p>
<p>Penutupan akun di atas dilakukan dengan telah menyelesaikan kewajiban dan biaya yang timbul karenanya.</p>
<p>Jika akun KTPKu Anda ditutup berdasarkan angka 1 di atas, maka setelah verifikasi dilakukan Kami dapat melakukan tindakan-tindakan penahanan, pemotongan, atau pendebetan saldo KTPKu Points dan/atau KTPKu Cash dalam akun Anda. Penahanan, pemotongan, atau pendebetan saldo dalam akun Anda dilakukan berdasarkan pertimbangan kami dan ketentuan yang berlaku, dan juga termasuk namun tidak terbatas untuk pemenuhan seluruh kewajiban pembayaran Anda kepada Kami, pembayaran biaya administrasi penutupan Akun dalam jumlah yang akan ditentukan oleh Kami dari waktu ke waktu atau biaya terutang lainnya (apabila ada) di akun KTPKu Anda. Anda dengan ini memberikan kuasa, hak dan wewenang penuh yang tidak dapat dicabut kembali dan tanpa syarat kepada Kami dengan hak substitusi, untuk sewaktu-waktu, tanpa memerlukan pemberitahuan terlebih dahulu kepada Anda dan tanpa persetujuan Anda terlebih dahulu.</p>

<h4>I. KERJASAMA DENGAN PIHAK KETIGA</h4>
<p>Sehubungan dengan program kerja sama loyalitas (loyalty program), saat ini Kami memiliki kerja sama dengan Hypermart dan Matahari Department Store dengan tunduk kepada Syarat dan Ketentuan ini serta syarat dan ketentuan lain yang diberlakukan oleh Hypermart dan Matahari Department Store.
Sehubungan dengan beberapa layanan dan produk antara lain produk asuransi, reksadana dan investasi, Kami menunjuk dan memberikan kewenangan kepada mitra kami, KTPKu Indonesia untuk dapat menjalankan, mengelola, dan mengoperasikan widget atau konten khusus yang terdapat dalam Aplikasi KTPKu; termasuk namun tidak terbatas pada widget atau konten khusus dalam menawarkan produk asuransi, reksadana, dan investasi. Selanjutnya, Anda juga setuju bahwa Kami dapat membagikan data Anda untuk digunakan oleh KTPKu Indonesia sebagai pengelola layanan untuk tujuan-tujuan tertentu; termasuk namun tidak terbatas pada menyimpan, melakukan segmentasi dan analisis terhadap data Anda untuk dapat memproses dan meneruskan permintaan Anda (sebagaimana relevan).
Anda diminta untuk membaca dengan saksama dan menyetujui setiap instruksi yang tersedia, untuk dapat menerima informasi dan layanan lebih lanjut, termasuk namun tidak terbatas dalam hal menerima penawaran dan meneruskan layanan yang terdapat di masing-masing widget atau konten tersebut.
Pada saat Anda telah berhasil mengakses widget atau konten khusus tersebut, Anda mengakui dan setuju bahwa Anda menjadi pengguna layanan yang disediakan oleh KTPKu Indonesia dan tunduk kepada syarat dan ketentuan yang akan diberlakukan oleh KTPKu Indonesia (sebagaimana relevan).</p>

<h4>J. KETENTUAN KHUSUS PADA WIDGET ATAU KONTEN KHUSUS DALAM APLIKASI KTPKu</h4>
<p>Dalam hal Anda telah berhasil mengakses widget atau konten khusus yang dijalankan mitra kami sebagaimana ayat (2) pada huruf I di atas, kami tidak memberikan jaminan bahwa pembelian produk atau layanan akan disetujui oleh Kami atau mitra Kami atau pihak yang bekerjasama dengan mitra Kami, yang memiliki wewenang untuk menerima, melanjutkan, menolak, dan/atau membatalkan permintaan Anda.
Anda diminta untuk mempelajari seluruh ketentuan dan/atau manfaat dari produk dan/atau layanan yang ditawarkan, Kami, mitra Kami, dan pihak yang bekerjasama dengan mitra Kami tidak bertanggung jawab atas kesalahan pemilihan atau pembelian produk dan/atau layanan tersebut, yang sepenuhnya disebabkan oleh kelalaian atau ketidaktelitian Anda.
Apabila diperlukan, mitra Kami atau pihak yang bekerjasama dengan mitra Kami dapat menerapkan syarat dan ketentuan khusus yang akan disampaikan kepada Anda, supaya Anda dapat menerima dan/atau melanjutkan produk atau layanan yang Anda pilih.</p>

<h4>K. HAK MELAKUKAN KOREKSI</h4>
<p>Kami berhak untuk melakukan koreksi atas saldo dan/atau transaksi pada akun Anda, apabila menurut catatan Kami telah terjadi transaksi tidak wajar di akun Anda. Koreksi tersebut termasuk atas pertimbangan dan keyakinan Kami yang wajar untuk melakukan penahanan sebagian atau seluruh dan penarikan atas saldo akun KTPKu Anda berhubungan dengan atau disebabkan oleh penipuan, penggelapan, penyalahgunaan, dan/atau tindakan pelanggaran hukum lainnya.
Anda dengan ini memberikan kuasa, hak dan wewenang penuh yang tidak dapat dicabut kembali dan tanpa syarat kepada Kami dengan hak substitusi, untuk sewaktu-waktu, tanpa memerlukan pemberitahuan terlebih dahulu kepada Anda dan tanpa persetujuan Anda terlebih dahulu untuk melakukan koreksi atas saldo akun KTPKu Anda.</p>

<h4>L. KEWAJIBAN, PERNYATAAN DAN JAMINAN</h4>
<p>Anda hanya dapat mengakses atau menggunakan Aplikasi KTPKu (a) sesuai dengan Syarat dan Ketentuan ini, (b) untuk tujuan yang sah, dan (c) tidak digunakan untuk tujuan atau tindakan penipuan, pelanggaran hukum, kriminal maupun tindakan, aktivitas, perbuatan atau tujuan lain yang melanggar atau bertentangan dengan hukum, peraturan perundang-undangan yang berlaku maupun hak atau kepentingan pihak manapun. Anda bertanggung jawab penuh untuk memeriksa dan memastikan bahwa Anda telah mengunduh (download) perangkat lunak yang benar untuk perangkat Anda. Kami tidak bertanggung jawab jika Anda tidak memiliki perangkat yang kompatibel dengan Aplikasi KTPKu atau jika Anda telah mengunduh (download) versi software yang salah untuk perangkat Anda.</p>
<p>Anda dilarang untuk menggunakan Aplikasi KTPKu atau melakukan Transaksi: (a) untuk tujuan, kegiatan, aktivitas atau aksi yang melanggar hukum atau melanggar hak atau kepentingan (termasuk Hak Kekayaan Intelektual atau hak privasi) milik pihak manapun; (b) yang memiliki materi atau unsur yang berbahaya atau yang merugikan pihak manapun; (c) yang mengandung virus software, worm, trojan horses atau kode komputer berbahaya lainnya, file, script, agen atau program; dan (d) yang mengganggu integritas atau kinerja Aplikasi KTPKu dan sistem pendukungnya.</p>
<p>Anda dilarang untuk melakukan tindakan apapun termasuk dalam atau melalui Aplikasi KTPKu yang dapat merusak atau mengganggu reputasi Kami.</p>
<p>Anda diwajibkan untuk memastikan tidak memberitahukan informasi keamanan Anda kepada pihak lain. Setiap perintah Transaksi yang dibuat melalui akun KTPKu Anda dari Aplikasi KTPKu atau Partner akan dianggap telah diotorisasi oleh Anda seperti penggunaan pin dan/atau biometric recognition untuk beberapa smartphone tertentu, dan merupakan perintah Anda untuk melakukan Transaksi. Anda akan menanggung segala kerugian yang ditimbulkan karena kelalaian Anda dan Kami tidak akan bertanggung jawab atas kelalaian Anda tersebut.</p>
<p>Anda dengan ini secara tegas menyetujui serta menyatakan dan menjamin bahwa:</p>
<p>Anda adalah individu yang secara hukum cakap untuk melakukan tindakan hukum berdasarkan hukum negara Republik Indonesia termasuk untuk mengikatkan diri dalam Syarat dan Ketentuan ini. Jika anda di bawah usia 18 (delapan belas) tahun atau di bawah pengampuan, Anda menjamin bahwa pembukaan akun KTPKu telah disetujui oleh orang tua, wali atau pengampu Anda yang sah.</p>
<p>Anda memiliki hak, wewenang dan kapasitas untuk menggunakan Aplikasi KTPKu serta untuk mematuhi dan melaksanakan seluruh Syarat dan Ketentuan ini.
Jika Anda melakukan pendaftaran atau mengunduh Aplikasi atas nama suatu badan hukum, persekutuan perdata atau pihak lain, Anda dengan ini menyatakan dan menjamin bahwa Anda memiliki kapasitas, hak dan wewenang yang sah untuk bertindak untuk dan atas nama badan usaha atau pihak lain tersebut termasuk tetapi tidak terbatas pada mengikat badan usaha atau pihak lain tersebut untuk tunduk pada seluruh isi Syarat dan Ketentuan ini.</p>
<p>Anda menyatakan dan menjamin bahwa dana yang dipergunakan dalam rangka transaksi bukan dana yang berasal dari tindak pidana yang dilarang berdasarkan peraturan perundang-undangan yang berlaku di Republik Indonesia, pembukaan rekening ini tidak dimaksudkan dan/atau ditujukan dalam rangka upaya melakukan tindak pidana pencucian uang sesuai dengan ketentuan peraturan perundang-undangan yang berlaku, transaksi tidak dilakukan untuk maksud mengelabui, mengaburkan, atau menghindari pelaporan kepada Pusat Pelaporan dan Analisis Transaksi Keuangan (PPATK) berdasarkan ketentuan peraturan perundang-undangan yang berlaku, dan Anda bertanggung jawab sepenuhnya serta melepaskan Kami dari segala tuntutan, klaim, atau ganti rugi dalam bentuk apapun, apabila Anda melakukan tindak pidana pencucian uang berdasarkan ketentuan peraturan perundang-undangan yang berlaku.</p>
Seluruh Data baik yang telah Anda sampaikan atau cantumkan maupun yang akan Anda sampaikan atau cantumkan baik langsung maupun tidak langsung di kemudian hari atau dari waktu ke waktu adalah benar, lengkap, akurat terkini dan tidak menyesatkan serta tidak melanggar hak (termasuk tetapi tidak terbatas pada Hak Kekayaan Intelektual) atau kepentingan pihak manapun. Penyampaian Data oleh Anda kepada Kami atau pada atau melalui Aplikasi atau sistem tidak bertentangan dengan hukum yang berlaku serta tidak melanggar akta, perjanjian, kontrak, kesepakatan atau dokumen lain di mana Anda merupakan pihak atau di mana Anda atau aset Anda terikat.
Aplikasi KTPKu akan digunakan untuk kepentingan Anda sendiri atau untuk kepentingan badan usaha atau pihak lain yang secara sah Anda wakili sebagaimana dimaksud huruf (c) di atas.
<p>Anda tidak akan memberikan hak, wewenang dan/atau kuasa dalam bentuk apapun dan dalam kondisi apapun kepada orang atau pihak lain untuk menggunakan Data, akun KTPKu, OTP dan/atau Security Code, dan Anda karena alasan apapun dan dalam kondisi apapun tidak akan dan dilarang untuk mengalihkan akun KTPKu kepada orang atau pihak manapun.
Dalam atau pada saat menggunakan Aplikasi KTPKu, Anda setuju untuk mematuhi dan melaksanakan seluruh ketentuan hukum dan peraturan perundang-undangan yang berlaku termasuk hukum dan peraturan perundang-undangan di negara asal Anda maupun di negara atau kota dimana Anda berada.
Dengan melaksanakan transaksi melalui Aplikasi KTPKu, Anda memahami bahwa seluruh komunikasi dan instruksi dari Anda yang diterima oleh Kami akan diperlakukan sebagai bukti solid meskipun tidak dibuat dalam bentuk dokumen tertulis atau diterbitkan dalam bentuk dokumen yang ditandatangani, dan, dengan demikian, Anda setuju untuk mengganti rugi dan melepaskan Kami dan rekanan-rekanan Kami dari segala kerugian, tanggung jawab, tuntutan dan pengeluaran (termasuk biaya litigasi) yang dapat muncul terkait dengan eksekusi dari instruksi Anda.</p>

<h4>M. PEMBATASAN TANGGUNG JAWAB</h4>
<p>Uang elektronik bukan merupakan simpanan sebagaimana dimaksud dalam peraturan perundang-undangan di bidang Perbankan sehingga tidak dijamin oleh Lembaga Penjamin Simpanan.</p>
<p>Layanan disediakan dalam kondisi “as is” atau “apa adanya”. Kami tidak memberikan pernyataan atau jaminan dalam bentuk apapun atas reliabilitas, keamanan, ketepatan waktu, kualitas, kesesuaian, ketersediaan, akurasi dan/atau kelengkapan layanan Aplikasi KTPKu. Kami tidak memberikan pernyataan atau jaminan dalam bentuk apapun bahwa:</p>
<p>penggunaan Aplikasi KTPKu (atau bagian daripadanya) akan aman, tepat waktu, tidak terganggu atau bebas dari kesalahan, gangguan, virus atau hambatan lain atau komponen berbahaya lainnya;</p>
<p>Aplikasi KTPKu dapat tetap beroperasi atau digunakan bersamaan dengan atau dengan kombinasi perangkat (baik perangkat keras maupun lunak) atau sistem pihak lain yang tidak kami sediakan atau miliki untuk pengoperasian Aplikasi KTPKu;</p>
<p>penyediaan Aplikasi KTPKu atau kelancaran bertransaksi atau penggunaan akun KTPKu akan memenuhi persyaratan atau harapan Anda;</p>
<p>setiap data transaksi yang Kami simpan atau tersimpan dalam Aplikasi KTPKu dan sistem penunjangnya adalah akurat atau benar;</p>
<p>kualitas layanan atau Aplikasi KTPKu akan memenuhi persyaratan atau harapan Anda; dan</p>
<p>tidak akan ada kesalahan, gangguan atau cacat dari Aplikasi KTPKu dan sistem penunjangnya.</p>
<p>Mengingat bahwa Kami hanya berperan sebagai penyedia platform, maka Kami tidak bertanggung jawab terhadap isi, bentuk, jenis dan/atau ketersediaan dari Layanan tersebut maupun hal-hal lain terkait dengan Layanan tersebut yang sepenuhnya merupakan tanggung jawab pihak penyedia Layanan.</p>
<p>Anda mengetahui dan setuju bahwa Kami juga berhak untuk memblokir, menon-aktifkan, menutup Akun, dan melakukan penurunan status (downgrade) apabila:</p>
<p>a. Kami memahami dan memiliki alasan yang memadai untuk menyatakan bahwa telah terjadi atau akan terjadi manipulasi keuangan atau perbankan atau kriminal yang terkait dengan Akun atau Rekening dan/atau layanan/fasilitas Pengguna KTPKu;</p>
<p>b. Pengguna KTPKu memberikan data yang tidak valid/tidak lengkap kepada Kami;</p>
<p>c. Terdapat permintaan tertulis dari instansi Kepolisian, Kejaksaan, Pengadilan, Pusat Pelaporan dan Analisis Transaksi Keuangan (PPATK), Kantor Pajak atau lembaga berwenang lainnya sesuai dengan hukum dan perundangan yang berlaku atau untuk memenuhi kewajiban/utang yang belum diselesaikan oleh Pengguna KTPKu.
Anda dengan ini mengetahui bahwa Layanan, Aplikasi, Akun dan/atau sistem mungkin atau dapat mengalami, terdapat atau terjadi pembatasan, keterlambatan, dan/atau masalah lain termasuk yang disebabkan karena atau sehubungan dengan (a) ketidaktersediaan atau terbatasnya jaringan (termasuk jaringan internet) dan/atau penggunaan atau (b) tidak tersedianya, terganggunya, atau tidak berfungsinya fitur tertentu pada perangkat yang Anda gunakan. Kami tidak bertanggung jawab atas segala keterlambatan, terhambatnya, tidak suksesnya, terganggunya atau gagalnya suatu Transaksi yang disebabkan karena hal tersebut di atas
Anda dengan ini mengetahui bahwa terdapat kemungkinan:</p>
<p>a. Sistem atau Aplikasi (atau bagian manapun dari sistem atau Aplikasi) tidak stabil, terganggu, terhenti, tidak berjalan dengan baik, tidak berjalan dengan sempurna dan/atau memiliki beberapa bug, dan/atau</p>
<p>b. Layanan (atau fitur-fitur atau bagian-bagian tertentu) dapat berubah, tidak tersedia,</p>
<p>atas terjadinya hal tersebut Anda setuju untuk tidak mengajukan Klaim kepada Kami.
Kami tidak bertanggung jawab atas setiap kerugian tidak langsung dan/atau kerugian immateriil, termasuk setiap biaya, keuntungan (termasuk bunga), kerusakan, dan kerugian dalam jenis atau bentuk apapun, yang Anda atau pihak ketiga lainnya alami, yang terjadi akibat;</p>
<p>a. kesalahan Partner KTPKu atau mitra Kami;</p>
<p>b. kesalahan merchant Kamu; dan/atau</p>
<p>c. kelalaian atau kesalahan pihak ketiga dan/atau Anda.</p>
<p>Kami hanya bertanggung jawab atas kerugian yang Anda alami secara langsung yang senyatanya dibayar, yaitu yang dapat dibuktikan terjadi akibat kesalahan Kami atau pelanggaran Kami atas Syarat dan Ketentuan ini. Jumlah maksimum yang akan Kami tanggung dalam kondisi apapun, yaitu hanya terbatas pada jumlah yang paling rendah antara nilai transaksi yang dimaksud (yang tercatat sesuai di sistem Kami) atau maksimum senilai Rp 10.000.000 (sepuluh juta Rupiah).
Kami tidak bertanggung jawab atas segala klaim dari pihak manapun termasuk Anda maupun atas kerugian Anda serta pihak manapun yang terjadi sebagai akibat dari atau sehubungan dengan tuntutan maupun gugatan yang dialami oleh Anda yang mungkin timbul sebagai akibat penyampaian informasi dari Anda yang tidak lengkap atau akibat tidak dilaksanakannya instruksi Anda, antara lain pembatalan, perubahan instruksi (untuk instruksi yang belum dijalankan) yang disampaikan kepada Kami, kecuali jika kerugian tersebut terjadi akibat kesalahan Kami yang disengaja atau kelalaian Kami.
Anda dengan ini setuju dan mengikatkan diri untuk membebaskan Kami dari setiap dan seluruh klaim dalam bentuk apapun, dari pihak manapun dan dimanapun yang diajukan, timbul atau terjadi sehubungan dengan atau sebagai akibat dari:
penggunaan Data oleh Kami berdasarkan Syarat dan Ketentuan ini atau berdasarkan persetujuan, pengakuan, wewenang, kuasa dan/atau hak yang Anda berikan baik secara langsung maupun tidak langsung kepada Kami dalam Syarat dan Ketentuan ini;
pemberian Data baik secara langsung maupun tidak langsung oleh Anda kepada Kami atau dalam atau melalui Aplikasi KTPKu yang Anda lakukan secara (i) melanggar atau melawan hukum atau peraturan perundang-undangan yang berlaku, (ii) melanggar hak (termasuk hak kekayaan intelektual) dari atau milik orang atau pihak manapun, atau (iii) melanggar kontrak, kerjasama, kesepakatan, akta, pernyataan, penetapan, keputusan dan/atau dokumen apapun dimana Anda merupakan pihak atau dimana Anda atau aset Anda terikat;
penggunaan Aplikasi, Akun dan/atau Layanan (i) secara tidak sah, (ii) melanggar hukum yang berlaku, (iii) melanggar Syarat dan Ketentuan ini, dan/atau (iv) untuk tindakan atau tujuan penipuan, kriminal, tindakan tidak sah atau tindakan pelanggaran hukum lainnya.
Sebagai Pengguna KTPKu, Anda bertanggung jawab untuk selalu menjaga kerahasiaan akun Anda, termasuk namun tidak terbatas pada; kerahasiaan sandi, kode OTP, dan Security Code yang Anda gunakan untuk login dan mengakses akun Anda dan layanan-layanan Kami. Oleh karena itu, Kami tidak bertanggung jawab dalam bentuk apapun apabila, Anda mengalami kerugian yang disebabkan oleh kelalaian Anda dalam menjaga kerahasiaan akun Anda, dan dengan ini Anda membebaskan Kami, direksi, dewan komisaris, karyawan, agen, dan/atau afiliasi Kami, dari setiap tuntutan, gugatan, ganti rugi, dan/atau klaim yang Anda ajukan sehubungan dengan penggunaan dan/atau akses yang tidak sah terhadap Akun Anda.
Anda dengan ini setuju dan mengikatkan diri untuk mengganti seluruh kerugian yang Kami alami dan mengganti seluruh biaya, ongkos, beban dan pengeluaran yang telah atau mungkin akan Kami keluarkan atau bayarkan sehubungan dengan atau sebagai akibat dari Klaim Anda atas ketentuan “Pembatasan Tanggung Jawab” ini (termasuk tetapi tidak terbatas pada biaya jasa hukum yang kami bayarkan atau keluarkan untuk melakukan pembelaan atau tindakan lain yang diperlukan terkait dengan Klaim tersebut).
Apabila ada hal yang ingin Anda sampaikan sehubungan dengan bagian “Pembatasan Tanggung Jawab” ini, Anda dapat menghubungi Kami</p>

<h4>N. RISIKO PENGGUNAAN</h4>
<p>Dengan menggunakan KTPKu, Anda dianggap telah mengerti atas segala risiko dari penggunaan uang elektronik, seperti pencurian saldo, pengambilalihan akun, dan penipuan. Oleh karena itu, kembali Kami ingatkan untuk selalu berhati-hati dalam menjaga akun Anda dari segala tindak kejahatan dengan:
Mengetahui bahwa Kami tidak pernah meminta security code maupun kode OTP kepada Anda. Oleh karena itu, Anda harus selalu berhati-hati dan tidak merespon segala bentuk panggilan maupun tautan yang dikirim melalui e-mail ataupun website (situs palsu menduplikasi situs resmi KTPKu) yang ditujukan untuk percobaan penipuan yang mengatasnamakan KTPKu yang meminta Anda untuk memberikan security code dan kode OTP.
Hanya menghubungi Contact Channel atau Layanan Pengguna KTPKu resmi kami seperti yang tertera dalam Syarat dan Ketentuan ini untuk menyampaikan pesan anda, dan tidak merespon kepada customer service di luar yang disebutkan dalam Syarat dan Ketentuan ini.
Jika terjadi kehilangan akses terhadap akun dan perangkat yang anda gunakan, untuk langsung menghubungi layanan resmi kami dan melaporkan kejadian yang terjadi.
Hanya melakukan transfer kepada sesama pengguna jika Anda yakin bahwa akun yang ditujukan adalah benar, jika tidak, transfer lebih baik dilakukan melalui akun bank.
Selalu perhatikan situasi di sekitar Anda saat hendak melakukan login dengan memasukkan security code Anda. Jika Anda berpikir bahwa situasi sekitar tidak aman, Anda dapat melakukan login dengan menggunakan sidik jari.
Selalu melakukan update untuk aplikasi KTPKu Anda, sehingga sistem keamanannya selalu terjaga.
Hanya melakukan pengisian saldo (top-up) akun KTPKu Anda melalui mitra dan/atau agen resmi yang Kami sediakan, termasuk melalui agen bank tertentu maupun pihak ketiga lain yang kami informasikan. Layanan ini mungkin dikenakan biaya tertentu dan berbeda tergantung dari setiap layanannya.</p>

<h4>O. PERLINDUNGAN DAN KERAHASIAAN DATA</h4>
<p>Anda setuju bahwa pengumpulan, pemanfaatan dan penyerahan Data yang Anda sampaikan kepada Kami akan dilakukan dengan tunduk kepada Syarat dan Ketentuan ini dan kebijakan privasi yang terdapat di: www.ktpku.com/dashboard/ketentuan-layanan yang menjadi satu kesatuan yang tak terpisahkan dengan Syarat dan Ketentuan ini.</p>

<h4>P. HAK KEKAYAAAN INTELEKTUAL</h4>
<p>Seluruh Aplikasi KTPKu dan sistem pendukungnya termasuk tetapi tidak terbatas pada seluruh: (a) layout, desain dan tampilan Aplikasi KTPKu yang terdapat dalam atau ditampilkan pada media Aplikasi KTPKu; (b) logo, foto, gambar, nama, merek, kata, huruf-huruf, angka-angka, tulisan, dan susunan warna yang terdapat dalam Aplikasi KTPKu; dan (c) kombinasi dari unsur-unsur sebagaimana dimaksud dalam huruf (a) dan (b), sepenuhnya merupakan Hak Kekayaan Intelektual milik Kami dan tidak ada pihak lain yang turut memiliki hak atas Aplikasi maupun atas layout, desain dan tampilan Aplikasi.</p>
<p>Anda tidak diperkenankan dari waktu ke waktu untuk (i) menyalin, memodifikasi, mengadaptasi, menerjemahkan, membuat karya turunan dari, mendistribusikan, menjual, mengalihkan, menampilkan di muka umum, membuat ulang, mentransmisikan, memindahkan, menyiarkan, menguraikan, atau membongkar bagian manapun dari atau dengan cara lain mengeksploitasi Aplikasi KTPKu (termasuk sistem pendukung dan layanan di dalamnya) yang dilisensikan kepada Anda, kecuali sebagaimana diperbolehkan dalam Syarat dan Ketentuan ini, (ii) memberikan lisensi, mensublisensikan, menjual, menjual kembali, memindahkan, mengalihkan, mendistribusikan atau mengeksploitasi secara komersial atau membuat tersedia kepada pihak ketiga dan/atau perangkat lunak dengan cara; (iii) menerbitkan, mendistribusikan atau memperbanyak dengan cara apapun materi yang dilindungi hak cipta, merek dagang, atau informasi yang Kami miliki lainnya tanpa memperoleh persetujuan terlebih dahulu dari Kami atau pemilik hak yang memberikan lisensi hak-nya kepada Kami, (iv) menghapus setiap hak cipta, merek dagang atau pemberitahuan hak milik lainnya yang terkandung dalam Aplikasi KTPKu, (v) merekayasa ulang atau mengakses Aplikasi KTPKu dan/atau sistem pendukungnya untuk (a) membangun produk atau layanan tandingan, (b) membangun produk dengan menggunakan ide, fitur, fungsi atau grafis sejenis, atau (c) menyalin ide, fitur , fungsi atau grafis, (vi) meluncurkan program otomatis atau script, termasuk, namun tidak terbatas pada, web crawlers, web robots, web indexers, bots, virus, worm, atau aplikasi lain sejenis dan segala program apapun yang mungkin membuat beberapa permintaan server per detik, menciptakan beban berat atau menghambat operasi dan/atau kinerja Aplikasi KTPKu, (vii) menggunakan robot, spider, pencarian situs/aplikasi pengambilan kembali, atau perangkat manual atau otomatis lainnya atau proses untuk mengambil, indeks, "tambang data" (data mine), atau dengan cara apapun memperbanyak atau menghindari struktur navigasi atau presentasi dari Aplikasi KTPKu dan isi yang terdapat di dalamnya.</p>

<h4>Q. KEADAAN KAHAR (FORCE MAJEURE)</h4>
<p>Tidak menutup kemungkinan Kami tidak dapat melaksanakan, melanjutkan atau meneruskan Transaksi maupun perintah atau instruksi dari Anda melalui Aplikasi atau Akun, baik sebagian maupun seluruhnya, yang disebabkan karena kejadian-kejadian atau hal-hal di luar kekuasaan atau kemampuan Kami termasuk namun tidak terbatas pada:</p>
<p>a. segala gangguan virus komputer</p>
<p>b. sistem trojan horses;</p>
<p>c. komponen atau sistem yang dapat membahayakan serta mengganggu Aplikasi, Layanan dan/atau Akun,</p>
<p>d. layanan atau jasa Internet Service Provider atau jasa atau layanan pihak ketiga lainnya yang tersedia dalam Layanan, dan/atau</p>
<p>e. bencana alam, perang, huru-hara, keadaan peralatan, sistem atau transmisi yang tidak berfungsi, gangguan listrik, gangguan telekomunikasi, kebijakan pemerintah, kegagalan sistem perbankan dan/atau keuangan serta kejadian-kejadian atau sebab-sebab lain diluar kekuasaan atau kemampuan Kami. Anda dengan ini setuju bahwa Kami akan dibebaskan dari segala tuntutan, jika Kami tidak dapat melaksanakan instruksi dari Anda, baik sebagian maupun sepenuhnya yang disebabkan oleh kejadian atau sebab Keadaan Kahar ini.</p>

<h4>R. HUKUM YANG BERLAKU DAN PENYELESAIAN PERSELISIHAN</h4>
<p>Syarat dan Ketentuan ini diatur dan ditafsirkan berdasarkan hukum Negara Republik Indonesia.</p>
<p>Segala perselisihan atau pertentangan yang timbul sehubungan dengan atau terkait dengan hal-hal yang diatur dalam Syarat dan Ketentuan (maupun bagian daripadanya) termasuk perselisihan yang disebabkan karena adanya atau dilakukannya perbuatan melawan hukum atau pelanggaran atas satu atau lebih Syarat dan Ketentuan ini (“Perselisihan”) wajib diselesaikan dengan cara sebagai berikut:
salah satu pihak baik Anda atau Kami (“Pihak Pertama”) wajib menyampaikan pemberitahuan tertulis kepada pihak lainnya (“Pihak Kedua”) atas telah terjadinya Perselisihan (“Pemberitahuan Perselisihan”). Perselisihan wajib diselesaikan secara musyawarah mufakat dalam waktu paling lambat 90 (sembilan puluh) hari kalender sejak tanggal Pemberitahuan Perselisihan Perselisihan (“Periode Penyelesaian Musyawarah”);
jika Perselisihan tidak dapat diselesaikan secara musyawarah mufakat sampai dengan berakhirnya Periode Penyelesaian Musyawarah, Pihak Pertama dan Pihak Kedua wajib untuk bersama-sama menunjuk pihak ketiga (“Mediator”) sebagai mediator untuk menyelesaikan Perselisihan dan penunjukan tersebut wajib dituangkan dalam bentuk tertulis yang ditandatangani bersama oleh Pihak Pertama dan Pihak Kedua.
proses mediasi oleh Mediator khusus akan diselesaikan oleh satu arbiter yang ditunjuk berdasarkan Peraturan Badan Arbitrase Nasional Indonesia yang beralamat di gedung Wahana Graha Lantai 2, Jl. Mampang Prapatan Nomor 2 Jakarta Selatan 12760, Republik Indonesia (BANI).
Ketentuan mengenai seluruh biaya, ongkos dan pengeluaran dalam rangka penyelesaian Perselisihan diputuskan berdasarkan putusan arbitrase yang final dan mengikat;
Kecuali disyaratkan berdasarkan hukum yang berlaku atau diminta berdasarkan permintaan, keputusan atau penetapan resmi yang diterbitkan, dikeluarkan atau dibuat oleh pengadilan atau instansi pemerintah yang berwenang, selama proses penyelesaian Perselisihan sebagaimana diatur di atas sampai dengan adanya keputusan yang sah, final dan mengikat Pihak Pertama dan Pihak Kedua, maka Pihak Pertama dan Pihak Kedua wajib untuk merahasiakan segala informasi terkait dengan Perselisihan maupun proses penyelesaiannya dan karenanya dilarang untuk dengan cara apapun menginformasikan, memberitahukan atau mengumumkan kepada pihak manapun adanya Perselisihan tersebut maupun proses penyelesaiannya termasuk tetapi tidak terbatas melalui media massa (koran, televisi atau media lainnya) dan/atau media sosial. Jika Anda melanggar ketentuan butir (e) ini, Anda dengan ini mengetahui dan setuju bahwa seluruh atau sebagian hak Anda untuk menggunakan Layanan, Aplikasi, Akun dan/atau PIN dapat sewaktu-waktu diakhiri atau di non-aktifkan oleh Kami baik untuk sementara waktu maupun untuk seterusnya.
Sehubungan dengan kuasa yang tidak ditarik kembali yang Anda berikan kepada Kami berdasarkan Syarat dan Ketentuan ini, kuasa tersebut akan terus berlaku dan tidak dapat berakhir karena alasan apapun juga termasuk alasan-alasan yang dimaksud dan diatur dalam Pasal 1813, 1814, dan 1816 Kitab Undang-Undang Hukum Perdata, kecuali dalam hal Anda menutup akun KTPKu Anda.</p>

<h4>S. MASA BERLAKU DAN PENGAKHIRAN</h4>
<p>Syarat dan Ketentuan ini berlaku sejak Anda menjadi Pengguna KTPKu dan tetap berlaku selama Anda belum melakukan penutupan akun KTPKu Anda atau Anda memiliki akun KTPKu yang aktif.</p>
<p>Anda sepakat dan mengikatkan diri untuk tidak melakukan tindakan apapun yang dapat membatasi, menghambat dan/atau mengurangi satu atau lebih hak dan/atau wewenang Kami berdasarkan Syarat dan Ketentuan ini maupun hukum yang berlaku.
Anda dengan ini sepakat untuk menyampingkan ketentuan Pasal 1266 ayat (2) dan (3) Kitab Undang-undang Hukum Perdata sehingga Syarat dan Ketentuan, Aplikasi dan/atau Layanan dapat diakhiri (baik sebagian maupun seluruhnya, baik sementara waktu maupun seterusnya) sesuai dengan Syarat dan Ketentuan tanpa diperlukan adanya keputusan atau penetapan dari hakim pengadilan.
Seluruh persetujuan, kuasa, wewenang dan/atau hak yang Anda berikan kepada Kami dalam Syarat dan Ketentuan ini tidak dapat berakhir karena alasan apapun termasuk karena alasan-alasan sebagaimana dimaksud dalam Pasal 1813, 1814, dan 1816 Kitab Undang-Undang Hukum Perdata selama Anda masih menggunakan Layanan atau Aplikasi atau masih menggunakan dan/atau memiliki Akun
Anda dengan ini membebaskan Kami dari seluruh Klaim sehubungan dengan pelaksanaan segala tindakan Kami berdasarkan kuasa, wewenang dan/atau hak yang Anda berikan kepada Kami dalam atau berdasarkan Syarat dan Ketentuan maupun pelaksanaan hak atau wewenang Kami berdasarkan Syarat dan Ketentuan.</p>

<h4>T. HUBUNGI KAMI</h4>
<p>Untuk setiap pengaduan terkait dengan Aplikasi KTPKu dan/atau akun Anda, atau pertanyaan mengenai Syarat dan Ketentuan ini Anda dapat meminta menghubungi Contact Channel atau Layanan Pengguna KTPKu (dengan menyertakan bukti pendukung), melalui:</p>
<p>e-mail ke admin@ktpku.id</p>
<p>Instagram (@ktpku_id)</p>
<p>Kami menghimbau Anda untuk segera melaporkan kepada kami melalui kanal-kanal di atas apabila Anda mengalami hal yang tidak biasa dan/atau di luar kewajaran atas Aplikasi KTPKu Anda untuk segera kami tindak lanjuti.
KTPKu akan menindaklanjuti setiap penerimaan pengaduan Anda sesuai dengan ketentuan yang berlaku.</p>
</p>Apabila terjadi perbedaan saldo pada aplikasi Anda dengan Kami, maka pengaduan akan kami tindak lanjuti sesuai dengan prosedur pengaduan di atas.</p>

<h4>U. PERUBAHAN</h4>
<p>Kami berhak untuk, dari waktu ke waktu, melakukan perubahan, penambahan, dan/atau modifikasi atas seluruh atau sebagian dari isi Syarat dan Ketentuan ini dan Kebijakan Privasi dengan diumumkan melalui aplikasi KTPKu dan/atau situs www.ktpku.com. Apabila Anda menggunakan layanan Aplikasi KTPKu secara terus-menerus dan berlanjut setelah perubahan, penambahan dan/atau modifikasi atas seluruh atau sebagian dari isi Syarat dan Ketentuan merupakan bentuk persetujuan Anda atas perubahan, penambahan dan/atau modifikasi tersebut. Apabila Anda tidak setuju atas perubahan, penambahan dan/atau modifikasi tersebut, Anda diminta berhenti mengakses dan/atau menggunakan Aplikasi KTPKu dan/atau Layanan Kami.</p>

<h4>V. TANGGAL BERLAKU</h4>
<p>Syarat dan Ketentuan ini berlaku sejak tanggal 01 November 2020.</p>


                    </div>                    
                </div>
                </div>
            </div>
            

	    </div>
	    
		<br>
		
	    <div class="clearfix"></div>
	
		<?php
		include("footer_frontsite.php");
		?>
		
		<script>
		 var typed3 = new Typed('#seciontwo', {
		    strings: ['Syarat', 'Dan', 'Ketentuan'],
		    typeSpeed: 50,
		    backSpeed: 50,
		    loop: true
		  });
		</script>

		</body>

</html>