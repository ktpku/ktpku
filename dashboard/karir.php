<?php
require '../config.php';
require '../lib/database.php';
include("header_frontsite.php");
?>

		<section class="section overlay section_two">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-12">
						<div class="box-wrapper">
							<span id="seciontwo"></span> 
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="section overlay section_three padding-on-body-mobile">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-12 col-lg-12">
						<div class="box-wrapper">
							<h2>Bergabung Bersama EKTPKu<div></div></h2>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-4 col-lg-4">
						<div class="box-wrapper">
							<img src="https://www.flaticon.com/svg/static/icons/svg/844/844255.svg" class="w-100" />
							<p>Bersama untuk berkembang</p>
						</div>
					</div>
					<div class="col-4 col-lg-4">
						<div class="box-wrapper">
							<img src="https://www.flaticon.com/svg/static/icons/svg/850/850053.svg" class="w-100" />
							<p>Keseruan dalam bekerja</p>
						</div>
					</div>
					<div class="col-4 col-lg-4">
						<div class="box-wrapper">
							<img src="https://www.flaticon.com/svg/static/icons/svg/852/852237.svg" class="w-100" />
							<p>Kesempatan yang sama</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="section overlay section_two">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-10 offset-lg-1">
						<p class="mb-0" style=" background: #fff; padding: 40px; color: #333; border-radius: 10px; ">Lowongan Belum Tersedia...</p>
					</div>
				</div>
			</div>
		</section>
		
	<div class="clearfix"></div>
	
		<?php
		include("footer_frontsite.php");
		?>
		
		<script>
		 var typed3 = new Typed('#seciontwo', {
		    strings: ['KARIR', 'EKTPKu'],
		    typeSpeed: 50,
		    backSpeed: 50,
		    loop: true
		  });
		</script>

		</body>

</html>