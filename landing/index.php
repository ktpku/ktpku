<?php
require '../config.php';
require '../lib/database.php';
?>

<!DOCTYPE html>
<html lang="en-US">

<!-- Mirrored from review.panelsosmed.com/ by HTTrack Website Copier/3.x [XR&CO'2017], Mon, 22 Jun 2020 17:56:12 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0">
<title>Technobooks.my.id &#8211; Panel Sosial Media Terbaik dan Termurah di Indonesia</title>
<meta name="robots" content="index,follow"/>
<meta property="og:type" content="website"/>
<meta property="og:site_name" content="technobooks.my.id"/>
<link rel="amphtml" href="amp/index.html" /><meta name="generator" content="AMP for WP 1.0.24"/><link rel="alternate" type="application/rss+xml" title="technobooks.my.id &raquo; Feed" href="feed/index.html" />
<link rel="alternate" type="application/rss+xml" title="Technobooks.my.id &raquo; Comments Feed" href="comments/feed/index.html" />
<link rel='stylesheet' id='font-awesome-css'  href='wp-content/themes/landingpress-wp/assets/lib/font-awesome/css/font-awesome.min1849.css' type='text/css' media='all' />
<link rel='stylesheet' id='wp-block-library-css'  href='wp-includes/css/dist/block-library/style.min03ec.css' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-icons-css'  href='wp-content/themes/landingpress-wp/addons/elementor/assets/lib/eicons/css/elementor-icons.minc07d.css' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-animations-css'  href='wp-content/themes/landingpress-wp/addons/elementor/assets/css/animations.minc07d.css' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-frontend-css'  href='wp-content/themes/landingpress-wp/addons/elementor/assets/css/frontend.minc07d.css' type='text/css' media='all' />
<style id='elementor-frontend-inline-css' type='text/css'>
.elementor-widget-heading .elementor-heading-title{color:#6ec1e4;font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-image .widget-image-caption{color:#7a7a7a;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-text-editor{color:#7a7a7a;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-text-editor.elementor-drop-cap-view-stacked .elementor-drop-cap{background-color:#6ec1e4;}.elementor-widget-text-editor.elementor-drop-cap-view-framed .elementor-drop-cap, .elementor-widget-text-editor.elementor-drop-cap-view-default .elementor-drop-cap{color:#6ec1e4;border-color:#6ec1e4;}.elementor-widget-button a.elementor-button, .elementor-widget-button .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-divider .elementor-divider-separator{border-top-color:#7a7a7a;}.elementor-widget-image-box .elementor-image-box-content .elementor-image-box-title{color:#6ec1e4;font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-image-box .elementor-image-box-content .elementor-image-box-description{color:#7a7a7a;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-icon.elementor-view-stacked .elementor-icon{background-color:#6ec1e4;}.elementor-widget-icon.elementor-view-framed .elementor-icon, .elementor-widget-icon.elementor-view-default .elementor-icon{color:#6ec1e4;border-color:#6ec1e4;}.elementor-widget-icon-box.elementor-view-stacked .elementor-icon{background-color:#6ec1e4;}.elementor-widget-icon-box.elementor-view-framed .elementor-icon, .elementor-widget-icon-box.elementor-view-default .elementor-icon{color:#6ec1e4;border-color:#6ec1e4;}.elementor-widget-icon-box .elementor-icon-box-content .elementor-icon-box-title{color:#6ec1e4;font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-icon-box .elementor-icon-box-content .elementor-icon-box-description{color:#7a7a7a;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-image-gallery .gallery-item .gallery-caption{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-image-carousel .elementor-image-carousel-caption{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-icon-list .elementor-icon-list-item:not(:last-child):after{border-top-color:#7a7a7a;}.elementor-widget-icon-list .elementor-icon-list-icon i{color:#6ec1e4;}.elementor-widget-icon-list .elementor-icon-list-text{color:#54595f;}.elementor-widget-icon-list .elementor-icon-list-item{font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-counter .elementor-counter-number-wrapper{color:#6ec1e4;font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-counter .elementor-counter-title{color:#54595f;font-family:"Roboto Slab", Sans-serif;font-weight:400;}.elementor-widget-progress .elementor-progress-wrapper .elementor-progress-bar{background-color:#6ec1e4;}.elementor-widget-progress .elementor-title{color:#6ec1e4;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-testimonial .elementor-testimonial-content{color:#7a7a7a;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-testimonial .elementor-testimonial-name{color:#6ec1e4;font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-testimonial .elementor-testimonial-job{color:#54595f;font-family:"Roboto Slab", Sans-serif;font-weight:400;}.elementor-widget-tabs .elementor-tab-title{color:#6ec1e4;font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-tabs .elementor-tab-title.elementor-active{color:#61ce70;}.elementor-widget-tabs .elementor-tab-content{color:#7a7a7a;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-accordion .elementor-accordion .elementor-tab-title{color:#6ec1e4;font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-accordion .elementor-accordion .elementor-tab-title.elementor-active{color:#61ce70;}.elementor-widget-accordion .elementor-accordion .elementor-tab-content{color:#7a7a7a;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-toggle .elementor-toggle .elementor-tab-title{color:#6ec1e4;font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-toggle .elementor-toggle .elementor-tab-title.elementor-active{color:#61ce70;}.elementor-widget-toggle .elementor-toggle .elementor-tab-content{color:#7a7a7a;font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-alert .elementor-alert-title{font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-alert .elementor-alert-description{font-family:"Roboto", Sans-serif;font-weight:400;}.elementor-widget-lp_navigation_menu .lp-navmenu-items li a, .elementor-widget-lp_navigation_menu .lp-navmenu-items li a:visited, .elementor-widget-lp_navigation_menu .lp-navmenu-button{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-lp_posts_grid .lp-posts-grid-wrapper li h4 a{font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-lp_posts_grid .lp-posts-grid-wrapper li p{font-family:"Roboto Slab", Sans-serif;font-weight:400;}.elementor-widget-lp_contact_form .elementor-lp-form-wrapper label{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-lp_contact_form .elementor-lp-form-wrapper input[type="text"], .elementor-widget-lp_contact_form .elementor-lp-form-wrapper input[type="email"], .elementor-widget-lp_contact_form .elementor-lp-form-wrapper textarea{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-lp_contact_form .elementor-lp-form-wrapper input[type="submit"], .elementor-widget-lp_contact_form .elementor-lp-form-wrapper button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-lp_confirmation_form .elementor-lp-form-wrapper label{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-lp_confirmation_form .elementor-lp-form-wrapper input[type="text"], .elementor-widget-lp_confirmation_form .elementor-lp-form-wrapper input[type="email"], .elementor-widget-lp_confirmation_form .elementor-lp-form-wrapper textarea{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-lp_confirmation_form .elementor-lp-form-wrapper input[type="submit"], .elementor-widget-lp_confirmation_form .elementor-lp-form-wrapper button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-lp_slider_image .lp-slider-wrapper .lp-slide-image-caption{font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-lp_slider_content .lp-slider-heading{font-family:"Roboto", Sans-serif;font-weight:600;}.elementor-widget-lp_slider_content .lp-slider-description{font-family:"Roboto Slab", Sans-serif;font-weight:400;}.elementor-widget-button_sms .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-button_tel .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-button_bbm .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-button_line .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-button_whatsapp .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-button_wagroup .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-button_messenger .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-button_telegram .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-button_instagram .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-button_video .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-countdown_simple .elementor-countdown-simple{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-optin .elementor-lp-form-wrapper label{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-optin .elementor-lp-form-wrapper input[type="text"], .elementor-widget-optin .elementor-lp-form-wrapper input[type="email"], .elementor-widget-optin .elementor-lp-form-wrapper textarea{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-widget-optin .elementor-lp-form-wrapper input[type="submit"], .elementor-widget-optin .elementor-lp-form-wrapper button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-popup-block-white .elementor-lp-form-wrapper label{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-popup-block-white .elementor-lp-form-wrapper input[type="text"], .elementor-popup-block-white .elementor-lp-form-wrapper input[type="email"], .elementor-popup-block-white .elementor-lp-form-wrapper textarea{font-family:"Roboto", Sans-serif;font-weight:500;}.elementor-popup-block-white .elementor-lp-form-wrapper input[type="submit"], .elementor-popup-block-white .elementor-lp-form-wrapper button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}.elementor-widget-optin_2steps .elementor-button{font-family:"Roboto", Sans-serif;font-weight:500;background-color:#61ce70;}
.elementor-54 .elementor-element.elementor-element-7214ec32{background-color:#ffffff;background-image:url("wp-content/uploads/2020/01/bg-matricgram.jpg");background-position:bottom center;background-repeat:no-repeat;background-size:contain;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;padding:0px 0px 30px 0px;}.elementor-54 .elementor-element.elementor-element-7214ec32 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-7157c1d4 > .elementor-element-populated >  .elementor-background-overlay{opacity:0.5;}.elementor-54 .elementor-element.elementor-element-7157c1d4 > .elementor-element-populated{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-7157c1d4 > .elementor-element-populated > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-e2613bc .elementor-spacer-inner{height:24px;}.elementor-54 .elementor-element.elementor-element-7c3a841a a.elementor-button, .elementor-54 .elementor-element.elementor-element-7c3a841a .elementor-button{font-family:"Montserrat", Sans-serif;font-size:20px;font-weight:500;color:#e22f41;background-color:rgba(0,0,0,0);border-radius:50px 50px 50px 50px;}.elementor-54 .elementor-element.elementor-element-7c3a841a .elementor-button{border-style:dotted;border-width:02px 02px 02px 02px;border-color:#e22f41;box-shadow:0px 0px 10px 0px rgba(0,0,0,0.5);}.elementor-54 .elementor-element.elementor-element-7c3a841a .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-4855be85 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-4855be85{color:#333333;font-family:"Montserrat", Sans-serif;font-size:25px;font-weight:bold;line-height:2.2em;letter-spacing:-0.9px;}.elementor-54 .elementor-element.elementor-element-4855be85 > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-28d6480c .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-28d6480c{color:#282222;font-family:"Montserrat", Sans-serif;font-size:30px;font-weight:200;line-height:1.2em;letter-spacing:-0.3px;}.elementor-54 .elementor-element.elementor-element-72e54623{margin-top:0px;margin-bottom:0px;}.elementor-54 .elementor-element.elementor-element-5a4b04e1 > .elementor-element-populated{padding:10px 10px 10px 10px;}.elementor-54 .elementor-element.elementor-element-78cce328 > .elementor-element-populated, .elementor-54 .elementor-element.elementor-element-78cce328 > .elementor-element-populated > .elementor-background-overlay{border-radius:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-78cce328 > .elementor-element-populated{padding:10px 10px 50px 10px;}.elementor-54 .elementor-element.elementor-element-50ea55bd .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-33974ce a.elementor-button, .elementor-54 .elementor-element.elementor-element-33974ce .elementor-button{font-family:"Montserrat", Sans-serif;font-size:26px;font-weight:500;line-height:1em;letter-spacing:0.7px;color:#ffffff;background-color:rgba(224,60,60,0.97);border-radius:50px 50px 50px 50px;padding:20px 20px 20px 20px;}.elementor-54 .elementor-element.elementor-element-33974ce a.elementor-button:hover, .elementor-54 .elementor-element.elementor-element-33974ce .elementor-button:hover{color:#ffffff;background-color:#23a455;border-color:#23a455;}.elementor-54 .elementor-element.elementor-element-33974ce .elementor-button{border-style:solid;border-width:3px 3px 3px 3px;border-color:rgba(224,60,60,0.97);}.elementor-54 .elementor-element.elementor-element-33974ce > .elementor-widget-container{padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-fa485ec{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;padding:20px 20px 20px 20px;}.elementor-54 .elementor-element.elementor-element-fa485ec > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-420489d3 .elementor-spacer-inner{height:50px;}.elementor-54 .elementor-element.elementor-element-7b99faf3{text-align:center;}.elementor-54 .elementor-element.elementor-element-7b99faf3 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-1e3dc364 .elementor-text-editor{text-align:left;}.elementor-54 .elementor-element.elementor-element-1e3dc364{color:#333333;font-family:"Poppins", Sans-serif;font-size:28px;font-weight:100;line-height:2em;letter-spacing:-1.5px;}.elementor-54 .elementor-element.elementor-element-1e3dc364 > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-4d199919{text-align:center;}.elementor-54 .elementor-element.elementor-element-4d199919 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-200ab648 .elementor-icon-list-item:not(:last-child){padding-bottom:calc(0px/2);}.elementor-54 .elementor-element.elementor-element-200ab648 .elementor-icon-list-item:not(:first-child){margin-top:calc(0px/2);}.elementor-54 .elementor-element.elementor-element-200ab648 .elementor-icon-list-icon i{color:rgba(110,193,228,0.92);font-size:20px;}.elementor-54 .elementor-element.elementor-element-200ab648 .elementor-icon-list-icon{width:20px;}.elementor-54 .elementor-element.elementor-element-200ab648 .elementor-icon-list-text{padding-left:0px;}.elementor-54 .elementor-element.elementor-element-200ab648 .elementor-icon-list-item{font-size:20px;line-height:2.5em;}.elementor-54 .elementor-element.elementor-element-5ebf021e{background-color:#f9f9f9;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;margin-top:0px;margin-bottom:0px;padding:10px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-5ebf021e > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-504e5baf .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-504e5baf{color:#333333;font-family:"Montserrat", Sans-serif;font-size:36px;font-weight:100;line-height:1.5em;letter-spacing:-1.5px;}.elementor-54 .elementor-element.elementor-element-504e5baf > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-36cacd19 .elementor-icon-list-item:not(:last-child){padding-bottom:calc(0px/2);}.elementor-54 .elementor-element.elementor-element-36cacd19 .elementor-icon-list-item:not(:first-child){margin-top:calc(0px/2);}.elementor-54 .elementor-element.elementor-element-36cacd19 .elementor-icon-list-icon i{color:rgba(110,193,228,0.92);font-size:20px;}.elementor-54 .elementor-element.elementor-element-36cacd19 .elementor-icon-list-icon{width:20px;}.elementor-54 .elementor-element.elementor-element-36cacd19 .elementor-icon-list-text{padding-left:0px;}.elementor-54 .elementor-element.elementor-element-36cacd19 .elementor-icon-list-item{font-size:20px;line-height:2.5em;}.elementor-54 .elementor-element.elementor-element-23ae49b4{text-align:center;}.elementor-54 .elementor-element.elementor-element-23ae49b4 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-524a86ae{background-color:#f9f9f9;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;margin-top:0px;margin-bottom:0px;padding:10px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-524a86ae > .elementor-background-overlay{opacity:0.5;transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-2467b4f2 > .elementor-element-populated{background-color:#ffffff;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-2467b4f2 > .elementor-element-populated > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-308364f5 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-308364f5{color:#333333;font-family:"Montserrat", Sans-serif;font-size:27px;font-weight:100;line-height:1.5em;letter-spacing:-1.5px;}.elementor-54 .elementor-element.elementor-element-308364f5 > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-203e42ea a.elementor-button, .elementor-54 .elementor-element.elementor-element-203e42ea .elementor-button{font-weight:500;background-color:rgba(32,163,34,0.56);border-radius:50px 50px 50px 50px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-203e42ea .elementor-button{border-style:dotted;border-width:2px 2px 2px 2px;border-color:#23a455;}.elementor-54 .elementor-element.elementor-element-203e42ea > .elementor-widget-container{background-color:rgba(35,164,85,0);}.elementor-54 .elementor-element.elementor-element-203e42ea .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-462158a0 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-462158a0{color:#333333;font-family:"Montserrat", Sans-serif;font-size:27px;font-weight:100;line-height:1.5em;letter-spacing:-1.5px;}.elementor-54 .elementor-element.elementor-element-462158a0 > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-18f8beab{background-color:#f4f4f4;background-image:url("wp-content/uploads/2020/01/bg-semen.jpg");transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;margin-top:0px;margin-bottom:0px;padding:10px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-18f8beab > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-423c9385 > .elementor-element-populated{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-423c9385 > .elementor-element-populated > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-56b3a456 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-56b3a456{color:#333333;font-family:"Montserrat", Sans-serif;font-size:35px;font-weight:bold;line-height:1.5em;letter-spacing:-1.5px;}.elementor-54 .elementor-element.elementor-element-56b3a456 > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-1f4ab39 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-1f4ab39{color:#333333;font-family:"Montserrat", Sans-serif;font-size:35px;font-weight:bold;line-height:1.5em;letter-spacing:-1.5px;}.elementor-54 .elementor-element.elementor-element-1f4ab39 > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-14d0e365{padding:10px 10px 30px 10px;}.elementor-54 .elementor-element.elementor-element-1860a361 .elementor-icon-list-item:not(:last-child){padding-bottom:calc(0px/2);}.elementor-54 .elementor-element.elementor-element-1860a361 .elementor-icon-list-item:not(:first-child){margin-top:calc(0px/2);}.elementor-54 .elementor-element.elementor-element-1860a361 .elementor-icon-list-icon{width:15px;}.elementor-54 .elementor-element.elementor-element-1860a361 .elementor-icon-list-icon i{font-size:15px;}.elementor-54 .elementor-element.elementor-element-1860a361 .elementor-icon-list-text{padding-left:0px;color:#000000;}.elementor-54 .elementor-element.elementor-element-1860a361 .elementor-icon-list-item{font-size:20px;}.elementor-54 .elementor-element.elementor-element-7a7d1d42 .elementor-icon-list-item:not(:last-child){padding-bottom:calc(0px/2);}.elementor-54 .elementor-element.elementor-element-7a7d1d42 .elementor-icon-list-item:not(:first-child){margin-top:calc(0px/2);}.elementor-54 .elementor-element.elementor-element-7a7d1d42 .elementor-icon-list-icon{width:15px;}.elementor-54 .elementor-element.elementor-element-7a7d1d42 .elementor-icon-list-icon i{font-size:15px;}.elementor-54 .elementor-element.elementor-element-7a7d1d42 .elementor-icon-list-text{padding-left:0px;color:#000000;}.elementor-54 .elementor-element.elementor-element-7a7d1d42 .elementor-icon-list-item{font-size:20px;}.elementor-54 .elementor-element.elementor-element-7852997b .elementor-icon-list-item:not(:last-child){padding-bottom:calc(0px/2);}.elementor-54 .elementor-element.elementor-element-7852997b .elementor-icon-list-item:not(:first-child){margin-top:calc(0px/2);}.elementor-54 .elementor-element.elementor-element-7852997b .elementor-icon-list-icon{width:15px;}.elementor-54 .elementor-element.elementor-element-7852997b .elementor-icon-list-icon i{font-size:15px;}.elementor-54 .elementor-element.elementor-element-7852997b .elementor-icon-list-text{padding-left:0px;color:#000000;}.elementor-54 .elementor-element.elementor-element-7852997b .elementor-icon-list-item{font-size:20px;}.elementor-54 .elementor-element.elementor-element-37ebe027 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-37ebe027{color:#333333;font-family:"Montserrat", Sans-serif;font-size:27px;font-weight:100;line-height:1.5em;letter-spacing:-1.5px;}.elementor-54 .elementor-element.elementor-element-37ebe027 > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-3099c5a9 a.elementor-button, .elementor-54 .elementor-element.elementor-element-3099c5a9 .elementor-button{font-family:"Montserrat", Sans-serif;font-size:19px;color:#e22f41;background-color:rgba(0,0,0,0);border-radius:50px 50px 50px 50px;}.elementor-54 .elementor-element.elementor-element-3099c5a9 .elementor-button{border-style:dotted;border-width:02px 02px 02px 02px;border-color:#e22f41;box-shadow:0px 0px 10px 0px rgba(0,0,0,0.5);}.elementor-54 .elementor-element.elementor-element-3099c5a9 .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-1330f9e2 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-1330f9e2{color:#333333;font-family:"Montserrat", Sans-serif;font-size:27px;font-weight:100;line-height:1.5em;letter-spacing:-1.5px;}.elementor-54 .elementor-element.elementor-element-1330f9e2 > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-73d4fbba a.elementor-button, .elementor-54 .elementor-element.elementor-element-73d4fbba .elementor-button{font-family:"Montserrat", Sans-serif;font-size:35px;color:#eab217;background-color:rgba(0,0,0,0);border-radius:10px 10px 10px 10px;padding:40px 40px 40px 40px;}.elementor-54 .elementor-element.elementor-element-73d4fbba .elementor-button{border-style:dotted;border-width:2px 2px 2px 2px;border-color:#e2ad2f;box-shadow:0px 0px 10px 0px rgba(0,0,0,0.5);}.elementor-54 .elementor-element.elementor-element-73d4fbba .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-c33b178 .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-478e2d96{background-color:#f9f9f9;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;padding:0px 0px 40px 0px;}.elementor-54 .elementor-element.elementor-element-478e2d96 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-28432b6f > .elementor-element-populated{padding:15px 15px 15px 15px;}.elementor-54 .elementor-element.elementor-element-af24312 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-af24312{color:#333333;font-family:"Montserrat", Sans-serif;font-size:27px;font-weight:100;line-height:1.5em;letter-spacing:-1.5px;}.elementor-54 .elementor-element.elementor-element-af24312 > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-bc504a2{text-align:center;}.elementor-54 .elementor-element.elementor-element-bc504a2 .elementor-heading-title{color:#23a455;font-family:"Montserrat", Sans-serif;text-decoration:underline;line-height:1.6em;text-shadow:0px 0px 4px rgba(0,0,0,0.3);}.elementor-54 .elementor-element.elementor-element-383ee4c6 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-383ee4c6{color:#333333;font-family:"Montserrat", Sans-serif;font-size:27px;font-weight:100;line-height:1.5em;letter-spacing:-1.5px;}.elementor-54 .elementor-element.elementor-element-383ee4c6 > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-3b1eee6{background-color:#f9f9f9;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;margin-top:0px;margin-bottom:0px;padding:10px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-3b1eee6 > .elementor-background-overlay{opacity:0.5;transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-63f65fc1 > .elementor-element-populated{background-color:#ffffff;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-63f65fc1 > .elementor-element-populated > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-4803d518 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-4803d518{color:#333333;font-family:"Montserrat", Sans-serif;font-size:27px;font-weight:100;line-height:1.5em;letter-spacing:-1.5px;}.elementor-54 .elementor-element.elementor-element-4803d518 > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-4884c207 a.elementor-button, .elementor-54 .elementor-element.elementor-element-4884c207 .elementor-button{font-family:"Montserrat", Sans-serif;font-size:20px;font-weight:500;color:#e22f41;background-color:rgba(0,0,0,0);border-radius:50px 50px 50px 50px;}.elementor-54 .elementor-element.elementor-element-4884c207 .elementor-button{border-style:dotted;border-width:02px 02px 02px 02px;border-color:#e22f41;box-shadow:0px 0px 10px 0px rgba(0,0,0,0.5);}.elementor-54 .elementor-element.elementor-element-4884c207 .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-1d21d7cd .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-1d21d7cd{color:#333333;font-family:"Montserrat", Sans-serif;font-size:27px;font-weight:100;line-height:1.5em;letter-spacing:-1.5px;}.elementor-54 .elementor-element.elementor-element-1d21d7cd > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-7e0b8fc{background-image:url("wp-content/uploads/2020/01/bg-matricgram.jpg");background-position:bottom center;background-repeat:no-repeat;background-size:cover;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;padding:20px 20px 20px 20px;}.elementor-54 .elementor-element.elementor-element-7e0b8fc > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-7ade8034{color:#333333;font-family:"Lato", Sans-serif;font-size:20px;}.elementor-54 .elementor-element.elementor-element-9df7c6c > .elementor-container{max-width:1000px;}.elementor-54 .elementor-element.elementor-element-9df7c6c{background-image:url("wp-content/uploads/2020/01/bg-patternsegitiga.jpg");transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-9df7c6c:hover{background-color:transparent;background-image:linear-gradient(180deg,  0%, #f2295b 100%);}.elementor-54 .elementor-element.elementor-element-9df7c6c > .elementor-background-overlay{background-color:rgba(0,0,0,0);opacity:0.8;transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-5bb0f1ca:hover > .elementor-element-populated{background-color:transparent;background-image:linear-gradient(180deg,  0%, #f2295b 100%);}.elementor-54 .elementor-element.elementor-element-71c0426b{text-align:center;}.elementor-54 .elementor-element.elementor-element-71c0426b .elementor-heading-title{color:#000000;font-weight:200;}.elementor-54 .elementor-element.elementor-element-4d74c081 .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-5015b750{text-align:center;}.elementor-54 .elementor-element.elementor-element-5015b750 .elementor-image img{max-width:56%;opacity:1;}.elementor-54 .elementor-element.elementor-element-73d78769{background-image:url("wp-content/uploads/2020/01/bg-matricgram.jpg");background-position:bottom center;background-repeat:no-repeat;background-size:cover;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;padding:20px 20px 20px 20px;}.elementor-54 .elementor-element.elementor-element-73d78769 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-29ecbe2{color:#333333;font-family:"Lato", Sans-serif;font-size:20px;}.elementor-54 .elementor-element.elementor-element-2820ec23{color:#333333;font-family:"Lato", Sans-serif;font-size:20px;}.elementor-54 .elementor-element.elementor-element-7b97870f{color:#333333;font-family:"Lato", Sans-serif;font-size:20px;}.elementor-54 .elementor-element.elementor-element-6fdab66{color:#333333;font-family:"Lato", Sans-serif;font-size:20px;}.elementor-54 .elementor-element.elementor-element-f66116c > .elementor-element-populated{background-image:url("wp-content/uploads/2020/01/bg-semen.jpg");transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-f66116c > .elementor-element-populated > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-492b03ec .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-492b03ec{color:#333333;font-family:"Montserrat", Sans-serif;font-size:33px;font-weight:bold;line-height:1.5em;letter-spacing:-1.5px;}.elementor-54 .elementor-element.elementor-element-492b03ec > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-138d07dd{background-color:#f9f9f9;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;padding:0px 0px 40px 0px;}.elementor-54 .elementor-element.elementor-element-138d07dd > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-4a76ce39 > .elementor-element-populated{padding:15px 15px 15px 15px;}.elementor-54 .elementor-element.elementor-element-7252a747{text-align:left;}.elementor-54 .elementor-element.elementor-element-7252a747 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-155db16e{text-align:left;}.elementor-54 .elementor-element.elementor-element-155db16e .elementor-heading-title{color:#e02f14;font-family:"Montserrat", Sans-serif;font-size:20px;font-weight:500;font-style:normal;}.elementor-54 .elementor-element.elementor-element-6082e286 > .elementor-element-populated{padding:15px 15px 15px 15px;}.elementor-54 .elementor-element.elementor-element-39867e95{text-align:left;}.elementor-54 .elementor-element.elementor-element-39867e95 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-3c83020f{text-align:left;}.elementor-54 .elementor-element.elementor-element-3c83020f .elementor-heading-title{color:#0078ce;font-family:"Montserrat", Sans-serif;font-size:20px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-364a7da7 > .elementor-element-populated{padding:15px 15px 15px 15px;}.elementor-54 .elementor-element.elementor-element-74d3cb6c{text-align:left;}.elementor-54 .elementor-element.elementor-element-74d3cb6c .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-2700ecf4{text-align:left;}.elementor-54 .elementor-element.elementor-element-2700ecf4 .elementor-heading-title{color:#6ec1e4;font-family:"Montserrat", Sans-serif;font-size:20px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-6ffa1561{background-color:#f9f9f9;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;padding:0px 0px 40px 0px;}.elementor-54 .elementor-element.elementor-element-6ffa1561 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-5ba0f4cb > .elementor-element-populated{padding:15px 15px 15px 15px;}.elementor-54 .elementor-element.elementor-element-50917378{text-align:left;}.elementor-54 .elementor-element.elementor-element-50917378 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-78c8938c{text-align:left;}.elementor-54 .elementor-element.elementor-element-78c8938c .elementor-heading-title{color:#f20800;font-family:"Montserrat", Sans-serif;font-size:20px;font-weight:500;font-style:normal;}.elementor-54 .elementor-element.elementor-element-365287b9 > .elementor-element-populated{padding:15px 15px 15px 15px;}.elementor-54 .elementor-element.elementor-element-cb532fb{text-align:left;}.elementor-54 .elementor-element.elementor-element-cb532fb .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-3147e9fa{text-align:left;}.elementor-54 .elementor-element.elementor-element-3147e9fa .elementor-heading-title{color:#0078ce;font-family:"Montserrat", Sans-serif;font-size:20px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-54feee91 > .elementor-element-populated{padding:15px 15px 15px 15px;}.elementor-54 .elementor-element.elementor-element-363e9bd7{text-align:left;}.elementor-54 .elementor-element.elementor-element-363e9bd7 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-68fb836b{text-align:left;}.elementor-54 .elementor-element.elementor-element-68fb836b .elementor-heading-title{color:#f23000;font-family:"Montserrat", Sans-serif;font-size:20px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-e85763a{background-color:#f9f9f9;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;padding:0px 0px 40px 0px;}.elementor-54 .elementor-element.elementor-element-e85763a > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-34f8489e > .elementor-element-populated{padding:15px 15px 15px 15px;}.elementor-54 .elementor-element.elementor-element-74de115b{text-align:left;}.elementor-54 .elementor-element.elementor-element-74de115b .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-3ba8765e{text-align:left;}.elementor-54 .elementor-element.elementor-element-3ba8765e .elementor-heading-title{color:rgba(242,8,0,0.82);font-family:"Montserrat", Sans-serif;font-size:20px;font-weight:500;font-style:normal;}.elementor-54 .elementor-element.elementor-element-4f2e6866 > .elementor-element-populated{padding:15px 15px 15px 15px;}.elementor-54 .elementor-element.elementor-element-5a5df74c{text-align:left;}.elementor-54 .elementor-element.elementor-element-5a5df74c .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-6ed9939d{text-align:left;}.elementor-54 .elementor-element.elementor-element-6ed9939d .elementor-heading-title{color:#f91000;font-family:"Montserrat", Sans-serif;font-size:20px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-70102a73 > .elementor-element-populated{padding:15px 15px 15px 15px;}.elementor-54 .elementor-element.elementor-element-2d4066f2{text-align:left;}.elementor-54 .elementor-element.elementor-element-2d4066f2 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-3400dd17{text-align:left;}.elementor-54 .elementor-element.elementor-element-3400dd17 .elementor-heading-title{color:#6ec1e4;font-family:"Montserrat", Sans-serif;font-size:20px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-f147217{background-color:#f9f9f9;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;padding:0px 0px 40px 0px;}.elementor-54 .elementor-element.elementor-element-f147217 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-3734e012 > .elementor-element-populated{padding:15px 15px 15px 15px;}.elementor-54 .elementor-element.elementor-element-64e66d69{text-align:left;}.elementor-54 .elementor-element.elementor-element-64e66d69 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-b67a148{text-align:left;}.elementor-54 .elementor-element.elementor-element-b67a148 .elementor-heading-title{color:#009ee2;font-family:"Montserrat", Sans-serif;font-size:20px;font-weight:500;font-style:normal;}.elementor-54 .elementor-element.elementor-element-6e038d61 > .elementor-element-populated{padding:15px 15px 15px 15px;}.elementor-54 .elementor-element.elementor-element-1c4126e2{text-align:left;}.elementor-54 .elementor-element.elementor-element-1c4126e2 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-74b54d5b{text-align:left;}.elementor-54 .elementor-element.elementor-element-74b54d5b .elementor-heading-title{color:#00ce1b;font-family:"Montserrat", Sans-serif;font-size:20px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-294bd794 > .elementor-element-populated{padding:15px 15px 15px 15px;}.elementor-54 .elementor-element.elementor-element-5f5c2187{text-align:left;}.elementor-54 .elementor-element.elementor-element-5f5c2187 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-48195e38{text-align:left;}.elementor-54 .elementor-element.elementor-element-48195e38 .elementor-heading-title{color:#f9a600;font-family:"Montserrat", Sans-serif;font-size:20px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-f388348{background-color:#f9f9f9;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-f388348 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-4063dc8f a.elementor-button, .elementor-54 .elementor-element.elementor-element-4063dc8f .elementor-button{font-family:"Montserrat", Sans-serif;font-size:26px;font-weight:500;line-height:1em;letter-spacing:0.7px;color:#ffffff;background-color:rgba(35,164,85,0.97);border-radius:50px 50px 50px 50px;padding:20px 20px 20px 20px;}.elementor-54 .elementor-element.elementor-element-4063dc8f a.elementor-button:hover, .elementor-54 .elementor-element.elementor-element-4063dc8f .elementor-button:hover{color:#ffffff;background-color:#23a455;border-color:#23a455;}.elementor-54 .elementor-element.elementor-element-4063dc8f .elementor-button{border-style:solid;border-width:3px 3px 3px 3px;border-color:rgba(35,164,85,0.97);}.elementor-54 .elementor-element.elementor-element-4063dc8f > .elementor-widget-container{padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-451f67f .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-67d63ef6{background-color:#f4f4f4;background-image:url("wp-content/uploads/2020/01/bg-semen.jpg");transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;margin-top:0px;margin-bottom:0px;padding:10px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-67d63ef6 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-69765778 > .elementor-element-populated{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-69765778 > .elementor-element-populated > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-6146f0f8 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-6146f0f8{color:#333333;font-family:"Montserrat", Sans-serif;font-size:33px;font-weight:bold;line-height:1.5em;letter-spacing:-1.5px;}.elementor-54 .elementor-element.elementor-element-6146f0f8 > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-3d100607{padding:10px 10px 30px 10px;}.elementor-54 .elementor-element.elementor-element-1709d5fb .elementor-counter-number-wrapper{color:#61ce70;}.elementor-54 .elementor-element.elementor-element-1709d5fb .elementor-counter-title{color:#a0a0a0;font-size:16px;}.elementor-54 .elementor-element.elementor-element-3c4ba7aa .elementor-counter-number-wrapper{color:#4db8d6;}.elementor-54 .elementor-element.elementor-element-3c4ba7aa .elementor-counter-title{color:#a0a0a0;font-size:16px;}.elementor-54 .elementor-element.elementor-element-47e24f78 .elementor-counter-number-wrapper{color:#ef8456;}.elementor-54 .elementor-element.elementor-element-47e24f78 .elementor-counter-title{color:#a0a0a0;font-size:16px;}.elementor-54 .elementor-element.elementor-element-30bc83fe .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-30bc83fe{color:#333333;font-family:"Montserrat", Sans-serif;font-size:33px;font-weight:bold;line-height:1.5em;letter-spacing:-1.5px;}.elementor-54 .elementor-element.elementor-element-30bc83fe > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-62ed68b{text-align:center;}.elementor-54 .elementor-element.elementor-element-62ed68b .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-620199d4{text-align:center;}.elementor-54 .elementor-element.elementor-element-620199d4 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-2def0a4{text-align:center;}.elementor-54 .elementor-element.elementor-element-2def0a4 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-1c079dfe{text-align:center;}.elementor-54 .elementor-element.elementor-element-1c079dfe .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-78b18ec5{text-align:center;}.elementor-54 .elementor-element.elementor-element-78b18ec5 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-1ca45244{text-align:center;}.elementor-54 .elementor-element.elementor-element-1ca45244 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-e495ebc{text-align:center;}.elementor-54 .elementor-element.elementor-element-e495ebc .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-58a9d80b{text-align:center;}.elementor-54 .elementor-element.elementor-element-58a9d80b .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-5bd23d2{background-image:url("wp-content/uploads/2020/01/bg-matricgram.jpg");background-position:bottom center;background-repeat:no-repeat;background-size:cover;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;padding:20px 20px 20px 20px;}.elementor-54 .elementor-element.elementor-element-5bd23d2 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-75c93f2a{color:#333333;font-family:"Lato", Sans-serif;font-size:20px;}.elementor-54 .elementor-element.elementor-element-66268ea6{color:#333333;font-family:"Lato", Sans-serif;font-size:20px;}.elementor-54 .elementor-element.elementor-element-41998dfa{color:#333333;font-family:"Lato", Sans-serif;font-size:20px;}.elementor-54 .elementor-element.elementor-element-7d8b7fe0{background-image:url("wp-content/uploads/2020/01/bg-matricgram.jpg");background-position:bottom center;background-size:cover;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-7d8b7fe0 > .elementor-background-overlay{background-color:#000000;opacity:0.5;transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-42f8e863 .elementor-spacer-inner{height:20px;}.elementor-54 .elementor-element.elementor-element-8cf79{text-align:center;}.elementor-54 .elementor-element.elementor-element-8cf79 .elementor-heading-title{color:#ffffff;font-family:"Montserrat Alternates", Sans-serif;font-size:41px;}.elementor-54 .elementor-element.elementor-element-5cb8254e > .elementor-container{max-width:671px;}.elementor-54 .elementor-element.elementor-element-5cb8254e{margin-top:0px;margin-bottom:0px;padding:10px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-580d161c > .elementor-element-populated{background-color:#ffffff;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;margin:15px 15px 18px 15px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-580d161c > .elementor-element-populated, .elementor-54 .elementor-element.elementor-element-580d161c > .elementor-element-populated > .elementor-background-overlay{border-radius:5px 5px 5px 5px;}.elementor-54 .elementor-element.elementor-element-580d161c > .elementor-element-populated > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-34e06367 .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-182a808a{text-align:center;}.elementor-54 .elementor-element.elementor-element-182a808a .elementor-heading-title{color:#000000;font-family:"Montserrat", Sans-serif;font-size:30px;text-shadow:0px 0px 10px rgba(122,122,122,0.3);}.elementor-54 .elementor-element.elementor-element-2c47f722{text-align:center;}.elementor-54 .elementor-element.elementor-element-2c47f722 .elementor-heading-title{color:#000000;font-family:"Montserrat", Sans-serif;font-size:15px;text-shadow:0px 0px 10px rgba(122,122,122,0.3);}.elementor-54 .elementor-element.elementor-element-712d0935 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-712d0935{color:#000000;font-family:"Verdana", Sans-serif;font-size:38px;font-weight:600;}.elementor-54 .elementor-element.elementor-element-712d0935 > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-2ec94ba4{text-align:center;}.elementor-54 .elementor-element.elementor-element-2ec94ba4 .elementor-heading-title{color:#141414;font-family:"Montserrat", Sans-serif;font-size:13px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-2ec94ba4 > .elementor-widget-container{padding:10px 0px 0px 0px;border-style:solid;border-width:2px 0px 0px 0px;border-color:#e2e2e2;}.elementor-54 .elementor-element.elementor-element-2ec94ba4 .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-142f2c56{text-align:center;}.elementor-54 .elementor-element.elementor-element-142f2c56 .elementor-heading-title{color:#141414;font-family:"Montserrat", Sans-serif;font-size:13px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-142f2c56 > .elementor-widget-container{padding:10px 0px 0px 0px;border-style:solid;border-width:2px 0px 0px 0px;border-color:#e2e2e2;}.elementor-54 .elementor-element.elementor-element-142f2c56 .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-1ccf387c{text-align:center;}.elementor-54 .elementor-element.elementor-element-1ccf387c .elementor-heading-title{color:#141414;font-family:"Montserrat", Sans-serif;font-size:13px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-1ccf387c > .elementor-widget-container{padding:10px 0px 0px 0px;border-style:solid;border-width:2px 0px 0px 0px;border-color:#e2e2e2;}.elementor-54 .elementor-element.elementor-element-1ccf387c .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-7bf2075b{text-align:center;}.elementor-54 .elementor-element.elementor-element-7bf2075b .elementor-heading-title{color:#141414;font-family:"Montserrat", Sans-serif;font-size:13px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-7bf2075b > .elementor-widget-container{padding:10px 0px 0px 0px;border-style:solid;border-width:2px 0px 0px 0px;border-color:#e2e2e2;}.elementor-54 .elementor-element.elementor-element-7bf2075b .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-4cde7cb7{text-align:center;}.elementor-54 .elementor-element.elementor-element-4cde7cb7 .elementor-heading-title{color:#141414;font-family:"Montserrat", Sans-serif;font-size:13px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-4cde7cb7 > .elementor-widget-container{padding:10px 0px 0px 0px;border-style:solid;border-width:2px 0px 0px 0px;border-color:#e2e2e2;}.elementor-54 .elementor-element.elementor-element-4cde7cb7 .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-605a3fce{text-align:center;}.elementor-54 .elementor-element.elementor-element-605a3fce .elementor-heading-title{color:#141414;font-family:"Montserrat", Sans-serif;font-size:13px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-605a3fce > .elementor-widget-container{padding:10px 0px 0px 0px;border-style:solid;border-width:2px 0px 0px 0px;border-color:#e2e2e2;}.elementor-54 .elementor-element.elementor-element-605a3fce .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-e656c71{text-align:center;}.elementor-54 .elementor-element.elementor-element-e656c71 .elementor-heading-title{color:#141414;font-family:"Montserrat", Sans-serif;font-size:13px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-e656c71 > .elementor-widget-container{padding:10px 0px 0px 0px;border-style:solid;border-width:2px 0px 0px 0px;border-color:#e2e2e2;}.elementor-54 .elementor-element.elementor-element-e656c71 .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-4b28d198{text-align:center;}.elementor-54 .elementor-element.elementor-element-4b28d198 .elementor-heading-title{color:#141414;font-family:"Montserrat", Sans-serif;font-size:13px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-4b28d198 > .elementor-widget-container{padding:10px 0px 0px 0px;border-style:solid;border-width:2px 0px 0px 0px;border-color:#e2e2e2;}.elementor-54 .elementor-element.elementor-element-4b28d198 .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-189a97a2{text-align:center;}.elementor-54 .elementor-element.elementor-element-189a97a2 .elementor-heading-title{color:#141414;font-family:"Montserrat", Sans-serif;font-size:13px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-189a97a2 > .elementor-widget-container{padding:10px 0px 0px 0px;border-style:solid;border-width:2px 0px 0px 0px;border-color:#e2e2e2;}.elementor-54 .elementor-element.elementor-element-189a97a2 .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-1714c842{text-align:center;}.elementor-54 .elementor-element.elementor-element-1714c842 .elementor-heading-title{color:#141414;font-family:"Montserrat", Sans-serif;font-size:13px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-1714c842 > .elementor-widget-container{padding:10px 0px 0px 0px;border-style:solid;border-width:2px 0px 0px 0px;border-color:#e2e2e2;}.elementor-54 .elementor-element.elementor-element-1714c842 .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-699a05b3 .elementor-button-sticky-yes{background-color:#6ec1e4;}.elementor-54 .elementor-element.elementor-element-699a05b3 a.elementor-button, .elementor-54 .elementor-element.elementor-element-699a05b3 .elementor-button{background-color:#4f5362;border-radius:0px 0px 5px 5px;}.elementor-54 .elementor-element.elementor-element-411dc27a > .elementor-element-populated{background-color:#ffffff;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;margin:15px 15px 295px 15px;padding:0px 0px 01px 0px;}.elementor-54 .elementor-element.elementor-element-411dc27a > .elementor-element-populated, .elementor-54 .elementor-element.elementor-element-411dc27a > .elementor-element-populated > .elementor-background-overlay{border-radius:5px 5px 5px 5px;}.elementor-54 .elementor-element.elementor-element-411dc27a > .elementor-element-populated > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-21dc1 .elementor-spacer-inner{height:13px;}.elementor-54 .elementor-element.elementor-element-21dc1 > .elementor-widget-container{padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-21dc1{z-index:100;}.elementor-54 .elementor-element.elementor-element-334148db{text-align:center;}.elementor-54 .elementor-element.elementor-element-334148db .elementor-heading-title{color:#000000;font-family:"Montserrat", Sans-serif;font-size:30px;text-shadow:0px 0px 10px rgba(122,122,122,0.3);}.elementor-54 .elementor-element.elementor-element-306eaab1{text-align:center;}.elementor-54 .elementor-element.elementor-element-306eaab1 .elementor-heading-title{color:#000000;font-family:"Montserrat", Sans-serif;font-size:15px;text-shadow:0px 0px 10px rgba(122,122,122,0.3);}.elementor-54 .elementor-element.elementor-element-c9aee6b .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-c9aee6b{color:#000000;font-family:"Verdana", Sans-serif;font-size:38px;font-weight:600;}.elementor-54 .elementor-element.elementor-element-c9aee6b > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-5390f5be{text-align:center;}.elementor-54 .elementor-element.elementor-element-5390f5be .elementor-heading-title{color:#141414;font-family:"Montserrat", Sans-serif;font-size:13px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-5390f5be > .elementor-widget-container{padding:10px 0px 0px 0px;border-style:solid;border-width:2px 0px 0px 0px;border-color:#e2e2e2;}.elementor-54 .elementor-element.elementor-element-5390f5be .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-65c4ff36{text-align:center;}.elementor-54 .elementor-element.elementor-element-65c4ff36 .elementor-heading-title{color:#141414;font-family:"Montserrat", Sans-serif;font-size:13px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-65c4ff36 > .elementor-widget-container{padding:10px 0px 0px 0px;border-style:solid;border-width:2px 0px 0px 0px;border-color:#e2e2e2;}.elementor-54 .elementor-element.elementor-element-65c4ff36 .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-7118a9c9{text-align:center;}.elementor-54 .elementor-element.elementor-element-7118a9c9 .elementor-heading-title{color:#141414;font-family:"Montserrat", Sans-serif;font-size:13px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-7118a9c9 > .elementor-widget-container{padding:10px 0px 0px 0px;border-style:solid;border-width:2px 0px 0px 0px;border-color:#e2e2e2;}.elementor-54 .elementor-element.elementor-element-7118a9c9 .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-3413129f{text-align:center;}.elementor-54 .elementor-element.elementor-element-3413129f .elementor-heading-title{color:#000000;font-family:"Montserrat", Sans-serif;font-size:13px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-3413129f > .elementor-widget-container{padding:10px 0px 0px 0px;border-style:solid;border-width:2px 0px 0px 0px;border-color:#e2e2e2;}.elementor-54 .elementor-element.elementor-element-3413129f .elementor-widget-container{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-56ed01dd a.elementor-button, .elementor-54 .elementor-element.elementor-element-56ed01dd .elementor-button{background-color:#4f5362;border-radius:0px 0px 5px 5px;}.elementor-54 .elementor-element.elementor-element-7587d1e0{background-image:url("wp-content/uploads/2020/01/bg-matricgram.jpg");background-size:cover;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-7587d1e0 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-76c4edc .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-7e79502e{text-align:center;}.elementor-54 .elementor-element.elementor-element-7e79502e .elementor-heading-title{color:#333333;font-family:"Montserrat", Sans-serif;font-size:47px;font-weight:bold;}.elementor-54 .elementor-element.elementor-element-b21a43f a.elementor-button, .elementor-54 .elementor-element.elementor-element-b21a43f .elementor-button{font-family:"Montserrat", Sans-serif;font-size:26px;font-weight:500;line-height:1em;letter-spacing:0.7px;color:#ffffff;background-color:rgba(110,193,228,0.97);border-radius:50px 50px 50px 50px;padding:20px 20px 20px 20px;}.elementor-54 .elementor-element.elementor-element-b21a43f a.elementor-button:hover, .elementor-54 .elementor-element.elementor-element-b21a43f .elementor-button:hover{color:#ffffff;background-color:#ff463f;border-color:#ff3835;}.elementor-54 .elementor-element.elementor-element-b21a43f .elementor-button{border-style:solid;border-width:3px 3px 3px 3px;border-color:rgba(110,193,228,0.97);}.elementor-54 .elementor-element.elementor-element-b21a43f > .elementor-widget-container{padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-73b013b6{text-align:center;}.elementor-54 .elementor-element.elementor-element-73b013b6 .elementor-heading-title{color:#353535;font-family:"Lato", Sans-serif;font-size:28px;font-weight:300;}.elementor-54 .elementor-element.elementor-element-66e3129{text-align:center;}.elementor-54 .elementor-element.elementor-element-66e3129 .elementor-heading-title{color:#353535;font-family:"Lato", Sans-serif;font-size:28px;font-weight:300;}.elementor-54 .elementor-element.elementor-element-1d90eece .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-1d90eece{color:#333333;font-family:"Lato", Sans-serif;font-size:20px;font-weight:300;}.elementor-54 .elementor-element.elementor-element-1fc77ac0{text-align:center;}.elementor-54 .elementor-element.elementor-element-1fc77ac0 .elementor-heading-title{color:#353535;font-family:"Lato", Sans-serif;font-size:28px;font-weight:300;}.elementor-54 .elementor-element.elementor-element-feb1242{text-align:center;}.elementor-54 .elementor-element.elementor-element-feb1242 .elementor-heading-title{color:#353535;font-family:"Lato", Sans-serif;font-size:28px;font-weight:300;}.elementor-54 .elementor-element.elementor-element-74e2d873 .elementor-spacer-inner{height:30px;}.elementor-54 .elementor-element.elementor-element-14941884{background-color:#f4f4f4;background-image:url("wp-content/uploads/2020/01/bg-semen.jpg");transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;margin-top:0px;margin-bottom:0px;padding:10px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-14941884 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-7b0cf108 > .elementor-element-populated{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-7b0cf108 > .elementor-element-populated > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-2053ce2e .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-2053ce2e{font-size:25px;}.elementor-54 .elementor-element.elementor-element-2202601c .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-2202601c{color:#6ec1e4;font-family:"Amarante", Sans-serif;font-size:25px;}.elementor-54 .elementor-element.elementor-element-5ae4f81e > .elementor-container{max-width:850px;}.elementor-54 .elementor-element.elementor-element-404c6f5{text-align:center;}.elementor-54 .elementor-element.elementor-element-404c6f5 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-15551367 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-15551367{color:#6ec1e4;font-family:"Amarante", Sans-serif;font-size:25px;}.elementor-54 .elementor-element.elementor-element-7a8e6e53 > .elementor-container{max-width:850px;}.elementor-54 .elementor-element.elementor-element-35a9ef3c{text-align:center;}.elementor-54 .elementor-element.elementor-element-35a9ef3c .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-a5356ce .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-a5356ce{color:#6ec1e4;font-family:"Amarante", Sans-serif;font-size:25px;}.elementor-54 .elementor-element.elementor-element-5fa3ee5b > .elementor-container{max-width:850px;}.elementor-54 .elementor-element.elementor-element-a87b32b{text-align:center;}.elementor-54 .elementor-element.elementor-element-a87b32b .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-14503529 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-14503529{color:#6ec1e4;font-family:"Amarante", Sans-serif;font-size:25px;}.elementor-54 .elementor-element.elementor-element-3353f1cc > .elementor-container{max-width:850px;}.elementor-54 .elementor-element.elementor-element-7f5c8083{text-align:center;}.elementor-54 .elementor-element.elementor-element-7f5c8083 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-67122075 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-67122075{color:#6ec1e4;font-family:"Amarante", Sans-serif;font-size:25px;}.elementor-54 .elementor-element.elementor-element-38576987 > .elementor-container{max-width:850px;}.elementor-54 .elementor-element.elementor-element-59d10202{text-align:center;}.elementor-54 .elementor-element.elementor-element-59d10202 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-7fdaee74 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-7fdaee74{color:#6ec1e4;font-family:"Amarante", Sans-serif;font-size:25px;}.elementor-54 .elementor-element.elementor-element-12f3a1e9 > .elementor-container{max-width:850px;}.elementor-54 .elementor-element.elementor-element-69d5ed2c{text-align:center;}.elementor-54 .elementor-element.elementor-element-69d5ed2c .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-10ab29cc .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-10ab29cc{color:#6ec1e4;font-family:"Amarante", Sans-serif;font-size:25px;}.elementor-54 .elementor-element.elementor-element-977f630 > .elementor-container{max-width:850px;}.elementor-54 .elementor-element.elementor-element-7e23cb00{text-align:center;}.elementor-54 .elementor-element.elementor-element-7e23cb00 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-49a391f7 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-49a391f7{color:#6ec1e4;font-family:"Amarante", Sans-serif;font-size:25px;}.elementor-54 .elementor-element.elementor-element-1c7ecc85 > .elementor-container{max-width:850px;}.elementor-54 .elementor-element.elementor-element-191549b{text-align:center;}.elementor-54 .elementor-element.elementor-element-191549b .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-13674e05 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-13674e05{color:#6ec1e4;font-family:"Amarante", Sans-serif;font-size:25px;}.elementor-54 .elementor-element.elementor-element-43cb76c4 > .elementor-container{max-width:850px;}.elementor-54 .elementor-element.elementor-element-69a4801c{text-align:center;}.elementor-54 .elementor-element.elementor-element-69a4801c .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-652faa84 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-652faa84{color:#6ec1e4;font-family:"Amarante", Sans-serif;font-size:25px;}.elementor-54 .elementor-element.elementor-element-40a8e7db > .elementor-container{max-width:850px;}.elementor-54 .elementor-element.elementor-element-135a8b6{text-align:center;}.elementor-54 .elementor-element.elementor-element-135a8b6 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-10e8b79a .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-10e8b79a{color:#6ec1e4;font-family:"Amarante", Sans-serif;font-size:25px;}.elementor-54 .elementor-element.elementor-element-5844c636 > .elementor-container{max-width:850px;}.elementor-54 .elementor-element.elementor-element-4f256312{text-align:center;}.elementor-54 .elementor-element.elementor-element-4f256312 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-3869cc59 .elementor-spacer-inner{height:20px;}.elementor-54 .elementor-element.elementor-element-7cbd872f .elementor-spacer-inner{height:20px;}.elementor-54 .elementor-element.elementor-element-71b1640{background-color:#f4f4f4;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;margin-top:0px;margin-bottom:0px;padding:10px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-71b1640 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-622bd53d > .elementor-element-populated{transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;}.elementor-54 .elementor-element.elementor-element-622bd53d > .elementor-element-populated > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-36f6a533 .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-36f6a533{font-size:25px;}.elementor-54 .elementor-element.elementor-element-3f73ab4e .elementor-icon-list-item:not(:last-child){padding-bottom:calc(5px/2);}.elementor-54 .elementor-element.elementor-element-3f73ab4e .elementor-icon-list-item:not(:first-child){margin-top:calc(5px/2);}.elementor-54 .elementor-element.elementor-element-3f73ab4e .elementor-icon-list-icon{width:20px;}.elementor-54 .elementor-element.elementor-element-3f73ab4e .elementor-icon-list-icon i{font-size:20px;}.elementor-54 .elementor-element.elementor-element-3f73ab4e .elementor-icon-list-text{color:#000000;}.elementor-54 .elementor-element.elementor-element-3f73ab4e > .elementor-widget-container{margin:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-7a0f3548 .elementor-icon-list-item:not(:last-child){padding-bottom:calc(5px/2);}.elementor-54 .elementor-element.elementor-element-7a0f3548 .elementor-icon-list-item:not(:first-child){margin-top:calc(5px/2);}.elementor-54 .elementor-element.elementor-element-7a0f3548 .elementor-icon-list-icon{width:20px;}.elementor-54 .elementor-element.elementor-element-7a0f3548 .elementor-icon-list-icon i{font-size:20px;}.elementor-54 .elementor-element.elementor-element-7a0f3548 .elementor-icon-list-text{color:#000000;}.elementor-54 .elementor-element.elementor-element-7a0f3548 > .elementor-widget-container{margin:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-1ecb598f .elementor-spacer-inner{height:20px;}.elementor-54 .elementor-element.elementor-element-c0502c5 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-2505f0a6{text-align:center;}.elementor-54 .elementor-element.elementor-element-2505f0a6 .elementor-heading-title{color:#009ee2;font-family:"Montserrat", Sans-serif;font-size:20px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-6c4c8755{text-align:center;}.elementor-54 .elementor-element.elementor-element-6c4c8755 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-3a50197f{text-align:center;}.elementor-54 .elementor-element.elementor-element-3a50197f .elementor-heading-title{color:#000000;font-family:"Montserrat", Sans-serif;font-size:20px;font-weight:500;font-style:normal;}.elementor-54 .elementor-element.elementor-element-6e8416d7{text-align:center;}.elementor-54 .elementor-element.elementor-element-6e8416d7 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-703e58ba{text-align:center;}.elementor-54 .elementor-element.elementor-element-703e58ba .elementor-heading-title{color:#f9a600;font-family:"Montserrat", Sans-serif;font-size:20px;font-weight:500;}.elementor-54 .elementor-element.elementor-element-64a506f4{text-align:center;}.elementor-54 .elementor-element.elementor-element-64a506f4 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-23449903{text-align:center;}.elementor-54 .elementor-element.elementor-element-23449903 .elementor-image img{max-width:100%;opacity:1;}.elementor-54 .elementor-element.elementor-element-6b88b5d0 .elementor-spacer-inner{height:20px;}.elementor-54 .elementor-element.elementor-element-645d3062{background-color:#ffffff;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;padding:20px 0px 30px 0px;}.elementor-54 .elementor-element.elementor-element-645d3062 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-72ff2e66{color:#000000;font-family:"Montserrat", Sans-serif;font-size:20px;}.elementor-54 .elementor-element.elementor-element-15d4cf63{background-image:url("wp-content/uploads/2020/01/bg-patternsegitiga-1.jpg");background-position:bottom center;background-repeat:repeat-x;background-size:contain;transition:background 0.3s, border 0.3s, border-radius 0.3s, box-shadow 0.3s;padding:20px 0px 30px 0px;}.elementor-54 .elementor-element.elementor-element-15d4cf63 > .elementor-background-overlay{transition:background 0.3s, border-radius 0.3s, opacity 0.3s;}.elementor-54 .elementor-element.elementor-element-34de9802{text-align:center;}.elementor-54 .elementor-element.elementor-element-34de9802 .elementor-image img{max-width:79%;opacity:1;box-shadow:0px 0px 10px 0px rgba(0,0,0,0.5);}.elementor-54 .elementor-element.elementor-element-4ba5b1e2 .elementor-spacer-inner{height:32px;}.elementor-54 .elementor-element.elementor-element-466626b4 a.elementor-button, .elementor-54 .elementor-element.elementor-element-466626b4 .elementor-button{text-transform:uppercase;letter-spacing:1px;background-color:#468fc4;}.elementor-54 .elementor-element.elementor-element-145aa121 a.elementor-button, .elementor-54 .elementor-element.elementor-element-145aa121 .elementor-button{text-transform:uppercase;background-color:#eda831;}.elementor-54 .elementor-element.elementor-element-7dab387a .elementor-spacer-inner{height:21px;}.elementor-54 .elementor-element.elementor-element-53103856 .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-698ba1dc .elementor-text-editor{text-align:center;}.elementor-54 .elementor-element.elementor-element-698ba1dc{font-size:14px;}@media(min-width:768px){.elementor-54 .elementor-element.elementor-element-5a4b04e1{width:12.857%;}.elementor-54 .elementor-element.elementor-element-78cce328{width:74.732%;}.elementor-54 .elementor-element.elementor-element-1e854728{width:12.411%;}.elementor-54 .elementor-element.elementor-element-7cb4da14{width:15.857%;}.elementor-54 .elementor-element.elementor-element-26d41531{width:84.143%;}.elementor-54 .elementor-element.elementor-element-580d161c{width:50%;}.elementor-54 .elementor-element.elementor-element-411dc27a{width:50%;}.elementor-54 .elementor-element.elementor-element-6ce7c816{width:54.196%;}.elementor-54 .elementor-element.elementor-element-74d8a9b9{width:45.803%;}}@media(max-width:1024px){.elementor-54 .elementor-element.elementor-element-e2613bc .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-7c3a841a a.elementor-button, .elementor-54 .elementor-element.elementor-element-7c3a841a .elementor-button{font-size:16px;}.elementor-54 .elementor-element.elementor-element-4855be85{font-size:17px;}.elementor-54 .elementor-element.elementor-element-28d6480c{font-size:22px;}.elementor-54 .elementor-element.elementor-element-50ea55bd .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-33974ce a.elementor-button, .elementor-54 .elementor-element.elementor-element-33974ce .elementor-button{font-size:14px;}.elementor-54 .elementor-element.elementor-element-7b99faf3{text-align:center;}.elementor-54 .elementor-element.elementor-element-26d41531 > .elementor-element-populated{padding:10px 10px 10px 10px;}.elementor-54 .elementor-element.elementor-element-1e3dc364{line-height:1.7em;}.elementor-54 .elementor-element.elementor-element-4d199919{text-align:center;}.elementor-54 .elementor-element.elementor-element-504e5baf{font-size:29px;}.elementor-54 .elementor-element.elementor-element-308364f5{font-size:29px;}.elementor-54 .elementor-element.elementor-element-462158a0{font-size:29px;}.elementor-54 .elementor-element.elementor-element-56b3a456{font-size:23px;}.elementor-54 .elementor-element.elementor-element-1f4ab39{font-size:23px;}.elementor-54 .elementor-element.elementor-element-37ebe027{font-size:29px;}.elementor-54 .elementor-element.elementor-element-3099c5a9 a.elementor-button, .elementor-54 .elementor-element.elementor-element-3099c5a9 .elementor-button{font-size:16px;}.elementor-54 .elementor-element.elementor-element-1330f9e2{font-size:29px;}.elementor-54 .elementor-element.elementor-element-73d4fbba a.elementor-button, .elementor-54 .elementor-element.elementor-element-73d4fbba .elementor-button{font-size:16px;}.elementor-54 .elementor-element.elementor-element-af24312{font-size:29px;}.elementor-54 .elementor-element.elementor-element-383ee4c6{font-size:29px;}.elementor-54 .elementor-element.elementor-element-4803d518{font-size:29px;}.elementor-54 .elementor-element.elementor-element-4884c207 a.elementor-button, .elementor-54 .elementor-element.elementor-element-4884c207 .elementor-button{font-size:16px;}.elementor-54 .elementor-element.elementor-element-1d21d7cd{font-size:29px;}.elementor-54 .elementor-element.elementor-element-7ade8034{font-size:15px;}.elementor-54 .elementor-element.elementor-element-9df7c6c{padding:50px 50px 0px 50px;}.elementor-54 .elementor-element.elementor-element-71c0426b .elementor-heading-title{font-size:17px;}.elementor-54 .elementor-element.elementor-element-5015b750{text-align:center;}.elementor-54 .elementor-element.elementor-element-29ecbe2{font-size:15px;}.elementor-54 .elementor-element.elementor-element-2820ec23{font-size:15px;}.elementor-54 .elementor-element.elementor-element-7b97870f{font-size:15px;}.elementor-54 .elementor-element.elementor-element-6fdab66{font-size:15px;}.elementor-54 .elementor-element.elementor-element-492b03ec{font-size:23px;}.elementor-54 .elementor-element.elementor-element-7252a747{text-align:center;}.elementor-54 .elementor-element.elementor-element-39867e95{text-align:center;}.elementor-54 .elementor-element.elementor-element-74d3cb6c{text-align:center;}.elementor-54 .elementor-element.elementor-element-50917378{text-align:center;}.elementor-54 .elementor-element.elementor-element-cb532fb{text-align:center;}.elementor-54 .elementor-element.elementor-element-363e9bd7{text-align:center;}.elementor-54 .elementor-element.elementor-element-74de115b{text-align:center;}.elementor-54 .elementor-element.elementor-element-5a5df74c{text-align:center;}.elementor-54 .elementor-element.elementor-element-2d4066f2{text-align:center;}.elementor-54 .elementor-element.elementor-element-64e66d69{text-align:center;}.elementor-54 .elementor-element.elementor-element-1c4126e2{text-align:center;}.elementor-54 .elementor-element.elementor-element-5f5c2187{text-align:center;}.elementor-54 .elementor-element.elementor-element-4063dc8f a.elementor-button, .elementor-54 .elementor-element.elementor-element-4063dc8f .elementor-button{font-size:14px;}.elementor-54 .elementor-element.elementor-element-6146f0f8{font-size:23px;}.elementor-54 .elementor-element.elementor-element-1709d5fb .elementor-counter-number-wrapper{font-size:60px;}.elementor-54 .elementor-element.elementor-element-3c4ba7aa .elementor-counter-number-wrapper{font-size:60px;}.elementor-54 .elementor-element.elementor-element-47e24f78 .elementor-counter-number-wrapper{font-size:60px;}.elementor-54 .elementor-element.elementor-element-30bc83fe{font-size:23px;}.elementor-54 .elementor-element.elementor-element-75c93f2a{font-size:15px;}.elementor-54 .elementor-element.elementor-element-66268ea6{font-size:15px;}.elementor-54 .elementor-element.elementor-element-41998dfa{font-size:15px;}.elementor-54 .elementor-element.elementor-element-34e06367 .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-712d0935{font-size:25px;}.elementor-54 .elementor-element.elementor-element-2ec94ba4 .elementor-heading-title{font-size:10px;}.elementor-54 .elementor-element.elementor-element-142f2c56 .elementor-heading-title{font-size:10px;}.elementor-54 .elementor-element.elementor-element-1ccf387c .elementor-heading-title{font-size:10px;}.elementor-54 .elementor-element.elementor-element-7bf2075b .elementor-heading-title{font-size:10px;}.elementor-54 .elementor-element.elementor-element-4cde7cb7 .elementor-heading-title{font-size:10px;}.elementor-54 .elementor-element.elementor-element-605a3fce .elementor-heading-title{font-size:10px;}.elementor-54 .elementor-element.elementor-element-e656c71 .elementor-heading-title{font-size:10px;}.elementor-54 .elementor-element.elementor-element-4b28d198 .elementor-heading-title{font-size:10px;}.elementor-54 .elementor-element.elementor-element-189a97a2 .elementor-heading-title{font-size:10px;}.elementor-54 .elementor-element.elementor-element-1714c842 .elementor-heading-title{font-size:10px;}.elementor-54 .elementor-element.elementor-element-21dc1 .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-c9aee6b{font-size:25px;}.elementor-54 .elementor-element.elementor-element-5390f5be .elementor-heading-title{font-size:10px;}.elementor-54 .elementor-element.elementor-element-65c4ff36 .elementor-heading-title{font-size:10px;}.elementor-54 .elementor-element.elementor-element-7118a9c9 .elementor-heading-title{font-size:10px;}.elementor-54 .elementor-element.elementor-element-3413129f .elementor-heading-title{font-size:10px;}.elementor-54 .elementor-element.elementor-element-76c4edc .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-b21a43f a.elementor-button, .elementor-54 .elementor-element.elementor-element-b21a43f .elementor-button{font-size:14px;}.elementor-54 .elementor-element.elementor-element-1d90eece{font-size:15px;}.elementor-54 .elementor-element.elementor-element-74e2d873 .elementor-spacer-inner{height:50px;}.elementor-54 .elementor-element.elementor-element-c0502c5{text-align:center;}.elementor-54 .elementor-element.elementor-element-6c4c8755{text-align:center;}.elementor-54 .elementor-element.elementor-element-6e8416d7{text-align:center;}.elementor-54 .elementor-element.elementor-element-64a506f4{text-align:center;}.elementor-54 .elementor-element.elementor-element-23449903{text-align:center;}.elementor-54 .elementor-element.elementor-element-34de9802{text-align:center;}.elementor-54 .elementor-element.elementor-element-4ba5b1e2 .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-7dab387a .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-53103856 .elementor-spacer-inner{height:10px;}}@media(max-width:767px){.elementor-54 .elementor-element.elementor-element-e2613bc .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-7c3a841a a.elementor-button, .elementor-54 .elementor-element.elementor-element-7c3a841a .elementor-button{font-size:15px;}.elementor-54 .elementor-element.elementor-element-4855be85{font-size:13px;}.elementor-54 .elementor-element.elementor-element-28d6480c{font-size:14px;}.elementor-54 .elementor-element.elementor-element-50ea55bd .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-ec1c0ac > .elementor-element-populated{padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-306887a4{padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-7b99faf3{text-align:center;}.elementor-54 .elementor-element.elementor-element-26d41531 > .elementor-element-populated{padding:10px 10px 10px 10px;}.elementor-54 .elementor-element.elementor-element-1e3dc364{font-size:24px;line-height:1.5em;}.elementor-54 .elementor-element.elementor-element-1e3dc364 > .elementor-widget-container{margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;}.elementor-54 .elementor-element.elementor-element-4d199919{text-align:center;}.elementor-54 .elementor-element.elementor-element-200ab648 .elementor-icon-list-icon{width:20px;}.elementor-54 .elementor-element.elementor-element-200ab648 .elementor-icon-list-icon i{font-size:20px;}.elementor-54 .elementor-element.elementor-element-200ab648 .elementor-icon-list-item{font-size:20px;}.elementor-54 .elementor-element.elementor-element-504e5baf{font-size:22px;}.elementor-54 .elementor-element.elementor-element-36cacd19 .elementor-icon-list-icon{width:20px;}.elementor-54 .elementor-element.elementor-element-36cacd19 .elementor-icon-list-icon i{font-size:20px;}.elementor-54 .elementor-element.elementor-element-36cacd19 .elementor-icon-list-item{font-size:20px;}.elementor-54 .elementor-element.elementor-element-308364f5{font-size:22px;}.elementor-54 .elementor-element.elementor-element-462158a0{font-size:22px;}.elementor-54 .elementor-element.elementor-element-56b3a456{font-size:16px;}.elementor-54 .elementor-element.elementor-element-1f4ab39{font-size:16px;}.elementor-54 .elementor-element.elementor-element-37ebe027{font-size:22px;}.elementor-54 .elementor-element.elementor-element-3099c5a9 a.elementor-button, .elementor-54 .elementor-element.elementor-element-3099c5a9 .elementor-button{font-size:15px;}.elementor-54 .elementor-element.elementor-element-1330f9e2{font-size:22px;}.elementor-54 .elementor-element.elementor-element-73d4fbba a.elementor-button, .elementor-54 .elementor-element.elementor-element-73d4fbba .elementor-button{font-size:15px;}.elementor-54 .elementor-element.elementor-element-af24312{font-size:22px;}.elementor-54 .elementor-element.elementor-element-383ee4c6{font-size:22px;}.elementor-54 .elementor-element.elementor-element-4803d518{font-size:22px;}.elementor-54 .elementor-element.elementor-element-4884c207 a.elementor-button, .elementor-54 .elementor-element.elementor-element-4884c207 .elementor-button{font-size:15px;}.elementor-54 .elementor-element.elementor-element-1d21d7cd{font-size:22px;}.elementor-54 .elementor-element.elementor-element-9df7c6c{padding:60px 30px 0px 30px;}.elementor-54 .elementor-element.elementor-element-5015b750{text-align:center;}.elementor-54 .elementor-element.elementor-element-492b03ec{font-size:16px;}.elementor-54 .elementor-element.elementor-element-7252a747{text-align:center;}.elementor-54 .elementor-element.elementor-element-39867e95{text-align:center;}.elementor-54 .elementor-element.elementor-element-74d3cb6c{text-align:center;}.elementor-54 .elementor-element.elementor-element-50917378{text-align:center;}.elementor-54 .elementor-element.elementor-element-cb532fb{text-align:center;}.elementor-54 .elementor-element.elementor-element-363e9bd7{text-align:center;}.elementor-54 .elementor-element.elementor-element-74de115b{text-align:center;}.elementor-54 .elementor-element.elementor-element-5a5df74c{text-align:center;}.elementor-54 .elementor-element.elementor-element-2d4066f2{text-align:center;}.elementor-54 .elementor-element.elementor-element-64e66d69{text-align:center;}.elementor-54 .elementor-element.elementor-element-1c4126e2{text-align:center;}.elementor-54 .elementor-element.elementor-element-5f5c2187{text-align:center;}.elementor-54 .elementor-element.elementor-element-6146f0f8{font-size:16px;}.elementor-54 .elementor-element.elementor-element-30bc83fe{font-size:16px;}.elementor-54 .elementor-element.elementor-element-34e06367 .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-2ec94ba4 .elementor-heading-title{font-size:13px;}.elementor-54 .elementor-element.elementor-element-142f2c56 .elementor-heading-title{font-size:13px;}.elementor-54 .elementor-element.elementor-element-1ccf387c .elementor-heading-title{font-size:13px;}.elementor-54 .elementor-element.elementor-element-7bf2075b .elementor-heading-title{font-size:13px;}.elementor-54 .elementor-element.elementor-element-4cde7cb7 .elementor-heading-title{font-size:13px;}.elementor-54 .elementor-element.elementor-element-605a3fce .elementor-heading-title{font-size:13px;}.elementor-54 .elementor-element.elementor-element-e656c71 .elementor-heading-title{font-size:13px;}.elementor-54 .elementor-element.elementor-element-4b28d198 .elementor-heading-title{font-size:13px;}.elementor-54 .elementor-element.elementor-element-189a97a2 .elementor-heading-title{font-size:13px;}.elementor-54 .elementor-element.elementor-element-1714c842 .elementor-heading-title{font-size:13px;}.elementor-54 .elementor-element.elementor-element-21dc1 .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-5390f5be .elementor-heading-title{font-size:13px;}.elementor-54 .elementor-element.elementor-element-65c4ff36 .elementor-heading-title{font-size:13px;}.elementor-54 .elementor-element.elementor-element-7118a9c9 .elementor-heading-title{font-size:13px;}.elementor-54 .elementor-element.elementor-element-3413129f .elementor-heading-title{font-size:13px;}.elementor-54 .elementor-element.elementor-element-76c4edc .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-7e79502e .elementor-heading-title{font-size:42px;}.elementor-54 .elementor-element.elementor-element-74e2d873 .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-c0502c5{text-align:center;}.elementor-54 .elementor-element.elementor-element-6c4c8755{text-align:center;}.elementor-54 .elementor-element.elementor-element-6e8416d7{text-align:center;}.elementor-54 .elementor-element.elementor-element-64a506f4{text-align:center;}.elementor-54 .elementor-element.elementor-element-23449903{text-align:center;}.elementor-54 .elementor-element.elementor-element-34de9802{text-align:center;}.elementor-54 .elementor-element.elementor-element-4ba5b1e2 .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-7dab387a .elementor-spacer-inner{height:10px;}.elementor-54 .elementor-element.elementor-element-53103856 .elementor-spacer-inner{height:10px;}}@media(min-width:1025px){.elementor-54 .elementor-element.elementor-element-7214ec32{background-attachment:fixed;}.elementor-54 .elementor-element.elementor-element-7e0b8fc{background-attachment:fixed;}.elementor-54 .elementor-element.elementor-element-9df7c6c{background-attachment:fixed;}.elementor-54 .elementor-element.elementor-element-73d78769{background-attachment:fixed;}.elementor-54 .elementor-element.elementor-element-5bd23d2{background-attachment:fixed;}}
</style>
<link rel='stylesheet' id='landingpress-css'  href='wp-content/themes/landingpress-wp/stylebb24.css' type='text/css' media='all' />
<link rel="canonical" href="index.html" />
<link rel='shortlink' href='index.html' />
<!-- HFCM by 99 Robots - Snippet # 1: Prove Source Sales Notification -->
<!-- Start of Async ProveSource Code --><script>!function(o,i){window.provesrc&&window.console&&console.error&&console.error("ProveSource is included twice in this page."),provesrc=window.provesrc={dq:[],display:function(){this.dq.push(arguments)}},o._provesrcAsyncInit=function(){provesrc.init({apiKey:"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhY2NvdW50SWQiOiI1ZDRkOTdlZGZiMWZjYTI4NmQzZjhiZGQiLCJpYXQiOjE1NjUzNjYyNTN9._wUMLRQCD_s-uOQgiyVcJBfNi4pATAIKXiYyYB4-xbQ",v:"0.0.4"})};var r=i.createElement("script");r.type="text/javascript",r.async=!0,r["ch"+"ar"+"set"]="UTF-8",r.src="../cdn.provesrc.com/provesrc.js";var e=i.getElementsByTagName("script")[0];e.parentNode.insertBefore(r,e)}(window,document);</script><!-- End of Async ProveSource Code -->
<!-- /end HFCM by 99 Robots -->
		<script>
			document.documentElement.className = document.documentElement.className.replace( 'no-js', 'js' );
		</script>
				<style>
			.no-js img.lazyload { display: none; }
			figure.wp-block-image img.lazyloading { min-width: 150px; }
							.lazyload, .lazyloading { opacity: 0; }
				.lazyloaded {
					opacity: 1;
					transition: opacity 400ms;
					transition-delay: 0ms;
				}
					</style>
		<link rel="stylesheet" type="text/css" href="../external.html?link=https://fonts.googleapis.com/css?family=Roboto:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic%7CRoboto+Slab:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic%7CMontserrat:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic%7CPoppins:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic%7CLato:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic%7CMontserrat+Alternates:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic%7CAmarante:100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic"><link rel="icon" href="wp-content/uploads/2020/01/logopanelsosmedxXx-150x150.png" sizes="32x32" />
<link rel="icon" href="<?php echo $config['web']['url'] ?>assets/media/logos/rsz_logos.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="wp-content/uploads/2020/01/logopanelsosmedxXx.png" />
<meta name="msapplication-TileImage" content="https://review.panelsosmed.com/wp-content/uploads/2020/01/logopanelsosmedxXx.png" />
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','../connect.facebook.net/en_US/fbevents.js');
fbq('init', '#');
fbq('track', 'PageView', {
	"source": "landingpress",
	"version": "2.9.5.4"
});
</script>
<!-- End 
Facebook Pixel Code -->
<script>
if ( typeof(fbq) != 'undefined' ) { 
	fbq('track', "ViewContent", {"source":"landingpress","version":"2.9.5.4","domain":"review.panelsosmed.com","campaign_url":"review-panelsosmed","content_name":"Review Panelsosmed","post_type":"page","value":"0.00","currency":"IDR"});
}
</script>
<!-- Global site tag (gtag.js) - Google Ads (AdWords) & Analytics -->
<script async src="../external.html?link=https://www.googletagmanager.com/gtag/js?id=UA-#"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-#');
</script>
<!-- End Global site tag (gtag.js) - Google Ads (AdWords) & Analytics -->
</head>
<body class="home page-template page-template-page_landingpress page-template-page_landingpress-php page page-id-54 page-landingpress page-landingpress-full header-inactive footer-inactive elementor-default elementor-page elementor-page-54">
<a class="skip-link screen-reader-text" href="#content">Skip to content</a>
<div class="site-canvas">
<div id="page" class="site-container">
<div class="site-inner">
		<div id="content" class="site-content">
		<div class="container">
			<div id="primary" class="content-area">
	<main id="main" class="site-main">
			<div class="elementor elementor-54">
			<div class="elementor-inner">
				<div class="elementor-section-wrap">
							<section data-id="7214ec32" class="elementor-element elementor-element-7214ec32 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="7157c1d4" class="elementor-element elementor-element-7157c1d4 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
							<div class="elementor-background-overlay"></div>
					<div class="elementor-widget-wrap">
				<div data-id="e2613bc" class="elementor-element elementor-element-e2613bc elementor-widget elementor-widget-spacer" data-element_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div data-id="7c3a841a" class="elementor-element elementor-element-7c3a841a elementor-align-center animated bounceIn elementor-invisible elementor-widget elementor-widget-button" data-settings="{&quot;_animation&quot;:&quot;bounceIn&quot;}" data-element_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper elementor-button-sticky-no">
			<a class="elementor-button elementor-size-sm elementor-animation-grow">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text"><b>RAHASIA</b> ini Belum Banyak Orang Yang Tau</span>
		</span>
					</a>
		</div>
				</div>
				</div>
				<div data-id="4855be85" class="elementor-element elementor-element-4855be85 animated fadeIn elementor-invisible elementor-widget elementor-widget-text-editor" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h1><strong>Mungkin Anda Penasaran, Bagaimana Caranya <span style="color: #ff0000;">Jasa Tambah Followers</span> Bisa Menambah Ribuan Followers Secara </strong><strong><span style="color: #ff0000;">Cepat dan Instant</span></strong></h1></div>
				</div>
				</div>
				<div data-id="28d6480c" class="elementor-element elementor-element-28d6480c animated fadeIn elementor-invisible elementor-widget elementor-widget-text-editor" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h4>Sebenarnya ini adalah <strong>RAHASIA</strong>, Tapi Tenang Akan Kami <strong>Bocorkan RAHASIA </strong>Tersebut, Hanya Untuk <strong>Anda</strong> Disini</h4></div>
				</div>
				</div>
				<section data-id="72e54623" class="elementor-element elementor-element-72e54623 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="5a4b04e1" class="elementor-element elementor-element-5a4b04e1 elementor-column elementor-col-33 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap">
					<div class="elementor-widget-wrap">
						</div>
			</div>
		</div>
				<div data-id="78cce328" class="elementor-element elementor-element-78cce328 elementor-column elementor-col-33 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="50ea55bd" class="elementor-element elementor-element-50ea55bd elementor-widget elementor-widget-spacer" data-element_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div data-id="33974ce" class="elementor-element elementor-element-33974ce elementor-align-center elementor-mobile-align-justify animated fadeIn elementor-invisible elementor-widget elementor-widget-button" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper elementor-button-sticky-no">
			<a href="#mau" class="elementor-button-link elementor-button elementor-size-lg elementor-animation-grow">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Ya, Saya Ingin Tau</span>
		</span>
					</a>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="1e854728" class="elementor-element elementor-element-1e854728 elementor-column elementor-col-33 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap">
					<div class="elementor-widget-wrap">
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="fa485ec" class="elementor-element elementor-element-fa485ec elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="ec1c0ac" class="elementor-element elementor-element-ec1c0ac elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="c5158ac" class="elementor-element elementor-element-c5158ac elementor-widget elementor-widget-menu-anchor" data-element_type="menu-anchor.default">
				<div class="elementor-widget-container">
					<div id="mau" class="elementor-menu-anchor"></div>
				</div>
				</div>
				<section data-id="306887a4" class="elementor-element elementor-element-306887a4 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="7cb4da14" class="elementor-element elementor-element-7cb4da14 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="420489d3" class="elementor-element elementor-element-420489d3 elementor-widget elementor-widget-spacer" data-element_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div data-id="7b99faf3" class="elementor-element elementor-element-7b99faf3 animated fadeIn elementor-hidden-phone elementor-invisible elementor-widget elementor-widget-image" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="1" height="1"   alt="" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/mind.png" class="attachment-thumbnail size-thumbnail lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="1" height="1" src="wp-content/uploads/2020/01/mind.png" class="attachment-thumbnail size-thumbnail" alt="" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="26d41531" class="elementor-element elementor-element-26d41531 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="1e3dc364" class="elementor-element elementor-element-1e3dc364 animated fadeIn elementor-invisible elementor-widget elementor-widget-text-editor" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h2>Tahukah <span style="color: #de5f5f;"><strong>Anda?</strong></span><br />Apa Saja Manfaat Banyak Followers Untuk Akun Sosial Media Anda, Lihat ini :</h2></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="4767e8ed" class="elementor-element elementor-element-4767e8ed elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="6e237a39" class="elementor-element elementor-element-6e237a39 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="4d199919" class="elementor-element elementor-element-4d199919 elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="351" height="550"   alt="technobooks.my.id" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/kaget-1.png" class="attachment-large size-large lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="351" height="550" src="wp-content/uploads/2020/01/kaget-1.png" class="attachment-large size-large" alt="technobooks.my.id" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="6cf54243" class="elementor-element elementor-element-6cf54243 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="200ab648" class="elementor-element elementor-element-200ab648 elementor-align-left animated fadeIn elementor-invisible elementor-widget elementor-widget-icon-list" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Akun Sosial Media Anda Makin Di Kenal</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Akun Sosial Media Anda Makin Di Percaya</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Meningkatkan Interaksi Likes Comment & Share </span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Meningkatkan Closing Penjualan</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Anda Bisa Menjadi Celebgram</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Anda Bisa Buka Jasa Endorse Produk</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Mudah Untuk Menjual Apapun Di Akun Sosial Media Anda</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Meningkatkan Traffic Web dan Youtube Anda</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Menjadikan Akun Sosial Media Anda Media Promosi Yang Ampuh</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Dan Segudang Manfaat Lainnyaa...</span>
									</li>
						</ul>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="5ebf021e" class="elementor-element elementor-element-5ebf021e elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="531bed3e" class="elementor-element elementor-element-531bed3e elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="504e5baf" class="elementor-element elementor-element-504e5baf animated fadeIn elementor-invisible elementor-widget elementor-widget-text-editor" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3>Dan Faktanya, Orang Lain Cenderung Akan :</h3></div>
				</div>
				</div>
				<section data-id="6679df92" class="elementor-element elementor-element-6679df92 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="ebd9997" class="elementor-element elementor-element-ebd9997 elementor-column elementor-col-100 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="36cacd19" class="elementor-element elementor-element-36cacd19 elementor-align-left animated fadeIn elementor-invisible elementor-widget elementor-widget-icon-list" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Memfollow Akun Sosial Media yang Sudah mempunyai Banyak Followers</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Membeli Produk di Akun Onlineshop yang mempunyai Banyak Followers</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Berinteraksi Likes Comment &amp; Share Di Akun Sosial Media Yang Mempunyai Banyak Followers</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Meng-Endorse Akun Sosial Media Yang Mempunyai Banyak Followers</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Lebih Percaya Kepada Akun Sosial Media Yang Mempunyai Banyak Followers daripada Yang Minim Followers</span>
									</li>
						</ul>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="357cae68" class="elementor-element elementor-element-357cae68 elementor-column elementor-col-100 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="23ae49b4" class="elementor-element elementor-element-23ae49b4 elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="287" height="550"   alt="technobooks.my.id" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/nunjuk-1.png" class="attachment-large size-large lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="287" height="550" src="wp-content/uploads/2020/01/nunjuk-1.png" class="attachment-large size-large" alt="technobooks.my.id" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="524a86ae" class="elementor-element elementor-element-524a86ae elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
							<div class="elementor-background-overlay"></div>
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="2467b4f2" class="elementor-element elementor-element-2467b4f2 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="308364f5" class="elementor-element elementor-element-308364f5 animated fadeIn elementor-invisible elementor-widget elementor-widget-text-editor" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3 style="text-align: center;">Dan, Pasti Anda Bertanya-tanya Apakah Bisa<br /> <strong>&#8220;Menambah Ribuan Followers</strong><strong>&#8221; </strong>Secara Instant dan Cepat ?</h3></div>
				</div>
				</div>
				<section data-id="4c952d17" class="elementor-element elementor-element-4c952d17 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="41b7a56b" class="elementor-element elementor-element-41b7a56b elementor-column elementor-col-33 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap">
					<div class="elementor-widget-wrap">
						</div>
			</div>
		</div>
				<div data-id="77c7074c" class="elementor-element elementor-element-77c7074c elementor-column elementor-col-33 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="203e42ea" class="elementor-element elementor-element-203e42ea elementor-align-justify elementor-widget elementor-widget-button" data-element_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a class="elementor-button elementor-size-sm">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text"></span>
		</span>
					</a>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="305fcc1c" class="elementor-element elementor-element-305fcc1c elementor-column elementor-col-33 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap">
					<div class="elementor-widget-wrap">
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<div data-id="462158a0" class="elementor-element elementor-element-462158a0 animated fadeIn elementor-invisible elementor-widget elementor-widget-text-editor" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3 style="text-align: center;">Tentu Saja <span style="color: #ff0000;"><strong>BISA !!!</strong></span></h3><h3><strong>Karena</strong>, Cara ini Yang Digunakan Oleh <span style="color: #000000;"><strong>Jasa Tambah Followers</strong></span> Untuk Menambah Followers <span style="color: #000000;"><strong>Secara Cepat &amp; Instant</strong></span> Sesuai Pesanan</h3></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="18f8beab" class="elementor-element elementor-element-18f8beab elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="423c9385" class="elementor-element elementor-element-423c9385 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="56b3a456" class="elementor-element elementor-element-56b3a456 animated fadeIn elementor-invisible elementor-widget elementor-widget-text-editor" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3>Namun <span style="color: #ff0000;"><strong>Sebelumnyaa..</strong></span></h3></div>
				</div>
				</div>
				<div data-id="1f4ab39" class="elementor-element elementor-element-1f4ab39 animated fadeIn elementor-invisible elementor-widget elementor-widget-text-editor" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3><strong>Mungkin Anda</strong> Pernah Melakukan Hal Ini :</h3></div>
				</div>
				</div>
				<section data-id="14d0e365" class="elementor-element elementor-element-14d0e365 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="f3b819e" class="elementor-element elementor-element-f3b819e elementor-column elementor-col-33 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="1860a361" class="elementor-element elementor-element-1860a361 elementor-align-left elementor-widget elementor-widget-icon-list" data-element_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-hand-o-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text"><b>Ngefollow</b> Akun Orang Lain</span>
									</li>
						</ul>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="227cb928" class="elementor-element elementor-element-227cb928 elementor-column elementor-col-33 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="7a7d1d42" class="elementor-element elementor-element-7a7d1d42 elementor-align-left elementor-widget elementor-widget-icon-list" data-element_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-hand-o-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text"><b>Ngelikes</b> Foto Orang Lain</span>
									</li>
						</ul>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="446a69e1" class="elementor-element elementor-element-446a69e1 elementor-column elementor-col-33 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="7852997b" class="elementor-element elementor-element-7852997b elementor-align-left elementor-widget elementor-widget-icon-list" data-element_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-hand-o-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text"><b>Comment</b> Dipostingan Orang Lain</span>
									</li>
						</ul>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<div data-id="37ebe027" class="elementor-element elementor-element-37ebe027 animated fadeIn elementor-invisible elementor-widget elementor-widget-text-editor" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3 style="text-align: center;">Dan Aktifitas Lainnya Guna Mengharapkan Interaksi <strong>Likes atau Follow Balik</strong> Dari Orang Lain Tersebut</h3></div>
				</div>
				</div>
				<div data-id="3099c5a9" class="elementor-element elementor-element-3099c5a9 elementor-align-center animated fadeIn elementor-invisible elementor-widget elementor-widget-button" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a class="elementor-button elementor-size-md elementor-animation-pop">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text"><b>Namun Apa Yang TERJADI ??</b> </span>
		</span>
					</a>
		</div>
				</div>
				</div>
				<div data-id="1330f9e2" class="elementor-element elementor-element-1330f9e2 animated fadeIn elementor-invisible elementor-widget elementor-widget-text-editor" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3>Anda <strong>Akhirnya Frustrasi</strong>, Karena <b>Followers Tak Kunjung Bertambah</b> Meskipun Sudah Melakukan Banyak Cara</h3></div>
				</div>
				</div>
				<div data-id="73d4fbba" class="elementor-element elementor-element-73d4fbba elementor-align-center animated fadeIn elementor-invisible elementor-widget elementor-widget-button" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a class="elementor-button elementor-size-md elementor-animation-pop">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Bukannya <b>Followers</b>.. eehhh Malah <b>FOLLOWING</b> nya Yang Makin Bertambah, Iya Kan?</span>
		</span>
					</a>
		</div>
				</div>
				</div>
				<div data-id="c33b178" class="elementor-element elementor-element-c33b178 elementor-widget elementor-widget-spacer" data-element_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="478e2d96" class="elementor-element elementor-element-478e2d96 elementor-section-boxed elementor-section-height-default elementor-section-height-default animated fadeIn elementor-invisible elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeIn&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="28432b6f" class="elementor-element elementor-element-28432b6f elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="af24312" class="elementor-element elementor-element-af24312 animated fadeIn elementor-invisible elementor-widget elementor-widget-text-editor" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3>Cara <b>Manual</b> Yang Anda Lakukan <span style="color: #ff0000;"><b>Tidak Akan Bisa</b></span> Menambah <strong>Ribuan Followers</strong> Secara <strong>Instan dan Cepat</strong>, <span style="color: #99cc00;"><b>Percaya Dehh..</b></span></h3></div>
				</div>
				</div>
				<div data-id="bc504a2" class="elementor-element elementor-element-bc504a2 animated bounceIn elementor-invisible elementor-widget elementor-widget-heading" data-settings="{&quot;_animation&quot;:&quot;bounceIn&quot;}" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-xl">Lalu Caranya Bagaimana?</h2>		</div>
				</div>
				<div data-id="383ee4c6" class="elementor-element elementor-element-383ee4c6 animated fadeIn elementor-invisible elementor-widget elementor-widget-text-editor" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3>Sudah Saatnya Anda <strong>Membutuhkan</strong> Sebuah <span style="text-decoration: underline; color: #ff0000;"><em><strong>Tools System</strong></em></span> Yang Bisa <b>Menambah Ribuan Followers</b> Secara <strong>Instan dan Cepat</strong></h3></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="3b1eee6" class="elementor-element elementor-element-3b1eee6 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
							<div class="elementor-background-overlay"></div>
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="63f65fc1" class="elementor-element elementor-element-63f65fc1 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="4803d518" class="elementor-element elementor-element-4803d518 animated fadeIn elementor-invisible elementor-widget elementor-widget-text-editor" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3 style="text-align: center;"><span style="color: #ff0000;"><b>Hahh..</b></span> Memang <span style="color: #ff0000;"><em><span style="text-decoration: underline;"><strong>Tools System</strong></span></em></span> Seperti Itu Ada?</h3></div>
				</div>
				</div>
				<div data-id="4884c207" class="elementor-element elementor-element-4884c207 elementor-align-center animated rotateIn elementor-invisible elementor-widget elementor-widget-button" data-settings="{&quot;_animation&quot;:&quot;rotateIn&quot;}" data-element_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper elementor-button-sticky-no">
			<a class="elementor-button elementor-size-md">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text"><b>Sudah Pasti, ADA !!!</b></span>
		</span>
					</a>
		</div>
				</div>
				</div>
				<div data-id="1d21d7cd" class="elementor-element elementor-element-1d21d7cd animated fadeIn elementor-invisible elementor-widget elementor-widget-text-editor" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3 style="text-align: center;">Sekarang Anda <strong>Gak Perlu Frustrasi</strong> Lagi..</h3></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="7e0b8fc" class="elementor-element elementor-element-7e0b8fc elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="5b4125b8" class="elementor-element elementor-element-5b4125b8 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="7ade8034" class="elementor-element elementor-element-7ade8034 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3 style="text-align: center;">Ada <strong>Kabar Baik</strong> Untuk Anda, Karena Sekarang Sudah Diciptakan <span style="text-decoration: underline; color: #ff0000;"><em><strong>Sebuah Tools System</strong></em></span> Yang Bisa <strong>Memecahkan Masalah Anda</strong></h3></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="9df7c6c" class="elementor-element elementor-element-9df7c6c elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
							<div class="elementor-background-overlay"></div>
						<div class="elementor-container elementor-column-gap-no">
				<div class="elementor-row">
				<div data-id="5bb0f1ca" class="elementor-element elementor-element-5bb0f1ca elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="71c0426b" class="elementor-element elementor-element-71c0426b elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default"><b>Kami Persembahkan..</b></h2>		</div>
				</div>
				<div data-id="4d74c081" class="elementor-element elementor-element-4d74c081 elementor-widget elementor-widget-spacer" data-element_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div data-id="5015b750" class="elementor-element elementor-element-5015b750 animated fadeIn elementor-invisible elementor-widget elementor-widget-image" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="389" height="479"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/Panelsosmed_New_2-1.png" class="elementor-animation-grow attachment-full size-full lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="389" height="479" src="wp-content/uploads/2020/01/Panelsosmed_New_2-1.png" class="elementor-animation-grow attachment-full size-full" alt="Panelsosmed.com" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="73d78769" class="elementor-element elementor-element-73d78769 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="4f793aaa" class="elementor-element elementor-element-4f793aaa elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="29ecbe2" class="elementor-element elementor-element-29ecbe2 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3 style="text-align: center;"><b>Technobooks</b> Adalah Sebuah Layanan <span style="text-decoration: underline; color: #ff0000;"><em><strong>Tools System</strong></em></span> Yang Bisa Menambah <strong>Ribuan Followers Secara Instan dan Cepat</strong></h3></div>
				</div>
				</div>
				<div data-id="2820ec23" class="elementor-element elementor-element-2820ec23 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3 style="text-align: center;"><em><span style="text-decoration: underline;"><span style="color: #ff0000; text-decoration: underline;"><strong>Tools System</strong></span></span></em> Ini Yang Biasa Digunakan Oleh <strong>Jasa Tambah Followers</strong> Diluaran Sana..</h3></div>
				</div>
				</div>
				<div data-id="7b97870f" class="elementor-element elementor-element-7b97870f elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3 style="text-align: center;"><strong>Bahkan,</strong> Bukan Cuma <strong>Followers</strong> Tapi <strong>Likes, View, Comment, Subscribes</strong> Bisa Ditambah Sesuai Keinginan Anda</h3></div>
				</div>
				</div>
				<div data-id="6fdab66" class="elementor-element elementor-element-6fdab66 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3 style="text-align: center;">Lebih <strong>GILA</strong> nya Lagi, Bisa Untuk <span style="text-decoration: underline; color: #ff0000;"><em><strong>Semua Sosial Media</strong></em></span></h3></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="4506ad29" class="elementor-element elementor-element-4506ad29 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="f66116c" class="elementor-element elementor-element-f66116c elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="492b03ec" class="elementor-element elementor-element-492b03ec animated fadeIn elementor-invisible elementor-widget elementor-widget-text-editor" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h2>Lihat <strong>Manfaat Fitur Dahsyat</strong> Yang Akan <strong>Anda Dapatkan</strong> :</h2></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="138d07dd" class="elementor-element elementor-element-138d07dd elementor-section-boxed elementor-section-height-default elementor-section-height-default animated fadeIn elementor-invisible elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeIn&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="4a76ce39" class="elementor-element elementor-element-4a76ce39 elementor-column elementor-col-33 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="7252a747" class="elementor-element elementor-element-7252a747 animated fadeIn elementor-invisible elementor-widget elementor-widget-image" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img  title="Panelsosmed.com" alt="Panelsosmed.com"  data-src="https://review.panelsosmed.com/wp-content/uploads/elementor/thumbs/Instagram_Link-1-ojh3y3bume6yqaaflgo6lw8qej3eet1h4yp7jaghyg.png" class="elementor-animation-grow lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img src="wp-content/uploads/elementor/thumbs/Instagram_Link-1-ojh3y3bume6yqaaflgo6lw8qej3eet1h4yp7jaghyg.png" title="Panelsosmed.com" alt="Panelsosmed.com" class="elementor-animation-grow" /></noscript>		</div>
				</div>
				</div>
				<div data-id="155db16e" class="elementor-element elementor-element-155db16e elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">INSTAGRAM</h2>		</div>
				</div>
				<div data-id="38d153a7" class="elementor-element elementor-element-38d153a7 elementor-widget elementor-widget-toggle" data-element_type="toggle.default">
				<div class="elementor-widget-container">
					<div class="elementor-toggle" role="tablist">
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-9531" class="elementor-tab-title" tabindex="9531" data-tab="1" role="tab" aria-controls="elementor-tab-content-9531">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Followers Instagram ~click~					</div>
					<div id="elementor-tab-content-9531" class="elementor-tab-content elementor-clearfix" data-tab="1" role="tabpanel" aria-labelledby="elementor-tab-title-9531"><p>System Akan Memproses Menambah Followers Secara Cepat dan Instant ke Akun Instagram Anda.</p><p>Terdapat Pilihan Followers Active Indonesia &amp; Random Worldwide (Bule).</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-9532" class="elementor-tab-title" tabindex="9532" data-tab="2" role="tab" aria-controls="elementor-tab-content-9532">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Likes Foto					</div>
					<div id="elementor-tab-content-9532" class="elementor-tab-content elementor-clearfix" data-tab="2" role="tabpanel" aria-labelledby="elementor-tab-title-9532"><p>System Akan Memproses Menambah Likes Secara Cepat dan Instant ke Foto Postingan Akun Instagram Anda.</p><p>Terdapat Pilihan Likes Active Indonesia &amp; Random Worldwide (Bule).</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-9533" class="elementor-tab-title" tabindex="9533" data-tab="3" role="tab" aria-controls="elementor-tab-content-9533">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Views Video					</div>
					<div id="elementor-tab-content-9533" class="elementor-tab-content elementor-clearfix" data-tab="3" role="tabpanel" aria-labelledby="elementor-tab-title-9533"><p>System Akan Memproses Menambah Viewers Secara Cepat dan Instant ke Video Postingan Akun Instagram Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-9534" class="elementor-tab-title" tabindex="9534" data-tab="4" role="tab" aria-controls="elementor-tab-content-9534">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Comment Postingan					</div>
					<div id="elementor-tab-content-9534" class="elementor-tab-content elementor-clearfix" data-tab="4" role="tabpanel" aria-labelledby="elementor-tab-title-9534"><p>System Akan Memproses Menambah Comment Secara Cepat dan Instant ke Postingan Akun Instagram Anda Sesuai Yang Anda Inginkan.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-9535" class="elementor-tab-title" tabindex="9535" data-tab="5" role="tab" aria-controls="elementor-tab-content-9535">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Instagram Story & Live View					</div>
					<div id="elementor-tab-content-9535" class="elementor-tab-content elementor-clearfix" data-tab="5" role="tabpanel" aria-labelledby="elementor-tab-title-9535"><p>System Akan Memproses Menambah Viewers Secara Cepat dan Instant ke Insta Story dan Insta Live Akun Instagram Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-9536" class="elementor-tab-title" tabindex="9536" data-tab="6" role="tab" aria-controls="elementor-tab-content-9536">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Instagram TV View & Likes					</div>
					<div id="elementor-tab-content-9536" class="elementor-tab-content elementor-clearfix" data-tab="6" role="tabpanel" aria-labelledby="elementor-tab-title-9536"><p>System Akan Memproses Menambah Likes dan Viewers Secara Cepat dan Instant ke Instagram TV Akun Instagram Anda.</p></div>
				</div>
					</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="6082e286" class="elementor-element elementor-element-6082e286 elementor-column elementor-col-33 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="39867e95" class="elementor-element elementor-element-39867e95 animated fadeIn elementor-invisible elementor-widget elementor-widget-image" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img  title="Panelsosmed.com" alt="Panelsosmed.com"  data-src="https://review.panelsosmed.com/wp-content/uploads/elementor/thumbs/faceboook-1-ojh3y3bume6yqaaflgo6lw8qej3eet1h4yp7jaghyg.png" class="elementor-animation-grow lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img src="wp-content/uploads/elementor/thumbs/faceboook-1-ojh3y3bume6yqaaflgo6lw8qej3eet1h4yp7jaghyg.png" title="Panelsosmed.com" alt="Panelsosmed.com" class="elementor-animation-grow" /></noscript>		</div>
				</div>
				</div>
				<div data-id="3c83020f" class="elementor-element elementor-element-3c83020f elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">FACEBOOK</h2>		</div>
				</div>
				<div data-id="698285c" class="elementor-element elementor-element-698285c elementor-widget elementor-widget-toggle" data-element_type="toggle.default">
				<div class="elementor-widget-container">
					<div class="elementor-toggle" role="tablist">
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1101" class="elementor-tab-title" tabindex="1101" data-tab="1" role="tab" aria-controls="elementor-tab-content-1101">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Followers Facebook ~click~					</div>
					<div id="elementor-tab-content-1101" class="elementor-tab-content elementor-clearfix" data-tab="1" role="tabpanel" aria-labelledby="elementor-tab-title-1101"><p>System Akan Memproses Menambah Followers Secara Cepat dan Instant ke Akun Facebook Personal Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1102" class="elementor-tab-title" tabindex="1102" data-tab="2" role="tab" aria-controls="elementor-tab-content-1102">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Likes Status/Post					</div>
					<div id="elementor-tab-content-1102" class="elementor-tab-content elementor-clearfix" data-tab="2" role="tabpanel" aria-labelledby="elementor-tab-title-1102"><p>System Akan Memproses Menambah Likes Secara Cepat dan Instant ke Photo/Status Postingan Akun Facebook Personal Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1103" class="elementor-tab-title" tabindex="1103" data-tab="3" role="tab" aria-controls="elementor-tab-content-1103">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Comment Status/Post					</div>
					<div id="elementor-tab-content-1103" class="elementor-tab-content elementor-clearfix" data-tab="3" role="tabpanel" aria-labelledby="elementor-tab-title-1103"><p>System Akan Memproses Menambah Comment Secara Cepat dan Instant ke Photo/Status Postingan Akun Facebook Personal Anda Sesuai Yang Anda Inginkan.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1104" class="elementor-tab-title" tabindex="1104" data-tab="4" role="tab" aria-controls="elementor-tab-content-1104">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Likes Fanspage					</div>
					<div id="elementor-tab-content-1104" class="elementor-tab-content elementor-clearfix" data-tab="4" role="tabpanel" aria-labelledby="elementor-tab-title-1104"><p>System Akan Memproses Menambah Likes Fanspage Secara Cepat dan Instant ke Fanspage Facebook Anda.</p></div>
				</div>
					</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="364a7da7" class="elementor-element elementor-element-364a7da7 elementor-column elementor-col-33 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="74d3cb6c" class="elementor-element elementor-element-74d3cb6c animated fadeIn elementor-invisible elementor-widget elementor-widget-image" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img  title="Panelsosmed.com" alt="Panelsosmed.com"  data-src="https://review.panelsosmed.com/wp-content/uploads/elementor/thumbs/twitter-logo-final-1-ojh3y3bume6yqaaflgo6lw8qej3eet1h4yp7jaghyg.png" class="elementor-animation-grow lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img src="wp-content/uploads/elementor/thumbs/twitter-logo-final-1-ojh3y3bume6yqaaflgo6lw8qej3eet1h4yp7jaghyg.png" title="Panelsosmed.com" alt="Panelsosmed.com" class="elementor-animation-grow" /></noscript>		</div>
				</div>
				</div>
				<div data-id="2700ecf4" class="elementor-element elementor-element-2700ecf4 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">TWITTER</h2>		</div>
				</div>
				<div data-id="34ddebf9" class="elementor-element elementor-element-34ddebf9 elementor-widget elementor-widget-toggle" data-element_type="toggle.default">
				<div class="elementor-widget-container">
					<div class="elementor-toggle" role="tablist">
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-8861" class="elementor-tab-title" tabindex="8861" data-tab="1" role="tab" aria-controls="elementor-tab-content-8861">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Followers Twitter ~click~					</div>
					<div id="elementor-tab-content-8861" class="elementor-tab-content elementor-clearfix" data-tab="1" role="tabpanel" aria-labelledby="elementor-tab-title-8861"><p>System Akan Memproses Menambah Followers Secara Cepat dan Instant ke Akun Twitter Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-8862" class="elementor-tab-title" tabindex="8862" data-tab="2" role="tab" aria-controls="elementor-tab-content-8862">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Re-Tweet					</div>
					<div id="elementor-tab-content-8862" class="elementor-tab-content elementor-clearfix" data-tab="2" role="tabpanel" aria-labelledby="elementor-tab-title-8862"><p>System Akan Memproses Re-Tweet Secara Cepat dan Instant ke Akun Twitter Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-8863" class="elementor-tab-title" tabindex="8863" data-tab="3" role="tab" aria-controls="elementor-tab-content-8863">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Favorites Tweet					</div>
					<div id="elementor-tab-content-8863" class="elementor-tab-content elementor-clearfix" data-tab="3" role="tabpanel" aria-labelledby="elementor-tab-title-8863"><p>System Akan Memproses Menambah Favorites Secara Cepat dan Instant ke Akun Twitter Anda.</p></div>
				</div>
					</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="6ffa1561" class="elementor-element elementor-element-6ffa1561 elementor-section-boxed elementor-section-height-default elementor-section-height-default animated fadeIn elementor-invisible elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeIn&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="5ba0f4cb" class="elementor-element elementor-element-5ba0f4cb elementor-column elementor-col-33 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="50917378" class="elementor-element elementor-element-50917378 animated fadeIn elementor-invisible elementor-widget elementor-widget-image" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img  title="Panelsosmed.com" alt="Panelsosmed.com"  data-src="https://review.panelsosmed.com/wp-content/uploads/elementor/thumbs/youtube-1-ojh3y49ot8891w92fz2t6e06zwyrmi57h3cp0kf3s8.png" class="elementor-animation-grow lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img src="wp-content/uploads/elementor/thumbs/youtube-1-ojh3y49ot8891w92fz2t6e06zwyrmi57h3cp0kf3s8.png" title="Panelsosmed.com" alt="Panelsosmed.com" class="elementor-animation-grow" /></noscript>		</div>
				</div>
				</div>
				<div data-id="78c8938c" class="elementor-element elementor-element-78c8938c elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">YOUTUBE</h2>		</div>
				</div>
				<div data-id="1267fada" class="elementor-element elementor-element-1267fada elementor-widget elementor-widget-toggle" data-element_type="toggle.default">
				<div class="elementor-widget-container">
					<div class="elementor-toggle" role="tablist">
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-3081" class="elementor-tab-title" tabindex="3081" data-tab="1" role="tab" aria-controls="elementor-tab-content-3081">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Subscribes Youtube ~click~					</div>
					<div id="elementor-tab-content-3081" class="elementor-tab-content elementor-clearfix" data-tab="1" role="tabpanel" aria-labelledby="elementor-tab-title-3081"><p>System Akan Memproses Menambah Subscribes Secara Cepat dan Instant ke Akun Youtube Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-3082" class="elementor-tab-title" tabindex="3082" data-tab="2" role="tab" aria-controls="elementor-tab-content-3082">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Views Video					</div>
					<div id="elementor-tab-content-3082" class="elementor-tab-content elementor-clearfix" data-tab="2" role="tabpanel" aria-labelledby="elementor-tab-title-3082"><p>System Akan Memproses Menambah Viewers Secara Cepat dan Instant ke Video Youtube Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-3083" class="elementor-tab-title" tabindex="3083" data-tab="3" role="tab" aria-controls="elementor-tab-content-3083">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Likes Video					</div>
					<div id="elementor-tab-content-3083" class="elementor-tab-content elementor-clearfix" data-tab="3" role="tabpanel" aria-labelledby="elementor-tab-title-3083"><p>System Akan Memproses Menambah Likes Secara Cepat dan Instant ke Video Youtube Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-3084" class="elementor-tab-title" tabindex="3084" data-tab="4" role="tab" aria-controls="elementor-tab-content-3084">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Dislikes Video					</div>
					<div id="elementor-tab-content-3084" class="elementor-tab-content elementor-clearfix" data-tab="4" role="tabpanel" aria-labelledby="elementor-tab-title-3084"><p>System Akan Memproses Menambah Dislikes Secara Cepat dan Instant ke Video Youtube Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-3085" class="elementor-tab-title" tabindex="3085" data-tab="5" role="tab" aria-controls="elementor-tab-content-3085">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Share Video					</div>
					<div id="elementor-tab-content-3085" class="elementor-tab-content elementor-clearfix" data-tab="5" role="tabpanel" aria-labelledby="elementor-tab-title-3085"><p>System Akan Memproses Menambah Share Video Secara Cepat dan Instant ke Akun Youtube Anda.</p></div>
				</div>
					</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="365287b9" class="elementor-element elementor-element-365287b9 elementor-column elementor-col-33 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="cb532fb" class="elementor-element elementor-element-cb532fb animated fadeIn elementor-invisible elementor-widget elementor-widget-image" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img  title="Panelsosmed.com" alt="Panelsosmed.com"  data-src="https://review.panelsosmed.com/wp-content/uploads/elementor/thumbs/telegramdei-1-ojh3y49ot8891w92fz2t6e06zwyrmi57h3cp0kf3s8.png" class="elementor-animation-grow lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img src="wp-content/uploads/elementor/thumbs/telegramdei-1-ojh3y49ot8891w92fz2t6e06zwyrmi57h3cp0kf3s8.png" title="Panelsosmed.com" alt="Panelsosmed.com" class="elementor-animation-grow" /></noscript>		</div>
				</div>
				</div>
				<div data-id="3147e9fa" class="elementor-element elementor-element-3147e9fa elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">TELEGRAM</h2>		</div>
				</div>
				<div data-id="678a73e0" class="elementor-element elementor-element-678a73e0 elementor-widget elementor-widget-toggle" data-element_type="toggle.default">
				<div class="elementor-widget-container">
					<div class="elementor-toggle" role="tablist">
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1731" class="elementor-tab-title" tabindex="1731" data-tab="1" role="tab" aria-controls="elementor-tab-content-1731">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Subscribes Channel ~click~					</div>
					<div id="elementor-tab-content-1731" class="elementor-tab-content elementor-clearfix" data-tab="1" role="tabpanel" aria-labelledby="elementor-tab-title-1731"><p>System Akan Memproses Menambah Subscribes/Member Channel Secara Cepat dan Instant ke Akun Channel Telegram Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1732" class="elementor-tab-title" tabindex="1732" data-tab="2" role="tab" aria-controls="elementor-tab-content-1732">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Views Post					</div>
					<div id="elementor-tab-content-1732" class="elementor-tab-content elementor-clearfix" data-tab="2" role="tabpanel" aria-labelledby="elementor-tab-title-1732"><p>System Akan Memproses Menambah Viewers Secara Cepat dan Instant ke  Postingan Channel Telegram Anda.</p></div>
				</div>
					</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="54feee91" class="elementor-element elementor-element-54feee91 elementor-column elementor-col-33 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="363e9bd7" class="elementor-element elementor-element-363e9bd7 animated fadeIn elementor-invisible elementor-widget elementor-widget-image" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img  title="Panelsosmed.com" alt="Panelsosmed.com"  data-src="https://review.panelsosmed.com/wp-content/uploads/elementor/thumbs/soundcloud-1-ojh3y49ot8891w92fz2t6e06zwyrmi57h3cp0kf3s8.png" class="elementor-animation-grow lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img src="wp-content/uploads/elementor/thumbs/soundcloud-1-ojh3y49ot8891w92fz2t6e06zwyrmi57h3cp0kf3s8.png" title="Panelsosmed.com" alt="Panelsosmed.com" class="elementor-animation-grow" /></noscript>		</div>
				</div>
				</div>
				<div data-id="68fb836b" class="elementor-element elementor-element-68fb836b elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">SOUNDCLOUD</h2>		</div>
				</div>
				<div data-id="1a0693eb" class="elementor-element elementor-element-1a0693eb elementor-widget elementor-widget-toggle" data-element_type="toggle.default">
				<div class="elementor-widget-container">
					<div class="elementor-toggle" role="tablist">
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-4361" class="elementor-tab-title" tabindex="4361" data-tab="1" role="tab" aria-controls="elementor-tab-content-4361">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Followers Soundcloud ~click~					</div>
					<div id="elementor-tab-content-4361" class="elementor-tab-content elementor-clearfix" data-tab="1" role="tabpanel" aria-labelledby="elementor-tab-title-4361"><p>System Akan Memproses Menambah Followers Secara Cepat dan Instant ke Akun Soundcloud Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-4362" class="elementor-tab-title" tabindex="4362" data-tab="2" role="tab" aria-controls="elementor-tab-content-4362">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Likes Soundcloud					</div>
					<div id="elementor-tab-content-4362" class="elementor-tab-content elementor-clearfix" data-tab="2" role="tabpanel" aria-labelledby="elementor-tab-title-4362"><p>System Akan Memproses Menambah Likes Secara Cepat dan Instant ke Akun Soundcloud Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-4363" class="elementor-tab-title" tabindex="4363" data-tab="3" role="tab" aria-controls="elementor-tab-content-4363">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Play Soundcloud					</div>
					<div id="elementor-tab-content-4363" class="elementor-tab-content elementor-clearfix" data-tab="3" role="tabpanel" aria-labelledby="elementor-tab-title-4363"><p>System Akan Memproses Menambah Play Secara Cepat dan Instant ke Akun Soundcloud Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-4364" class="elementor-tab-title" tabindex="4364" data-tab="4" role="tab" aria-controls="elementor-tab-content-4364">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Download Soundcloud					</div>
					<div id="elementor-tab-content-4364" class="elementor-tab-content elementor-clearfix" data-tab="4" role="tabpanel" aria-labelledby="elementor-tab-title-4364"><p>System Akan Memproses Menambah Download Secara Cepat dan Instant ke Akun Soundcloud Anda.</p></div>
				</div>
					</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="e85763a" class="elementor-element elementor-element-e85763a elementor-section-boxed elementor-section-height-default elementor-section-height-default animated fadeIn elementor-invisible elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeIn&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="34f8489e" class="elementor-element elementor-element-34f8489e elementor-column elementor-col-33 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="74de115b" class="elementor-element elementor-element-74de115b animated fadeIn elementor-invisible elementor-widget elementor-widget-image" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img  title="Panelsosmed.com" alt="Panelsosmed.com"  data-src="https://review.panelsosmed.com/wp-content/uploads/elementor/thumbs/googleplus-1-ojh3y49ot8891w92fz2t6e06zwyrmi57h3cp0kf3s8.png" class="elementor-animation-grow lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img src="wp-content/uploads/elementor/thumbs/googleplus-1-ojh3y49ot8891w92fz2t6e06zwyrmi57h3cp0kf3s8.png" title="Panelsosmed.com" alt="Panelsosmed.com" class="elementor-animation-grow" /></noscript>		</div>
				</div>
				</div>
				<div data-id="3ba8765e" class="elementor-element elementor-element-3ba8765e elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">GOOGLE +</h2>		</div>
				</div>
				<div data-id="7a03ceab" class="elementor-element elementor-element-7a03ceab elementor-widget elementor-widget-toggle" data-element_type="toggle.default">
				<div class="elementor-widget-container">
					<div class="elementor-toggle" role="tablist">
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-2041" class="elementor-tab-title" tabindex="2041" data-tab="1" role="tab" aria-controls="elementor-tab-content-2041">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Followers Google+ ~click~					</div>
					<div id="elementor-tab-content-2041" class="elementor-tab-content elementor-clearfix" data-tab="1" role="tabpanel" aria-labelledby="elementor-tab-title-2041"><p>System Akan Memproses Menambah Followers Secara Cepat dan Instant ke Akun Google Plus Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-2042" class="elementor-tab-title" tabindex="2042" data-tab="2" role="tab" aria-controls="elementor-tab-content-2042">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Likes Google Plus					</div>
					<div id="elementor-tab-content-2042" class="elementor-tab-content elementor-clearfix" data-tab="2" role="tabpanel" aria-labelledby="elementor-tab-title-2042"><p>System Akan Memproses Menambah Likes Secara Cepat dan Instant ke Akun Google Plus Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-2043" class="elementor-tab-title" tabindex="2043" data-tab="3" role="tab" aria-controls="elementor-tab-content-2043">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Share Postingan					</div>
					<div id="elementor-tab-content-2043" class="elementor-tab-content elementor-clearfix" data-tab="3" role="tabpanel" aria-labelledby="elementor-tab-title-2043"><p>System Akan Memproses Menambah Share Secara Cepat dan Instant ke Akun Google Plus Anda.</p></div>
				</div>
					</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="4f2e6866" class="elementor-element elementor-element-4f2e6866 elementor-column elementor-col-33 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="5a5df74c" class="elementor-element elementor-element-5a5df74c animated fadeIn elementor-invisible elementor-widget elementor-widget-image" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img  title="Panelsosmed.com" alt="Panelsosmed.com"  data-src="https://review.panelsosmed.com/wp-content/uploads/elementor/thumbs/Pinterest-Icon-300x300-2-ojh3y57j029jdi7pahhfqvrnlau4u78xt806hudpm0.png" class="elementor-animation-grow lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img src="wp-content/uploads/elementor/thumbs/Pinterest-Icon-300x300-2-ojh3y57j029jdi7pahhfqvrnlau4u78xt806hudpm0.png" title="Panelsosmed.com" alt="Panelsosmed.com" class="elementor-animation-grow" /></noscript>		</div>
				</div>
				</div>
				<div data-id="6ed9939d" class="elementor-element elementor-element-6ed9939d elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">PINTEREST</h2>		</div>
				</div>
				<div data-id="5e02e432" class="elementor-element elementor-element-5e02e432 elementor-widget elementor-widget-toggle" data-element_type="toggle.default">
				<div class="elementor-widget-container">
					<div class="elementor-toggle" role="tablist">
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1571" class="elementor-tab-title" tabindex="1571" data-tab="1" role="tab" aria-controls="elementor-tab-content-1571">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Followers Pinterest ~click~					</div>
					<div id="elementor-tab-content-1571" class="elementor-tab-content elementor-clearfix" data-tab="1" role="tabpanel" aria-labelledby="elementor-tab-title-1571"><p>System Akan Memproses Menambah Followers Secara Cepat dan Instant ke Akun Pinterest Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1572" class="elementor-tab-title" tabindex="1572" data-tab="2" role="tab" aria-controls="elementor-tab-content-1572">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Likes Pinterest					</div>
					<div id="elementor-tab-content-1572" class="elementor-tab-content elementor-clearfix" data-tab="2" role="tabpanel" aria-labelledby="elementor-tab-title-1572"><p>System Akan Memproses Menambah Pin Likes Secara Cepat dan Instant ke Akun Pinterest Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1573" class="elementor-tab-title" tabindex="1573" data-tab="3" role="tab" aria-controls="elementor-tab-content-1573">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												TambahRe-Pins 					</div>
					<div id="elementor-tab-content-1573" class="elementor-tab-content elementor-clearfix" data-tab="3" role="tabpanel" aria-labelledby="elementor-tab-title-1573"><p>System Akan Memproses Menambah Re-Pins Secara Cepat dan Instant ke Akun Pinterest Anda.</p></div>
				</div>
					</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="70102a73" class="elementor-element elementor-element-70102a73 elementor-column elementor-col-33 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="2d4066f2" class="elementor-element elementor-element-2d4066f2 animated fadeIn elementor-invisible elementor-widget elementor-widget-image" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img  title="Panelsosmed.com" alt="Panelsosmed.com"  data-src="https://review.panelsosmed.com/wp-content/uploads/elementor/thumbs/linkedin-1-ojh3y57j029jdi7pahhfqvrnlau4u78xt806hudpm0.png" class="elementor-animation-grow lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img src="wp-content/uploads/elementor/thumbs/linkedin-1-ojh3y57j029jdi7pahhfqvrnlau4u78xt806hudpm0.png" title="Panelsosmed.com" alt="Panelsosmed.com" class="elementor-animation-grow" /></noscript>		</div>
				</div>
				</div>
				<div data-id="3400dd17" class="elementor-element elementor-element-3400dd17 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">LINKEDIN</h2>		</div>
				</div>
				<div data-id="5be364e5" class="elementor-element elementor-element-5be364e5 elementor-widget elementor-widget-toggle" data-element_type="toggle.default">
				<div class="elementor-widget-container">
					<div class="elementor-toggle" role="tablist">
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1541" class="elementor-tab-title" tabindex="1541" data-tab="1" role="tab" aria-controls="elementor-tab-content-1541">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Followers Linkedin ~click~					</div>
					<div id="elementor-tab-content-1541" class="elementor-tab-content elementor-clearfix" data-tab="1" role="tabpanel" aria-labelledby="elementor-tab-title-1541"><p>System Akan Memproses Menambah Followers Secara Cepat dan Instant ke Akun Linkedin Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1542" class="elementor-tab-title" tabindex="1542" data-tab="2" role="tab" aria-controls="elementor-tab-content-1542">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Connections Linkedin					</div>
					<div id="elementor-tab-content-1542" class="elementor-tab-content elementor-clearfix" data-tab="2" role="tabpanel" aria-labelledby="elementor-tab-title-1542"><p>System Akan Memproses Menambah Connection Secara Cepat dan Instant ke Akun Linkedin Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1543" class="elementor-tab-title" tabindex="1543" data-tab="3" role="tab" aria-controls="elementor-tab-content-1543">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Likes Post					</div>
					<div id="elementor-tab-content-1543" class="elementor-tab-content elementor-clearfix" data-tab="3" role="tabpanel" aria-labelledby="elementor-tab-title-1543"><p>System Akan Memproses Menambah Likes Secara Cepat dan Instant ke Postingan Linkedin Anda.</p></div>
				</div>
					</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="f147217" class="elementor-element elementor-element-f147217 elementor-section-boxed elementor-section-height-default elementor-section-height-default animated fadeIn elementor-invisible elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeIn&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="3734e012" class="elementor-element elementor-element-3734e012 elementor-column elementor-col-33 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="64e66d69" class="elementor-element elementor-element-64e66d69 animated fadeIn elementor-invisible elementor-widget elementor-widget-image" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img  title="Panelsosmed.com" alt="Panelsosmed.com"  data-src="https://review.panelsosmed.com/wp-content/uploads/elementor/thumbs/trafiic-1-ojh3y57j029jdi7pahhfqvrnlau4u78xt806hudpm0.png" class="elementor-animation-grow lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img src="wp-content/uploads/elementor/thumbs/trafiic-1-ojh3y57j029jdi7pahhfqvrnlau4u78xt806hudpm0.png" title="Panelsosmed.com" alt="Panelsosmed.com" class="elementor-animation-grow" /></noscript>		</div>
				</div>
				</div>
				<div data-id="b67a148" class="elementor-element elementor-element-b67a148 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">TRAFFIC WEB</h2>		</div>
				</div>
				<div data-id="46a00902" class="elementor-element elementor-element-46a00902 elementor-widget elementor-widget-toggle" data-element_type="toggle.default">
				<div class="elementor-widget-container">
					<div class="elementor-toggle" role="tablist">
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1181" class="elementor-tab-title" tabindex="1181" data-tab="1" role="tab" aria-controls="elementor-tab-content-1181">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Real Traffic Indonesia ~click~					</div>
					<div id="elementor-tab-content-1181" class="elementor-tab-content elementor-clearfix" data-tab="1" role="tabpanel" aria-labelledby="elementor-tab-title-1181"><p>System Akan Memproses Menambah Real Traffic Indonesia Secara Cepat dan Instant ke Website Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1182" class="elementor-tab-title" tabindex="1182" data-tab="2" role="tab" aria-controls="elementor-tab-content-1182">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Real Traffic Worlwide					</div>
					<div id="elementor-tab-content-1182" class="elementor-tab-content elementor-clearfix" data-tab="2" role="tabpanel" aria-labelledby="elementor-tab-title-1182"><p>System Akan Memproses Menambah Real Traffic Worldwide Secara Cepat dan Instant ke Website Anda.</p></div>
				</div>
					</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="6e038d61" class="elementor-element elementor-element-6e038d61 elementor-column elementor-col-33 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="1c4126e2" class="elementor-element elementor-element-1c4126e2 animated fadeIn elementor-invisible elementor-widget elementor-widget-image" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img  title="Panelsosmed.com" alt="Panelsosmed.com"  data-src="https://review.panelsosmed.com/wp-content/uploads/elementor/thumbs/Spotify-1-ojh3y65d6watp46c4zw2bdj46opi1wco5cnnz4cbfs.png" class="elementor-animation-grow lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img src="wp-content/uploads/elementor/thumbs/Spotify-1-ojh3y65d6watp46c4zw2bdj46opi1wco5cnnz4cbfs.png" title="Panelsosmed.com" alt="Panelsosmed.com" class="elementor-animation-grow" /></noscript>		</div>
				</div>
				</div>
				<div data-id="74b54d5b" class="elementor-element elementor-element-74b54d5b elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">SPOTIFY</h2>		</div>
				</div>
				<div data-id="7a35aae3" class="elementor-element elementor-element-7a35aae3 elementor-widget elementor-widget-toggle" data-element_type="toggle.default">
				<div class="elementor-widget-container">
					<div class="elementor-toggle" role="tablist">
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-2051" class="elementor-tab-title" tabindex="2051" data-tab="1" role="tab" aria-controls="elementor-tab-content-2051">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Followers Spotify ~click~					</div>
					<div id="elementor-tab-content-2051" class="elementor-tab-content elementor-clearfix" data-tab="1" role="tabpanel" aria-labelledby="elementor-tab-title-2051"><p>System Akan Memproses Menambah Followers Secara Cepat dan Instant ke Akun Spotify Anda.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-2052" class="elementor-tab-title" tabindex="2052" data-tab="2" role="tab" aria-controls="elementor-tab-content-2052">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Plays Spotify					</div>
					<div id="elementor-tab-content-2052" class="elementor-tab-content elementor-clearfix" data-tab="2" role="tabpanel" aria-labelledby="elementor-tab-title-2052"><p>System Akan Memproses Menambah Plays Secara Cepat dan Instant ke Akun Spotify Anda.</p></div>
				</div>
					</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="294bd794" class="elementor-element elementor-element-294bd794 elementor-column elementor-col-33 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="5f5c2187" class="elementor-element elementor-element-5f5c2187 animated fadeIn elementor-invisible elementor-widget elementor-widget-image" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img  title="Panelsosmed.com" alt="Panelsosmed.com"  data-src="https://review.panelsosmed.com/wp-content/uploads/elementor/thumbs/snapchat-1-ojh3y65d6watp46c4zw2bdj46opi1wco5cnnz4cbfs.png" class="elementor-animation-grow lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img src="wp-content/uploads/elementor/thumbs/snapchat-1-ojh3y65d6watp46c4zw2bdj46opi1wco5cnnz4cbfs.png" title="Panelsosmed.com" alt="Panelsosmed.com" class="elementor-animation-grow" /></noscript>		</div>
				</div>
				</div>
				<div data-id="48195e38" class="elementor-element elementor-element-48195e38 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">SNAPCHAT</h2>		</div>
				</div>
				<div data-id="797cd413" class="elementor-element elementor-element-797cd413 elementor-widget elementor-widget-toggle" data-element_type="toggle.default">
				<div class="elementor-widget-container">
					<div class="elementor-toggle" role="tablist">
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-2031" class="elementor-tab-title" tabindex="2031" data-tab="1" role="tab" aria-controls="elementor-tab-content-2031">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Tambah Followers Snapchat ~click~					</div>
					<div id="elementor-tab-content-2031" class="elementor-tab-content elementor-clearfix" data-tab="1" role="tabpanel" aria-labelledby="elementor-tab-title-2031"><p>System Akan Memproses Menambah Followers Secara Cepat dan Instant ke Akun Snapchat Anda.</p></div>
				</div>
					</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="f388348" class="elementor-element elementor-element-f388348 elementor-section-boxed elementor-section-height-default elementor-section-height-default animated fadeIn elementor-invisible elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;animation&quot;:&quot;fadeIn&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="4fddeb27" class="elementor-element elementor-element-4fddeb27 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="4063dc8f" class="elementor-element elementor-element-4063dc8f elementor-align-center elementor-mobile-align-justify animated fadeIn elementor-invisible elementor-widget elementor-widget-button" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper elementor-button-sticky-no">
			<a class="elementor-button elementor-size-lg elementor-animation-grow">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Dan Segudang Fitur Dahsyat Lainnya...</span>
		</span>
					</a>
		</div>
				</div>
				</div>
				<div data-id="451f67f" class="elementor-element elementor-element-451f67f elementor-widget elementor-widget-spacer" data-element_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="67d63ef6" class="elementor-element elementor-element-67d63ef6 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="69765778" class="elementor-element elementor-element-69765778 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="6146f0f8" class="elementor-element elementor-element-6146f0f8 animated fadeIn elementor-invisible elementor-widget elementor-widget-text-editor" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3>Dan.. Taukah <strong>Anda</strong>? </h3><h3>Tercatat Sudah <strong>Ribuan Pengguna</strong> &amp; <strong>Jutaan Order</strong> Followers di  <strong>&#8220;Technobooks&#8221;</strong></h3></div>
				</div>
				</div>
				<section data-id="3d100607" class="elementor-element elementor-element-3d100607 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="6297528c" class="elementor-element elementor-element-6297528c elementor-column elementor-col-33 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="1709d5fb" class="elementor-element elementor-element-1709d5fb elementor-widget elementor-widget-counter" data-element_type="counter.default">
				<div class="elementor-widget-container">
					<div class="elementor-counter">
			<div class="elementor-counter-number-wrapper">
				<span class="elementor-counter-number-prefix"></span>
				<span class="elementor-counter-number" data-duration="1000" data-to-value="3622" data-delimiter=","></span>
				<span class="elementor-counter-number-suffix"></span>
			</div>
							<div class="elementor-counter-title">Reseller Aktif</div>
					</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="6a4ed9a5" class="elementor-element elementor-element-6a4ed9a5 elementor-column elementor-col-33 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="3c4ba7aa" class="elementor-element elementor-element-3c4ba7aa elementor-widget elementor-widget-counter" data-element_type="counter.default">
				<div class="elementor-widget-container">
					<div class="elementor-counter">
			<div class="elementor-counter-number-wrapper">
				<span class="elementor-counter-number-prefix"></span>
				<span class="elementor-counter-number" data-duration="1000" data-to-value="211849" data-delimiter=","></span>
				<span class="elementor-counter-number-suffix"></span>
			</div>
							<div class="elementor-counter-title">Total Order</div>
					</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="27fd6b17" class="elementor-element elementor-element-27fd6b17 elementor-column elementor-col-33 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="47e24f78" class="elementor-element elementor-element-47e24f78 elementor-widget elementor-widget-counter" data-element_type="counter.default">
				<div class="elementor-widget-container">
					<div class="elementor-counter">
			<div class="elementor-counter-number-wrapper">
				<span class="elementor-counter-number-prefix"></span>
				<span class="elementor-counter-number" data-duration="1000" data-to-value="74868019" data-delimiter=","></span>
				<span class="elementor-counter-number-suffix"></span>
			</div>
							<div class="elementor-counter-title">Followers Terkirim</div>
					</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="232f4edd" class="elementor-element elementor-element-232f4edd elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="3c757d54" class="elementor-element elementor-element-3c757d54 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="30bc83fe" class="elementor-element elementor-element-30bc83fe animated fadeIn elementor-invisible elementor-widget elementor-widget-text-editor" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h4><strong>Anda Jangan</strong> Dulu Percaya, Sebelum Dengar <strong>Apa Kata Mereka </strong>Yang Sudah <strong>Menggunakan Technobooks</strong></h4></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="4b081faf" class="elementor-element elementor-element-4b081faf elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="38a4298" class="elementor-element elementor-element-38a4298 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="62ed68b" class="elementor-element elementor-element-62ed68b elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="576" height="1024"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/7-1-576x1024.jpeg" class="attachment-large size-large lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="576" height="1024" src="wp-content/uploads/2020/01/7-1-576x1024.jpg" class="attachment-large size-large" alt="Panelsosmed.com" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="394823ee" class="elementor-element elementor-element-394823ee elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="620199d4" class="elementor-element elementor-element-620199d4 elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="576" height="1024"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/59362-1-576x1024.jpg" class="attachment-large size-large lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="576" height="1024" src="wp-content/uploads/2020/01/59362-1-576x1024.jpg" class="attachment-large size-large" alt="Panelsosmed.com" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="210cd6ed" class="elementor-element elementor-element-210cd6ed elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="2def0a4" class="elementor-element elementor-element-2def0a4 elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="576" height="1024"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/59354-1-576x1024.jpg" class="attachment-large size-large lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="576" height="1024" src="wp-content/uploads/2020/01/59354-1-576x1024.jpg" class="attachment-large size-large" alt="Panelsosmed.com" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="23df9567" class="elementor-element elementor-element-23df9567 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="1c079dfe" class="elementor-element elementor-element-1c079dfe elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="576" height="1024"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/59353-1-576x1024.jpg" class="attachment-large size-large lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="576" height="1024" src="wp-content/uploads/2020/01/59353-1-576x1024.jpg" class="attachment-large size-large" alt="Panelsosmed.com" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="7e0bce6" class="elementor-element elementor-element-7e0bce6 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="731bd073" class="elementor-element elementor-element-731bd073 elementor-column elementor-col-25 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="78b18ec5" class="elementor-element elementor-element-78b18ec5 elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="576" height="1024"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/5-1-576x1024.jpeg" class="attachment-large size-large lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="576" height="1024" src="wp-content/uploads/2020/01/5-1-576x1024.jpg" class="attachment-large size-large" alt="Panelsosmed.com" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="313f9daf" class="elementor-element elementor-element-313f9daf elementor-column elementor-col-25 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="1ca45244" class="elementor-element elementor-element-1ca45244 elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="576" height="1024"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/59351-1-576x1024.jpg" class="attachment-large size-large lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="576" height="1024" src="wp-content/uploads/2020/01/59351-1-576x1024.jpg" class="attachment-large size-large" alt="Panelsosmed.com" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="65ef57b8" class="elementor-element elementor-element-65ef57b8 elementor-column elementor-col-25 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="e495ebc" class="elementor-element elementor-element-e495ebc elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="576" height="1024"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/4-1-576x1024.jpeg" class="attachment-large size-large lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="576" height="1024" src="wp-content/uploads/2020/01/4-1-576x1024.jpg" class="attachment-large size-large" alt="Panelsosmed.com" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="365b7d9a" class="elementor-element elementor-element-365b7d9a elementor-column elementor-col-25 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="58a9d80b" class="elementor-element elementor-element-58a9d80b elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="576" height="1024"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/1-1-576x1024.jpeg" class="attachment-large size-large lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="576" height="1024" src="wp-content/uploads/2020/01/1-1-576x1024.jpg" class="attachment-large size-large" alt="Panelsosmed.com" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="5bd23d2" class="elementor-element elementor-element-5bd23d2 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="709f2da2" class="elementor-element elementor-element-709f2da2 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="75c93f2a" class="elementor-element elementor-element-75c93f2a elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3 style="text-align: center;"><b>Nah, Akhirnya..</b></h3><h3 style="text-align: center;">Sekarang<b> Anda</b> sudah mengetahui <b>Rahasia</b> tersebut. Tools System inilah yg digunakan oleh <br />para <b>Jasa Tambah Followers</b> yang ada di luaran sana.</h3></div>
				</div>
				</div>
				<div data-id="66268ea6" class="elementor-element elementor-element-66268ea6 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3 style="text-align: center;"><b>Technobooks</b> sudah digunakan oleh para pelaku bisnis <b>Jasa Tambah Followers</b>, para <b>Celebgram</b>, <br /><b>Youtubers</b>, artis ternama, onlineshop2, dropshipper, Reseller, Business Owner, komunitas, <br />dokter, instansi negeri, swasta dan lainnya dengan jenis profesi berbagai <br />macam yang eksis dan active di sosial media.</h3></div>
				</div>
				</div>
				<div data-id="41998dfa" class="elementor-element elementor-element-41998dfa elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3 style="text-align: center;"><b>Sekarang</b>, Anda bisa <b>bayangkan</b> setelah mengetahui <b>Rahasia</b> ini, <b>Rasakan</b> diri anda pun <br />menggunakan layanan <b>Technobooks</b> ini untuk optimized sosial media anda.</h3><h3 style="text-align: center;">Dan, berapakah kira kira <strong>investasi</strong> yang harus anda keluarkan untuk menikmati layanan tools <br />Technobooks ini?</h3><h3 style="text-align: center;">Kami membangun system ini <b>membutuhkan effort</b> waktu, tenaga, pikiran dan uang yg sangat besar <br />hingga tools ini tercipta dg baik. Sehingga jika anda Developt sendiri akan <b>menghabiskan <br />biaya</b></h3><h3 style="text-align: center;"><b>5 juta..<br />10 juta..<br />Bahkan bisa sampai 15 juta lebih..</b></h3><h3 style="text-align: center;">Tapi tenang, <b>Anda</b> adalah salah satu orang yang <b>beruntung</b> bisa membaca sejauh ini. Karena <br />anda tidak perlu mengeluarkan biaya sebanyak itu.</h3><h3 style="text-align: center;">Untuk bisa menikmati layanan <b>tools system Technobooks</b> cukup investasi :</h3></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="7d8b7fe0" class="elementor-element elementor-element-7d8b7fe0 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
							<div class="elementor-background-overlay"></div>
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="70098ba" class="elementor-element elementor-element-70098ba elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="2ab7d028" class="elementor-element elementor-element-2ab7d028 elementor-widget elementor-widget-menu-anchor" data-element_type="menu-anchor.default">
				<div class="elementor-widget-container">
					<div id="order" class="elementor-menu-anchor"></div>
				</div>
				</div>
				<div data-id="42f8e863" class="elementor-element elementor-element-42f8e863 elementor-widget elementor-widget-spacer" data-element_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div data-id="8cf79" class="elementor-element elementor-element-8cf79 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Pilih Level Yang Anda Inginkan</h2>		</div>
				</div>
				<section data-id="5cb8254e" class="elementor-element elementor-element-5cb8254e elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="580d161c" class="elementor-element elementor-element-580d161c elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="34e06367" class="elementor-element elementor-element-34e06367 elementor-widget elementor-widget-spacer" data-element_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div data-id="182a808a" class="elementor-element elementor-element-182a808a elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default"><b>RESELLER</b></h2>		</div>
				</div>
				<div data-id="2c47f722" class="elementor-element elementor-element-2c47f722 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h1 class="elementor-heading-title elementor-size-small">(Lifetime - Sekali bayar)</h1>		</div>
				</div>
				<div data-id="712d0935" class="elementor-element elementor-element-712d0935 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p><span style="color: #00ccff;"><strong>Rp 100.000</strong></span></p></div>
				</div>
				</div>
				<div data-id="2ec94ba4" class="elementor-element elementor-element-2ec94ba4 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Bonus Saldo Rp 50.000​</h2>		</div>
				</div>
				<div data-id="142f2c56" class="elementor-element elementor-element-142f2c56 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Proses Order Instant</h2>		</div>
				</div>
				<div data-id="1ccf387c" class="elementor-element elementor-element-1ccf387c elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Bisa Pakai Semua FItur</h2>		</div>
				</div>
				<div data-id="7bf2075b" class="elementor-element elementor-element-7bf2075b elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Full Support 24 Jam​</h2>		</div>
				</div>
				<div data-id="4cde7cb7" class="elementor-element elementor-element-4cde7cb7 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Bisa Mendaftarkan Member</h2>		</div>
				</div>
				<div data-id="605a3fce" class="elementor-element elementor-element-605a3fce elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Bisa Transfer Saldo Ke Member</h2>		</div>
				</div>
				<div data-id="e656c71" class="elementor-element elementor-element-e656c71 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Bisa Mendaftarkan Reseller</h2>		</div>
				</div>
				<div data-id="4b28d198" class="elementor-element elementor-element-4b28d198 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Bisa Transer Saldo Ke Reseller</h2>		</div>
				</div>
				<div data-id="189a97a2" class="elementor-element elementor-element-189a97a2 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Dapat Semua Bonus Premium</h2>		</div>
				</div>
				<div data-id="1714c842" class="elementor-element elementor-element-1714c842 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Masuk Group Private</h2>		</div>
				</div>
				<div data-id="699a05b3" class="elementor-element elementor-element-699a05b3 elementor-align-justify elementor-widget elementor-widget-button" data-element_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper elementor-button-sticky-no">
			<a href="checkout-order-panelsosmed/index.html" class="elementor-button-link elementor-button elementor-size-lg">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">DAFTAR SEKARANG</span>
		</span>
					</a>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="411dc27a" class="elementor-element elementor-element-411dc27a elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="21dc1" class="elementor-element elementor-element-21dc1 elementor-widget elementor-widget-spacer" data-element_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div data-id="334148db" class="elementor-element elementor-element-334148db elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h1 class="elementor-heading-title elementor-size-large"><b>MEMBER</b></h1>		</div>
				</div>
				<div data-id="306eaab1" class="elementor-element elementor-element-306eaab1 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h1 class="elementor-heading-title elementor-size-small">(Lifetime - Sekali bayar)</h1>		</div>
				</div>
				<div data-id="c9aee6b" class="elementor-element elementor-element-c9aee6b elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p><span style="color: #00ccff;"><strong>Rp 50.000</strong></span></p></div>
				</div>
				</div>
				<div data-id="5390f5be" class="elementor-element elementor-element-5390f5be elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-large">Bonus Saldo Rp 25.000</h2>		</div>
				</div>
				<div data-id="65c4ff36" class="elementor-element elementor-element-65c4ff36 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-large">Proses Order Instant</h2>		</div>
				</div>
				<div data-id="7118a9c9" class="elementor-element elementor-element-7118a9c9 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-large">Bisa Pakai Semua FItur</h2>		</div>
				</div>
				<div data-id="3413129f" class="elementor-element elementor-element-3413129f elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-large">Full Support 24 Jam</h2>		</div>
				</div>
				<div data-id="56ed01dd" class="elementor-element elementor-element-56ed01dd elementor-align-justify elementor-widget elementor-widget-button" data-element_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper elementor-button-sticky-no">
			<a href="checkout-order-panelsosmed/index.html" class="elementor-button-link elementor-button elementor-size-lg">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">DAFTAR SEKARANG</span>
		</span>
					</a>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="7587d1e0" class="elementor-element elementor-element-7587d1e0 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="29ea6f1a" class="elementor-element elementor-element-29ea6f1a elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="76c4edc" class="elementor-element elementor-element-76c4edc elementor-widget elementor-widget-spacer" data-element_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div data-id="7e79502e" class="elementor-element elementor-element-7e79502e elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-medium">Cara Kerja Technobooks</h2>		</div>
				</div>
				<div data-id="b21a43f" class="elementor-element elementor-element-b21a43f elementor-align-center elementor-mobile-align-justify animated fadeIn elementor-invisible elementor-widget elementor-widget-button" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper elementor-button-sticky-no">
			<a class="elementor-button elementor-size-md elementor-animation-grow">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Sekali Klik, Ribuan Followers Masuk</span>
		</span>
					</a>
		</div>
				</div>
				</div>
				<div data-id="73b013b6" class="elementor-element elementor-element-73b013b6 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-medium"><b>Setelah pendaftaran</b>, Anda akan dapat akses login ke <b>Technobooks</b>. Jika level anda <b>Member</b> akan mendapatkan saldo sebesar <b>Rp 25.000</b> sedangkan Jika level anda <b>Reseller</b> akan mendapatkan Saldo <b>Rp 50.000</b></h3>		</div>
				</div>
				<div data-id="66e3129" class="elementor-element elementor-element-66e3129 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-medium">Setiap proses di dalamnya akan memotong saldo yang anda miliki. Tapi tenang, harga layanan di dalamnya udah <b>Termurah di Indonesia</b>. </h3>		</div>
				</div>
				<div data-id="1d90eece" class="elementor-element elementor-element-1d90eece elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3 style="text-align: center;">Coba <b>Anda</b> Lihat ini :</h3><h3 style="text-align: center;"><b>100 Followers</b> harga cuma <b>Rp 3.000</b><br /><b>100 Likes</b> harga cuma <b>Rp 2.500</b><br /><b>100 Views</b> hagra cuma <b>Rp 500</b></h3></div>
				</div>
				</div>
				<div data-id="1fc77ac0" class="elementor-element elementor-element-1fc77ac0 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-medium">Jika level anda <b>Reseller</b>, akan <b>Mendapatkan Benefit</b> yang lebih banyak yaitu Anda bisa <b>Mendaftarkan Member</b> dan <b>Mendaftarkan Reseller</b>. Uang padaftaran tersebut <b>full buat Anda.</b></h3>		</div>
				</div>
				<div data-id="feb1242" class="elementor-element elementor-element-feb1242 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h3 class="elementor-heading-title elementor-size-medium">Bukan hanya itu, Jika level <b>Anda Reseller</b> akan mendapatkan <b>bonus yang berlimpah</b> senilai <b>jutaan rupiah</b> &amp; masuk <b>group private khusus</b>.</h3>		</div>
				</div>
				<div data-id="74e2d873" class="elementor-element elementor-element-74e2d873 elementor-widget elementor-widget-spacer" data-element_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="14941884" class="elementor-element elementor-element-14941884 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="7b0cf108" class="elementor-element elementor-element-7b0cf108 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="2053ce2e" class="elementor-element elementor-element-2053ce2e elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3 style="text-align: center;">Ini <b>Bonus Special</b> khusus bagi <b>Anda</b> yang mendaftar level <b>Reseller</b> :</h3></div>
				</div>
				</div>
				<div data-id="2202601c" class="elementor-element elementor-element-2202601c elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h5 style="text-align: center;">Bonus #1 &#8211; 125+ Instagram Banner High Quality <span style="color: #ff0000;">(senilai Rp 299.000)</span></h5></div>
				</div>
				</div>
				<section data-id="5ae4f81e" class="elementor-element elementor-element-5ae4f81e elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="47105d14" class="elementor-element elementor-element-47105d14 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="404c6f5" class="elementor-element elementor-element-404c6f5 elementor-widget elementor-widget-image_video" data-element_type="image_video.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
			<a class="elementor-clickable elementor-magnific-popup-video elementor-image-link" href="../external.html?link=https://www.youtube.com/watch?v=a1htiflzlLc">
				<i class="fa fa-youtube-play" aria-hidden="true"></i>
				<img width="750" height="409"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/2018-08-06_000424-1.png" class="attachment-large size-large lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="750" height="409" src="wp-content/uploads/2020/01/2018-08-06_000424-1.png" class="attachment-large size-large" alt="Panelsosmed.com" /></noscript>			</a>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="9b1642c" class="elementor-element elementor-element-9b1642c elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="6db8ab77" class="elementor-element elementor-element-6db8ab77 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p>Ratusan desain Instagram Banner High Quality untuk promosi di Instagram. File PSD &amp; PNG Pilih Template – Ganti gambar dan Text – Drag n Drop. Bisa anda gunakan untuk Promosi di Semua Sosial Media (Facebook, Instagram, Twitter, BBM, Website atau Sosial Media lainnya).</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<div data-id="15551367" class="elementor-element elementor-element-15551367 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h5 style="text-align: center;">Bonus #2 &#8211; 10 Instagram Promotion Product <span style="color: #ff0000;">(senilai Rp 147.000)</span></h5></div>
				</div>
				</div>
				<section data-id="7a8e6e53" class="elementor-element elementor-element-7a8e6e53 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="4e22376a" class="elementor-element elementor-element-4e22376a elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="10f9402d" class="elementor-element elementor-element-10f9402d elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p>Template Powerpoint untuk promosi produk anda di instagram. Dengan kualitas High Quality, template siap pakai drag and drop tinggal edit.</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="3317924f" class="elementor-element elementor-element-3317924f elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="35a9ef3c" class="elementor-element elementor-element-35a9ef3c elementor-widget elementor-widget-image_video" data-element_type="image_video.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
			<a class="elementor-clickable elementor-magnific-popup-video elementor-image-link" href="../external.html?link=https://www.youtube.com/watch?v=IugT5wrI1Nw">
				<i class="fa fa-youtube-play" aria-hidden="true"></i>
				<img width="512" height="245"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/1-1.jpg" class="attachment-large size-large lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="512" height="245" src="wp-content/uploads/2020/01/1-1.jpg" class="attachment-large size-large" alt="Panelsosmed.com" /></noscript>			</a>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<div data-id="a5356ce" class="elementor-element elementor-element-a5356ce elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h5 style="text-align: center;">Bonus #3 &#8211; 10 Instagram Stories Template <span style="color: #ff0000;">(senilai Rp 147.000)</span></h5></div>
				</div>
				</div>
				<section data-id="5fa3ee5b" class="elementor-element elementor-element-5fa3ee5b elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="76cb564" class="elementor-element elementor-element-76cb564 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="a87b32b" class="elementor-element elementor-element-a87b32b elementor-widget elementor-widget-image_video" data-element_type="image_video.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
			<a class="elementor-clickable elementor-magnific-popup-video elementor-image-link" href="../external.html?link=https://www.youtube.com/watch?v=x7zw-trc-TU">
				<i class="fa fa-youtube-play" aria-hidden="true"></i>
				<img width="547" height="236"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/2-1.jpg" class="attachment-large size-large lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="547" height="236" src="wp-content/uploads/2020/01/2-1.jpg" class="attachment-large size-large" alt="Panelsosmed.com" /></noscript>			</a>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="24d3841a" class="elementor-element elementor-element-24d3841a elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="4919a081" class="elementor-element elementor-element-4919a081 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p>Template Powerpoint untuk promosi produk anda di stories instagram. Dengan kualitas High Quality, template siap pakai drag and drop tinggal edit.</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<div data-id="14503529" class="elementor-element elementor-element-14503529 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h5 style="text-align: center;">Bonus #4 &#8211; 10 Instagram Video Timeline <span style="color: #ff0000;">(senilai Rp 149.000)</span></h5></div>
				</div>
				</div>
				<section data-id="3353f1cc" class="elementor-element elementor-element-3353f1cc elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="7cb5f9a6" class="elementor-element elementor-element-7cb5f9a6 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="6d91417a" class="elementor-element elementor-element-6d91417a elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p>Template Powerpoint untuk promosi produk anda di timeline instagram. Dengan kualitas High Quality, template siap pakai drag and drop tinggal edit.</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="7bdc7bc8" class="elementor-element elementor-element-7bdc7bc8 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="7f5c8083" class="elementor-element elementor-element-7f5c8083 elementor-widget elementor-widget-image_video" data-element_type="image_video.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
			<a class="elementor-clickable elementor-magnific-popup-video elementor-image-link" href="../external.html?link=https://www.youtube.com/watch?v=ISuxZgjQX58">
				<i class="fa fa-youtube-play" aria-hidden="true"></i>
				<img width="515" height="239"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/3-1.jpg" class="attachment-large size-large lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="515" height="239" src="wp-content/uploads/2020/01/3-1.jpg" class="attachment-large size-large" alt="Panelsosmed.com" /></noscript>			</a>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<div data-id="67122075" class="elementor-element elementor-element-67122075 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h5 style="text-align: center;">Bonus #5 &#8211; PLR Product &#8211; Raja Ngeblog <span style="color: #ff0000;">(senilai Rp 250.000)</span></h5></div>
				</div>
				</div>
				<section data-id="38576987" class="elementor-element elementor-element-38576987 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="6decefb1" class="elementor-element elementor-element-6decefb1 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="59d10202" class="elementor-element elementor-element-59d10202 elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="300" height="165"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/Raja-Ngeblog-1-300x165.png" class="attachment-medium size-medium lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="300" height="165" src="wp-content/uploads/2020/01/Raja-Ngeblog-1-300x165.png" class="attachment-medium size-medium" alt="Panelsosmed.com" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="75a9b9d0" class="elementor-element elementor-element-75a9b9d0 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="7205ca58" class="elementor-element elementor-element-7205ca58 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p>Video tutorial super lengkap mengajarkan Anda step by step hingga Mahir bagaimana membuat Blog yang profesional dan Profitable. Lebih dahsyatnya lagi PLR Product ini bisa Anda jual kembali dengan keuntungan full buat Anda.</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<div data-id="7fdaee74" class="elementor-element elementor-element-7fdaee74 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h5 style="text-align: center;">Bonus #6 &#8211; PLR Product &#8211; Master Facebook Ads <span style="color: #ff0000;">(senilai Rp 250.000)</span></h5></div>
				</div>
				</div>
				<section data-id="12f3a1e9" class="elementor-element elementor-element-12f3a1e9 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="6d752939" class="elementor-element elementor-element-6d752939 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="70395465" class="elementor-element elementor-element-70395465 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p>Video tutorial super lengkap mengajarkan Anda step by step hingga Mahir Facebook Ads yang bisa meningkatkan profit bisnis Anda 100 kali lipat. Lebih dahsyatnya lagi PLR Product ini bisa Anda jual kembali dengan keuntungan full buat Anda.</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="506f753" class="elementor-element elementor-element-506f753 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="69d5ed2c" class="elementor-element elementor-element-69d5ed2c elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="300" height="165"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/masterfacebookads-1-300x165.png" class="attachment-medium size-medium lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="300" height="165" src="wp-content/uploads/2020/01/masterfacebookads-1-300x165.png" class="attachment-medium size-medium" alt="Panelsosmed.com" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<div data-id="10ab29cc" class="elementor-element elementor-element-10ab29cc elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h5 style="text-align: center;">Bonus #7 &#8211; PLR Product &#8211; Master Email Marketing <span style="color: #ff0000;">(senilai Rp 250.000)</span></h5></div>
				</div>
				</div>
				<section data-id="977f630" class="elementor-element elementor-element-977f630 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="43bb2729" class="elementor-element elementor-element-43bb2729 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="7e23cb00" class="elementor-element elementor-element-7e23cb00 elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="300" height="165"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/emailmarketing-1-300x165.png" class="attachment-medium size-medium lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="300" height="165" src="wp-content/uploads/2020/01/emailmarketing-1-300x165.png" class="attachment-medium size-medium" alt="Panelsosmed.com" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="2adbd871" class="elementor-element elementor-element-2adbd871 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="69bb0ccd" class="elementor-element elementor-element-69bb0ccd elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p>Video tutorial super lengkap mengajarkan Anda step by step hingga Mahir Email Marketing, List Building Anda akan meningkat 100% dengan Email Marketing. Lebih dahsyatnya lagi PLR Product ini bisa Anda jual kembali dengan keuntungan full buat Anda.</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<div data-id="49a391f7" class="elementor-element elementor-element-49a391f7 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h5 style="text-align: center;">Bonus #8 &#8211; Online Shop Mastery <span style="color: #ff0000;">(senilai Rp 497.000)</span></h5></div>
				</div>
				</div>
				<section data-id="1c7ecc85" class="elementor-element elementor-element-1c7ecc85 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="4697143f" class="elementor-element elementor-element-4697143f elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="439f522f" class="elementor-element elementor-element-439f522f elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p>Video tutorial yang Membahas tentang Mindset Online Shop, Mencari Supplier, Hal-hal teknis dalam menyiapkan web toko online, Cara Mendapatkan Visitor dari Social Media, Mempromosikan Toko Online hingga Strategi meningkatkan Income Bisnis Toko Online Anda.</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="91e793b" class="elementor-element elementor-element-91e793b elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="191549b" class="elementor-element elementor-element-191549b elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="300" height="185"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/ONLINE-SHOP-MASTERY-3-BOX-1-1-300x185.png" class="attachment-medium size-medium lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="300" height="185" src="wp-content/uploads/2020/01/ONLINE-SHOP-MASTERY-3-BOX-1-1-300x185.png" class="attachment-medium size-medium" alt="Panelsosmed.com" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<div data-id="13674e05" class="elementor-element elementor-element-13674e05 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h5 style="text-align: center;">Bonus #9 &#8211; 12 Ribu Link Group Whatsapp <span style="color: #ff0000;">(senilai Rp 179.000)</span></h5></div>
				</div>
				</div>
				<section data-id="43cb76c4" class="elementor-element elementor-element-43cb76c4 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="44943cf0" class="elementor-element elementor-element-44943cf0 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="69a4801c" class="elementor-element elementor-element-69a4801c elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="300" height="200"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/linkgroupwa2-1-300x200.png" class="attachment-post-thumbnail-medium size-post-thumbnail-medium lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="300" height="200" src="wp-content/uploads/2020/01/linkgroupwa2-1-300x200.png" class="attachment-post-thumbnail-medium size-post-thumbnail-medium" alt="Panelsosmed.com" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="6230d72e" class="elementor-element elementor-element-6230d72e elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="3a1724f3" class="elementor-element elementor-element-3a1724f3 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p>Dapatkan 12.000 Link Group Whatsapp ini gratis secara cuma-cuma untuk anda. Bisa anda gunakan untuk media promosi broadcast ke group-group tersebut. Bayangkan, dengan link group whatsapp sebanyak itu, berapa banyak potensi closing penjualan yang akan anda dapatkan. Terdapat pilihan berbagai macam niche link Group tersedia lengkap.</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<div data-id="652faa84" class="elementor-element elementor-element-652faa84 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h5 style="text-align: center;">Bonus #10 &#8211; 798 Database Supplier Premium Tokopedia <span style="color: #ff0000;">(senilai Rp 99.000)</span></h5></div>
				</div>
				</div>
				<section data-id="40a8e7db" class="elementor-element elementor-element-40a8e7db elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="a6a1484" class="elementor-element elementor-element-a6a1484 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="181e333a" class="elementor-element elementor-element-181e333a elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p>798 Database Supplier Premium Tokopedia special kami berikan untuk anda. Anta tidak perlu lagi pusing atau susah mencari supplier yang terbaik. Dengan database ini, anda akan menjadi dropshipper yang sukses dengan mendapatkan Database Supplier Premium Tokopedia tersebut. Gratis hanya untuk anda.</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="48d2956e" class="elementor-element elementor-element-48d2956e elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="135a8b6" class="elementor-element elementor-element-135a8b6 elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="150" height="150"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/databasesuppliertokopedia-1-150x150.jpg" class="attachment-thumbnail size-thumbnail lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="150" height="150" src="wp-content/uploads/2020/01/databasesuppliertokopedia-1-150x150.jpg" class="attachment-thumbnail size-thumbnail" alt="Panelsosmed.com" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<div data-id="10e8b79a" class="elementor-element elementor-element-10e8b79a elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h5 style="text-align: center;">Bonus #11 &#8211; PLR Product &#8211; Master Google Ads <span style="color: #ff0000;">(senilai Rp 250.000)</span></h5></div>
				</div>
				</div>
				<section data-id="5844c636" class="elementor-element elementor-element-5844c636 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="79dc1ae2" class="elementor-element elementor-element-79dc1ae2 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="4f256312" class="elementor-element elementor-element-4f256312 elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="300" height="165"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/mastergoogleadword-1-300x165.png" class="attachment-medium size-medium lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="300" height="165" src="wp-content/uploads/2020/01/mastergoogleadword-1-300x165.png" class="attachment-medium size-medium" alt="Panelsosmed.com" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="75a91d45" class="elementor-element elementor-element-75a91d45 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="19958d48" class="elementor-element elementor-element-19958d48 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p>Video tutorial super lengkap mengajarkan Anda step by step hingga Mahir Google Ads yang bisa meningkatkan profit bisnis Anda 100 kali lipat. Lebih dahsyatnya lagi PLR Product ini bisa Anda jual kembali dengan keuntungan full buat Anda.</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<div data-id="3869cc59" class="elementor-element elementor-element-3869cc59 elementor-widget elementor-widget-spacer" data-element_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<section data-id="1894f4d7" class="elementor-element elementor-element-1894f4d7 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="4d5b5596" class="elementor-element elementor-element-4d5b5596 elementor-column elementor-col-100 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="7e6e78d5" class="elementor-element elementor-element-7e6e78d5 elementor-button-danger elementor-align-center elementor-widget elementor-widget-button" data-element_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="#order" class="elementor-button-link elementor-button elementor-size-md elementor-animation-pulse">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Bonus Total Senilai Rp 2.517.000 Bisa Anda Dapatkan Gratis !!!</span>
		</span>
					</a>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<div data-id="7cbd872f" class="elementor-element elementor-element-7cbd872f elementor-widget elementor-widget-spacer" data-element_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="71b1640" class="elementor-element elementor-element-71b1640 elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="622bd53d" class="elementor-element elementor-element-622bd53d elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="36f6a533" class="elementor-element elementor-element-36f6a533 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3 style="text-align: center;">Ini dia <b>Benefit</b> Yang Akan <b>Anda Dapatkan </b>dari<b> Panelsosmed </b> :</h3></div>
				</div>
				</div>
				<section data-id="194d24ca" class="elementor-element elementor-element-194d24ca elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="37e26624" class="elementor-element elementor-element-37e26624 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="3f73ab4e" class="elementor-element elementor-element-3f73ab4e elementor-align-left elementor-widget elementor-widget-icon-list" data-element_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Anda akan Memiliki System Optimized Sosial Media Canggih</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Anda Bisa Mempunyai Bisnis Jasa Tambah Followers</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Memperbanyak Followers Semudah Membalikkan Tangan</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Dengan Technobooks Anda Bisa Menjadi Celebgram</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Anda Bisa Mempunyai Jasa Endorse</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Akun Sosial Media Anda Makin Di Percaya Oleh Customer</span>
									</li>
						</ul>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="6d0ab7af" class="elementor-element elementor-element-6d0ab7af elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="7a0f3548" class="elementor-element elementor-element-7a0f3548 elementor-align-left elementor-widget elementor-widget-icon-list" data-element_type="icon-list.default">
				<div class="elementor-widget-container">
					<ul class="elementor-icon-list-items">
							<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Anda Bisa Mengirim Followers Ke Akun Manapun</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Mendapatkan Harga Layanan Termurah Di Indonesia</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Mendapatkan Bonus Premium Senilai 1 Juta Lebih</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Masuk Group Private Khusus</span>
									</li>
								<li class="elementor-icon-list-item" >
											<span class="elementor-icon-list-icon">
							<i class="fa fa-arrow-right" aria-hidden="true"></i>
						</span>
										<span class="elementor-icon-list-text">Full Support, Bimbingan & Sharing2 di Dalam Group</span>
									</li>
						</ul>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<div data-id="1ecb598f" class="elementor-element elementor-element-1ecb598f elementor-widget elementor-widget-spacer" data-element_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<section data-id="6ea01d9f" class="elementor-element elementor-element-6ea01d9f elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="6a7ae14e" class="elementor-element elementor-element-6a7ae14e elementor-column elementor-col-33 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="c0502c5" class="elementor-element elementor-element-c0502c5 animated fadeIn elementor-invisible elementor-widget elementor-widget-image" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img  title="Panelsosmed.com" alt="Panelsosmed.com"  data-src="https://review.panelsosmed.com/wp-content/uploads/elementor/thumbs/group-1-ojh3yghla2oz8trbgmcyksx6pxajekhpuru095wzjc.png" class="elementor-animation-grow lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img src="wp-content/uploads/elementor/thumbs/group-1-ojh3yghla2oz8trbgmcyksx6pxajekhpuru095wzjc.png" title="Panelsosmed.com" alt="Panelsosmed.com" class="elementor-animation-grow" /></noscript>		</div>
				</div>
				</div>
				<div data-id="2505f0a6" class="elementor-element elementor-element-2505f0a6 elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Group Khusus</h2>		</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="15b753f9" class="elementor-element elementor-element-15b753f9 elementor-column elementor-col-33 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="6c4c8755" class="elementor-element elementor-element-6c4c8755 animated fadeIn elementor-invisible elementor-widget elementor-widget-image" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img  title="Panelsosmed.com" alt="panelsosmed.com"  data-src="https://review.panelsosmed.com/wp-content/uploads/elementor/thumbs/support-1-ojh3yghla2oz8trbgmcyksx6pxajekhpuru095wzjc.png" class="elementor-animation-grow lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img src="wp-content/uploads/elementor/thumbs/support-1-ojh3yghla2oz8trbgmcyksx6pxajekhpuru095wzjc.png" title="Panelsosmed.com" alt="panelsosmed.com" class="elementor-animation-grow" /></noscript>		</div>
				</div>
				</div>
				<div data-id="3a50197f" class="elementor-element elementor-element-3a50197f elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Team Support 24 Jam</h2>		</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="2bc4a799" class="elementor-element elementor-element-2bc4a799 elementor-column elementor-col-33 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="6e8416d7" class="elementor-element elementor-element-6e8416d7 animated fadeIn elementor-invisible elementor-widget elementor-widget-image" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img  title="Panelsosmed.com" alt="Panelsosmed.com"  data-src="https://review.panelsosmed.com/wp-content/uploads/elementor/thumbs/mater5-1-ojh3yhffgwq9kfpyb4rl5aonbb5wm9lg6whhqfvld4.png" class="elementor-animation-grow lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img src="wp-content/uploads/elementor/thumbs/mater5-1-ojh3yhffgwq9kfpyb4rl5aonbb5wm9lg6whhqfvld4.png" title="Panelsosmed.com" alt="Panelsosmed.com" class="elementor-animation-grow" /></noscript>		</div>
				</div>
				</div>
				<div data-id="703e58ba" class="elementor-element elementor-element-703e58ba elementor-widget elementor-widget-heading" data-element_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-default">Bonus Permium</h2>		</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="42e4eea" class="elementor-element elementor-element-42e4eea elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="7e3027ed" class="elementor-element elementor-element-7e3027ed elementor-column elementor-col-100 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="419d7ea8" class="elementor-element elementor-element-419d7ea8 elementor-widget elementor-widget-menu-anchor" data-element_type="menu-anchor.default">
				<div class="elementor-widget-container">
					<div id="beli" class="elementor-menu-anchor"></div>
				</div>
				</div>
				<div data-id="64a506f4" class="elementor-element elementor-element-64a506f4 elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
						<a href="#order" class="elementor-clickable elementor-image-link" data-elementor-open-lightbox="default">
		<img width="300" height="80"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/button-beli-sekarang-1-300x80.png" class="elementor-animation-wobble-vertical attachment-medium size-medium lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="300" height="80" src="wp-content/uploads/2020/01/button-beli-sekarang-1-300x80.png" class="elementor-animation-wobble-vertical attachment-medium size-medium" alt="Panelsosmed.com" /></noscript>				</a>
				</div>
				</div>
				</div>
				<div data-id="23449903" class="elementor-element elementor-element-23449903 elementor-widget elementor-widget-image" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="300" height="25"   alt="Panelsosmed.com" data-src="https://review.panelsosmed.com/wp-content/uploads/2020/01/secure-checkout-logo-1-300x25.png" class="attachment-medium size-medium lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="300" height="25" src="wp-content/uploads/2020/01/secure-checkout-logo-1-300x25.png" class="attachment-medium size-medium" alt="Panelsosmed.com" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<div data-id="6b88b5d0" class="elementor-element elementor-element-6b88b5d0 elementor-widget elementor-widget-spacer" data-element_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="645d3062" class="elementor-element elementor-element-645d3062 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="62ce99f5" class="elementor-element elementor-element-62ce99f5 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="72ff2e66" class="elementor-element elementor-element-72ff2e66 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><h3 style="text-align: center;">Yang Biasa <b>Ditanyakan</b>, Silahkan <b>Anda</b> Baca :</h3></div>
				</div>
				</div>
				<section data-id="11b530b" class="elementor-element elementor-element-11b530b elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="7a8df44a" class="elementor-element elementor-element-7a8df44a elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="201902f0" class="elementor-element elementor-element-201902f0 elementor-widget elementor-widget-toggle" data-element_type="toggle.default">
				<div class="elementor-widget-container">
					<div class="elementor-toggle" role="tablist">
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-5381" class="elementor-tab-title" tabindex="5381" data-tab="1" role="tab" aria-controls="elementor-tab-content-5381">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Apa Sih Technobooks itu ?					</div>
					<div id="elementor-tab-content-5381" class="elementor-tab-content elementor-clearfix" data-tab="1" role="tabpanel" aria-labelledby="elementor-tab-title-5381"><p>Technobooks adalah layanan tools system yang berfungsi untuk optimized sosial media seperti instagram Twitter facebook youtube soundcloud dll</p><p>Salah satu fiturnya adalah untuk menambah followers likes view comment yg diinginkan secara instan.</p><p>Sehingga membuka peluang untuk orang-orang yg ingin punya bisnis jasa optimized sosial media. Seperti tambah followers likes view comment dll untuk semua sosial media.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-5382" class="elementor-tab-title" tabindex="5382" data-tab="2" role="tab" aria-controls="elementor-tab-content-5382">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												System Technobooks Web atau Aplikasi ?					</div>
					<div id="elementor-tab-content-5382" class="elementor-tab-content elementor-clearfix" data-tab="2" role="tabpanel" aria-labelledby="elementor-tab-title-5382"><p>Technobooks basis web, lebih simple bisa dibuka dari mana saja tinggal buka lewat browser Anda. Tersedia juga aplikasi Androidnya.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-5383" class="elementor-tab-title" tabindex="5383" data-tab="3" role="tab" aria-controls="elementor-tab-content-5383">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Maksudnya Saldo Untuk Apa ?					</div>
					<div id="elementor-tab-content-5383" class="elementor-tab-content elementor-clearfix" data-tab="3" role="tabpanel" aria-labelledby="elementor-tab-title-5383"><p>System Technobooks menggunakan system saldo, setiap submit prosesnya akan memotong saldo yang anda miliki.</p><p>Tapi tenang, harga layanan di dalamnya udah termurah di Indonesia.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-5384" class="elementor-tab-title" tabindex="5384" data-tab="4" role="tab" aria-controls="elementor-tab-content-5384">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Jika Saldo Habis, Cara Ngisinya Gimaa ?					</div>
					<div id="elementor-tab-content-5384" class="elementor-tab-content elementor-clearfix" data-tab="4" role="tabpanel" aria-labelledby="elementor-tab-title-5384"><p>Bisa Topup Saldo via bank transfer atau via pulsa. Didalamnya sudah tersedia lengkap.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-5385" class="elementor-tab-title" tabindex="5385" data-tab="5" role="tab" aria-controls="elementor-tab-content-5385">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Apakah Saya Bisa Jualan Followers ?					</div>
					<div id="elementor-tab-content-5385" class="elementor-tab-content elementor-clearfix" data-tab="5" role="tabpanel" aria-labelledby="elementor-tab-title-5385"><p>Iya pasti, Anda akan dipandu sampai bisa, bahkan sampai sukses memiliki bisnis jasa tambah followers. Full support, bimbingan dan sharing2 di dalam group khusus. </p><p>Para jasa tambah followers diluaran sana banyak yang menggunakan Technobooks system.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-5386" class="elementor-tab-title" tabindex="5386" data-tab="6" role="tab" aria-controls="elementor-tab-content-5386">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Apakah Ada Jaminan Sukses Jualan ?					</div>
					<div id="elementor-tab-content-5386" class="elementor-tab-content elementor-clearfix" data-tab="6" role="tabpanel" aria-labelledby="elementor-tab-title-5386"><p>Technobooks tidak bisa menjamin anda langsung sukses dalam bisnis/jualan.</p><p>Tapi Technobooks menjamin bisa menambah followers, likes, views, subscribes dll secara instant.</p></div>
				</div>
					</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="3be664c6" class="elementor-element elementor-element-3be664c6 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="5b94f5af" class="elementor-element elementor-element-5b94f5af elementor-widget elementor-widget-toggle" data-element_type="toggle.default">
				<div class="elementor-widget-container">
					<div class="elementor-toggle" role="tablist">
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1531" class="elementor-tab-title" tabindex="1531" data-tab="1" role="tab" aria-controls="elementor-tab-content-1531">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Apakah Hanya Untuk Instagram ?					</div>
					<div id="elementor-tab-content-1531" class="elementor-tab-content elementor-clearfix" data-tab="1" role="tabpanel" aria-labelledby="elementor-tab-title-1531"><p>Bukan hanya instagram, tapi semua sosial media bisa.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1532" class="elementor-tab-title" tabindex="1532" data-tab="2" role="tab" aria-controls="elementor-tab-content-1532">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Apakah Followersnya Bisa Tertarget ?					</div>
					<div id="elementor-tab-content-1532" class="elementor-tab-content elementor-clearfix" data-tab="2" role="tabpanel" aria-labelledby="elementor-tab-title-1532"><p>Bisa Banget, Didalamnya tersedia Pilihan Followers Active Indoesia &amp; Random Worldwide (Bule).</p><p>Bahkan lebih Dahsyatnya lagi tersedia pilihan Followers tertarget berdasarkan Kota di Indonesia.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1533" class="elementor-tab-title" tabindex="1533" data-tab="3" role="tab" aria-controls="elementor-tab-content-1533">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Berapa Lama Prosesnya ?					</div>
					<div id="elementor-tab-content-1533" class="elementor-tab-content elementor-clearfix" data-tab="3" role="tabpanel" aria-labelledby="elementor-tab-title-1533"><p>Proses sangat cepat dan instant, tergantung server yang dipilih dan jumlah yang di order.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1534" class="elementor-tab-title" tabindex="1534" data-tab="4" role="tab" aria-controls="elementor-tab-content-1534">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Apakah Ada Biaya Bulanan ?					</div>
					<div id="elementor-tab-content-1534" class="elementor-tab-content elementor-clearfix" data-tab="4" role="tabpanel" aria-labelledby="elementor-tab-title-1534"><p>Tidak ada biaya bulanan atau tahunan. Pendaftaran Lifetime hanya satu kali untuk selamanya.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1535" class="elementor-tab-title" tabindex="1535" data-tab="5" role="tab" aria-controls="elementor-tab-content-1535">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Apakah Bisa Redund ?					</div>
					<div id="elementor-tab-content-1535" class="elementor-tab-content elementor-clearfix" data-tab="5" role="tabpanel" aria-labelledby="elementor-tab-title-1535"><p>Mohon Maaf, Ketentuan Kami Tidak Bisa Refund, Tapi Tenang Saja Kami Akan Memberikan Layanan dan Support Yang Terbaik </p><p>Saldo Didalam Panel Akan Tetap Ada Selama Tidak Anda Digunakan, Aman Tenang Saja.</p><p> </p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1536" class="elementor-tab-title" tabindex="1536" data-tab="6" role="tab" aria-controls="elementor-tab-content-1536">
												<span class="elementor-toggle-icon elementor-toggle-icon-left" aria-hidden="true">
							<i class="elementor-toggle-icon-closed fa fa-caret-right"></i>
							<i class="elementor-toggle-icon-opened fa fa-caret-up"></i>
						</span>
												Apakah Ada Panduan dan Support nya?					</div>
					<div id="elementor-tab-content-1536" class="elementor-tab-content elementor-clearfix" data-tab="6" role="tabpanel" aria-labelledby="elementor-tab-title-1536"><p>Iya pasti, Anda akan dipandu sampai bisa, bahkan sampai sukses memiliki bisnis jasa tambah followers. Full support, bimbingan dan sharing2 di dalam group khusus.</p></div>
				</div>
					</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section data-id="15d4cf63" class="elementor-element elementor-element-15d4cf63 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="1e319ab5" class="elementor-element elementor-element-1e319ab5 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<section data-id="bbe733e" class="elementor-element elementor-element-bbe733e elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div data-id="6ce7c816" class="elementor-element elementor-element-6ce7c816 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="34de9802" class="elementor-element elementor-element-34de9802 animated fadeIn elementor-invisible elementor-widget elementor-widget-image" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
		<img width="300" height="179"   alt="Panelsosmed.com" data-src="<?php echo $config['web']['url'] ?>assets/media/logos/logo-4.png" class="attachment-medium size-medium lazyload" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" /><noscript><img width="300" height="179" src="<?php echo $config['web']['url'] ?>assets/media/logos/logo-4.png" class="attachment-medium size-medium" alt="Panelsosmed.com" /></noscript>		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div data-id="74d8a9b9" class="elementor-element elementor-element-74d8a9b9 elementor-column elementor-col-50 elementor-inner-column" data-element_type="column">
			<div class="elementor-column-wrap elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div data-id="4ba5b1e2" class="elementor-element elementor-element-4ba5b1e2 elementor-widget elementor-widget-spacer" data-element_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div data-id="466626b4" class="elementor-element elementor-element-466626b4 elementor-button-danger elementor-align-center elementor-mobile-align-justify animated fadeIn elementor-invisible elementor-widget elementor-widget-button" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper elementor-button-sticky-no">
			<a href="https://www.instagram.com/technobooks" class="elementor-button-link elementor-button elementor-size-md elementor-animation-grow" target="_blank">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-align-icon-left elementor-button-icon">
				<i class="fa fa-instagram" aria-hidden="true"></i>
			</span>
						<span class="elementor-button-text">technobooks</span>
		</span>
					</a>
		</div>
				</div>
				</div>
				<div data-id="145aa121" class="elementor-element elementor-element-145aa121 elementor-align-center elementor-mobile-align-justify animated fadeIn elementor-invisible elementor-widget elementor-widget-button" data-settings="{&quot;_animation&quot;:&quot;fadeIn&quot;}" data-element_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper elementor-button-sticky-no">
			<a href="#order" class="elementor-button-link elementor-button elementor-size-md elementor-animation-grow">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Daftar Sekarang</span>
		</span>
					</a>
		</div>
				</div>
				</div>
				<div data-id="7dab387a" class="elementor-element elementor-element-7dab387a elementor-widget elementor-widget-spacer" data-element_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<div data-id="53103856" class="elementor-element elementor-element-53103856 elementor-widget elementor-widget-spacer" data-element_type="spacer.default">
				<div class="elementor-widget-container">
					<div class="elementor-spacer">
			<div class="elementor-spacer-inner"></div>
		</div>
				</div>
				</div>
				<div data-id="698ba1dc" class="elementor-element elementor-element-698ba1dc elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><p><strong>All Rights Reserved. Cybercode.my.id 2018.</strong><br />About &#8211; Privacy Policy &#8211; Terms Of Use &#8211; Disclaimer &#8211; Contact</p></div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
			</main>
</div>
					</div>
	</div>
	</div></div></div><div id="back-to-top"><i class="fa fa-angle-up"></i></div><link rel='stylesheet' id='magnific-popup-css'  href='wp-content/themes/landingpress-wp/assets/lib/magnific-popup/jquery.magnific.popup.minc64e.css' type='text/css' media='all' />
<script type='text/javascript'>
window.lazySizesConfig = window.lazySizesConfig || {};

window.lazySizesConfig.lazyClass    = 'lazyload';
window.lazySizesConfig.loadingClass = 'lazyloading';
window.lazySizesConfig.loadedClass  = 'lazyloaded';

lazySizesConfig.loadMode = 1;
</script>
<script type='text/javascript' src='wp-content/plugins/wp-smushit/app/assets/js/smush-lazy-load.minccfb.js' defer='defer'></script>
<script type='text/javascript'>
lazySizes.init();
</script>
<script type='text/javascript' src='wp-includes/js/jquery/jquery4a5f.js'></script>
<script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.min330a.js' defer='defer'></script>
<script type='text/javascript' src='wp-content/themes/landingpress-wp/addons/elementor/assets/lib/jquery-numerator/jquery-numerator.min3958.js' defer='defer'></script>
<script type='text/javascript' src='wp-content/themes/landingpress-wp/assets/lib/magnific-popup/jquery.magnific.popup.minc64e.js' defer='defer'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/position.mine899.js' defer='defer'></script>
<script type='text/javascript' src='wp-content/themes/landingpress-wp/addons/elementor/assets/lib/dialog/dialog.min50fa.js' defer='defer'></script>
<script type='text/javascript' src='wp-content/themes/landingpress-wp/addons/elementor/assets/lib/waypoints/waypoints.min05da.js' defer='defer'></script>
<script type='text/javascript' src='wp-content/themes/landingpress-wp/addons/elementor/assets/lib/swiper/swiper.jquery.minccfb.js' defer='defer'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var elementorFrontendConfig = {"isEditMode":"","settings":{"page":[],"general":{"elementor_global_image_lightbox":"yes","elementor_enable_lightbox_in_editor":"yes"}},"is_rtl":"","urls":{"assets":"https:\/\/review.panelsosmed.com\/wp-content\/themes\/landingpress-wp\/addons\/elementor\/assets\/"},"post":{"id":54,"title":"Review Panelsosmed","excerpt":""}};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/themes/landingpress-wp/addons/elementor/assets/js/frontend.minc07d.js' defer='defer'></script>
<script type='text/javascript' src='wp-content/themes/landingpress-wp/assets/js/script.minbb24.js' defer='defer'></script>
<!-- Facebook Pixel Code -->
<noscript><img height="1" width="1" alt="fbpx" style="display:none" src="../external.html?link=https://www.facebook.com/tr?id=#&amp;ev=PageView&amp;noscript=1" /></noscript>
<!-- End Facebook Pixel Code -->

<!--[if LandingPress]></body></html><![endif]-->
<!-- </body></html> -->
</body>

<!-- Mirrored from review.panelsosmed.com/ by HTTrack Website Copier/3.x [XR&CO'2017], Mon, 22 Jun 2020 17:57:20 GMT -->
</html>

<!-- Page supported by LiteSpeed Cache 2.9.9.2 on 2020-06-23 00:56:12 -->