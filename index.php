<?php 
session_start();
require("config.php");

    if (isset($_SESSION['user'])) {
        
        $sess_username = $_SESSION['user']['username'];
        $check_user = $conn->query("SELECT * FROM users WHERE username = '$sess_username'");
        $data_user = $check_user->fetch_assoc();
        $check_username = $check_user->num_rows;
        if ($check_username == 0) {
	        header("Location: ".$config['web']['url']."logout.php");
        } else if ($data_user['status'] == "Tidak Aktif") {
	        header("Location: ".$config['web']['url']."logout.php");
        }

    } else {
        $_SESSION['user'] = $data_user;
        header("Location: ".$config['web']['url']."dashboard");
    }

include("lib/header.php");
if (isset($_SESSION['user'])) {
?>

        
        <style>
            .kt-portlet.kt-portlet--height-fluid {
                background: transparent;
                border: solid 1px #ffffff;
                padding: 10px;
                border-radius: 4px;
            }
            a.text-primary:focus, a.text-primary:hover {
                color: #0070C0!important;
            }
            .i-bg-circle {
    		font-size: 18px;padding-top: 5px;background: #2475c4;color: #fff;padding: 16px 18px;border-radius: 50%/50%;margin-bottom: 8px;
    	    }
    	    .box-menu-middle-pay {
		    padding: 15px 15px 10px;
		    background: #fff;
		    border-radius: 0;
	    }
            @media(max-width:767px){
            	body {
            		background-color: #f9f9fc;
            		background-image: none!important;
            	}
            	.kt-header-mobile {
			background-color: #f9f9fc;
            	}
            	.kt-header-mobile__toolbar-topbar-toggler i {
            		color:#0070C0!important;
            	}
            	.dasboard-m-2 {
            		color:#333;
            		font-size: 28px;
            		text-align:center;
            	}
            	.product-catagory-wrap .catagory-card {
		    margin-bottom: 15px;
		    border: solid 0.5px #e1e1e1!important;
		}
		.card .card-body {
		    padding: 1.5rem 6px;
		}
		.box-menu-middle-pay {
		    padding: 10px;
		    background: transparent;
		}
            }
        </style>
	        
		<div class="kt-container my-4">
		    <div style=" padding: 10px 0;">
		        <div class="row align-items-center">
                    <div class="col-12 col-md-6">
                        <h4 class="mb-0 dasboard-m-2"><span style=" display: block; font-size: 14px; letter-spacing: 1px;">Saldo E-KTP</span> Rp. <?php echo number_format($data_user['saldo_top_up'],0,',','.'); ?></h4>
                    </div>
                    <div class="col-12 col-md-6 text-right">
                        <a href="<?php echo $config['web']['url'] ?>staff/transfer-balance" class="btn btn-primary btn-md d-none d-sm-inline"> <i class="fas fa-expand"></i> Bayar / Transfer</a>
                    </div>
                </div>
            </div>
            <div class="box-menu-middle-pay">
                <div class="row align-items-center text-center">
                    <div class="col-4">
                        <a href="<?php echo $config['web']['url'] ?>scan" style="font-size: 14px;color:#0070C0;"> <i class="fas fa-qrcode i-bg-circle"></i><br><span> QR Code</span></a>            
                    </div>
                     <div class="col-4">
                        <a href="<?php echo $config['web']['url'] ?>riwayat" style="font-size: 14px;color:#0070C0;"> <i class="fas fa-history i-bg-circle"></i><br><span> Riwayat</span></a>            
                    </div>
                    <div class="col-4">
                        <a href="<?php echo $config['web']['url'] ?>deposit-balance" style="font-size: 14px;color:#0070C0;"><i class="fas fa-plus i-bg-circle"></i><br><span>Isi Saldo</span></a>
                    </div>
                </div>
            </div>
		    
		</div>

		<!-- Start Card Box Order -->
        <div class="kt-container">
            <div class="product-catagory-wrap">
                <div class="row">
                    <div class="col-12">
                        <p class="pt-3"><b>Layanan</b></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="card catagory-card">
                            <span class="">
                                <div class="card-body p-0">
                                    <div class="card">
                                        <div class="card-body">
                                            <a href="<?php echo $config['web']['url'] ?>order/pulsa-reguler"><img src="<?php echo $config['web']['url'] ?>assets/media/menu/news.png" alt="" style=" padding-top: 3px; "><span>Bayar SPP</span></a>
                                        </div>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="card catagory-card">
                            <span class="">
                                <div class="card-body p-0">
                                    <div class="card">
                                        <div class="card-body">
                                            <a href="<?php echo $config['web']['url'] ?>order/pln-pascabayar">
                                                <img src="<?php echo $config['web']['url'] ?>assets/media/menu/mortarboard.png" alt=""><span>Bayar Kuliah</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="card catagory-card">
                            <span class="">
                                <div class="card-body p-0">
                                    <div class="card">
                                        <div class="card-body">
                                            <a href="<?php echo $config['web']['url'] ?>order/bpjs-kesehatan">
                                                <img src="<?php echo $config['web']['url'] ?>assets/media/menu/salary.png" alt=""><span>Iuran RT</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="card catagory-card">
                            <span class="">
                                <div class="card-body p-0">
                                    <div class="card">
                                        <div class="card-body">
                                            <a href="<?php echo $config['web']['url'] ?>order/token-pln"><img src="<?php echo $config['web']['url'] ?>assets/media/menu/stayhome.png" alt=""><span>Beramal</span></a>
                                        </div>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="card catagory-card">
                            <span class="">
                                <div class="card-body p-0">
                                    <div class="card">
                                        <div class="card-body">
                                            <a href="<?php echo $config['web']['url'] ?>order/pdam">
                                                <img src="<?php echo $config['web']['url'] ?>assets/media/menu/sketchbook.png" alt=""><span>Bank Sampah</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="card catagory-card">
                            <span class="">
                                <div class="card-body p-0">
                                    <div class="card">
                                        <div class="card-body">
                                            <a href="<?php echo $config['web']['url'] ?>app/ktpku-security?email=<?php echo $data_user['email'] ?>">
                                                <img src="<?php echo $config['web']['url'] ?>assets/media/menu/block-user.png" alt=""><span>Akun Security</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                </div>                
                
            </div>
        </div>
        <!-- End Card Box Order -->

        <!-- Poin 
        <div class="kt-container">
            <div class="cta-area">
                <div class="px-4 py-5" style="background-color: #00b0f0;border-radius: 0 0 10px 10px;box-shadow: 0 0 9px rgba(82,63,105,.15);">
                    <h4 class="text-white">Dapatkan Koin Gratis!</h4>
                    <p class="text-white">Ajak Temen Atau Keluarga Untuk Mendaftar Disini Dengan Kode Referral Kamu.</p>
                    <a class="btn btn-primary" href="<?php echo $config['web']['url'] ?>page/program-referral"><i class="fa fa-coins"></i> Koin Anda : <?php echo number_format($data_user['koin'],0,',','.'); ?></a>
                    <a class="btn btn-primary" href="<?php echo $config['web']['url'] ?>page/withdraw-coin-to-balance"><i class="fa fa-share-square"></i> Tarik</a>
                </div>
            </div>
        </div>
        Poin -->

        <br />

        <!-- Start Content -->
        <div class="kt-container kt-grid__item kt-grid__item--fluid">

        <!-- Start Sub Icon Box -->
	    <div class="row">
        
        <!-- Start Modal Content -->
        <?php 
        if ($data_user['read_news'] == 0) {
        ?>
        <div class="modal fade show" id="news" tabindex="-1" role="dialog" aria-labelledby="NewsInfo" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title mt-0" id="NewsInfo"><b><i class="flaticon2-bell text-primary"></i>  Berita & Informasi</b></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" style="max-height: 400px; overflow: auto;">
                    <?php
                    $cek_berita = $conn->query("SELECT * FROM berita ORDER BY id DESC LIMIT 5");
                    while ($data_berita = $cek_berita->fetch_assoc()) {
                    if ($data_berita['tipe'] == "INFO") {
                        $label = "info";
                    } else if ($data_berita['tipe'] == "PERINGATAN") {
                        $label = "warning";
                    } else if ($data_berita['tipe'] == "PENTING") {
                        $label = "danger";    
                    }
                    ?>  
                    <div class="alert alert-secondary">
                        <div class="alert-text">
                            <p><span class="float-right text-muted"><?php echo tanggal_indo($data_berita['date']); ?>, <?php echo $data_berita['time']; ?></span></p>
                            <h5 class="inbox-item-author mt-0 mb-1"><?php echo $data_berita['title']; ?></h5>
                            <h5><span class="badge badge-<?php echo $label; ?>"><?php echo $data_berita['tipe']; ?></span></h5>
                            <?php echo nl2br($data_berita['konten']); ?>
                        </div>
                    </div>
                    <?php } ?>   
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="read_news()"><i class="flaticon2-check-mark"></i> Saya Sudah Membaca</button>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <!-- End Modal Content-->

        </div>
        <!-- End Page -->
            
        </div>
        <!-- End Content -->

<?php 
}
require 'lib/footer.php';
?>