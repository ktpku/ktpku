<?php 
session_start();
require '../config.php';
require '../lib/session_login.php';
require '../lib/session_user.php';

	    if (isset($_POST['transfer'])) {
	        
	        //Tujuan = Pengirim
	        $post_metode = $conn->real_escape_string($_POST['radio7']);
            $jumlah = $conn->real_escape_string(trim(filter($_POST['jumlah'])));
            $pin = $conn->real_escape_string(trim(filter($_POST['pin'])));
            $merchant = $conn->real_escape_string(trim(filter($_POST['merchant'])));
            
            // cek id kartu
            // =====================
            $id_kartu = $conn->real_escape_string(trim(filter($_POST['id_kartu'])));
            $cek_id_kartu = $conn->query("SELECT * FROM users WHERE id_kartu = '$id_kartu'");
			$cek_id_kartu_rows = mysqli_num_rows($cek_id_kartu);
			
			if ($cek_id_kartu_rows == 0 ) {
			    
			    $_SESSION['hasil'] = array('alert' => 'warning', 'pesan' => 'Untuk transaksi pertama kali, Kamu harus input NIK E-KTP. Jangan khawatir, untuk transaksi selanjutnya, Kamu tidak perlu input NIK lagi.');
			    $tujuan = $conn->real_escape_string(trim(filter($_POST['tujuan'])));

		    } else {
		        
		        $ceknim = $conn->query("SELECT * FROM users WHERE id_kartu = '$id_kartu'");
                $cek_ceknim_db = mysqli_fetch_assoc($ceknim);
                
		        $tujuan = $conn->real_escape_string(trim(filter( $cek_ceknim_db['username'] )));
		        
		    }
		    // =====================
            
            $cek_tujuan = $conn->query("SELECT * FROM users WHERE username = '$tujuan'");
            $cek_tujuan_rows = mysqli_num_rows($cek_tujuan);
            $cek_tujuan_db = mysqli_fetch_assoc($cek_tujuan);
            
            $post_metodee = "Saldo Top Up";
            
            $error = array();
            if (empty($tujuan)) {
		        $error ['tujuan'] = '*Tidak Boleh Kosong.';
            }
            if (empty($jumlah)) {
		        $error ['jumlah'] = '*Tidak Boleh Kosong.';
            }
            if (empty($pin)) {
		        $error ['pin'] = '*Tidak Boleh Kosong.';
            } else if ($pin <> $data_user['pin']) {
		        $error ['pin'] = '*PIN Yang Kamu Masukkan Salah.';
            } else {

		    if ($cek_tujuan_rows == 0 ) {
			    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Hmm, NIK E-KTP Tidak Ditemukan.<script>swal("Ups Gagal!", "E-KTP Tidak Ditemukan.", "error");</script>');
			    exit(header("Location: ".$config['web']['url']."staff/pembayaran"));
		    } else if ($jumlah < 1000 ) {
			    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Hmm, Minimal Transaksi Adalah Rp 1.000.<script>swal("Ups Gagal!", "Minimal Transaksi Adalah Rp 1.000.", "error");</script>');
		    } else if ( $cek_tujuan_db['saldo_top_up'] < $jumlah ) {
			    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Hmm, Saldo E-KTP Kamu Tidak Mencukupi Untuk Melakukan Transaksi.<script>swal("Ups Gagal!", "Saldo E-KTP Kamu Tidak Mencukupi Untuk Melakukan Transaksi.", "error");</script>');
			    exit(header("Location: ".$config['web']['url']."staff/pembayaran"));
		    } else {

			            $check_top = $conn->query("SELECT * FROM top_depo WHERE username = '$tujuan'");
			            $data_top = mysqli_fetch_assoc($check_top);
			            
			        if ($conn->query("UPDATE users set $post_metode = $post_metode - $jumlah WHERE username = '$tujuan'") == true) {
			            
			            $id_trx = str_replace("-","", $date ).'-'.acak_nomor(6);
			            
			            //Merchant
				        $conn->query("UPDATE users set saldo_top_up = saldo_top_up + $jumlah WHERE username = '$sess_username'");
				        $conn->query("INSERT INTO riwayat_saldo_koin VALUES ('', '$sess_username', 'Saldo', 'Penambahan Saldo', '$jumlah', 'Mendapatkan Saldo Transaksi Dari $tujuan Sejumlah Rp $jumlah ', '$date', '$time', '$id_trx')");
				        
				        //Pengirim
				        $conn->query("UPDATE users set pemakaian_saldo = pemakaian_saldo + $jumlah  WHERE username = '$tujuan'");
				        $conn->query("INSERT INTO riwayat_saldo_koin VALUES ('', '$tujuan', 'Saldo', 'Pengurangan Saldo', '$jumlah', 'Mengurangi Saldo Transaksi Ke $sess_username Sejumlah Rp $jumlah', '$date', '$time', '$id_trx')");	
				        
				        // Update ID Kartu Pengirim Jika Belum Pernah Transaksi
				        // =====================
				        if ($cek_id_kartu_rows == 0 ) { $conn->query("UPDATE users set id_kartu = $id_kartu  WHERE username = '$tujuan'"); }
				        // =====================
				        
                        $conn->query("INSERT INTO riwayat_transfer VALUES ('', '$post_metodee', '$tujuan', '$sess_username', '$jumlah','$date', '$time', '$id_trx')");
                        
					    if (mysqli_num_rows($check_top) == 0) {
				            $insert_topup = $conn->query("INSERT INTO top_depo VALUES ('', 'Deposit', '$tujuan', '$jumlah', '1')");
				        } else {
				            $insert_topup = $conn->query("UPDATE top_depo SET jumlah = ".$data_top['jumlah']." + $jumlah, total = ".$data_top['total']." + 1 WHERE username = '$tujuan' AND method = 'Transaksi'");
				        }
				        
                        $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'No. Transaksi : <b>#'.$id_trx.'</b><br>Transaksi Berhasil, Saldo Anda Berkurang <b>Rp.'.$jumlah.'</b><br>Dari NIK E-KTP <b>'.$tujuan.'</b><hr style=" border-style: dashed; "><i class="fa fa-check fa-lg"></i> Ini Bukti Pembayaran Yang Sah.' );
                        exit(header("Location: ".$config['web']['url']."staff/pembayaran"));
                        
				    } else {
				        $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Hmm, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Hmm Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
				        exit(header("Location: ".$config['web']['url']."staff/pembayaran"));
			        }
			    }					
		    }
		    
	    }

        require '../lib/header.php';

?>

        <style>
            #kt_header, #kt_footer, .kt-header-mobile--fixed .kt-header-mobile, .fixed-action-btn-two {
                display:none;
            }
            body {
            		background-color: #f9f9fc;
            		background-image: none!important;
            	}
            .logoktp {
                height: 50px; 
                margin:20px 0;
            }
            .alert.alert-success {
                background: transparent;
                border: solid 2px #1dc9b7;
                color: #1dc9b7;
                display: block;
            }
            #id_kartu:focus {
                border: dashed 3px #00b0f0;
            }
            @media(max-width:767px){
                .logoktp { height: auto; width: 100%; }
            }
        </style>
        
        <!-- Start Content -->
        <div class="kt-container kt-grid__item kt-grid__item--fluid mt-4">

        <!-- Start Page Transfer Balance -->
        <div class="row align-items-center">
	        <div class="col-4 col-md-6">
	            <a href="<?php echo $config['web']['url'] ?>"><img alt="Logo" src="<?php echo $config['web']['url'] ?>assets/media/logos/logo_color.png" class="logoktp" /></a>
	        </div>
	        <div class="col-8  col-md-6">
	            <h3 class="text-right text-muted"><span style=" display: block; font-size: 14px; ">Nama Merchant</span> <?php echo $data_user['nama']; ?></h3>
	        </div>
	    </div>
        <div class="row">
	        <div class="col-md-12">
		        <div class="kt-portlet">
			        <div class="kt-portlet__head">
				        <div class="kt-portlet__head-label">
					        <h3 class="kt-portlet__head-title">
					            SCAN E-KTP Untuk Bayar
					        </h3>
				        </div>
			        </div>
			        <div class="kt-portlet__body">
                    <?php
                    if (isset($_SESSION['hasil'])) {
                    ?>
                    <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $_SESSION['hasil']['pesan'] ?>
                    </div>
                    <?php
                    unset($_SESSION['hasil']);
                    }
                    ?>
					<form class="form-horizontal" role="form" method="POST">
						<input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
						
							<input type="hidden" name="radio7" id="metod" value="saldo_top_up" >
							
							<div class="form-group row" id="wrapper_id_kartu">
								<div class="col-lg-12">
									<div class="input-group">
                                        <input type="text" name="id_kartu" class="form-control" autocomplete="off" value="<?php echo $id_kartu; ?>" id="id_kartu" tabindex="1" autofocus 
                                        style="height: 200px;text-align: center;background-color: white;background-image: url('https://pintarmemilih.id/assets/backup-assets/images/ektp.png');background-position: 50%;background-size: contain;background-repeat: no-repeat;color: transparent;">
                                    </div>
								</div>
								<div class="col-lg-12">
								    <h6 class="mt-4 mb-0 text-center text-primary" id="scanner-desc">Scan Sekarang</h6>
								</div>
							</div>
							
							<div id="wrapper_input">
    							<div class="form-group row" id="wrapper_id_tujuan">
    								<div class="col-lg-12">
    									<div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-credit-card"></i></span></div>
                                            <input type="hidden" name="merchant" value="<?php echo $data_user['username']; ?>">
                                            <input type="text" name="tujuan" class="form-control" placeholder="NIK E-KTP Pengirim" value="<?php echo $tujuan; ?>" tabindex="2" id="id_tujuan">
                                        </div>
    									<span class="form-text text-muted"><?php echo ($error['tujuan']) ? $error['tujuan'] : '';?></span>
    								</div>
    							</div>
    							<div class="form-group row">
    								<div class="col-lg-12">
    									<div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Rp.</span></div>
                                            <input type="number" name="jumlah" class="form-control" placeholder="Nominal Pembayaran" value="<?php echo $jumlah; ?>" tabindex="3" id="id_rupiah">
                                        </div>
                                        <span class="form-text text-muted"><?php echo ($error['jumlah']) ? $error['jumlah'] : '';?></span>
    								</div>
    							</div>
                                <div class="form-group row">
                                    <div class="col-lg-12">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text"><i class="la la-lock"></i></span></div>
                                            <input type="password" name="pin" class="form-control" placeholder="PIN Transaksi (6 Digit Angka)" tabindex="4">
                                        </div>
                                        <span class="form-text text-muted"><?php echo ($error['pin']) ? $error['pin'] : '';?></span>
                                    </div>
                                </div>
                                <div class="kt-portlet__foot p-0" style="border:none;">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button type="submit" name="transfer" class="btn btn-primary btn-elevate btn-pill btn-elevate-air w-100" tabindex="4">Bayar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
						</div>    
					</form>
				</div>
			</div>
     
        </div>
        <!-- End Page Transfer Balance -->

        </div>
        <!-- End Content -->

<script>
    document.getElementById("wrapper_input").style.display = "none";
    var id_card = document.getElementById("id_kartu");
    if (id_card.value !== "") {
        document.getElementById("wrapper_id_kartu").style.display = "none";
        document.getElementById("wrapper_input").style.display = "block";
        document.getElementById("id_tujuan").focus();
    } 
    var id_tujuan = document.getElementById("id_tujuan");
    if (id_tujuan.value !== "") {
        document.getElementById("id_tujuan").setAttribute("readonly","readonly");
        document.getElementById("id_rupiah").focus();
    } 
    
    $('#id_kartu').on({
        focus: function () {
            $("#scanner-desc").text("SCAN Sekarang");
            $("#scanner-desc").addClass("text-primary").removeClass("text-danger");
        },
        blur: function () {
            $("#scanner-desc").text("SCAN Belum Siap");
            $("#scanner-desc").addClass("text-danger").removeClass("text-primary");
        }
    });
</script>
    
<?php
require '../lib/footer.php';