<?php 
session_start();
require '../config.php';
require '../lib/session_login.php';
require '../lib/session_user.php';

       

	    if (isset($_POST['transfer'])) {
	        $post_metode = $conn->real_escape_string($_POST['radio7']);
            $tujuan = $conn->real_escape_string(trim(filter($_POST['tujuan'])));
            $jumlah = $conn->real_escape_string(trim(filter($_POST['jumlah'])));
            $pin = $conn->real_escape_string(trim(filter($_POST['pin'])));

            $cek_tujuan = $conn->query("SELECT * FROM users WHERE username = '$tujuan'");
            $cek_tujuan_rows = mysqli_num_rows($cek_tujuan);

            if ($post_metode == "saldo_sosmed") {
		        $post_metodee = "Saldo Sosial Media";
            } else if ($post_metode == "saldo_top_up") {
		        $post_metodee = "Saldo Top Up";
            }

            $error = array();
            if (empty($post_metode)) {
		        $error ['radio7'] = '*Wajib Pilih Salah Satu.';
            }
            if (empty($tujuan)) {
		        $error ['tujuan'] = '*Tidak Boleh Kosong.';
            }
            if (empty($jumlah)) {
		        $error ['jumlah'] = '*Tidak Boleh Kosong.';
            }
            if (empty($pin)) {
		        $error ['pin'] = '*Tidak Boleh Kosong.';
            } else if ($pin <> $data_user['pin']) {
		        $error ['pin'] = '*PIN Yang Kamu Masukkan Salah.';
            } else {

		    if ($cek_tujuan_rows == 0 ) {
			    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Nama Pengguna Tujuan Tidak Ditemukan.<script>swal("Ups Gagal!", "Nama Pengguna Tujuan Tidak Ditemukan.", "error");</script>');
		    } else if ($jumlah < 5000 ) {
			    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Minimal Transfer Saldo Adalah Rp 5.000.<script>swal("Ups Gagal!", "Minimal Transfer Saldo Adalah Rp 5.000.", "error");</script>');
		    } else if ($data_user['saldo_top_up'] < $jumlah ) {
			    $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Saldo Top Up Kamu Tidak Mencukupi Untuk Melakukan Transfer Saldo.<script>swal("Ups Gagal!", "Saldo Top Up Kamu Tidak Mencukupi Untuk Melakukan Transfer Saldo.", "error");</script>');
		    } else {

			            $check_top = $conn->query("SELECT * FROM top_depo WHERE username = '$tujuan'");
			            $data_top = mysqli_fetch_assoc($check_top);
			        if ($conn->query("UPDATE users set $post_metode = $post_metode + $jumlah WHERE username = '$tujuan'") == true) {
				        $conn->query("UPDATE users set saldo_top_up = saldo_top_up - $jumlah, pemakaian_saldo = pemakaian_saldo + $jumlah  WHERE username = '$sess_username'");
                        $conn->query("INSERT INTO riwayat_saldo_koin VALUES ('', '$sess_username', 'Saldo', 'Pengurangan Saldo', '$jumlah', 'Mengurangi Saldo Top Up Melalui Transfer Saldo Ke $tujuan Sejumlah Rp $jumlah', '$date', '$time')");	
                        $conn->query("INSERT INTO riwayat_saldo_koin VALUES ('', '$tujuan', 'Saldo', 'Penambahan Saldo', '$jumlah', 'Mendapatkan $post_metodee Melalui Transfer Saldo Dari $sess_username Sejumlah Rp $jumlah ', '$date', '$time')");
                        $conn->query("INSERT INTO riwayat_transfer VALUES ('', '$post_metodee', '$sess_username', '$tujuan', '$jumlah','$date', '$time')");
					    if (mysqli_num_rows($check_top) == 0) {
				            $insert_topup = $conn->query("INSERT INTO top_depo VALUES ('', 'Deposit', '$tujuan', '$jumlah', '1')");
				        } else {
				            $insert_topup = $conn->query("UPDATE top_depo SET jumlah = ".$data_top['jumlah']."+$jumlah, total = ".$data_top['total']."+1 WHERE username = '$tujuan' AND method = 'Deposit'");
				        }
                        $_SESSION['hasil'] = array('alert' => 'success', 'pesan' => 'Sip, Berhasil Transfer Saldo.');
				    } else {
				        $_SESSION['hasil'] = array('alert' => 'danger', 'pesan' => 'Ups, Gagal! Sistem Kami Sedang Mengalami Gangguan.<script>swal("Ups Gagal!", "Sistem Kami Sedang Mengalami Gangguan.", "error");</script>');
			        }
			    }					
		    }
	    }

        require '../lib/header.php';

?>

        <!-- Start Content -->
        <div class="kt-container kt-grid__item kt-grid__item--fluid mt-4">

        <!-- Start Page Transfer Balance -->
        <div class="row mb-3">
	        <div class="col-md-12">
		        <div class="kt-portlet">
			        <div class="kt-portlet__head">
				        <div class="kt-portlet__head-label">
					        <h3 class="kt-portlet__head-title">
					            <i class="fa fa-exchange-alt"></i>
					            Bayar Atau Transfer
					        </h3>
				        </div>
			        </div>
			        <div class="kt-portlet__body pb-0">
                    <?php
                    if (isset($_SESSION['hasil'])) {
                    ?>
                    <div class="alert alert-<?php echo $_SESSION['hasil']['alert'] ?> alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo $_SESSION['hasil']['pesan'] ?>
                    </div>
                    <?php
                    unset($_SESSION['hasil']);
                    }
                    ?>
					<form class="form-horizontal" role="form" method="POST">
						<input type="hidden" name="csrf_token" value="<?php echo $config['csrf_token'] ?>">
							<div class="form-group row d-none">
						        <label class="col-xl-3 col-lg-3 col-form-label">Transfer Saldo Ke</label>
						        <div class="col-lg-9 col-xl-6">
							         <div class="kt-radio-list">
								        <label class="kt-radio kt-radio--bold kt-radio--brand">
									        <input type="radio" name="radio7" id="metod" value="saldo_top_up" checked>Top Up
									        <span></span>
								        </label>
								        <span class="form-text text-muted"><?php echo ($error['radio7']) ? $error['radio7'] : '';?></span>
							        </div>
						        </div>
							</div>
							<div class="form-group row">
								<div class="col-lg-12 col-xl-12">
								    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-credit-card"></i></span></div>
                                        <input type="number" name="tujuan" class="form-control" placeholder="Tujuan Transfer (NIK E-KTP)" value="<?php echo $tujuan; ?>" id="result">
                                        <div class="input-group-prepend"><span id="startButton" class="input-group-text btn btn-primary" data-toggle="modal" data-target="#modal-scan-barcode">SCAN <i class="fa fa-expand ml-2 text-white"></i></span></div>
                                    </div>
									<span class="form-text text-muted"><?php echo ($error['tujuan']) ? $error['tujuan'] : '';?></span>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-lg-12 col-xl-12">
									<div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text">Rp</span></div>
                                        <input type="number" name="jumlah" class="form-control" placeholder="Jumlah Transfer" value="<?php echo $jumlah; ?>">
                                    </div>
									<span class="form-text text-muted"><?php echo ($error['jumlah']) ? $error['jumlah'] : '';?></span>
								</div>
							</div>
                            <div class="form-group row">
                                <div class="col-lg-12 col-xl-12">
                                    <div class="input-group">
                                        <div class="input-group-prepend"><span class="input-group-text"><i class="la la-lock"></i></span></div>
                                        <input type="password" name="pin" class="form-control" placeholder="Masukkan PIN (6 Digit)">
                                    </div>
                                    <span class="form-text text-muted"><?php echo ($error['pin']) ? $error['pin'] : '';?></span>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <div class="col-lg-6 col-8">
                                    <button type="submit" name="transfer" class="btn btn-primary btn-elevate btn-pill btn-elevate-air w-100">Proses</button>
                                </div>
                                <div class="col-lg-6 col-4">
                                    <button type="reset" class="btn btn-danger btn-elevate btn-pill btn-elevate-air w-100">Reset</button>
                                </div>
                            </div>
						</div>    
					</form>
				</div>
			</div>
     
        </div>
        <!-- End Page Transfer Balance -->

        <!--<div class="kt-container">-->
        <!--    <div class="row mb-5">-->
                
        <!--        <div class="col-12 text-center">-->
        <!--            <a href="<?php echo $config['web']['url'] ?>history/riwayat-pemakaian" class="h5">Riwayat Transfer <i class="fa fa-exchange-alt"></i></a>-->
        <!--        </div>-->
                
        <!--    </div>-->
        <!--</div>-->
        
        </div>
        <!-- End Content -->

<style>
#modal-scan-barcode .modal-dialog {
    min-width: 100%;
    margin: 0;
}
#modal-scan-barcode .modal-content {
    height: 100vh;
    border-radius: 0;
}
#video {
    height: 100vh;
    width: 100%;
}
#sourceSelectPanel {
    width: 100%;
    position: absolute;
    top: 0;
    z-index: 100;
    left: 0;
    padding: 15px;
    border: solid 1px;
    background: rgb(255 255 255 / .5);
    text-align: center;
}
</style>

<!-- Modal -->
<div class="modal fade" id="modal-scan-barcode" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <div class="modal-body p-0">
        
            <main class="wrapper">
            
                <section id="demo-content">
        
                  <div>
                    <video id="video" width="100%" height="200" style="border: 1px solid gray"></video>
                  </div>
            
                  <div id="sourceSelectPanel" style="display:none">
                    <label for="sourceSelect">Ganti Kamera :</label>
                    <select id="sourceSelect" style="width:100%">
                    </select>
                    <button type="button" class="btn btn-secondary m-2" data-dismiss="modal" id="resetButton" style="position: fixed;bottom: 20px;background: red;color: #fff;padding: 10px 20px;border-radius: 20px;border-width: 2px;left: 50%;transform: translateX(-50%);">Tutup</button>
                  </div>
                  
            </main>
        
        
        </div>
    </div>
  </div>
</div>


<script type="text/javascript" src="https://unpkg.com/@zxing/library@latest"></script>
<script type="text/javascript">
window.addEventListener('load', function () {
  let selectedDeviceId;
  const codeReader = new ZXing.BrowserMultiFormatReader()
  console.log('ZXing code reader initialized')
  codeReader.listVideoInputDevices()
    .then((videoInputDevices) => {
      const sourceSelect = document.getElementById('sourceSelect')
      selectedDeviceId = videoInputDevices[0].deviceId
      if (videoInputDevices.length >= 1) {
        videoInputDevices.forEach((element) => {
          const sourceOption = document.createElement('option')
          sourceOption.text = element.label
          sourceOption.value = element.deviceId
          sourceSelect.appendChild(sourceOption)
        })

        sourceSelect.onchange = () => {
            selectedDeviceId = sourceSelect.value;
            codeReader.reset()
            codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
              if (result) {
                console.log(result)
                document.getElementById('result').value = result.text
                $('#modal-scan-barcode').modal('hide');
              }
              if (err && !(err instanceof ZXing.NotFoundException)) {
                console.error(err)
                document.getElementById('result').value = err
                $('#modal-scan-barcode').modal('hide');
              }
            })
            console.log(`Started continous decode from camera with id ${selectedDeviceId}`)
        };

        const sourceSelectPanel = document.getElementById('sourceSelectPanel')
        sourceSelectPanel.style.display = 'block'
      }

      document.getElementById('startButton').addEventListener('click', () => {
        codeReader.decodeFromVideoDevice(selectedDeviceId, 'video', (result, err) => {
          if (result) {
            console.log(result)
            document.getElementById('result').value = result.text
            $('#modal-scan-barcode').modal('hide');
          }
          if (err && !(err instanceof ZXing.NotFoundException)) {
            console.error(err)
            document.getElementById('result').value = err
            $('#modal-scan-barcode').modal('hide');
          }
        })
        console.log(`Started continous decode from camera with id ${selectedDeviceId}`)
      })

      document.getElementById('resetButton').addEventListener('click', () => {
        codeReader.reset()
        document.getElementById('result').value = '';
        console.log('Reset.')
      })

    })
    .catch((err) => {
      console.error(err)
    })
})
</script>

<?php
require '../lib/footer.php';