            </div>
		</div>
		<!-- End Body Content -->

		<!-- Start Footer -->
		<div class="kt-footer  kt-footer--extended  kt-grid__item" id="kt_footer">
			<div class="kt-footer__top">
			<div class="kt-container ">
				<div class="row">
					<div class="col-lg-3">
						<div  class="kt-footer__section">
							<h3 class="kt-footer__title">Tentang <?php echo $data['short_title']; ?></h3>
							<div class="kt-footer__content">
								<img class="mb-3 d-block" alt="Logo" src="<?php echo $config['web']['url'] ?>assets/media/logos/logo_color.png" style=" height: 40px; "/>
								<b><?php echo $data['short_title']; ?></b> Dompet Digital Berbasis E-KTP Pertama Di Indonesia
							</div>
						</div>
					</div>
					<div class="col-lg-2">
						<div  class="kt-footer__section">
							<h3 class="kt-footer__title">Informasi</h3>
							<div class="kt-footer__content">
								<div class="kt-footer__nav">
									<div class="kt-footer__nav-section">
										<a href="<?php echo $config['web']['url'] ?>page/how-to-top-up-balance">Cara Isi Saldo</a>
										<a href="<?php echo $config['web']['url'] ?>page/how-to-transaction">Cara Transaksi</a>
										<a href="<?php echo $config['web']['url'] ?>page/balance-top-up-method">Pembayaran Isi Saldo</a>
										<a href="<?php echo $config['web']['url'] ?>page/service-promo">Layanan Promo</a>
									</div>
								</div>	
							</div>
						</div>
					</div>
					<div class="col-lg-2">
						<div  class="kt-footer__section">
							<h3 class="kt-footer__title">Lainnya</h3>
							<div class="kt-footer__content">
								<div class="kt-footer__nav">
									<div class="kt-footer__nav-section">
										<a href="<?php echo $config['web']['url'] ?>dashboard/kontak">Kontak Kami</a>
										<a href="<?php echo $config['web']['url'] ?>dashboard/ketentuan-layanan">Ketentuan Layanan</a>
										<a href="<?php echo $config['web']['url'] ?>dashboard/faq">FAQ</a>
										<a href="<?php echo $config['web']['url'] ?>dashboard/karir">Karir</a>
									</div>
								</div>	
							</div>
						</div>
					</div>
					<?php
					$cek_kontak = $conn->query("SELECT * FROM kontak_website ORDER BY id DESC");
					while ($data_kontak = $cek_kontak->fetch_assoc()) {
					?>
					<div class="col-lg-2">
						<div  class="kt-footer__section">
							<h3 class="kt-footer__title">Sosial Media</h3>
							<div class="kt-footer__content">
								<div class="kt-footer__nav">
									<div class="kt-footer__nav-section">
										<a href="<?php echo $data_kontak['link_ig']; ?>" target="_blank"><i class="flaticon-instagram-logo fa-2x"></i> &nbsp;Instagam</a>
										<a href="https://api.whatsapp.com/send?phone=<?php echo $data_kontak['no_wa']; ?>" target="_blank"><i class="flaticon-whatsapp fa-2x"></i> &nbsp;WhatsApp</a>
									</div>
								</div>	
							</div>
						</div>
					</div>
					<?php
					}
					?>
					<div class="col-lg-3">
						<div class="kt-footer__section">
							<h3 class="kt-footer__title">Download App</h3>
							<div class="kt-footer__content">
							    <div class="row">
							        <div class="col-xs-6 col-md-6 pr-2">
							        	<a target="_BLANK" href="https://play.google.com/store/apps/details?id=ktpku.com" class="footer-btn-app"><img data-src="<?php echo $config['web']['url'] ?>assets/media/icon/download_play.png" alt="playstore" class="w-100" src="<?php echo $config['web']['url'] ?>assets/media/icon/download_play.png"></a>
							        </div>
							        <div class="col-xs-6 col-md-6 pl-2">
							        	<a href="#" class="footer-btn-app"><img data-src="<?php echo $config['web']['url'] ?>assets/media/icon/download_app.png" alt="playstore" class="w-100" src="<?php echo $config['web']['url'] ?>assets/media/icon/download_app.png"></a>
							        </div>
					    		    </div>								
							</div>
						</div>
					</div>				
				</div>				
			</div>	
		</div> 	 
		<div class="kt-footer__bottom">
		<div class="kt-container ">
			<div class="kt-footer__wrapper">
						<div class="kt-footer__logo w-100">	 			 
							<div class="kt-footer__copyright text-center w-100 pb-5">
								© 2020 <?php echo $data['short_title']; ?>. All Rights Reserved.
							</div>
						</div>
				
					</div>
				</div>
			</div>
		</div>
		<!-- End Footer -->

		       </div>
		    </div>
    	</div>
		<!-- End Page -->
		
		<div class="fixed-action-btn-two">
		    
	        <a href="<?php echo $config['web']['url'] ?>auth/login" class="btn-fab-floating-two">
		        <div class="text-center">
		          <i class="fas fa-home fa-2x"></i>
		          <div style=" font-size: 11px; margin-top: -2px; ">Beranda</div>
		        </div>
			</a>
			
		<?php if ($data_user['level'] == "Merchant") { ?>
	        <a href="<?php echo $config['web']['url'] ?>staff/pembayaran" class="btn-fab-floating-two">
    	          <div class="text-center">
    	            <i class="fas fa-id-card fa-2x"></i>
    	            <div style=" font-size: 11px; margin-top: -2px; ">Terima</div>
    	          </div>
	        </a>
	        <?php } else { ?>
	        <a href="<?php echo $config['web']['url'] ?>staff/transfer-balance" class="btn-fab-floating-two">
    	          <div class="text-center">
    	            <i class="fas fa-expand fa-2x"></i>
    	            <div style=" font-size: 11px; margin-top: -2px; ">Bayar</div>
    	          </div>
	        </a>
	        <?php } ?>
			
		    <a href="<?php echo $config['web']['url'] ?>page/help" class="btn-fab-floating-two">
			    <div class="text-center">
		          <i class="fas fa-headset fa-2x"></i>
		          <div style=" font-size: 11px; margin-top: -2px; ">Tanya</div>
		        </div>
			</a>
			<a href="<?php echo $config['web']['url'] ?>page/profile" class="btn-fab-floating-two">
			<div class="text-center">
			      <?php if ($data_user['level'] == "Merchant") { ?>
			      <i class="fas fa-store fa-2x"></i>
		          <div style=" font-size: 11px; margin-top: -2px; ">Merchant</div>
			      <?php } else { ?>
		          <i class="fas fa-user-circle fa-2x"></i>
		          <div style=" font-size: 11px; margin-top: -2px; ">Saya</div>
		          <?php } ?>
		        </div>
			</a>		
		</div>
		
		<!--Loader Blank-->
		<div id="loader-blank" style="position: fixed; top: 0; left: 0; width: 100%; height: 100vh; background: rgb(255 255 255 / .5); z-index: 99999; "></div>
		<div id="loader-start" style="position: fixed;top: 45%;left: 50%;transform: translateX(-50%);background: rgb(255, 255, 255);color: #1e70c0;z-index: 99999;border: solid 1px;border-radius: 50%/50%;padding: 14px 10px;"><i class="fas fa-sync fa-spin fa-3x fa-fw"></i></div>
		
		<!-- Global Config (global config for global JS sciprts) -->
		<script>
            var KTAppOptions = {"colors":{"state":{"brand":"#366cf3","light":"#ffffff","dark":"#282a3c","primary":"#5867dd","success":"#34bfa3","info":"#36a3f7","warning":"#ffb822","danger":"#fd3995"},"base":{"label":["#c5cbe3","#a1a8c3","#3d4465","#3e4466"],"shape":["#f0f3ff","#d9dffa","#afb4d4","#646c9a"]}}};
		</script>
		<!-- End Global Config -->

		<!-- Global Theme Bundle (used by all pages) -->
		<script src="<?php echo $config['web']['url'] ?>assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
		<script src="<?php echo $config['web']['url'] ?>assets/js/scripts.bundle.js" type="text/javascript"></script>
		<!-- End Global Theme Bundle -->
        
        <?php if ($data_user['level'] == "Developers") { ?>
		<!-- Page Vendors (used by this page) -->
		<script src="<?php echo $config['web']['url'] ?>assets/plugins/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
		<script src="<?php echo $config['web']['url'] ?>assets/plugins/custom/gmaps/gmaps.js" type="text/javascript"></script>
		<!-- End Page Vendors -->
		<?php } ?>

		<!-- Page Scripts (used by this page) -->
		<?php if ($data_user['level'] == "Developers") { ?>
		<script src="<?php echo $config['web']['url'] ?>assets/js/pages/components/charts/line-chart/morris.min.js" type="text/javascript"></script>
		<?php } ?>
		<script src="<?php echo $config['web']['url'] ?>assets/js/pages/components/charts/raphael/raphael-min.js" type="text/javascript"></script>
		<?php if ($data_user['level'] == "Developers") { ?>
		<script src="<?php echo $config['web']['url'] ?>assets/js/pages/custom/chat/chat.js" type="text/javascript"></script>
		<?php } ?>
		<script src="<?php echo $config['web']['url'] ?>assets/js/pages/custom/voucher/theme.js" type="text/javascript"></script>
		<script src="<?php echo $config['web']['url'] ?>assets/js/pages/custom/voucher/clipboard.min.js" type="text/javascript"></script>
		<script src="<?php echo $config['web']['url'] ?>assets/js/pages/custom/wizard/wizard-4.js" type="text/javascript"></script>
		<script src="<?php echo $config['web']['url'] ?>assets/js/pages/custom/contacts/list-columns.js" type="text/javascript"></script>
		<script src="<?php echo $config['web']['url'] ?>assets/js/pages/crud/datatables/basic/basic.js" type="text/javascript"></script>
		<script src="<?php echo $config['web']['url'] ?>assets/js/pages/components/extended/bootstrap-notify.js" type="text/javascript"></script>
		<script src="<?php echo $config['web']['url'] ?>assets/js/pages/dashboard.js" type="text/javascript"></script>
		<script src="<?php echo $config['web']['url'] ?>assets/js/pages/custom/user/profile.js" type="text/javascript"></script>
		<!-- End Page Scripts -->
		
		<script src="<?php echo $config['web']['url'] ?>assets/js/pages/components/topbar/topbar.min.js" type="text/javascript"></script>
        <script>
        function resetToDefaults() {
            topbar.config({
              autoRun      : true,
              barThickness : 4,
              barColors    : {
                '0'      : 'rgba(26,  188, 156, .9)',
                '.25'    : 'rgba(52,  152, 219, .9)',
                '.50'    : 'rgba(241, 196, 15,  .9)',
                '.75'    : 'rgba(230, 126, 34,  .9)',
                '1.0'    : 'rgba(211, 84,  0,   .9)'
              },
              shadowBlur   : 10,
              shadowColor  : 'rgba(0,   0,   0,   .6)'
            })
          }

            // Page load
            topbar.show();
            $('#loader-blank').show();
            $(window).on("load", function () {
                topbar.hide()
                $('#loader-blank').hide();
                $('#loader-start').hide();
            });
        </script>
        
        <script>
            $('#news').modal('show');
            function read_news() {
              $.ajax({
                type: "GET",
                url: "<?php echo $config['web']['url'] ?>ajax/read-news.php"
              });
            }
            window.onbeforeunload = function() {
                  $('#loader-start').show();
                $('#loader-blank').show();
            };
        </script>
		
		</body>

</html>