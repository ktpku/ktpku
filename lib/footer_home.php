		<!-- Start JS -->
		<script src="<?php echo $config['web']['url'] ?>assets/js/pages/custom/login/jquery-2.2.0.min.js"></script>
		<script src="<?php echo $config['web']['url'] ?>assets/js/pages/custom/login/popper.min.js"></script>
		<script src="<?php echo $config['web']['url'] ?>assets/js/pages/custom/login/bootstrap.min.js"></script>
		<!-- End JS -->
		
		<script>
		$('.numeric-16').on('input', function (event) { 
		    this.value = this.value.replace(/[^0-9]/g, '');
		    if (this.value.length > 16){ this.value = this.value.slice(0,16); }
		});
		$('.numeric-6').on('input', function (event) { 
		    this.value = this.value.replace(/[^0-9]/g, '');
		    if (this.value.length > 6){ this.value = this.value.slice(0,6); }
		});
		</script>
        
        <!-- Menu On Auth Pages -->
        <div class="d-flex align-content-stretch flex-wrap align-items-center menu-float-right">
            <div class="p-2 bd-highlight menu_"><a href="<?php echo $config['web']['url'] ?>">Depan</a></div>
            <div class="p-2 bd-highlight menu_"><a href="<?php echo $config['web']['url'] ?>auth/login.php">Login</a></div>
            <div class="p-2 bd-highlight menu_"><a href="<?php echo $config['web']['url'] ?>auth/register.php">Daftar</a></div>
            <div class="p-2 bd-highlight menu_"><a href="<?php echo $config['web']['url'] ?>auth/verification-account.php">Verifikasi Akun</a></div>
            <div class="p-2 bd-highlight menu_"><a href="<?php echo $config['web']['url'] ?>auth/forgot-password.php">Lupa Sandi</a></div>
        </div>
        <!--<div class="fixed-action-btn-two">-->
        <!--	<a href="<?php echo $config['web']['url'] ?>auth/login" class="btn-fab-floating-two">-->
        <!--        <div class="text-center">-->
        <!--          <i class="fa fa-sign-in fa-2x"></i>-->
        <!--          <div style=" font-size: 11px; margin-top: -2px; ">Masuk</div>-->
        <!--        </div>-->
        <!--	</a>-->
        <!--      	<a href="<?php echo $config['web']['url'] ?>auth/register" class="btn-fab-floating-two">-->
        <!--	<div class="text-center">-->
        <!--          <i class="fa fa-user-plus fa-2x"></i>-->
        <!--          <div style=" font-size: 11px; margin-top: -2px; ">Daftar</div>-->
        <!--        </div>-->
        <!--	</a>-->
        <!--        <a href="<?php echo $config['web']['url'] ?>auth/login" class="btn-fab-floating-two">-->
        <!--          <div class="text-center">-->
        <!--            <i class="fa fa-barcode fa-2x"></i>-->
        <!--            <div style=" font-size: 11px; margin-top: -2px; ">Scan</div>-->
        <!--          </div>-->
        <!--        </a>-->
        <!--        <a href="<?php echo $config['web']['url'] ?>auth/verification-account" class="btn-fab-floating-two">-->
        <!--	<div class="text-center">-->
        <!--          <i class="fa fa-check fa-2x"></i>-->
        <!--          <div style=" font-size: 11px; margin-top: -2px; ">Verif Akun</div>-->
        <!--        </div>-->
        <!--	</a>-->
        <!--	<a href="<?php echo $config['web']['url'] ?>auth/forgot-password" class="btn-fab-floating-two">-->
        <!--	<div class="text-center">-->
        <!--          <i class="fa fa-key fa-2x"></i>-->
        <!--          <div style=" font-size: 11px; margin-top: -2px; ">Lupa Sandi</div>-->
        <!--        </div>-->
        <!--	</a>		-->
        <!--</div>-->
        <!-- Menu On Auth Pages -->
        
        <!--Loader Blank-->
		<div id="loader-blank" style="position: fixed; top: 0; left: 0; width: 100%; height: 100vh; background: rgb(255 255 255 / .5); z-index: 99999; "></div>
		<div id="loader-start" style="position: fixed;top: 45%;left: 50%;transform: translateX(-50%);background: rgb(255, 255, 255);color: #1e70c0;z-index: 99999;border: solid 1px;border-radius: 50%/50%;padding: 9.5px 3.5px;"><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i></div>
		
        <script src="<?php echo $config['web']['url'] ?>assets/js/pages/components/topbar/topbar.min.js" type="text/javascript"></script>
        <script>
        function resetToDefaults() {
            topbar.config({
              autoRun      : true,
              barThickness : 4,
              barColors    : {
                '0'      : 'rgba(26,  188, 156, .9)',
                '.25'    : 'rgba(52,  152, 219, .9)',
                '.50'    : 'rgba(241, 196, 15,  .9)',
                '.75'    : 'rgba(230, 126, 34,  .9)',
                '1.0'    : 'rgba(211, 84,  0,   .9)'
              },
              shadowBlur   : 10,
              shadowColor  : 'rgba(0,   0,   0,   .6)'
            })
          }

            // Page load
            topbar.show();
            $('#loader-blank').show();
            $(window).on("load", function () {
                topbar.hide()
                $('#loader-blank').hide();
                $('#loader-start').hide();
            });
            window.onbeforeunload = function() {
                  $('#loader-start').show();
                $('#loader-blank').show();
            };
        </script>

		</body>
</html>