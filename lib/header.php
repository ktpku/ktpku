<?php
require 'session_login.php';
require 'database.php';
require 'csrf_token.php';
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <title><?php echo $data['title']; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta content="<?php echo $data['deskripsi_web']; ?>" name="description" />
        <meta content="ArCode" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <!-- Start Favicon Icon -->
        <link rel="shortcut icon" href="<?php echo $config['web']['url'] ?>assets/media/logos/rsz_logos.png" />
        <!-- End Favicon Icon -->

        <!-- Start CSS -->
        <link href="<?php echo $config['web']['url'] ?>assets/css/pages/support-center/home-2.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $config['web']['url'] ?>assets/css/pages/voucher/voucher-1.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $config['web']['url'] ?>assets/css/pages/pricing/pricing-3.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $config['web']['url'] ?>assets/css/pages/invoices/invoice-1.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $config['web']['url'] ?>assets/css/pages/invoices/invoice-2.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $config['web']['url'] ?>assets/css/pages/wizard/wizard-4.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $config['web']['url'] ?>assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $config['web']['url'] ?>assets/css/pages/support-center/faq-2.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $config['web']['url'] ?>assets/css/pages/login/login-2.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $config['web']['url'] ?>assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $config['web']['url'] ?>assets/css/style.bundle.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo $config['web']['url'] ?>assets/css/style.css" rel="stylesheet" type="text/css" />
        <!-- End CSS -->
    
        <!-- Start Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
        <!-- End Fonts -->

        <!-- Start Script JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <!-- End Script JS -->
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <?php if ($data_user['level'] == "Developers") { ?>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
        <?php } ?>

        <!-- Start Hotjar Tracking -->
        <script>
        (function(h,o,t,j,a,r){
            h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
            h._hjSettings={hjid:1070954,hjsv:6};
            a=o.getElementsByTagName('head')[0];
            r=o.createElement('script');r.async=1;
            r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
            a.appendChild(r);
        })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
        </script>
        <!-- End Hotjar Tracking -->

</head>

<?php
if (isset($_SESSION['user'])) {
?>

        <!-- Start Body -->
        <body  style="background-image: linear-gradient(0, #00b0f0, #00b0f0);; background-position: center top; background-size: 100% 350px;"  class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-page--loading">

        <!-- Start Page -->

        <!-- Start Header Mobile -->
        <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed">
	        <div class="kt-header-mobile__logo">
		        <a href="<?php echo $config['web']['url'] ?>">
			        <img alt="Logo" src="<?php echo $config['web']['url'] ?>assets/media/logos/logo_color.png" style=" height: 33px; margin-top: 8px;"/>
		        </a>
	        </div>
	        <div class="kt-header-mobile__toolbar" onclick="showhidenotif()">
		        <button class="kt-header-mobile__toolbar-topbar-toggler kt-pulse kt-pulse--light">
		            <i class="fas fa-bell fa-2x"></i>
			        <span class="kt-pulse__ring" style="margin-left: -9px;"></span>
		        </button>
	        </div>
        </div>
        <script>
            function showhidenotif() {
              var x = document.getElementById("notifikasi_main");
              if (x.style.display === "none") {
                x.style.display = "block";
              } else {
                x.style.display = "none";
              }
            }
        </script>
        <!-- End Header Mobile -->

        <!-- Start Grid -->
	        <div class="kt-grid kt-grid--hor kt-grid--root">
		        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

        <!-- Start Header -->
        <div id="kt_header" class="kt-header  kt-header--fixed" data-ktheader-minimize="on">
	        <div class="kt-container">

        <!-- Start Brand -->
        <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
	        <a class="kt-header__brand-logo" href="<?php echo $config['web']['url'] ?>">
		        <img alt="Logo" src="<?php echo $config['web']['url'] ?>assets/media/logos/logo_white.png" style=" height: 40px; " class="kt-header__brand-logo-default"/>
		        <img alt="Logo" src="<?php echo $config['web']['url'] ?>assets/media/logos/logo_color.png" style=" height: 40px; " class="kt-header__brand-logo-sticky"/>
	        </a>		
        </div>
        <!-- End Brand -->

        <!-- Start Header Menu -->
        <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
        <div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
            <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile">
                <ul class="kt-menu__nav">
                    
                    <?php
                    if ($data_user['level'] == "Developers") {
                    ?>
                    <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                        <a href="<?php echo $config['web']['url'] ?>admin" class="kt-menu__link"><span class="kt-menu__link-text">Admin Tools</span>
                        </a>
                    </li>
                    
                    <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                        <a  href="javascript:;" class="kt-menu__link kt-menu__toggle">
                            <span class="kt-menu__link-text">Menu Staff</span>
                        </a>
                        <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                        <ul class="kt-menu__subnav">
                            <li class="kt-menu__item  kt-menu__item--submenu" data-ktmenu-submenu-toggle="hover" aria-haspopup="true">
                                <a  href="<?php echo $config['web']['url'] ?>staff/code-invitation-new" class="kt-menu__link">
                                <span class="kt-menu__link-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24"/>
                                            <polygon fill="#000000" opacity="0.3" transform="translate(8.885842, 16.114158) rotate(-315.000000) translate(-8.885842, -16.114158) " points="6.89784488 10.6187476 6.76452164 19.4882481 8.88584198 21.6095684 11.0071623 19.4882481 9.59294876 18.0740345 10.9659914 16.7009919 9.55177787 15.2867783 11.0071623 13.8313939 10.8837471 10.6187476"/>
                                            <path d="M15.9852814,14.9852814 C12.6715729,14.9852814 9.98528137,12.2989899 9.98528137,8.98528137 C9.98528137,5.67157288 12.6715729,2.98528137 15.9852814,2.98528137 C19.2989899,2.98528137 21.9852814,5.67157288 21.9852814,8.98528137 C21.9852814,12.2989899 19.2989899,14.9852814 15.9852814,14.9852814 Z M16.1776695,9.07106781 C17.0060967,9.07106781 17.6776695,8.39949494 17.6776695,7.57106781 C17.6776695,6.74264069 17.0060967,6.07106781 16.1776695,6.07106781 C15.3492424,6.07106781 14.6776695,6.74264069 14.6776695,7.57106781 C14.6776695,8.39949494 15.3492424,9.07106781 16.1776695,9.07106781 Z" fill="#000000" transform="translate(15.985281, 8.985281) rotate(-315.000000) translate(-15.985281, -8.985281) "/>
                                        </g>
                                    </svg></span><span class="kt-menu__link-text">Kode Undangan</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php } ?>
                    
                    <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                        <a href="<?php echo $config['web']['url'] ?>" class="kt-menu__link"><span class="kt-menu__link-text"><h5 class="mb-0 mr-2">Depan</h5> <i class="fas fa-home fa-2x"></i></span>
                        </a>
                    </li>
                    
                    <!--<li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                        <a href="<?php echo $config['web']['url'] ?>page/program-referral" class="kt-menu__link"><span class="kt-menu__link-text"><i class="fas fa-sitemap fa-2x"></i></span>&nbsp;&nbsp;<?php if (mysqli_num_rows($CallDBTiket) !== 0) { ?><span class="badge badge-primary"><?php echo mysqli_num_rows($CallDBTiket); ?></span><?php } ?>
                        </a>
                    </li>-->
                    <!--<li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">-->
                    <!--    <a href="
                    <!--<?php echo $config['web']['url'] ?>-->
                    <!--page/user-ranking" class="kt-menu__link"><span class="kt-menu__link-text">Peringkat Bulanan</span>-->
                    <!--    </a>-->
                    <!--</li>-->
                    
                    <?php
                    if ($data_user['level'] == "Merchant") {
                    ?>
                    <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                        <a href="<?php echo $config['web']['url'] ?>staff/pembayaran" class="kt-menu__link"><span class="kt-menu__link-text"><h5 class="mb-0 mr-2">Scan EKTP</h5> <i class="fas fa-id-card fa-2x"></i></span></a>
                    </li>
                    <?php } ?>
               
                    <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                        <a href="<?php echo $config['web']['url'] ?>page/help" class="kt-menu__link"><span class="kt-menu__link-text"><h5 class="mb-0 mr-2">Tanya CS</h5> <i class="fas fa-headset fa-2x"></i></span>&nbsp;&nbsp;<?php if (mysqli_num_rows($CallDBTiket) !== 0) { ?><span class="badge badge-primary"><?php echo mysqli_num_rows($CallDBTiket); ?></span><?php } ?></a>
                    </li>
                    
               </ul>
            </div>
        </div>
        <!--End Header Menu -->

        <!-- Start Header Topbar -->
        <div class="kt-header__topbar kt-grid__item">

        <!--Start Notifications -->
        <div class="kt-header__topbar-item dropdown">
		    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
			    <span class="kt-header__topbar-icon kt-pulse kt-pulse--light">
                    		<i class="fas fa-bell fa-2x"></i>
			        <span class="kt-pulse__ring"></span>
			    </span>
			    
		    </div>

			<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl" id="notifikasi_main">
			    <form>

                    <form>

                    <!-- Start Head -->
                    <div class="kt-head kt-head--skin-dark kt-head--fit-x kt-head--fit-b pt-1" style=" background: #0070C0; ">
                        <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-success kt-notification-item-padding-x" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active show" data-toggle="tab" href="#topbar_notifications_notifications" role="tab" aria-selected="true">Pesan</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#topbar_notifications_logs" role="tab" aria-selected="false">Riwayat</a>
                            </li>
                        </ul>
                    </div>
                    <!-- End Head -->

                    <div class="tab-content">
                        <div class="tab-pane active show" id="topbar_notifications_notifications" role="tabpanel">
                            <div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
                                <?php 
                                $cek_saldo = $conn->query("SELECT * FROM riwayat_saldo_koin WHERE username = '$sess_username' ORDER BY id DESC LIMIT 10");
                                while ($data_saldo = $cek_saldo->fetch_assoc()) {
                                if ($data_saldo['aksi'] == "Penambahan Saldo") {
                                    $pesan = "Penambahan Saldo";
                                    $icon = "fa fa-plus";
                                    $alert = "success";
                                } else if ($data_saldo['aksi'] == "Pengurangan Saldo") {
                                    $pesan = "Pengurangan Saldo";
                                    $icon = "fa fa-minus";
                                    $alert = "danger";
                                } else if ($data_saldo['aksi'] == "Penambahan Koin") {
                                    $pesan = "Penambahan Koin";
                                    $icon = "fa fa-minus";
                                    $alert = "success";
                                } else if ($data_saldo['aksi'] == "Pengurangan Koin") {                                
                                    $pesan = "Pengurangan Koin";
                                    $icon = "fa fa-minus";
                                    $alert = "danger";
                                }                                                        
                                ?>
                                <a href="<?php echo $config['web']['url'] ?>riwayat" class="kt-notification__item">
                                    <div class="kt-notification__item-icon">
                                        <i class="<?= $icon ?> kt-font-<?= $alert ?>"></i>
                                    </div>
                                    <div class="kt-notification__item-details">
                                        <div class="kt-notification__item-title">
                                            <?= $pesan ?> (Rp <?php echo number_format($data_saldo['nominal'],0,',','.'); ?>)
                                        </div>
                                        <div class="kt-notification__item-time">
                                            <?php echo tanggal_indo($data_saldo['date']); ?>, <?php echo $data_saldo['time']; ?>
                                        </div>
                                    </div>
                                </a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
                            <div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
                                <?php 
                                $cek = $conn->query("SELECT * FROM semua_pembelian WHERE user = '$sess_username' ORDER BY id DESC LIMIT 10");
                                while ($data_nya = $cek->fetch_assoc()) {
                                ?>
                                <a href="<?php echo $config['web']['url'] ?>page/receipt?oid=<?php echo $data_nya['id_pesan']; ?>" class="kt-notification__item">
                                    <div class="kt-notification__item-icon">
                                        <i class="flaticon2-shopping-cart kt-font-primary"></i>
                                    </div>
                                    <div class="kt-notification__item-details">
                                        <div class="kt-notification__item-title">
                                            Pembelian <?php echo $data_nya['layanan']; ?>
                                        </div>
                                        <div class="kt-notification__item-time">
                                            <?php echo tanggal_indo($data_nya['date']); ?>, <?php echo $data_nya['time']; ?>
                                        </div>
                                    </div>
                                </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
			    </form>
			</div>
        </div>
	    <!-- End Notifications -->

		<!-- Start User Bar -->
		<div class="kt-header__topbar-item kt-header__topbar-item--user">
		    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
			    <span class="kt-header__topbar-username"><?php echo $data_user['username']; ?></span>
			    <span class="kt-header__topbar-username"><i class="fas fa-user-circle fa-2x"></i></span>
			    <img alt="Pic" src="<?php echo $config['web']['url'] ?>assets/media/icon/user.jpg" class="kt-hidden"/>
		    </div>
		    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
    
                <!-- Start Navigation -->
                <div class="kt-notification">
                    <a href="<?php echo $config['web']['url'] ?>page/profile" class="kt-notification__item d-none d-sm-flex">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-calendar-3 kt-font-success"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title kt-font-bold">
                                Profile
                            </div>
                            <div class="kt-notification__item-time">
                                Pengaturan Akun
                            </div>
                        </div>
                    </a>
                    <a href="<?php echo $config['web']['url'] ?>history/account-activity" class="kt-notification__item">
                        <div class="kt-notification__item-icon">
                            <i class="flaticon2-rocket-1 kt-font-danger"></i>
                        </div>
                        <div class="kt-notification__item-details">
                            <div class="kt-notification__item-title kt-font-bold">
                                Aktifitas
                            </div>
                            <div class="kt-notification__item-time">
                                Riwayat Akun
                            </div>
                        </div>
                    </a>
        	        <a href="<?php echo $config['web']['url'] ?>dashboard/ketentuan-layanan" class="kt-notification__item d-flex d-sm-none">
        	            <div class="kt-notification__item-icon">
        	                <i class="flaticon2-writing kt-font-primary"></i>
        	            </div>
        	            <div class="kt-notification__item-details">
        	                <div class="kt-notification__item-title kt-font-bold">
        	                    Ketentuan Layanan
        	                </div>
        	                <div class="kt-notification__item-time">
        	                    Baca Ketetuan Kami
        	                </div>
        	            </div>
        	        </a>
                    
                    <div class="kt-notification__custom kt-space-between">
                        <a href="<?php echo $config['web']['url'] ?>logout" class="btn btn-label btn-label-brand btn-sm btn-bold">Keluar</a>
                    </div>
                </div>
                <!-- End Navigation -->

	        </div>
        </div>
        <!-- End User Bar -->

        </div>
        <!-- End Header Topbar -->

	        </div>
        </div>
        <!-- End Header -->

<?php } ?>